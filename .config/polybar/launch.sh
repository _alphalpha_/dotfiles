#!/bin/sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

#polybar example &

if [ "$WM" = "i3" ] ; then
  for i in $(polybar -m | awk -F: '{print $1}'); do MONITOR=$i polybar i3 -c ~/.config/polybar/config_i3 & done
elif [ "$WM" = "bspwm" ] ; then
  for i in $(polybar -m | awk -F: '{print $1}'); do MONITOR=$i polybar i3 -c ~/.config/polybar/config_bspwm & done
elif [ "$WM" = "exwm" ] ; then
  for i in $(polybar -m | awk -F: '{print $1}'); do MONITOR=$i polybar i3 -c ~/.config/polybar/config_exwm & done
else
  for i in $(polybar -m | awk -F: '{print $1}'); do MONITOR=$i polybar i3 -c ~/.config/polybar/config & done
fi

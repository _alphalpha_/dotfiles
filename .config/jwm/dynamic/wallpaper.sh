#!/bin/sh
# Simple wallpaper changer
# written by Michael

echo "<JWM>"
echo "<Program label=\"Plain Color\">$HOME/.local/Scripts/yad-plain-wallpaper.sh</Program>"
echo "<Separator/>"
WP=".local/wallpaper.png"
WP_DIR="/usr/share/backgrounds"
for IMG in $(find "$WP_DIR" -name "*.png" -type f | sort); do
  echo "<Program label=\"$(echo "$IMG" | cut -d '/' -f 5)\"> [ -f \"$WP\" ] && rm \"$WP\"; ln -s $IMG \"$WP\" && xwallpaper --zoom \"$WP\"</Program>"
done
echo "</JWM>


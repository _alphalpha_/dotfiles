#!/bin/sh
# Simple Network Interface Menu for JWM
# written by Michael

echo "<JWM>"
  echo "<Program label=\"/etc/network/interfaces\">$TERMINAL -e sudo -p 'Enter password to edit /etc/network/interfaces' nano /etc/network/interfaces </Program>"

for INTERFACE in $(ip a | awk '/^[0-9]:/ &&  $2 != "lo:" {print $2}' | cut -d ':' -f 1); do
  echo "<Menu label=\"$INTERFACE\">"
  if [ "$(ip a show "$INTERFACE" | cut -d\> -f 1 | cut -d\< -f 2 | tr ',' ' ' | grep -w 'UP')" ]; then
    echo "<Menu label=\"STATE: UP\">"
    echo "<Program label=\"Configure\">$TERMINAL -T 'Network Configuration' -e sudo -p 'Enter password to configure the network: ' Ceni --iface "$INTERFACE"</Program>"
    echo "<Program label=\"Bring Down\">sudo ifdown $INTERFACE</Program>"
    echo "</Menu>"
    if [ "$(ip addr show $INTERFACE | grep 'NO-CARRIER')" ]; then
        echo "<Program label=\"NO-CARRIER !\">$TERMINAL -T 'Network Configuration' -e sudo -p 'Enter password to configure the network: ' Ceni --iface "$INTERFACE"</Program>"
	fi
  else
    echo "<Menu label=\"STATE: DOWN\">"
    echo "<Program label=\"Configure\">$TERMINAL -T 'Network Configuration' -e sudo -p 'Enter password to configure the network: ' Ceni --iface "$INTERFACE"</Program>"
    echo "<Program label=\"Bring Up\">sudo ifup $INTERFACE</Program>"
    echo "</Menu>"
  fi

  IP=$(ip address show $INTERFACE | awk '$1 == "inet" {print $2}' | cut -d '/' -f 1)
  if [ "$IP" ]; then
    echo "<Program label=\"IP: $IP\"></Program>"
  else
    echo "<Menu label=\"IP: Not Connected\">"
    echo "<Program label=\"Connect to DHCP Server\">sudo dhclient $INTERFACE</Program>"
    echo "</Menu>"
  fi
  echo "</Menu>"
done

echo "</JWM>"

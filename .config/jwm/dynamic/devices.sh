#!/bin/sh
# Dynamic Device Menu for JWM
# written by Michael

echo "<JWM>"
for DEVICE in $(lsblk -dno NAME); do
MENU="$MENU <Menu label=\"$DEVICE\">"
DEVICE_PATH="/dev/$DEVICE"
  for PARTITION in $(lsblk -nro NAME "$DEVICE_PATH" | grep -e "^$DEVICE[0-9]"); do
	PARTITION_PATH="/dev/$PARTITION"
	MOUNTPOINT=$(lsblk "$PARTITION_PATH" -no MOUNTPOINT)
	[ "$MOUNTPOINT" ] && IS_MOUNTED="1" || IS_MOUNTED="0"

	if [ "$IS_MOUNTED" = "1" ]; then
	  if [ "$(echo "$MOUNTPOINT" | grep -e "^/media/")" ] && [ -x "/usr/bin/pumount" ]; then
		UNMOUNT_CMD="pumount $PARTITION_PATH"
	  else
		UNMOUNT_CMD="$TERMINAL -e sudo -p \"Enter password to unmount $MOUNTPOINT: \" umount $MOUNTPOINT"
	  fi

	SUBMENU_PARTITION="
	<Menu label=\"$PARTITION\">
	  <Program label=\"$MOUNTPOINT\"> $FILEMANAGER $MOUNTPOINT </Program>
      <Separator/>
	  <Program label=\"Unmount\"> $UNMOUNT_CMD </Program>
      <Separator/>
	  <Program label=\"Label:  $(lsblk "$PARTITION_PATH" -no LABEL)\"> $TERMINAL -e /usr/bin/zsh -c 'cd $MOUNTPOINT; zsh' </Program>
	  <Program label=\"Size:   $(lsblk "$PARTITION_PATH" -no SIZE | head -n1)\"> gdmap -f $MOUNTPOINT </Program>
	  <Program label=\"Used:    $(df -h "$PARTITION_PATH" | tail -n 1 | awk '{print $5}')\"> $TERMINAL -e ncdu $MOUNTPOINT </Program>
	  <Program label=\"Free:   $(df -h "$PARTITION_PATH" | tail -n 1 | awk '{print $4}')\"> qdirstat $MOUNTPOINT </Program>
	  <Program label=\"Filesystem: $(lsblk "$PARTITION_PATH" -no FSTYPE)\"> $FILEMANAGER $MOUNTPOINT </Program>
		<Menu label=\"Explore\">
		  <Program label=\"Terminal\"> $TERMINAL -e /usr/bin/zsh -c 'cd $MOUNTPOINT; zsh' </Program>
		  <Program label=\"File Manager\"> $FILEMANAGER $MOUNTPOINT </Program>
		  <Program label=\"QDirStat\"> qdirstat $MOUNTPOINT </Program>
		  <Program label=\"gdmap\"> gdmap -f $MOUNTPOINT </Program>
		  <Program label=\"ncdu\"> $TERMINAL -e ncdu $MOUNTPOINT </Program>
		</Menu>
	</Menu>"

	elif [ "$IS_MOUNTED" = "0" ]; then
	  if [ ! "$(grep -e "^$PARTITION_PATH" /etc/fstab)" ] && \
	  	 [ "$(lsblk -o SUBSYSTEMS $PARTITION_PATH | grep usb)" ] && \
	  	 [ -x "/usr/bin/pmount" ]; then
		MOUNT_CMD="pmount $PARTITION_PATH"
	  elif [ "$(grep -e "^$PARTITION_PATH" /etc/fstab)" ]; then
		MOUNT_CMD="sudo -p 'Enter password to mount "$PARTITION_PATH": ' mount $MOUNTPOINT"
	  else
		MOUNT_CMD="	[ ! -d "/media/$PARTITION" ] && sudo mkdir "/media/$PARTITION"; sudo -p 'Enter password to mount "$PARTITION_PATH": ' mount $PARTITION_PATH /media/$PARTITION"
	  fi

	SUBMENU_PARTITION="
	<Menu label=\"$PARTITION\">
	  <Program label=\"Mount\"> $MOUNT_CMD && $FILEMANAGER /media/$PARTITION </Program>
	  <Separator/>
  	  <Program label=\"Label:  $(lsblk "$PARTITION_PATH" -no LABEL)\"> $TERMINAL -e /usr/bin/zsh -c 'lsblk $PARTITION_PATH; zsh' </Program>
	  <Program label=\"Size:   $(lsblk "$PARTITION_PATH" -no SIZE)\"> $FILEMANAGER $MOUNTPOINT </Program>
	  <Program label=\"Filesystem: $(lsblk "$PARTITION_PATH" -no FSTYPE)\"> $FILEMANAGER $MOUNTPOINT </Program>
	</Menu>"
	fi

	MENU="$MENU $SUBMENU_PARTITION"
  done
MENU="$MENU </Menu>"
done

echo "$MENU</JWM>"

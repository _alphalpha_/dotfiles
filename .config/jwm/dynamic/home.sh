#!/bin/sh
# Menu to open stuff in home directory by Michael
for x in $(ls -1 -d */ | sed s'/ /\^space\^/g'); do
  LABEL=$(echo $x | cut -b 1-50)
  ITEMS="$ITEMS <Program label=\"$LABEL\">xdg-open \"$x\"</Program>"
done
ITEMS=$(echo "<Program label=\"Home\">pcmanfm</Program><Separator/> $ITEMS" | sed s'/\^space\^/ /g')
echo "<JWM>"
echo "$ITEMS\n"
echo "</JWM>"

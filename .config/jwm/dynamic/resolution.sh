#!/bin/sh
# Simple Dynamic Screen Resolution Changer for JWM based on xrandr
# written by Michael

echo "<JWM>"
for RES in $(xrandr | awk ' /^[    0-90-9]/ {print $1}'); do
  echo "<Program label=\"$RES\">xrandr -s $RES</Program>"
done
echo "</JWM>"

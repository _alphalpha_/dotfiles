#!/bin/sh
# Simple Service Manager Menu for JWM written by Michael

echo "<JWM>"
for SERVICE in $(rc-status -Cs | grep 'started' | cut -d ' ' -f 2| sort); do
  echo "<Menu label=\"$SERVICE\">"
    echo "<Program label=\"stop\">sudo -A -p 'Enter password to stop $SERVICE' service $SERVICE stop</Program>"
    echo "<Program label=\"restart\">sudo -A -p 'Enter password to restart $SERVICE' service $SERVICE restart</Program>"
    echo "<Program label=\"disable\">sudo -A -p 'Enter password to disable $SERVICE permanently' service $SERVICE stop && sudo update-rc.d $SERVICE disable</Program>"
  echo "</Menu>"
done
echo "<Separator/>"
for SERVICE in $(rc-status -Cs | grep 'stopped' | cut -d ' ' -f 2 | sort); do
  echo "<Menu label=\"$SERVICE\">"
    echo "<Program label=\"start\">sudo -A -p 'Enter password to start $SERVICE' service $SERVICE start</Program>"
    echo "<Program label=\"enable\">sudo -A -p 'Enter password to enable $SERVICE init script' service $SERVICE start && sudo update-rc.d $SERVICE enable</Program>"
  echo "</Menu>"
done
echo "</JWM>"

#!/bin/sh

# load jwm theme
#./.config/jwm/load_theme.sh &

# load Xresources
xrdb -merge .Xresources &

# start compositor
compton -f --config .config/compton/compton.conf &

# start volumeicon
volumeicon &

# start qps in tray
qps -mini &

# show battery icon if battery is detected
[ $(acpi -b  | grep 'Battery' | wc -l ) -ge 1 ] && cbatticon &

# load touchscreen calibrator if touchscreen is detected
[ ! "$(xinput_calibrator --list)" = "No calibratable devices found." ] && x11-touchscreen-calibrator &

# start conky
conky -c .config/conkyrc &

# Fkey sounds
sxhkd -c .config/sxhkd/Fkeys-sounds.conf &

# Michaels
# ▀▀█ █▀▀ █ █   █▀▀ █▀█ █▀█ █▀▀ ▀█▀ █▀▀ #
# ▄▀  ▀▀█ █▀█   █   █ █ █ █ █▀▀  █  █ █ #
# ▀▀▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀ #
autoload -U colors && colors
autoload -U compinit && compinit
autoload -U vcs_info && vcs_info
autoload promptinit && promptinit
zmodload zsh/complist
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
unsetopt beep
scriptdir="$HOME/.local/Scripts"
HISTFILE="$HOME/.cache/zshistory"
HISTSIZE=100000
SAVEHIST=100000
#setopt appendhistory
setopt inc_append_history
setopt share_history


# █▀█ █▀▄ █▀█ █▄ ▄█ █▀█ ▀█▀ #
# █▀▀ █▀▄ █ █ █ ▀ █ █▀▀  █  #
# ▀   ▀ ▀ ▀▀▀ ▀   ▀ ▀    ▀  #
#PS1="%D{%H:%M} [%B%F{yellow} %~/%f%k%b]"
PS1="┌─[%B%F{yellow}%~/%f%k%b]-%D{%H:%M}-[%F{yellow}%B%n%b%f@%F{yellow}%B%M%b%f]
└> "
PS2='\`%_> '
PS3='?# '
PS4='+%N:%i:%_> '

if [[ -z "$WM" ]]; then
#  PS1="[%n@%M] %~/%f%k%b "
PS1="┌─[%B%F{yellow}%~/%f%k%b]-%D{%H:%M}-[%F{yellow}%B%n%b%f@%F{yellow}%B%M%b%f]
└> "

  PS2="\`%_> "
#  RPROMPT="%D{%H:%M}"
fi

# █▀▀ █ █ █▀█ █▀▀ ▀█▀ ▀█▀ █▀█ █▀█ █▀▀ #
# █▀▀ █ █ █ █ █    █   █  █ █ █ █ ▀▀█ #
# ▀   ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀ #
source $scriptdir/functions/update_all.func
source $scriptdir/functions/main_menu.func
source $scriptdir/functions/extract.func
source $scriptdir/functions/qemu.func
source $scriptdir/functions/search_things.func
source $scriptdir/functions/math.func
source $scriptdir/functions/check_file.func
source $scriptdir/functions/figlet_demo.func
print_alert() {
[ "$1" = "-c" ] && MSG='# ███' || MSG=' ███'
echo -e "\n$MSG\n$MSG\n$MSG\n$MSG\n\n$MSG\n"
}


# █▀█ █   ▀█▀ █▀█ █▀▀ █▀▀ █▀▀ #
# █▀█ █    █  █▀█ ▀▀█ █▀▀ ▀▀█ #
# ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀ ▀▀▀ ▀▀▀ #
source $ZDOTDIR/aliases


# █ █ █▀▀ █ █ █▀▀ #
# █▀▄ █▀▀  █  ▀▀█ #
# ▀ ▀ ▀▀▀  ▀  ▀▀▀ #
source $ZDOTDIR/keys

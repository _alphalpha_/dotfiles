#!/usr/bin/zsh

check_file() {
if [ -e "$1" ]; then
  du -h "$1"
  ls -la "$1" | awk '{print $3,$4}'
  [ -f "$1" ] && echo "$1 is a regular file."
  [ -d "$1" ] && echo "$1 is a directory."
  [ -r "$1" ] && echo "$1 is a readable."
  [ -w "$1" ] && echo "$1 is a writable."
  [ -x "$1" ] && echo "$1 is a executable."
  [ -L "$1" ] && echo "$1 is a symbolic link." && ls -la "$1" | awk '{print $8,$9,$10}'
else
  echo "$1 does not exist."
fi
}
#!/usr/bin/zsh
check_kvm() { [ -e /dev/kvm ] && KVM_FLAG="-enable-kvm -cpu host" }
#emuarm() { ISO="$1"; check_kvm; QEMU_CMD="sudo qemu-system-arm ${KVM_FLAG} -m 2560 -drive format=raw,file=$ISO"; `echo $QEMU_CMD` }
emu32() { ISO="$1"; check_kvm; QEMU_CMD="sudo qemu-system-i386 ${KVM_FLAG} -m 2560 -drive format=raw,file=$ISO"; `echo $QEMU_CMD` }
emu64() { ISO="$1"; check_kvm; QEMU_CMD="sudo qemu-system-x86_64 ${KVM_FLAG} -m 2560 -drive format=raw,file=$ISO"; `echo $QEMU_CMD` }
alias emu="emu64"

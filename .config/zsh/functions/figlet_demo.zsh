#!/bin/sh

figlet_demo() {
for figlet_font_name in $(ls -1A /usr/share/figlet/*.*lf | cut -d\/ -f 5 | cut -d\. -f 1) ; do
printf "$figlet_font_name :\n"
figlet -f $figlet_font_name $figlet_font_name
printf "\n"
done
}


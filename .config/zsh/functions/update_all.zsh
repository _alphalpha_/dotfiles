#!/usr/bin/zsh

update_all() {
## palemoon
"$scriptdir"/palemoon.sh -u

## qutebrowser
"$scriptdir"/update_qutebrowser.sh

## icecat
"$scriptdir"/update_icecat.sh

## deadbeef
"$scriptdir"/update_deadbeef.sh

## shotcut
"$scriptdir"/update_shotcut.sh

## youtube-dl
echo "Searching updates for youtube-dl..."
/usr/local/bin/youtube-dl -U

## apt update && apt upgrade
echo "Updating package list..."
sudo apt update
echo "Installing updates..."
sudo apt upgrade
}

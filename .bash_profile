if [ -f $HOME/.bashrc ]; then
	. $HOME/.bashrc
fi

# change colors on tty
if [ "$TERM" = "linux" ]; then
    echo -en "\e]P0201010" #0
    echo -en "\e]P1E1DACE" #1
    echo -en "\e]P27CCC00" #2
    echo -en "\e]P3FEEE92" #3
    echo -en "\e]P43977A7" #4
    echo -en "\e]P5FFB52A" #5
    echo -en "\e]P600AD9C" #6
    echo -en "\e]P7DC6434" #7
    echo -en "\e]P8201020" #8
    echo -en "\e]P9DC6434" #9
    echo -en "\e]PA7CCC00" #10
    echo -en "\e]PBFEEE92" #11
    echo -en "\e]PC3977A7" #12
    echo -en "\e]PDFFB52A" #13
    echo -en "\e]PE00AD9C" #14
    echo -en "\e]PFE1DACE" #15
    clear
fi

# login menu
scriptdir="$HOME/.local/Scripts"
source $scriptdir/functions/main_menu.func
[ "$(xset q)" ] || main_menu
#[ "$(xset q)" ] || { export WM="jwm" && startx;}
#[ "$(xset q)" ] || { export WM="i3" && startx;}
#[ "$(xset q)" ] || { export WM="bspwm" && startx;}
#[ "$(xset q)" ] || { export WM="spectrwm" && startx;}

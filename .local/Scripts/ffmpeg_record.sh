#!/bin/sh
output_dir="$HOME/Rec"
output_name=$(date +Record-%Y-%m-%d_%H-%M-%S)
output_format=".mp4"
input_format="x11grab"
input_source=":0.0"
video_res=$(xdpyinfo | awk '/dimensions/ {print $2}')
video_framerate="24"
video_codec="libx264"
audio_codec="pcm_s16le"
preset="-preset ultrafast"

if [ "$1" = "-sepia" ]; then
  option="-filter_complex colorchannelmixer=.393:.769:.189:0:.349:.686:.168:0:.272:.534:.131"
elif [ "$1" = "-greyscale" ]; then
  option="-filter_complex colorchannelmixer=.3:.4:.3:0:.3:.4:.3:0:.3:.4:.3"
elif [ "$1" = "-webcam" ]; then
  WEBCAM="1"
fi

[ -d "$output_dir" ] || mkdir -pv "$output_dir"

if [ "$WEBCAM" = "1" ]; then
  ffmpeg -f video4linux2 -framerate 30 -video_size hd720 -i /dev/video0 "$output_dir/$output_name$output_format"
else
  ffmpeg -ac 2 -f "$input_format" -r "$video_framerate" -s "$video_res" -i "$input_source" -acodec "$audio_codec" -vcodec "$video_codec" ${preset} ${option} "$output_dir/$output_name$output_format"
fi

exit 0

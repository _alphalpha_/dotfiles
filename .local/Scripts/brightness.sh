#!/bin/sh
monitor=$(xrandr | awk '$2 == "connected" { print $1 }')
brightness=$(xrandr --verbose | awk '$1 == "Brightness:" { print $2 }' | cut -d '.' -f2 | cut -b 1)
[ "$brightness" = "0" ] && brightness="10"
case "$1" in
  +) brightness="$((brightness + 3))";;
  -) brightness="$((brightness - 3))";;
esac
if [ "$brightness" -le "0" ]; then
  value="0.1"
elif [ "$brightness" -ge "10" ]; then
  value="1"
else
  value="0.$brightness"
fi
xrandr --output $monitor --brightness "$value"
exit 0

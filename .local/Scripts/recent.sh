#!/bin/sh

if [ -z "$1" ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  echo "Show files in the current directory that were recently modified."
  echo "\033[1mExamples:\033[0m"
  echo "  show files that were modified in the last 5 minutes:"
  echo "    \033[1mrecent 5m\033[0m"
  echo "  show files that were modified in the last hour:"
  echo "    \033[1mrecent 1h\033[0m"
  echo "  show files that were modified in the last 7 days:"
  echo "    \033[1mrecent 7d\033[0m"
else
  DIMENSION="$(echo "$1" | awk '{print substr($0,length,1)}')"
  VALUE="$(echo "$1" | tr -d -c 0-9)"
  if [ "$VALUE" ]; then
    case "$DIMENSION" in
      m|M) OPTION="-mmin";;
      h|H) DIMENSION="m"; VALUE="$(($VALUE * 60))"; OPTION="-mmin";;
      d|D) OPTION="-mtime";;
      *) OPTION="-mmin";;
    esac
    find . -type f $OPTION -$VALUE
  fi
fi

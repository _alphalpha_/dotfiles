#!/bin/sh
GAME_DIR="$HOME/PSX"

if [ ! -f /usr/bin/cdrdao ] || [ ! -f /usr/bin/bchunk ]; then
  printf "Error: Missing dependencies\\n"
  exit 1
fi

if [ "$1" ]; then
  GAME="$1"
else
 printf "ERROR: Invalid usage.\\nExample: rip-psx.sh 'filename'\\nwhere filename is the desired name, minus extensions.\\n"
 exit 1
fi

[ -d "$GAME_DIR" ] || mkdir -p "$GAME_DIR"
CURRENCT_DIR="$(pwd)"
cd "$GAME_DIR"
cdrdao read-cd --read-raw --datafile "$GAME".bin --device /dev/sr0 --driver generic-mmc-raw "$GAME".toc
toc2cue "$GAME".toc "$GAME".cue
bchunk -r "$GAME".bin "$GAME".cue "$GAME"
[ -f "$GAME"01.iso ] && mv "$GAME"01.iso "$GAME".iso 
[ -f "$GAME".bin ] && rm "$GAME".bin
[ -f "$GAME".toc ] && rm "$GAME".toc
[ -f "$GAME".cue ] && rm "$GAME".cue
cd "$CURRENT_DIR"
exit 0

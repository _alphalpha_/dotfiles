#!/bin/sh
###########################################################################################
# update_kernel.sh version 1.1.1
# by Michael
###########################################################################################
# This is a script to download and compile the Linux kernel or the Linux-Libre Kernel.
# Use at your own risk!

# You can choose between 3 different approaches of building the kernel:
# Approach 1 uses 'make-kpkg' which is part of kernel-package.
# For some reason kernel-package did not make it into the Beowulf backports repo.
# I installed the Ascii version.
# Approach 2 uses 'make deb-pgk' wich depends on debhelper.
# Approach 3 uses plain 'make' and 'make install'. This will not create a deb package.
# (Linux Libre always uses 'make deb-pkg')

###########################################################################################
### CONFIG SECTION

# APPROACH can be 'make-kpkg', 'make-deb' or 'make-install'. See the information above.
APPROACH="make-kpkg"

# TMP_DIR is the directory where the kernel will be downloaded and built.
TMP_DIR="$HOME/tmp"

# NEEDED_SPACE is the amount of disk space that this script will check if it is available.
# I set it to 30GB, which is more than enough. You can reduce this if you need.
NEEDED_SPACE="30000000"

# APPENDIX adds a short text string at the end of the created kernel package.
# This only works if APPROACH is set to 'make-kpkg'
APPENDIX="-custom"


###########################################################################################
### SCRIPT STARTS HERE

# install missing dependencies
SCRIPT_DEPENDS="iselect coreutils dpkg sudo curl wget lzip tar debhelper"
BUILD_DEPENDS="make bison build-essential fakeroot flex libelf-dev libncurses5-dev libssl-dev"
[ "$APPROACH" = "make-kpgk" ] && APPROACH_DEPENDS="kernel-package"
[ "$APPROACH" = "make-deb" ] && APPROACH_DEPENDS="debhelper"
DEPENDS="$SCRIPT_DEPENDS $BUILD_DEPENDS ${APPROACH_DEPENDS}"
for PKG in $(echo "$DEPENDS" | xargs -n1); do
  printf "checking if %s is installed... " "$PKG"
  if dpkg -l "$PKG" | grep -qe ^"ii"; then
    printf "OK!\\n"
  else
    printf "Not found!\\nInstall %s now? [Y/n]\\n" "$PKG"
    read -r ans
    if [ "$ans" = "Y" ] || [ "$ans" = "y" ] || [ -z "$ans" ] ; then
      sudo apt install "$PKG" --no-install-recommends
    else
      exit 0
    fi
  fi
done

# Linux-Libre or standart Linux?
KTYPE=$(iselect "Select a Kernel:" "  > Linux-Libre Kernel from fsfla.org<s:LIBRE>" "  > Linux Kernel from kernel.org<s:LINUX>" -p 2)

# check installed kernel version
LINE="$(uname -r | cut -d '-' -f 1 | cut -d '.' -f 1,2)"

# check free space
check_free_space() {
# find the partition where TMP_DIR is located
CHECK_DIR="$TMP_DIR"
CHECK_DIR_DEPTH="$(echo "$CHECK_DIR" | grep -o '/' | wc -l)"
TARGET_PART=""
TARGET_PART_SIZE=""
if [ "$CHECK_DIR_DEPTH" -gt 1 ]; then
  while [ ! -d "$CHECK_DIR" ] && [ "$CHECK_DIR_DEPTH" -gt 1 ]; do
    CHECK_DIR="$(echo "$CHECK_DIR" | cut -d '/' -f 1-"$CHECK_DIR_DEPTH")"
    CHECK_DIR_DEPTH="$((CHECK_DIR_DEPTH - 1))"
  done
  if [ -d "$CHECK_DIR" ]; then
    TARGET_PART="$(df --output=source "$CHECK_DIR" | awk '(NR == 2 ) {print $1}')"
    TARGET_PART_SIZE="$(df --output=avail "$TARGET_PART" | awk '(NR == 2 ) {print $1}')"
  else
    TARGET_PART="$(df --output=source / | awk '(NR == 2 ) {print $1}')"
    TARGET_PART_SIZE="$(df --output=avail "$TARGET_PART" | awk '(NR == 2 ) {print $1}')"
  fi
else
  TARGET_PART="$(df --output=source / | awk '(NR == 2 ) {print $1}')"
  TARGET_PART_SIZE="$(df --output=avail "$TARGET_PART" | awk '(NR == 2 ) {print $1}')"
fi
}

check_free_space
while [ "$NEEDED_SPACE" -gt "$TARGET_PART_SIZE" ]; do
  printf "There is not enough free space on %s \\n" "$TARGET_PART"
  printf "Enter a new location and confirm with enter: "
  read -r TMP_DIR
  # remove trailing slash if it exists
  if [ "$(printf "%s\\n" "$TMP_DIR" | sed -e "s/^.*\(.\)$/\1/")" = "/" ]; then
    TMP_DIR="$(printf "$TMP_DIR" | head -c -1)"
  fi
  if ! echo "$TMP_DIR" | grep '/'; then TMP_DIR="$(pwd)/$TMP_DIR"; fi
  check_free_space
done

# create directory
if [ ! -d "$TMP_DIR/KERNEL" ]; then
  mkdir -pv "$TMP_DIR/KERNEL"
  printf "creating %s" "$TMP_DIR/KERNEL"
elif  [ -d "$TMP_DIR/KERNEL" ] && [ ! "$(find "$TMP_DIR/KERNEL" -maxdepth 0 -type d -empty)" ]; then
  printf "%s/KERNEL is not empty, remove everithing inside it now? (y/N)" "$TMP_DIR"
  read ans
  if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
    rm -rdv "$TMP_DIR"/KERNEL/*
  fi
fi
CURRENT_DIR="$(pwd)"
cd "$TMP_DIR/KERNEL"

# download or local?
ask_dl() {
if [ "$KTYPE" = "LINUX" ]; then
  printf "\\nDownload a new version from kernel.org or use a local tarball? [D/l] "
elif [ "$KTYPE" = "LIBRE" ]; then
  printf "\\nDownload a new version from fsfla.org or use a local tarball? [D/l] "
fi
read -r DL_L
}
ask_dl

[ -z "$DL_L" ] && DL_L="D"
if [ "$DL_L" = "D" ] || [ "$DL_L" = "d" ]; then
  if [ "$KTYPE" = "LINUX" ]; then
    #  # ping kernel.org or exit
    #  [ "$(sudo ping -c 1 kernel.org)" ] || { echo "\n\ping to kernel.org failed" ; exit 1 ; }

    # check new versions
    VERSION_LIST="$(curl -s https://www.kernel.org/feeds/kdist.xml | tr '"' ' ' | xargs -n1 | grep https | tr '/' ' ' | xargs -n 1 | grep tar.xz | sed 's/.tar.xz//g;s/$/<s>/g')"

    # select new version
    MENU_PRESELECTED_LINE="$(echo "$VERSION_LIST" | grep -n "linux-$LINE" | cut -d ':' -f 1 | tail -n 1)"
    VERSION="$(iselect "Your current version is $VERSION" "Select the new version that you want to download:" \
    		"" "$VERSION_LIST" -p $((MENU_PRESELECTED_LINE + 3)) )"
    MAIN_VERSION="$(echo "$VERSION" | cut -d '-' -f 2 | cut -b 1)"

	[ "$VERSION" ] || exit 1

    # download
    wget -c "$(curl -s https://www.kernel.org/feeds/kdist.xml | tr '"' ' ' | xargs -n 1 | grep https | grep "$VERSION".tar.xz)"
    wget -c "$(curl -s https://www.kernel.org/feeds/kdist.xml | tr '"' ' ' | xargs -n 1 | grep https | grep "$VERSION".tar.sign)"
    curl -s https://cdn.kernel.org/pub/linux/kernel/v"$MAIN_VERSION".x/sha256sums.asc | grep -i "$VERSION".tar.xz > "$VERSION".tar.xz.sha256

    # compare checksum
    if [ -f "$VERSION".tar.xz ] && [ "$(cat "$VERSION".tar.xz.sha256)" = "$(sha256sum "$VERSION".tar.xz)" ]; then
      echo "SHA256 checksum ok."
    else
      echo "Warning! SHA256 checksum not matching." && exit 1
    fi

    #gpg2 --locate-keys torvalds@kernel.org gregkh@kernel.org
    #gpg2 --verify "$version".tar.sign

  elif [ "$KTYPE" = "LIBRE" ]; then
    # check new versions
    VERSION_LIST="$(curl -s http://linux-libre.fsfla.org/pub/linux-libre/freesh/ | sed 's/</ /g;s/>/ /g' | xargs -n 1 | grep -e ^"linux-libre" | grep -e ".tar.lz"$ | sed 's/$/<s>/g')"

    # select new version
    MENU_PRESELECTED_LINE="$(echo "$VERSION_LIST" | grep -n "linux-libre-$LINE" | cut -d ':' -f 1 | tail -n 1)"
    VERSION="$(iselect "Your current version is $VERSION" "Select the new version that you want to download:" \
    		"" "$VERSION_LIST" -p "$((MENU_PRESELECTED_LINE + 3))")"
    MAIN_VERSION="$(echo "$VERSION" | cut -d '-' -f 2 | cut -b 1)"

	[ "$VERSION" ] || exit 1

    #download
    wget -c "http://linux-libre.fsfla.org/pub/linux-libre/freesh/$VERSION"
    wget -c "http://linux-libre.fsfla.org/pub/linux-libre/freesh/$VERSION.asc"
    wget -c "http://linux-libre.fsfla.org/pub/linux-libre/freesh/sha512sum"

    # compare checksum
    if [ -f "$VERSION" ]; then
      if [ "$(sha512sum "$VERSION")" = "$(grep "$VERSION" sha512sum)" ]; then
        echo "SHA512 checksum ok."
      else
        echo "Warning! SHA512 checksum not matching." && exit 1
      fi
    else
      echo "$VERSION not found" && exit 1
    fi
    VERSION="$(echo "$VERSION" | sed 's/.tar.lz//')"
  fi

elif [ "$DL_L" = "L" ] || [ "$DL_L" = "l" ]; then
  while [ ! -f "$LOCAL_FILE" ]; do
    printf "\\nEnter the absolute path to the local tarball: "
    read -r LOCAL_FILE
    [ "$LOCAL_FILE" = "exit" ] && exit
  done
# copy file to correct location if needed
  if [ ! -f "$(echo "$LOCAL_FILE" | tr '/' ' ' | awk '{print $NF}')" ]; then
    cp -v "$LOCAL_FILE" .  || exit 1
  fi
  VERSION="$(echo "$LOCAL_FILE" | tr '/' ' ' | xargs -n1 | tail -n1 | sed 's/.tar//;s/.xz//;s/.lz//')"
elif [ "$DL_L" = "exit" ]; then
  exit 0
else
  ask_dl
fi

# extract kernel archive
if [ -f "$VERSION".tar.xz ]; then
  tar xJvf "$VERSION".tar.xz
elif [ -f "$VERSION".tar.lz ]; then
  tar --lzip -xvf "$VERSION".tar.lz
fi

[ -d "$VERSION" ] && cd "$VERSION" || exit 1

# make clean
if [ "$KTYPE" = "LINUX" ]; then
  make clean && make mrproper
elif [ "$KTYPE" = "LIBRE" ]; then
  cd linux
  make clean && make mrproper
  cd ..
fi

# copy config
OLD_CONFIG="/boot/config-$(uname -r)"
WHAT_CONFIG=""
while [ -z "$WHAT_CONFIG" ]; do
  if [ "$KTYPE" = "LINUX" ]; then
    WHAT_CONFIG="$(iselect "Use $OLD_CONFIG?<s:OLD>" "Use other config file?<s:OTHER>")"
  elif [ "$KTYPE" = "LIBRE" ]; then
    WHAT_CONFIG="$(iselect "Use config file that was shipped with the libre kernel?<s:LIBRE>" \
    			"Use $OLD_CONFIG?<s:OLD>" "Use other config file?<s:OTHER>")"
  fi
done

if [ "$WHAT_CONFIG" = "OLD" ] && [ -f "$OLD_CONFIG" ]; then
  if [ -f "$OLD_CONFIG" ]; then
    cat "$OLD_CONFIG" > .config
  fi
elif [ "$WHAT_CONFIG" = "LIBRE" ] && [ -d "configs" ]; then
   CONFIG=$(iselect "$(ls -1A "configs" | sort -r | sed 's/$/<s>/g')")
   echo "$CONFIG"
   cat configs/"$CONFIG" > .config
else
  while [ ! -f "$OTHER_CONFIG" ]; do
    printf "\\nEnter the absolute path to the config file that you want to use:\\n"
    read -r OTHER_CONFIG
    if [ -f "$OTHER_CONFIG" ]; then
      cat "$OTHER_CONFIG" > .config
    else
      printf "Error, file not found.\\n" && sleep 1
    fi
  done
fi

# remove the trusted keys line or the build will fail
# make[4]: *** No rule to make target 'debian/certs/debian-uefi-certs.pem'
sed -i "s:$(grep CONFIG_SYSTEM_TRUSTED_KEYS .config):CONFIG_SYSTEM_TRUSTED_KEYS=\"\":g" .config

if [ "$KTYPE" = "LIBRE" ]; then
  mv .config linux/.config
  cd linux
  APPROACH="make-deb"
fi

# localmodconfig
printf "\\nMake localmodconfig? [y/N]"
read -r ans
if [ "$ans" = "y" ] || [ "$ans" = "Y" ]; then
  make localmodconfig
fi

# menuconfig
make menuconfig

msg_ok() { echo "Compiling finished. Install new kernel now? [y/n]"; }
msg_fail() { echo "Error" && exit 1; }

if [ "$APPROACH" = "make-kpkg" ]; then
  if [ "$APPENDIX" ]; then
    fakeroot make-kpkg -j "$(nproc)" --initrd --append-to-version=$APPENDIX kernel_image kernel_headers && msg_ok || msg_fail
  else
    fakeroot make-kpkg -j "$(nproc)" --initrd kernel_image kernel_headers && msg_ok || msg_fail
  fi
elif [ "$APPROACH" = "make-deb" ]; then
  make -j "$(nproc)" deb-pkg && msg_ok || msg_fail
elif [ "$APPROACH" = "make-install" ]; then
  make -j "$(nproc)"
else
  echo "Warning! APPROACH can be 'make-kpkg', 'make-deb' or 'make-install'." && exit 1
fi

cd ..

if [ "$APPROACH" = "make-kpkg" ] || [ "$APPROACH" = "make-deb" ]; then
  MENU_LINE="2"
  while [ "$SELECTION" != "OK" ]; do
    SELECTION="$(iselect "Select a package that you want to install:" "$(ls *.deb | sed 's/$/<s>/g')" "" "OK<s>" -p "$MENU_LINE" )"
    MENU_LINE="$((MENU_LINE + 1))"
    [ "$SELECTION" = "OK" ] || sudo dpkg -i "$SELECTION"
  done
elif [ "$APPROACH" = "make-install" ]; then
  printf "\\nCompiling finished !\\nInstall new kernel now? [y/N]"
  read ans
  if [ "$ans" = "y" ] || [ "$ans" = "Y" ] ; then
    sudo make install
  fi
fi

# modify syslinux.cfg / grub.cfg
edit_boot_cfg() {
if [ "$KTYPE" = "LINUX" ]; then
  VERSION="$(echo "$VERSION" | sed 's/linux-//')"
elif [ "$KTYPE" = "LIBRE" ]; then
  VERSION="$(echo "$VERSION" | sed 's/linux-libre-//;s/-source/-gnu/')"
fi  
NEW_VMLINUZ="$(find /boot -not \( -path /boot/lost+found -prune \) -name "*vmlinuz*" | sed 's:/boot/::' | grep "$VERSION")"
NEW_INITRD="$(find /boot -not \( -path /boot/lost+found -prune \) -name "*initrd*" | sed 's:/boot/::' | grep "$VERSION")"
printf "modifying %s \\n" "$BOOT_CFG"
sudo sed -i "s/vmlinuz-$(uname -r)/$NEW_VMLINUZ/g;s/initrd.img-$(uname -r)/$NEW_INITRD/g" "$BOOT_CFG"
}

SYSLINUX_CFG=$(find /boot -not \( -path /boot/lost+found -prune \) -name syslinux.cfg)
GRUB_CFG=$(find /boot -not \( -path /boot/lost+found -prune \) -name grub.cfg)

if [ -f "$SYSLINUX_CFG" ] && [ -f "$GRUB_CFG" ]; then
  while [ "$SELECTION" != "OK" ]; do
    SELECTION="$(iselect "Select config file to configure:" "$SYSLINUX_CFG" "$GRUB_CFG" "" "OK<s>" -p "$MENU_LINE" )"
    BOOT_CFG="$SELECTION"
    [ "$SELECTION" = "OK" ] || edit_boot_cfg
    printf "%s modified.\\n" "$BOOT_CFG"
  done
elif [ -f "$SYSLINUX_CFG" ]; then
  printf "\\nModify syslinux.cfg now? [Y/n]"
  read -r ans
  if [ "$ans" = "y" ] || [ "$ans" = "Y" ] || [ -z "$ans" ]; then
    BOOT_CFG="$SYSLINUX_CFG" && edit_boot_cfg
    printf "%s modified.\\n" "$BOOT_CFG"
  else
    printf "%s was not modified.\\n" "$BOOT_CFG"
  fi
elif [ -f "$GRUB_CFG" ]; then
  printf "\\nModify grub.cfg now? [Y/n]"
  read -r ans
  if [ "$ans" = "y" ] || [ "$ans" = "Y" ] || [ -z "$ans" ]; then
    BOOT_CFG="$GRUB_CFG" && edit_boot_cfg
    printf "%s modified.\\n" "$BOOT_CFG"
  else
    printf "%s was not modified.\\n" "$BOOT_CFG"
  fi
else
  printf "No Bootloader config modified.\\n"
fi

printf "\\nDone.\\n\\n\\n"
cd "$CURRENT_DIR"

echo "You can reboot the system now."
echo "You may want to clean $TMP_DIR and uninstall the old kernel when you are sure that the new kernel is working."

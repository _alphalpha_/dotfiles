#!/bin/sh
# script to install/update deadbeef
DEBUG="0"

# install path
INSTALL_PATH="$HOME/.local/opt"

# make sure unhtml is installed
which unhtml 1>/dev/null || { printf "Error: could not find 'unhtml'\\n"; exit 1;} 

# connection test
curl --connect-timeout 3 -s "https://deadbeef.sourceforge.io/download.html" 1>/dev/null \
  || { printf "Error: could not connect to 'https://deadbeef.sourceforge.io/download.html'\\n"; exit 1;} 

# get latest version number
LATEST_VERSION="$(curl -s 'https://deadbeef.sourceforge.io/download.html' \
  | grep 'Linux x86_64' | unhtml | xargs -n1 | grep -E "[0-9]?{3}\.[0-9]?{3}\.?[0-9]?{3}")"

# get installed version number
if [ -f "$INSTALL_PATH"/deadbeef/doc/ChangeLog ]; then
  CURRENT_VERSION="$(awk '(NR == 1) {print $2}' "$INSTALL_PATH"/deadbeef/doc/ChangeLog)"
  printf "Searching updates for deadbeef... "
else
  CURRENT_VERSION="0"
fi

# print version numbers for debugging
if [ "$DEBUG" = "1" ]; then
  printf "\\nupdate_deadbeef.sh debug message:\\n"
  printf "current version %s\\n" "$CURRENT_VERSION"
  printf "latest version %s\\n" "$LATEST_VERSION"
  exit 0
fi

# compare versions
if [ "$LATEST_VERSION" = "$CURRENT_VERSION" ]; then
  printf "%s up to date.\\n" "$CURRENT_VERSION" && exit 0
elif [ "$CURRENT_VERSION" = "0" ]; then
  printf "Installing deadbeef %s\\n" "$LATEST_VERSION"
else
  printf "found new version %s\\n" "$LATEST_VERSION"
fi

# download
[ -d "$INSTALL_PATH"/deadbeef/ ] && rm -rdf "$INSTALL_PATH"/deadbeef/
[ -d "$INSTALL_PATH" ] || mkdir -p "$INSTALL_PATH"
DL_URL="$(curl -s 'https://deadbeef.sourceforge.io/download.html' | grep 'Linux x86_64'  | cut -d\" -f 2)"
wget -c -O "$INSTALL_PATH"/deadbeef-latest.tar.bz2 "$DL_URL" 
tar xvjf "$INSTALL_PATH"/deadbeef-latest.tar.bz2 -C "$INSTALL_PATH" && rm "$INSTALL_PATH"/deadbeef-latest.tar.bz2
[ -d "$INSTALL_PATH"/deadbeef-"$LATEST_VERSION" ] && mv "$INSTALL_PATH"/deadbeef-"$LATEST_VERSION" "$INSTALL_PATH"/deadbeef

exit 0

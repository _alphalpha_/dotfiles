#!/bin/sh

color_0() { echo "\033[1;30m"; }; bg_color_0() { echo "\033[1;40m"; }
color_1() { echo "\033[1;31m"; }; bg_color_1() { echo "\033[1;41m"; }
color_2() { echo "\033[1;32m"; }; bg_color_2() { echo "\033[1;42m"; }
color_3() { echo "\033[1;33m"; }; bg_color_3() { echo "\033[1;43m"; }
color_4() { echo "\033[1;34m"; }; bg_color_4() { echo "\033[1;44m"; }
color_5() { echo "\033[1;35m"; }; bg_color_5() { echo "\033[1;45m"; }
color_6() { echo "\033[1;36m"; }; bg_color_6() { echo "\033[1;46m"; }
color_7() { echo "\033[1;37m"; }; bg_color_7() { echo "\033[1;47m"; }
color_end() { echo "\033[0m"; }

text_normal() { echo "\033[0m"; }
text_bold() { echo "\033[1m"; }
text_italic() { echo "\033[3m"; }
text_underlined() { echo "\033[4m"; }
text_blinking() { echo "\033[5m"; }
text_special() { echo "\033[7m"; }

# exmaple
# echo "$(color_4)This $(color_5)is $(color_2)a $(color_3)test$(text_blinking)! $(color_end)"


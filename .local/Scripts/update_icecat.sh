#!/bin/sh
# script to download/update GNU Icecat
DEBUG="0"
INSTALL_PATH="$HOME/.local/opt"

# Connection test
curl --connect-timeout 3 -s "https://www.gnu.org/software/gnuzilla/" 1>/dev/null \
  || { printf "Error: could not connect to 'https://www.gnu.org/software/gnuzilla/'\\n"; exit 1;} 

# Figure out the latest version
URL="$(curl -s https://www.gnu.org/software/gnuzilla/ \
	| awk 'BEGIN{
RS="</a>"
IGNORECASE=1
}
{
  for(o=1;o<=NF;o++){
    if ( $o ~ /Latest/){
      gsub(/.*href=\042/,"",$o)
      gsub(/\042.*/,"",$o)
      print $(o)
    }
  }
}' | cut -b3-)"

# few mirrors actually dont work with this script so try a few times
TRY="0"
while [ -z "$DL_URL" ] && [ "$TRY" -le 5 ]; do
  TRY="$((TRY+1))"
  DL_URL="$(lynx --dump "$URL" | awk '/icecat-[0-9]*\.[0-9a-zA-Z\-\.-_]*-x86_64\.tar\.bz2$/ {print $2}')"
done
[ "$TRY" -gt 5 ] && exit 1

LATEST_VERSION="$(echo "$URL" | grep -E -o "[0-9]?[0-9]?[0-9]\.[0-9]?[0-9]?[0-9]\.?[0-9]?[0-9]?[0-9]" | head -n1)"
# Check local version
if [ -x "$INSTALL_PATH"/icecat/icecat-bin ]; then
  CURRENT_VERSION="$("$INSTALL_PATH"/icecat/icecat-bin -v | awk '{print $3}')"
  printf "Searching updates for GNU icecat... "
else
  CURRENT_VERSION="0"
fi

# Print versions
if [ "$DEBUG" = "1" ]; then
  printf "\\nupdate_icecat.sh debug message:\\n"
  printf "current version %s\\n" "$CURRENT_VERSION"
  printf "latest version %s\\n" "$LATEST_VERSION"
  exit 0
fi

# Compare versions
if [ "$LATEST_VERSION" = "$CURRENT_VERSION" ]; then
  printf "%s up to date.\\n" "$CURRENT_VERSION"  && exit 0
elif [ "$CURRENT_VERSION" = "0" ]; then
  printf "Installing GNU Icecat $LATEST_VERSION\\n"
else
  printf "found new version %s\\n" "$LATEST_VERSION"
fi

# Download
[ -d "$INSTALL_PATH" ] || mkdir -p "$INSTALL_PATH"
[ -f "$INSTALL_PATH/icecat-latest.tar.bz2" ] && rm "$INSTALL_PATH/icecat-latest.tar.bz2"
wget -c -O "$INSTALL_PATH"/icecat-latest.tar.bz2 "$DL_URL"
tar xvjf "$INSTALL_PATH/icecat-latest.tar.bz2" -C "$INSTALL_PATH" && rm "$INSTALL_PATH/icecat-latest.tar.bz2"

exit 0

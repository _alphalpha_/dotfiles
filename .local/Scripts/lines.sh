#!/bin/sh

show_help(){
echo 'Example: "lines.sh 1,5-7 file.txt" will print lines 1 and 5 to 7 of file.txt'
}

[ "$1" = "-h" ] || [ "$1" = "--help" ] || [ -z "$1" ] && show_help && exit 0

if [ -f "$1" ] && [ "$2" ]; then
  FILE="$1"
  LINES="$2"
elif [ -f "$2" ] && [ "$1" ]; then
  FILE="$2"
  LINES="$1"
else
  echo "Syntax error." && show_help
fi

awk_loop(){
for i in $LINES; do
  LINE="$(awk '(NR == '$i' ) {print}' "$FILE")"
  LINE="$i: $LINE"
  [ "$(echo "$LINE" | sed '/^$/d')" ] && echo "$LINE"
done
}

comma2space(){
[ "$(echo "$LINES" | grep ',')" ] && LINES="$(echo "$LINES" | tr ',' ' ')"
}

dash2numbers(){
for n in $(echo "$LINES" | xargs -n1 | grep '-'); do
  X="$(echo "$n" | cut -d '-' -f 1)"
  Y="$(echo "$n" | cut -d '-' -f 2)"
  while [ "$X" -le "$Y" ]; do
    LINES="$LINES $X"
    X="$(($X+1))"
  done
done
}

echo "$FILE"
if [ "$(echo "$LINES" | grep -v "[,-]")" ]; then
  awk_loop
  exit 0
elif [ "$(echo "$LINES" | grep ',')" ] && [ "$(echo "$LINES" | grep -v '-')" ]; then
  comma2space
  awk_loop
  exit 0
elif [ "$(echo "$LINES" | grep -v ',')" ] && [ "$(echo "$LINES" | grep '-')" ]; then
  dash2numbers
  awk_loop
  exit 0
elif [ "$(echo "$LINES" | grep ',')" ] && [ "$(echo "$LINES" | grep '-')" ]; then
  comma2space
  dash2numbers
  awk_loop
  exit 0
fi

exit 0

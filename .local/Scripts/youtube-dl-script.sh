#!/bin/sh
# poorly written front-end for youtube-dl
# by Michael

SCRIPTVERSION="1.8"
TEMPFILE="$HOME/.cache/ytdl_script_list.tmp"
quali="18"
noplaylist="on"
LIMITRATE="off"
version="$(youtube-dl --version)"
DL_DIR="$(pwd)"

header () {
clear
echo "  ╻ ╻┏━┓╻ ╻╺┳╸╻ ╻┏┓ ┏━╸   ╺┳┓╻     ┏━┓┏━╸┏━┓╻┏━┓╺┳╸"
echo "  ┗┳┛┃ ┃┃ ┃ ┃ ┃ ┃┣┻┓┣╸ ╺━╸ ┃┃┃     ┗━┓┃  ┣┳┛┃┣━┛ ┃ "
echo "   ╹ ┗━┛┗━┛ ╹ ┗━┛┗━┛┗━╸   ╺┻┛┗━╸   ┗━┛┗━╸╹┗╸╹╹   ╹ "
echo "                     Version $SCRIPTVERSION"
echo "________________________________________________________"
echo "commands:"
echo "\t (e)dit  -  edit list"
echo "\t (c)lear -  clear list"
echo "\t (d)irectory  -  ($DL_DIR)"
echo "\t (re)fresh - reload list"
echo "\t (r)un  -  start downloading"
echo "\t (s)et quality - ($quali)"
echo "\t (l)imit rate - ($LIMITRATE)"
echo "\t (n)o playlist - ($noplaylist)"
echo "\t (u)pdate youtube-dl - ($version)"
echo "\t (q)uit"
echo "________________________________________________________"
echo "URL List:"
[ -f "$TEMPFILE" ] && cat "$TEMPFILE" || echo "\t*Empty*"
echo "________________________________________________________"
echo ""
echo "Enter URL or Command"
}
header

while read input; do

# (q)uit
if [ $input = "q" ] || [ $input = "Q" ] || [ $input = "quit" ] || [ $input = "exit" ]; then
  exit 0

# (u)pdate youtube-dl
elif [ $input = "update" ] || [ $input = "u" ]; then
  youtube-dl -U ; header

# (c)lear
elif [ $input = "clear" ] || [ $input = "c" ]; then
  echo -n "" > "$TEMPFILE"; header

# (s)et quality
elif [ $input = "s" ] || [ $input = "set quality" ]; then
  echo "Set Quality: 0 -> off ; 1 -> best ; 2 -> low ; 3 -> manual"
  read ans
  case $ans in
    0) quali=""; QUALITY_OPT="";;
    1) quali="best"; QUALITY_OPT="-f best";;
    2) quali="18"; QUALITY_OPT="-f 18";;
    3) echo "enter value"; read quali ; QUALITY_OPT="-f $quali";;
  esac
  header

# (d)irectory
elif [ $input = "d" ] || [ $input = "directory" ]; then
  echo "Set Directory"
  read NEW_DL_DIR
  if [ ! -z $NEW_DL_DIR ] && [ -d $NEW_DL_DIR ]; then
    DL_DIR=$NEW_DL_DIR
  elif [ -z $NEW_DL_DIR ] ; then
    header
  else
    header; echo "Error: $NEW_DL_DIR is not a directory \n Enter URL or Command"
  fi

# (n)o playlist
elif [ $input = "n" ] || [ $input = "no playlist" ]; then
  if [ "$noplaylist" = "on" ] ; then
    noplaylist="off"; PLAYLIST_OPT=""
  else
    noplaylist="on"; PLAYLIST_OPT="--no-playlist"
  fi
  header

# (e)dit
elif [ $input = "e" ] || [ $input = "edit" ]; then
  header; $EDITOR "$TEMPFILE"

# (l)imit rate
elif [ $input = "l" ] || [ $input = "limit rate" ]; then
  echo "Enter maximum download rate in bytes per second (e.g. 50K or 4.2M)"
  read LIMITRATE
  lastbyte=$(echo $LIMITRATE | cut -b $(echo -n $LIMITRATE | wc -c))
  [ "$lastbyte" = "m" ] && LIMITRATE=$(echo $LIMITRATE | sed s'/m/M/') && lastbyte="M"
  [ "$lastbyte" = "k" ] && LIMITRATE=$(echo $LIMITRATE | sed s'/k/K/') && lastbyte="K"
  [ ! "$lastbyte" = "K" ] || [ ! "$lastbyte" = "M" ] && LIMITRATE="$(echo "$LIMITRATE" | sed 's/$/K/')"
  LIMIT_OPT="--limit-rate $LIMITRATE"
  [ -z "$LIMITRATE" ] || [ "$LIMITRATE" = "0" ] || [ "$LIMITRATE" = "off" ] || [ "$LIMITRATE" = "OFF" ] && LIMIT_OPT=""
  header

# (r)un
elif [ $input = "r" ] || [ $input = "run" ]; then
  cd "$DL_DIR"
  header; echo "Downloading"
  MAX_COUNTER="$(wc -l "$TEMPFILE" | cut -d ' ' -f 1)"
  COUNTER="0"
  for VID in $(cat "$TEMPFILE"); do
    COUNTER="$((COUNTER + 1))"
    echo "________________________________________________________"
    echo " $COUNTER / $MAX_COUNTER   $VID"
    youtube-dl $VID ${QUALITY_OPT} ${PLAYLIST_OPT} ${LIMIT_OPT}
    echo ""
    [ "$COUNTER" = "$MAX_COUNTER" ] && exit 0
  done

# (re)fresh
elif [ $input = "re" ] || [ $input = "refresh" ]; then
  header

elif [ -z $input  ]; then
  header

else
  echo "$input" >> "$TEMPFILE" ; header
fi
done

exit 0

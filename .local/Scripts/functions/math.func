# mathematical constants
pi() { echo "3.1415926535897932"; }
e() { echo "2.7182818284590452"; }
phi() { echo "1.6180339887498948"; }
tau() { echo "6.2831853071795864"; }

# scan for , and make it work
comma2dot() {
commafix="0" && [ $(echo "$INPUT" | grep ',' ) ] && commafix="1" && OUTPUT="$(echo $INPUT | tr ',' '.')"
}
dot2comma() {
commafix="1" && [ ! -z "$(echo $INPUT | grep '.' )" ] && OUTPUT="$(echo $INPUT | tr '.' ',')"
}


# addition and substraction of multipe inputs
calculate() {
VALUE="0"
while [ "$#" -gt 0 ]; do
  INPUT="$1"; comma2dot; [ "$commafix" = "1" ] && INPUT="$OUTPUT"
  VALUE=$(echo "$VALUE + $INPUT" | bc ); shift
done; echo "$VALUE"
}

# sinus
sin() { echo "scale=5;s($1*0.017453293)" | bc -l; }

# cosinus
cos() { echo "scale=5;c($1*0.017453293)" | bc -l; }

# tangenz
tan() { echo "scale=5;s($1*0.017453293)/c($1*0.017453293)" | bc -l; }

# cotangenz
cotan() { echo "scale=5;c($1*0.017453293)/s($1*0.017453293)" | bc -l; }

# secant
sec() { echo "scale=5;1/c($1*0.017453293)" | bc -l; }

# cosecant
cosec() { echo "scale=5;1/s($1*0.017453293)" | bc -l; }

# arc sin
asin()
{
    if (( $(echo "$1 == 1" | bc -l) )); then
       echo "90"
    elif (( $(echo "$1 < 1" | bc -l) )); then
       echo "scale=3;a(sqrt((1/(1-($1^2)))-1))/0.017453293" | bc -l
    elif (( $(echo "$1 > 1" | bc -l) )); then
       echo "error"
    fi
}

# arc cos
acos()
{
    if (( $(echo "$1 == 0" | bc -l) )); then
       echo "90"
    elif (( $(echo "$1 <= 1" | bc -l) )); then
       echo "scale=3;a(sqrt((1/($1^2))-1))/0.017453293" | bc -l
    elif (( $(echo "$1 > 1" | bc -l) )); then
       echo "error"
    fi
}

# arc tan
atan() { echo "scale=3;a($1)/0.017453293" | bc -l; }

# arc cotan
acot() { echo "scale=5;a(1/$1)/0.017453293" | bc -l; }

# arc secant
asec() { echo "scale=5;a(sqrt(($1^2)-1))/0.017453293" | bc -l; }

# arc cosecant
acsc() { echo "scale=5;a(1/(sqrt($1^2)-1))/0.017453293" | bc -l; }


miles2meters() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=3;$VALUE*1609.344" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE miles = $result meters")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/miles/mile/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

meters2miles() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=3;$VALUE/1609.344" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE meters = $result miles")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/miles/mile/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

miles2kilometers() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=3;$VALUE*1.609344" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE miles = $result kilometers")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/miles/mile/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

kilometers2miles() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=3;$VALUE*0.6214" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE kilometers = $result miles")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/miles/mile/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}


inch2centimeter() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=2;$VALUE*2.54" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE inches = $result centimeters")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/inches/inch/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/centimeters/centimeter/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

centimeter2inch() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=2;$VALUE*0.3937" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE centimeters = $result inches")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/centimeters/centimeter/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/inches/inch/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

inch2meter() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=4;$VALUE*0.0254" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE inches = $result meters")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/inches/inch/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

meter2inch() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=2;$VALUE*39.37" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE meters = $result inches")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/inches/inch/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

nauticalmiles2meters() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=2;$VALUE*1852" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE nautical miles = $result meters")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/miles/mile/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}

meters2nauticalmiles() {
	VALUE="$1"
	INPUT="$VALUE"; comma2dot; [ "$commafix" = "1" ] && VALUE="$OUTPUT"
	result=$(echo "scale=5;$VALUE/1852" | bc -l)
	[ "$(echo $result | cut -b 1)" = "." ] && result=$(echo "$result" | sed s'/^/0/')
	answer=$(echo "$VALUE meters = $result nautical miles")
	(( $1 <= 1 )) && answer=$(echo "$answer" | sed s'/meters/meter/')
	(( 1 >= $result )) && answer=$(echo "$answer" | sed s'/miles/mile/')
	commafix="1" && INPUT="$answer" && dot2comma && answer="$OUTPUT"
	echo "$answer"
}



#! /bin/sh
df -T | tail -n +2 | awk '{printf "%s\n%s\n%s\n%s\n%s\n%s\n", $1,$7, $2, $3, $4, $6}' |\
yad --title="Disk space usage" --width=800 --height=450 --image=drive-harddisk --text="Disk space usage" \
    --list --no-selection --column="Device" --column="Mountpoint" --column="Type" \
    --column="Total:sz" --column="Free:sz" --column="Usage:bar" &
exit 0

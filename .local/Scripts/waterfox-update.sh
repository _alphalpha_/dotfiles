#!/bin/sh
# script to update waterfox
 
url=$(curl --silent "https://www.waterfox.net/download/" \
 | sed s'/"//g' \
 | xargs -n1 \
 | grep https \
 | grep 'linux-x86_64' \
 | grep waterfox-classic \
 | cut -d '>' -f 1 \
 | cut -d '=' -f 2)
[ -z "$url" ] && exit 1

version=$(~/.local/opt/waterfox-classic/waterfox-bin -v \
 | sed s'/Waterfox //' | sed s'/Classic //')
[ -z "$version" ] && exit 1

[ "$(echo $url | grep $version)" ] && STATUS="1" || STATUS="0"

[ "$STATUS" = "1" ] \
 && echo "Waterfox is up to date. ($version)" \
 && exit 0

[ "$STATUS" = "0" ] \
 && echo "New version of Waterfox available." \
 && extention=$(echo "$url" | sed s'/linux-x86_64./ /' | cut -d ' ' -f 2) \
 && current_dir=$(pwd) \
 && cd ~/.local/opt \
 && wget -c "$url" -O "waterfox_latest.$extention" \
 && rm -rd "waterfox-classic" \
 && /usr/local/bin/extract.sh "waterfox_latest.$extention" \
 && rm "waterfox_latest.$extention" \
 && cd "$current_dir" \
 && echo "Update complete." \
 && exit 0

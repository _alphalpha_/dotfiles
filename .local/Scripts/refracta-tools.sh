#!/bin/sh
SCRIPT_VERSION="2021.08.21"
#  , __       _                               ______         _
# /|/  \     | |                             (_) |          | |
#  |___/  _  | |  ,_    __,   __ _|_  __,        | __   __  | |  ,
#  | \   |/  |/  /  |  /  |  /    |  /  |----- _ |/  \_/  \_|/  / \_
#  |  \_/|__/|__/   |_/\_/|_/\___/|_/\_/|_/   (_/ \__/ \__/ |__/ \/
#            |\
#            |/  								Michael's Edition

# Based on refractainstaller 9.5.3 by fsmithred@gmail.com which is based on refractainstaller-8.0.3 by Dean Linkous
# and also based on refractasnapshot-10.2.5 by fsmithred@gmail.com which is based on refractasnapshot-8.0.4 by Dean Linkous
# with ideas borrowed from dzsnapshot-gui.sh by David Hare, which was based on an earlier version of refractasnapshot.
# Portions may be copyright fsmithred@gmail.com and/or Dean Linkous and/or David Hare and/or others.
# UEFI code adapted from similar scripts by Colin Watson and Patrick J. Volkerding
# The code inside the create_new_usb_grub function is adapted from https://mbusb.aguslr.com/
# Licence: GPL-3
# This is free software with no warrantees. Use at your own risk!!

#####################################################################################################################
#    ______            _____          _____           __  _
#   / ____/___  ____  / __(_)___ _   / ___/___  _____/ /_(_)___  ____
#  / /   / __ \/ __ \/ /_/ / __ `/   \__ \/ _ \/ ___/ __/ / __ \/ __ \
# / /___/ /_/ / / / / __/ / /_/ /   ___/ /  __/ /__/ /_/ / /_/ / / / /
# \____/\____/_/ /_/_/ /_/\__, /   /____/\___/\___/\__/_/\____/_/ /_/
#                        /____/

### INSTALLER CONFIG:
HOSTNAME="";				ROOT_PART=""; 		ROOT_FS_TYPE="ext4"; 	ROOT_ENCRYPT=""
SEPERATE_BOOT="yes";		BOOT_PART=""; 		BOOT_FS_TYPE="ext2";
SEPERATE_HOME="no" ;		HOME_PART=""; 		HOME_FS_TYPE="ext4";	HOME_ENCRYPT=""
SEPERATE_SWAP="swapfile";	SWAP_PART="";		SWAPFILE="/.swapfile"
BOOT_METHOD=""
BOOT_LOADER=""
USER_NAME=""
DISABLE_AUTODESK="yes"
DISABLE_IPV6="yes"
USE_UUID="yes"
USE_UUID_IN_EXTLINUX="yes"
BLACKLIST_PCSPKR_IN_EXTLINUX="yes"
#use_existing_swap="no"
additional_partitions="no"
#no_format="yes"
home_boot_excludes="/usr/lib/refractainstaller/home_boot_exclude.list"

# SWAPFILE_SIZE can be 256MB 512MB 1G 2G 4G 8G 16G
SWAPFILE_SIZE="256MB"
# SWAPFILE_COUNT will be overwritten when SWAPFILE_SIZE is triggered in the menu
SWAPFILE_COUNT="262144"
#DEBUG="yes"

##### Don't mess with these unless you know what you're doing.
# This comments out the line in /etc/pmount.allow which allows a user to mount all fixed drives. (/dev/sd[a-z][0-9]*)
# Change it to "yes" or leave it blank to retain this. (not recommended)
pmount_fixed="no"

# SSH Settings
# If ssh_pass="yes", then PasswordAuthentication will be set to "yes"
# If ssh_pass="no", then PasswordAuthentication will be set to "no"
# In either of the above cases, if PermitRootLogin was set to "yes",
# it will be changed to "prohibit-password" (meaning with auth keys only)
# If ssh_pass is null or set to anything other than "yes" or "no", then
# /etc/ssh/sshd_config will not be altered.
ssh_pass="yes"

# Location of grub-efi packages. If grub-efi is not installed, you can
# install the packages in the chroot to install the efi bootloader.

# SORT OUT i386 vs. amd64 packages (for future use, maybe)
#grub_efi_pkg="/lib/live/mount/medium/pool/DEBIAN/main/g/grub2/grub-efi-amd64_*.deb"			# grub-efi*.deb should work
#grub_efi_bin_pkg="/lib/live/mount/medium/pool/DEBIAN/main/g/grub2/grub-efi-amd64-bin_*.deb"	# and also gets ia32
#grub_pc_pkg="/lib/live/mount/medium/pool/DEBIAN/main/g/grub2/grub-pc-bin*.deb   # grub-pc*.deb should work
#./pool/DEBIAN/main/g/grub2/grub-pc_2.02~beta2-22+deb8u1_amd64.deb

# Directory that holds grub packages (in case wrong grub is in the live system).
# Leave it commented if the packages are in / (root of the live filesystem.)
#grub_package_dir=""

# UEFI ID
# grub-efi will use some magic to choose a name for the bootloader
# directory found in boot/efi/EFI/, such as debian, devuan or refracta.
# If you want to specify a name, put it here by uncommenting the line
# and replacing newinstall with the new name.
# Don't make a null variable here! (Or let me know what it does.)
# Default is commented out.
#efi_name_opt="--bootloader-id=newinstall"

tempdir="$(mktemp -d /tmp/work_temp.XXXX)"
rsync_excludes="$tempdir/installer_exclude_list.txt"
snapshot_excludes="$tempdir/snapshot_exclude.list"

### SNAPSHOT CONFIG:
limit_cpu="no"
limit="90"
#[ $(nproc) -gt 1 ] && limit=$(( $limit * $(nproc) ))

snapshot_dir="/home/snapshot"
work_dir="/home/snapshot/work"
efi_work="/home/snapshot/efi-files"
ERROR_LOG_INSTALL="/refractainstaller_$(date +%d-%m-%Y_%H-%M).log"
ERROR_LOG_SNAPSHOT="$snapshot_dir/refractasnapshot_$(date +%d-%m-%Y_%H-%M).log"

make_efi="yes"
force_efi="no"
save_work="yes"

kernel_image="/vmlinuz"
initrd_image="/initrd.img"

stamp="datetime"
snapshot_basename="snapshot"
volid="liveiso"
make_sha256sum="yes"
make_pkglist="yes"
make_isohybrid="yes"
WAIT_OPT="no"
# rsync delete options (or any others you want to add). Use only one option per variable!
# This is only for copy_filesystem() and only if $save_work is "yes"
rsync_option1="--delete-before"
rsync_option2=" --delete-excluded"
rsync_option3=""

#SQUASHFS_COMPRESSION="gzip"
#SQUASHFS_COMPRESSION="xz"
SQUASHFS_COMPRESSION="xz-smaller"

####################################
# Turn stuff on and off section

# Allow users to mount all fixed drives with pmount for live iso.
# Refractainstaller removes this upon installation. (Default is "yes")
pmount_fixed="yes"

update_mlocate="yes"
clear_geany="yes"

# Allow password login to ssh for users (not root).
# If ssh_pass="yes", then PasswordAuthentication will be set to "yes"
# If ssh_pass="no", then PasswordAuthentication will be set to "no"
# In either of the above cases, if PermitRootLogin was set to "yes",
# it will be changed to "prohibit-password" (meaning with auth keys only)
# If ssh_pass is null or set to anything other than "yes" or "no", then
# /etc/ssh/sshd_config will not be altered.
ssh_pass="yes"

########################################################
# Custom boot menu and help files section.

# If you're running refractasnapshot on some linux distribution other
# than Refracta, You might also want to edit or remove some of the help
# files in the isolinux directory. (f1.txt, f2.txt...)
# If you want those changes to persist between runs, you should create
# a custom iso/isolinux directory, and set iso_dir (below) to point
# to that directory.

# If the primary user's name is not "user", then live-boot needs to see
# the user's name in the boot command. In that case, the script will
# automatically add the correct option. If you set a user name here, it
# will override that process. Use this if you want to log into the live
# media as someone other than the primary user (i.e. any user whose
# uid:gid are not 1000:1000.)
# Under most circumstances, leave this blank or commented out.
#username=""

# Change to "yes" if you want to be able to view or edit the boot menu
# or any other config files before the final image is made.
# NOTE: For SolusOS and possibly others, boot entries should contain "union=unionfs" in place of "union=aufs".
# NOTE: For anything later than jessie, union=aufs should be removed or possibly replaced with union=overlay
edit_boot_menu="no"

# You can change iso_dir if you want to use customized files for the
# boot menu and boot help pages on the live-cd.
# NOTE:
# If you're using custom versions (different from what's installed
# on your system) of isolinux.bin and vesamenu.c32, you'll need to edit
# or comment out the rsync commands in the script that copy these two
# files from your system to the work directory. To find the lines, see
# /usr/bin/refractasnapshot or /usr/bin/refractsnapshot-gui for the
# copy_isolinux function, around line 520 or 630, respectively.
iso_dir="/usr/lib/refractasnapshot/iso"

# Change this if you're using customized boot menu files, AND your
# menu file is other than the default, live.cfg, AND you set
# $edit_boot_menu to "yes".
boot_menu="live.cfg"

# Uncomment this to add boot help files specific to the Refracta distribution.
# Otherwise, generic help files, mostly empty, will be used. If you want
# to use your own customized files, see iso_dir settings above.
#refracta_boot_help="yes"

# Prepare the initrd to support encrypted volumes. Uncomment this
# if you plan to use the snapshot on a live usb with an encrypted
# persistent volume. This will edit /etc/cryptsetup-initramfs/conf-hook
# to set CRYPTSETUP=y
#initrd_crypt="yes"

# Uncomment to include your network configuration in the snapshot.
# This will preserve your /etc/network/interfaces and any saved wireless
# configurations. This works for NetworkManager, simple-netaid/netman
# and wicd.
# It will also add "ip=frommedia" to the boot command, so that the saved
# configuration will be used.
# Default is commented; interfaces file in $work_dir/myfs gets replaced
# and only contains the loopback interface.
#
# NOTE!!! If you're using some other network manager, and you don't want
# your configs to be copied, you need to add the appropriate files to
# the excludes list.
#netconfig_opt="ip=frommedia"

# Uncomment to use old or new style interface names.
# Use net.ifnames=0 to force old interface names with udev. (eth0)
# Use net.ifnames=1 to force new interface names with eudev. (enp0s1)
#ifnames_opt="net.ifnames=0"

# DEPRECATED:
# This patch is no longer needed. Instead, the script will create some
# files in /dev to help with booting. If you leave this variable set
# to "yes" the script will check for previous application of the patch
# and give you the chance to edit the file manually. If you don't remove
# the lines that were added by the patch, nothing bad will happen.
#
# Debian Jessie systems without systemd and with util-linux-2.25 will
# create an unbootable iso. The workaround is to add a few lines to
# /usr/share/initramfs-tools/init and then rebuild the initrd.
# If this option is set to "yes" then the script will check for systemd
# and for the version of util-linux. If needed, the script will apply
# the patch and rebuild the initrd.
#
# Warning: If you also need to run the nocrypt.sh script because you're
# creating a snapshot from a system INSTALLED ON AN ENCRYPTED PARTITION,
# you need to run nocrypt.sh after letting this patch run. (Hint: you
# can abort the snapshot run at the Disk Space Report, run nocrypt,
# then make your snapshot.)
#
# Default is "no" or commented out.
#patch_init_nosystemd="yes"

# Check if user is root
[ "$(id -u)" -eq "0" ] || { printf "\\n\\tYou need to be root :)\\n"; exit 1;}
SUDOER=$(printenv | grep 'SUDO_USER' | cut -d '=' -f 2)

#Check if X Server is running
if [ "$DISPLAY" ] && [ "$(xset -q)" ]; then
  X_STATUS="UP"
else
  X_STATUS="DOWN"
fi

# Set text editor
#if [ "$X_STATUS" = "UP" ] && [ -f /usr/bin/featherpad ]; then
#  TEXT_EDITOR="/usr/bin/featherpad"
#else
  TEXT_EDITOR="/bin/nano"
#fi

MENU_BOTTOM_TEXT="Refracta Installer - Michaels Edition $SCRIPT_VERSION"
MENU_BOTTOM_OPTIONS=' [Enter: select]  [Q: back]'
SEPERATOR='+--------------------------------------------------------+'
START_MENU_TEXT='

      ____       ____                __       
     / __ \___  / __/________ ______/ /_____ _
    / /_/ / _ \/ /_/ ___/ __ `/ ___/ __/ __ `/
   / _, _/  __/ __/ /  / /_/ / /__/ /_/ /_/ / 
  /_/ |_|\___/_/ /_/   \__,_/\___/\__/\__,_/  
           ______            __    
          /_  __/___  ____  / /____
           / / / __ \/ __ \/ / ___/
          / / / /_/ / /_/ / /__  / 
         /_/  \____/\____/_/____/  
                 Version: PLACE_HOLDER

          > Install to disk<s:INSTALL>
          > Make new ISO<s:SNAPSHOT>
          > Multiboot USB<s:MULTI>
          > Help<s:SHOW_HELP>
          < Exit<s>
'
START_MENU_TEXT=$(echo "$START_MENU_TEXT" | sed s"/PLACE_HOLDER/$SCRIPT_VERSION/")

HELP_TEXT='
	valid options:
	-h, --help	show this help text
	-v, --version	display the version information
	-d, --debug	debug mode
	-i, --install	install from live system to disk
	-s, --snapshot	generate a iso file from running system
	-m, --multi	open the multiboot usb menu
'

check_exit() {
printf "\\n\\tOops, something went wrong\\n\\tSee %s for details.\\n" "$ERROR_LOG"
[ "$ACTIVE_SWAP" ] && swapoff "$ACTIVE_SWAP"
exit 1
}

check_internet()  {
if [ "$(ping 'deb.devuan.org' -n -c 1 | cut -d , -f 2 | awk '/received/ {print $1}')" = "1" ]; then
  INTERNET_STATUS="CONNECTED"
else
  INTERNET_STATUS="NOT CONNECTED"
  printf "no connection to the internet, press enter to start Ceni\\n"; read -r "x" && /usr/sbin/Ceni
fi
}

check_mounts() {
MOUNTS="$(awk '$1 ~ "^/dev/" {print $1,$2}' /proc/mounts)"
FSTAB="$(awk '$1 !~ "#" {print $1,$2}' /etc/fstab)"
CHECK=""
for device in $MOUNTS; do
  if ! echo "$FSTAB" | grep -q "$device"; then CHECK="$CHECK $(echo "$device" | awk '{print $1}')"; fi
done
if [ "$CHECK" ]; then
 while [ "$MESSAGE" != "OK" ]; do
  MESSAGE=$(iselect "" "Looks like something is mounted that is not specified in /etc/fstab"\
	      "" "$CHECK" "" "" "> unmount<s:UNMOUNT>" "> edit fstab<s:EDIT>" "> continue<s:OK>" -p 8)
  case $MESSAGE in
   EDIT)	nano /etc/fstab;;
   UNMOUNT) for x in $CHECK; do umount "$x"; done;;
  esac
 done
fi
}

network_options() {
if [ "$SCRIPT_STATUS_CENI" = "OK" ]; then
  EXTRA_MENU_CENI="launch Ceni network configuration<s:LAUNCH_CENI>"
else
  EXTRA_MENU_CENI="launch Ceni network configuration (Ceni is not installed)"
fi
INTERNET_WARNING="not connected to the internet...
...$EXTRA_MENU_CENI
...Refresh<s:REFRESH>
...go back<s:BACK>
"
[ "$INTERNET_STATUS" != "CONNECTED" ] && INTERNET_MENU=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" ""  "" "$INTERNET_WARNING")

case "$INTERNET_MENU" in
  LAUNCH_CENI) /usr/sbin/Ceni;;
  REFRESH) check_internet;;
  BACK) ;;
esac
}

list_editors() {
  EDITOR_SEARCH_LIST="nano vi vim joe gedit mousepad featherpad leafpad juffed pluma medit geany mcedit"
  EDITOR_LIST=""
  for x in $(printf "%s" "$EDITOR_SEARCH_LIST"); do
  #[ -f "$(type "$x" | cut -d ' ' -f 3)" ] && EDITOR_LIST="$EDITOR_LIST \\n $(type "$x" | cut -d ' ' -f 3) <s:$(type "$x" | cut -d ' ' -f 3)>"; done
  if [ "$(whereis "$x" | cut -d ':' -f 2)" ]; then EDITOR_LIST="$EDITOR_LIST $x<s>"; fi
done
  EDITOR_LIST="$(echo "$EDITOR_LIST" | xargs -n 1)"
#  EDITOR_LIST=$(printf "%s" "$EDITOR_LIST" | tail -n +2)
}

check_name_status() {
NEW_NAME_STATUS=""
NEW_NAME_LENGHT="$(printf "%s" "$NEW_NAME" | wc -c)"
#remove spaces
if echo "$NEW_NAME" | grep -q ' '; then NEW_NAME=$(echo "$NEW_NAME" | tr -d '[:blank:]'); fi
#make sure there are only alphanumeric characters and '-' or '_' except at the beginning or end
if [ -z "$(printf "%s" "$NEW_NAME" | tr -d _- | tr -d '[:alnum:]')" ] && \
   ! printf "%s" "$NEW_NAME" | cut -b 1,"$NEW_NAME_LENGHT" | grep -e '[-_]' && \
   [ "$NEW_NAME_LENGHT" -ge 1 ] && [ "$NEW_NAME_LENGHT" -lt 63 ]; then
  NEW_NAME_STATUS="OK"
else
  NEW_NAME_STATUS="Not OK"
fi
}

check_name_case() {
#convert uppercase to lowercase
if echo "$NEW_NAME" | grep -q '[[:upper:]]'; then NEW_NAME=$(printf "%s" "$NEW_NAME" | tr '[:upper:]' '[:lower:]'); fi
}

check_name_forbidden() {
# make sure username is not a group name
for x in $(grep -v ':1000:' /etc/group | cut -d ':' -f 1); do
  [ "$NEW_NAME" = "$x" ] && NEW_NAME_STATUS="Not OK"
done
}

check_filepath() {
NEW_PATH_STATUS=""
PATH_NOT_EMPTY=""
PATH_SPACES=""
PATH_CHARS=""
PATH_DOUBLE_SLASH=""
PATH_DOUBLE_DOT=""
PATH_END_NO_DOT=""
PATH_END_NO_SLASH=""
PATH_LENGHT_CHECK=""
NEW_PATH_LENGTH="$(printf "%s" "$NEW_PATH" | wc -c)"
# check if there is input
if [ "$NEW_PATH" ]; then
  PATH_NOT_EMPTY="OK"
else
  PATH_NOT_EMPTY="Error"
fi
# check if there are spaces
if echo "$NEW_PATH" | grep -q ' '; then
  PATH_SPACES="ERROR"
else
  PATH_SPACES="OK"
fi
# check if path only contains alphanumerical characters and '.' or '/'
if [ "$(echo "$NEW_PATH" | tr -d /. | tr -d '[:alnum:]')" ]; then
  PATH_CHARS="ERROR"
else
  PATH_CHARS="OK"
fi
# make sure there is no '//'
if echo "$NEW_PATH" | grep -qF '//'; then
  PATH_DOUBLE_SLASH="ERROR"
else
  PATH_DOUBLE_SLASH="OK"
fi
# make sure there is no '..'
if echo "$NEW_PATH" | grep -qF '..'; then
  PATH_DOUBLE_DOT="ERROR"
else
  PATH_DOUBLE_DOT="OK"
fi
# make sure last byte is not '.'
if echo "$NEW_PATH" | cut -b "$NEW_PATH_LENGTH" | grep -qF '.'; then
  PATH_END_NO_DOT="ERROR"
else
  PATH_END_NO_DOT="OK"
fi
# make sure last byte is not '/'
if echo "$NEW_PATH" | cut -b "$NEW_PATH_LENGTH" | grep -qF '/'; then
  PATH_END_NO_SLASH="ERROR"
else
  PATH_END_NO_SLASH="OK"
fi
# check length
if [ "$(printf "%s" "$NEW_PATH" | tr -d /.  | wc -c)" -ge 1 ] && [ "$NEW_PATH_LENGTH" -le 63 ]; then
  PATH_LENGHT_CHECK="OK"
else
  PATH_LENGHT_CHECK="ERROR"
fi
# check if path starts with '/' and remove if it is (beginning slash will be added automatically)
[ "$(printf "%s" "$NEW_PATH" | cut -b 1)" = "/" ] && NEW_PATH=$(printf "%s" "$NEW_PATH" | cut -b 2-)

if [ "$PATH_NOT_EMPTY" = "OK" ] && \
   [ "$PATH_SPACES" = "OK" ] && \
   [ "$PATH_CHARS" = "OK" ] && \
   [ "$PATH_DOUBLE_SLASH" = "OK" ] && \
   [ "$PATH_DOUBLE_DOT" = "OK" ] && \
   [ "$PATH_END_NO_DOT" = "OK" ] && \
   [ "$PATH_END_NO_SLASH" = "OK" ] && \
   [ "$PATH_LENGHT_CHECK" = "OK" ]; then
     NEW_PATH_STATUS="OK"
elif [ "$PATH_CHARS" = "ERROR" ] || \
     [ "$PATH_SPACES" = "ERROR" ] || \
     [ "$PATH_DOUBLE_SLASH" = "ERROR" ] || \
     [ "$PATH_DOUBLE_DOT" = "ERROR" ] || \
     [ "$PATH_END_NO_DOT" = "ERROR" ] || \
     [ "$PATH_END_NO_SLASH" = "ERROR" ]; then
     clear; printf "\\n\\tError, invalid character detected. Try again.\\n%s" "$NEW_PATH_MSG"
elif [ "$PATH_LENGHT_CHECK" = "ERROR" ]; then
     clear; printf "\\nLenght error. Try again.\\n%s" "$NEW_PATH_MSG\\n"
else
     NEW_PATH_STATUS=""
fi
}

select_usb() {
if ! lsblk -o NAME,TRAN,SUBSYSTEMS -d | grep -q 'usb'; then echo "No USB Devices found!"; exit 1; fi
TARGET_DEVICE=""
if [ -z "$TARGET_DEVICE" ]; then
  device_list=""
  for usb_device in $(lsblk -dpo NAME,TRAN | awk '/usb/ {print $1}'); do
    device_list="$device_list
 $(lsblk -dnpo NAME,MODEL,SIZE $usb_device) <s:$usb_device>"
  done
  TITLE="SELECT YOUR USB DEVICE   ${WARNING}"
  TARGET_DEVICE=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" "$TITLE" "$device_list" -p 4)
fi
if [ -b "$TARGET_DEVICE" ] && [ "$(lsblk "$TARGET_DEVICE" -l -o MOUNTPOINT -n)" ]; then
TARGET_MOUNTED=$(iselect "" "$TARGET_DEVICE is currently mounted" "unmount now<s:unmount_target>" "exit<s:exit>" -p3)
  case $TARGET_MOUNTED in
	unmount_target) for device in $(df | grep "$TARGET_DEVICE" | awk '{print $6}'); do umount "$device"; done;;
	exit) exit;;
  esac
fi
clear
}

generate_grub_template() {
[ "$DISABLE_IPV6" = "yes" ] && ipv6_opt='ipv6.disable=1' || ipv6_opt=""
KEYBOARD_LAYOUT="$(grep 'XKBLAYOUT' /etc/default/keyboard | cut -d\" -f 2)"
if [ -n "$KEYBOARD_LAYOUT" ]; then
  KBD_OPT="keyboard-layouts=$KEYBOARD_LAYOUT"
fi
LACALE_LANG="$(cut -d "=" -f 2 /etc/default/locale)"
if [ -n "$LOCALE_LANG" ]; then
  LOCALE_OPT="locales=$LOCALE_LANG"
fi
grub_template="$tempdir/grub.cfg.template"
echo '
if loadfont $prefix/font.pf2 ; then
  set gfxmode=640x480
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod gfxterm
  insmod jpeg
  insmod png
  terminal_output gfxterm
fi

background_image /boot/grub/splash.png
set menu_color_normal=white/black
set menu_color_highlight=dark-gray/white
set timeout=15

menuentry "${DISTRO} (defaults) US English" {
    set gfxpayload=keep
    linux   /live/vmlinuz boot=live keyboard-layouts=us locales=en_US.UTF-8 ${ifnames_opt} ${netconfig_opt} ${username_opt} ${ipv6_opt}
    initrd  /live/initrd.img
}

menuentry "Other language (Press e to edit)" {
    set gfxpayload=keep
    linux   /live/vmlinuz boot=live ${ifnames_opt} ${netconfig_opt} ${username_opt} ${LOCALE_OPT} ${KBD_OPT} ${ipv6_opt}
    initrd  /live/initrd.img
}

submenu "Advanced options ..." {

    menuentry "${DISTRO} (to RAM)" {
	set gfxpayload=keep
	linux   /live/vmlinuz boot=live toram ${ifnames_opt} ${netconfig_opt} ${username_opt} ${ipv6_opt}
	initrd  /live/initrd.img
    }

    menuentry "${DISTRO} (failsafe)" {
	set gfxpayload=keep
	linux   /live/vmlinuz boot=live nocomponents=xinit noapm noapic nolapic nodma nosmp forcepae nomodeset vga=normal ${ifnames_opt} ${netconfig_opt} ${username_opt}
	initrd  /live/initrd.img
    }

    menuentry "Memory test" {
	kernel   /live/memtest
    }
}
' > "$grub_template"
}

################################################################################################################
# █ █ █▀▀ █▀▀ ▀█▀   ▀█▀ █▀▀ █▀▀ ▀█▀
# █ █ █▀▀ █▀▀  █     █  █▀▀ ▀▀█  █
# ▀▀▀ ▀▀▀ ▀   ▀▀▀    ▀  ▀▀▀ ▀▀▀  ▀
early_efi_test() {
# Test for efi boot
if [ -d /sys/firmware/efi ]; then
	UEFI_POSSIBLE="yes"
fi
bios_grub_dev=$(env LC_ALL=C fdisk -l | awk '/BIOS boot/ { print $1 }')
# Test for grub version
grubversion="$(dpkg -l | grep -E "ii|hi" | grep -v 'doc\|bin' | awk '$2 ~ "grub-[eglp]" { print $2}')"
}

efi_test() {
# Check for UEFI boot and EFI partition
if [ -d /sys/firmware/efi ]; then
	uefi_boot="yes"
	esp_count=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $0 }' | wc -l)

#	if [ -z "$gpt_list" ]; then
#		gpt_message="There is no disk with a gpt partition table.
#	You should exit this script and run gdisk to create one for uefi boot."
#	fi
#	if [ "$esp_count" -eq 1 ]; then
#		esp_dev=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $1 }')
#		esp_dev_message="EFI partition found at ${esp_dev}
#	If this is not on the first hard disk, something may be wrong,
#	and you should investigate the situation."
#		if ! blkid -c /dev/null -s TYPE "$esp_dev" | grep -q 'vfat' ; then
#			must_choose_esp="yes"
#			esp_dev_message="EFI partition found at ${esp_dev}
#	will need to be formatted FAT32"
#		fi
#	else
#		must_choose_esp="yes"
#		if [ "$esp_count" -eq 0 ]; then
#			esp_dev_message="There is no EFI partition. You will need to create one."
#		elif [ "$esp_count" -gt 1 ]; then
#			esp_dev_message="More than one EFI partition was detected.
#	You will need to select one. Normally, it's on the first hard disk."
#		fi
#	fi

	if ! echo "$grubversion" | grep -qe 'grub-efi'; then   # grub-efi-${grub_arch}*.deb to include grub-efi-ia32
		grub_package="grub-efi*.deb"  # make sep vars for grub-x and grub-x-bin. Maybe sep. messages. Or sep. dirs?
		grub_debs="$(ls "$grub_package_dir"/${grub_package})"    # don't quote $grub_package here.
		if [ -n "$grub_debs" ]; then
			grub_package_message="grub package(s) found in $grub_package_dir"
		fi
		grub_efi_warning="			### WARNING ###
	grub-efi is not installed.

	If you have the deb packages, you will be given a chance to install
	them into the new system.
${grub_package_message}
${grub_debs}"
	fi
#	while true; do
#		echo "
#	${grub_efi_warning}
#	${esp_dev_message}
#	${gpt_message}
#
#	DO NOT FORMAT A PRE-EXISTING EFI PARTITION!!!
#
#	1) Help
#	2) Continue
#	3) Abort the installation
#"
#		read -r "ans"
#		case "$ans" in
##			1) show_installer_help; break;;
#			1) break;;
#			2) break;;
#			3) exit 0;;
#		esac
#	done
else
	# not uefi, do bios install.
	esp_list=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $0 }')
	if [ -n "$esp_list" ]; then
		esp_dev_message="EFI partition(s) found. Do not format any EFI
 partitions if you plan to use them for uefi booting.
 ${esp_list}"
	fi
	if [ -n "$gpt_list" ] && [ -z "$bios_grub_dev" ]; then
		gpt_message="To boot a gpt disk in legacy bios you must create a
 small (>1M) unformatted partition with bios_grub flag in parted/gparted
 or EF02 in gdisk. Or boot from a disk that has dos partition table.
 More info: http://www.rodsbooks.com/gdisk/bios.html"
	fi

###### grub-pc and grub-pc-bin get installed out of order
###### Need to make $grub_package and $grub_bin_package
###### and install them in correct order.
#	if  [ "$grubversion" =~ grub-efi ] || [ -z "$grubversion" ] ; then
	if echo "$grubversion" | grep -q 'grub-efi' || [ -z "$grubversion" ]; then
		grub_package="grub-pc*.deb"
		grub_debs="$(ls "$grub_package_dir"/${grub_package})"  # don't quote $grub_package here.
		[ -n "$grub_debs" ] && grub_package_message="grub package(s) found in $grub_package_dir"
		grub_efi_warning="		### WARNING ###
	grub-pc is not installed but you booted in bios mode.
	If you have the grub-pc deb packages, you will be given a chance to
	install them into the new system.
${grub_package_message}
${grub_debs}"
	elif echo "$grubversion" | grep -q 'grub-pc'; then
		grub_efi_warning="Boot method: bios
	GRUB version: grub-pc (for bios boot)
If this is not what you want, exit and examine the situation."

		while true; do
		echo "
	${grub_efi_warning}
	${esp_dev_message}
	${gpt_message}

	1) Help
	2) Continue
	3) Abort the installation
"
		read -r "ans"
		case "$ans" in
#			1) show_installer_help; break;;
			1) break;;
			2) break;;
			3) exit 0;;
		esac
	done
	fi
fi
}

ask_format_efi () {
ASK_EFI=""
while [ -z "$ASK_EFI" ]; do
  ASK_EFI="$(iselect " WARNING:  The selected partition does not contain a FAT32 filesystem." \
					 " If you just created a new efi partition (ef00), you need to format it." "" \
					 "	DO NOT FORMAT A PRE-EXISTING EFI PARTITION!!!" "" \
					 "  > Yes, create a fat32 filesystem on $esp_dev <s:YES>" \
					 "  > No, proceed without a bootloader. <s:NO>" \
					 "  < Abort the install to investigate the situation. <s:EXIT>" -p 6)"
done
case $ASK_EFI in
	YES) printf "Formating %s to FAT32" "$esp_dev"; mkfs.vfat -F 32 "$esp_dev";;
	NO) printf "%s will not be formated" "$esp_dev";;
	EXIT) exit 0;;
esac
}

choose_esp () {
esp_info=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $0 }')
esp_dev_list=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $1 }')
esp_count=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $0 }' | wc -l)

if [ "$esp_count" -eq 0 ]; then
	esp_dev_message="There is no EFI partition.\nYou will need to create one or proceed without a bootloader."
	echo "$esp_dev_message"
##		ask_partition
else
	echo "
******************************************************
 Enter the device name for the EFI partition to use.
 (example: /dev/sda1)

$esp_info

enter device:"
	read -r "esp_dev"
	if ! echo "$esp_dev_list" | grep -q "$esp_dev"; then
		printf "Not a valid EFI partition.\\n"
		printf "Press ctrl-c to exit, or press ENTER to proceed without a bootloader.\\n"
		printf "\\nDO NOT SELECT AN EFI PARITION FOR ANOTHER PURPOSE.\\n"
		esp_dev=""
	fi

	if [ -n "$esp_dev" ]; then
		if ! blkid -c /dev/null -s TYPE "$esp_dev" | grep -q 'vfat'; then
			ask_format_efi
		fi
	fi
fi
}

recheck_efi() {
# Re-check EFI partition count after partitioning.
[ "$esp_count" -eq 1 ] && esp_count=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $0 }' | wc -l)
[ "$esp_count" -gt 1 ] && must_choose_esp="yes"
[ "$must_choose_esp" = "yes" ] && choose_esp
}

gpt_check() {
# Test for esp partition, test for gpt partition table
if [ "$BOOT_METHOD" = "UEFI" ] && [ -b "$EFI_PART" ]; then
  gpt_list="$(env LC_ALL=C fdisk -l "$(echo "$EFI_PART" | tr -d '[:digit:]')" | awk '/Disklabel type/ { print $3 }' | grep 'gpt')"
  if [ -n "$gpt_list" ]; then
    GPT_TEST="OK"
  else
    GPT_TEST="Not OK"
  fi
elif [ "$BOOT_METHOD" = "Legacy" ]; then
  if [ "$SEPERATE_BOOT" = "yes" ]; then
    gpt_list="$(env LC_ALL=C fdisk -l "$(echo "$BOOT_PART" | tr -d '[:digit:]')" | awk '/Disklabel type/ { print $3 }' | grep 'gpt')"
    bios_grub_dev=$(env LC_ALL=C fdisk -l "$(echo "$BOOT_PART" | tr -d '[:digit:]')" | awk '/BIOS boot/ { print $1 }')
  elif [ "$SEPERATE_BOOT" = "no" ]; then
    gpt_list="$(env LC_ALL=C fdisk -l "$(echo "$ROOT_PART" | tr -d '[:digit:]')" | awk '/Disklabel type/ { print $3 }' | grep 'gpt')"
  fi
else
  GPT_TEST="Not OK"
fi
}

bootflag_check() {
if [ "$SEPERATE_BOOT" = "yes" ]; then
  FLAG_DEV_PART="$BOOT_PART"
  FLAG_DEV="$(echo "$BOOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$BOOT_PART" | tr -d '[:alpha:] [=/=]')"
else
  FLAG_DEV_PART="$ROOT_PART"
  FLAG_DEV="$(echo "$ROOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$ROOT_PART" | tr -d '[:alpha:] [=/=]')"
fi

if [ "$(blkid -s PTTYPE -o value "$FLAG_DEV_PART")" = "gpt" ]; then
  if [ "$(sgdisk "$FLAG_DEV" --attributes="$FLAG_PART":show | grep 'legacy BIOS bootable')" ]; then
    BOOTFLAG_MSG=""
  else
    BOOTFLAG_MSG="Looks like you did not set a boot flag on $FLAG_DEV_PART"
  fi
else
  if [ "$(fdisk -l | grep -e ^"$FLAG_DEV_PART" | awk '{print $2}')" = '*' ]; then
    BOOTFLAG_MSG=""
  else
    BOOTFLAG_MSG="Looks like you did not set a boot flag on $FLAG_DEV_PART"
  fi
fi
}

#####################################################################################################################
#     ____           __        ____             _____           _       __
#    /  _/___  _____/ /_____ _/ / /__  _____   / ___/__________(_)___  / /_
#    / // __ \/ ___/ __/ __ `/ / / _ \/ ___/   \__ \/ ___/ ___/ / __ \/ __/
#  _/ // / / (__  ) /_/ /_/ / / /  __/ /      ___/ / /__/ /  / / /_/ / /_
# /___/_/ /_/____/\__/\__,_/_/_/\___/_/      /____/\___/_/  /_/ .___/\__/
#                                                            /_/
#
make_install() {
ERROR_LOG="$ERROR_LOG_INSTALL"
exec 2> "$ERROR_LOG"
printf "\\nStarting install script %s" "$(date +%a_%d.%m.%Y-%H:%M:%S)" >> "$ERROR_LOG"

# Check if this is a live session
if [ ! -d /lib/live/mount/medium ] && [ ! -d /lib/live/mount/findiso ] && \
   [ ! -d /lib/live/mount/fromiso ] && [ ! -d /lib/live/mount/persistence ] && \
   [ ! -d /run/live/medium ]; then
  clear; printf "\\n\\n\\t###\\tWARNING: Not running from live-CD or live-USB\\t###\\n"
  printf "\\t###\\tor unsupported configuration. Be sure you know\\t###\\n"
  printf "\\t###\\twhat you are doing. This may not work.\\t\\t###\\n"
  printf "\\n\\t\\tPress ENTER to proceed or ctrl-c to exit."; read -r "confirm";
fi

# █ █ █▀█ █▀▄ ▀█▀ █▀█ █▀▄ █   █▀▀ █▀▀
# ▀▄▀ █▀█ █▀▄  █  █▀█ █▀▄ █   █▀▀ ▀▀█
#  ▀  ▀ ▀ ▀ ▀ ▀▀▀ ▀ ▀ ▀▀  ▀▀▀ ▀▀▀ ▀▀▀
# Set default variables (if not configured above)
OLD_USER_NAME=$(awk -F: '/1000:1000/ { print $1 }' /etc/passwd)
[ "$ERROR_LOG" ] || ERROR_LOG="/var/log/refractainstaller.log"
# ROOT_PART
if [ -z "$ROOT_PART" ] && [ "$X_STATUS" = "UP" ]; then
  ROOT_PART="█"
elif [ -z "$ROOT_PART" ] && [ "$X_STATUS" = "DOWN" ]; then
  ROOT_PART="*NEED TO SET A ROOT PARTITION !"
fi
# ROOT_FS_TYPE
[ "$ROOT_FS_TYPE" ] || ROOT_FS_TYPE="ext4"
# ROOT_ENCRYPT
[ "$ROOT_ENCRYPT" ] || ROOT_ENCRYPT="no"
# SEPERATE_BOOT
[ "$SEPERATE_BOOT" ] || SEPERATE_BOOT="yes"
if [ -z "$BOOT_PART" ] && [ "$X_STATUS" = "UP" ]; then
  BOOT_PART="█"
elif [ -z "$BOOT_PART" ] && [ "$X_STATUS" = "DOWN" ]; then
  BOOT_PART="*NEED TO SET A BOOT PARTITION !"
fi
# BOOT_FS_TYPE
[ "$BOOT_FS_TYPE" ] || BOOT_FS_TYPE="ext2"
# BOOT_ENCRYPT
[ "$BOOT_ENCRYPT" ] || BOOT_ENCRYPT="no"
# SEPERATE_HOME
[ "$SEPERATE_HOME" ] || SEPERATE_HOME="no"
# HOME_PART
if [ -z "$HOME_PART" ] && [ "$X_STATUS" = "UP" ]; then
  HOME_PART="█"
elif [ -z "$HOME_PART" ] && [ "$X_STATUS" = "DOWN" ]; then
  HOME_PART="*NEED TO SET A HOME PARTITION !"
fi
# HOME_FS_TYPE
[ "$HOME_FS_TYPE" ] || HOME_FS_TYPE="ext4"
# HOME_ENCRYPT
[ "$HOME_ENCRYPT" ] || HOME_ENCRYPT="no"
# SEPERATE_SWAP
[ "$SEPERATE_SWAP" ] || SEPERATE_SWAP="swapfile"
# SWAP PART
if [ -z "$SWAP_PART" ] && [ "$X_STATUS" = "UP" ]; then
  SWAP_PART="█"
elif [ -z "$SWAP_PART" ] && [ "$X_STATUS" = "DOWN" ]; then
  SWAP_PART="*NEED TO SET A SWAP PARTITION !"
fi
# SWAP_FILE
[ "$SWAPFILE" ] || SWAPFILE="/.swapfile"
# BOOT_METHOD
[ "$BOOT_METHOD" ] || BOOT_METHOD="Legacy"
if [ -z "$EFI_PART" ] && [ "$X_STATUS" = "UP" ]; then
  EFI_PART="█"
elif [ -z "$EFI_PART" ] && [ "$X_STATUS" = "DOWN" ]; then
  EFI_PART="*NEED TO SET AN EFI PARTITION !"
fi
# BOOT_LOADER
[ -z "$BOOT_LOADER" ] && NEW_BOOTLOADER="ExtLinux" || NEW_BOOTLOADER="$BOOT_LOADER"
# HOSTNAME
[ "$HOSTNAME" ] && NEW_HOSTNAME="$HOSTNAME" || NEW_HOSTNAME=$(hostname)
# USER_NAME
[ -z "$USER_NAME" ] && NEW_USER_NAME="$OLD_USER_NAME" || NEW_USER_NAME="$USER_NAME"
# NEW_USER_HOME_DIR
[ -z "$NEW_USER_HOME_DIR" ] && NEW_USER_HOME_DIR=$(grep ':1000:' /etc/passwd | cut -d ':' -f 6)
# NEW_USER_LOGIN_SHELL
[ -z "$NEW_USER_LOGIN_SHELL" ] && NEW_USER_LOGIN_SHELL=$(grep ':1000:' /etc/passwd | cut -d ':' -f 7)
# SUDO
if [ -z "$SUDO_SELECTOR" ]; then
  SUDO_SELECTOR="PERMIT_SUDO"
  SUDO_MENU_HANDLE="Permit sudo for new user (and keep root account.)"
fi
# AUTODESK
[ "$DISABLE_AUTODESK" ] || DISABLE_AUTODESK="no"
# UUID
[ "$USE_UUID" ] || USE_UUID="no"

INSTALL_READY="false"
SCRIPT_STATUS="OK"
SCRIPT_ERROR=""
exit_code="0"
esp_count="0"

update_system_settings_menu_data(){
  TIMEZONE_DATA="$(sed 's:Etc/::g' /etc/timezone)"
  KB_LAYOUT_DATA="$(grep XKB /etc/default/keyboard | cut -d '=' -f 2 | tr -d '"' | paste -s -d '-')"
  LOCALE_DATA="$(cut -d '=' -f 2 /etc/default/locale)"
  CONSOLE_SETUP_DATA="$(grep ^CHARMAP /etc/default/console-setup) $(grep ^FONTSIZE /etc/default/console-setup)"
}
update_system_settings_menu_data

# █▀▀ █ █ █▀▀ █▀▀ █ █   █▀▄ █▀▀ █▀█ █▀▀ █▀█ █▀▄ █▀▀
# █   █▀█ █▀▀ █   █▀▄   █ █ █▀▀ █▀▀ █▀▀ █ █ █ █ ▀▀█
# ▀▀▀ ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀▀  ▀▀▀ ▀   ▀▀▀ ▀ ▀ ▀▀  ▀▀▀
check_dependencies(){
# Check whereis
if [ "$(whereis whereis | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_WHEREIS="OK"
else
  SCRIPT_STATUS_WHEREIS="Not found"
  SCRIPT_STATUS="CRITICAL"
fi
echo "SCRIPT_STATUS_WHEREIS=$SCRIPT_STATUS_WHEREIS" >> "$ERROR_LOG"

# Check dpkg
if [ "$(whereis dpkg | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_DPKG="OK"
else
  SCRIPT_STATUS_DPKG="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'dpkg'\\n"; exit 1
fi
echo "SCRIPT_STATUS_DPKG=$SCRIPT_STATUS_DPKG" >> "$ERROR_LOG"

# Check dpkg-reconfigure
if [ "$(whereis dpkg-reconfigure | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_DPKG_RECONFIGURE="OK"
else
  SCRIPT_STATUS_DPKG_RECONFIGURE="Not found"
  SCRIPT_STATUS="INCOMPLETE"
fi
echo "SCRIPT_STATUS_DPKG_RECONFIGURE=$SCRIPT_STATUS_DPKG_RECONFIGURE" >> "$ERROR_LOG"

# Check dosfstools
if [ "$(whereis mkdosfs | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_DOSFSTOOLS="OK"
else
  SCRIPT_STATUS_DOSFSTOOLS="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'mkdosfs' from the 'dosfstools' package\\n"; exit 1
fi
echo "SCRIPT_STATUS_DOSFSTOOLS=$SCRIPT_STATUS_DOSFSTOOLS" >> "$ERROR_LOG"

# Check lvm
if [ "$(whereis lvm | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_LVM="OK"
else
  SCRIPT_STATUS="INCOMPLETE"
  SCRIPT_STATUS_LVM="Not found"
fi
echo "SCRIPT_STATUS_LVM=$SCRIPT_STATUS_LVM" >> "$ERROR_LOG"

# Check iselect
if [ "$(whereis iselect | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_ISELECT="OK"
else
  SCRIPT_STATUS_ISELECT="Not Found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'iselect'\\n"; exit 1
fi
echo "SCRIPT_STATUS_ISELECT=$SCRIPT_STATUS_ISELECT" >> "$ERROR_LOG"

# Check cat
if [ "$(whereis cat | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_CAT="OK"
else
  SCRIPT_STATUS_CAT="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'cat'\\n"; exit 1
fi
echo "SCRIPT_STATUS_CAT=$SCRIPT_STATUS_CAT" >> "$ERROR_LOG"

# Check cut
if [ "$(whereis cut | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_CUT="OK"
else
  SCRIPT_STATUS_CUT="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'cut'\\n"; exit 1
fi
echo "SCRIPT_STATUS_CUT=$SCRIPT_STATUS_CUT" >> "$ERROR_LOG"

# Check find
if [ "$(whereis find | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_FIND="OK"
else
  SCRIPT_STATUS_FIND="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'find'\\n"; exit 1
fi
echo "SCRIPT_STATUS_FIND=$SCRIPT_STATUS_FIND" >> "$ERROR_LOG"

# Check grep
if [ "$(whereis grep | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_GREP="OK"
else
  SCRIPT_STATUS_GREP="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'grep'\\n"; exit 1
fi
echo "SCRIPT_STATUS_GREP=$SCRIPT_STATUS_GREP" >> "$ERROR_LOG"

# Check sed
if [ "$(whereis sed | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_SED="OK"
else
  SCRIPT_STATUS_SED="Not Found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'sed'\\n"; exit 1
fi
echo "SCRIPT_STATUS_SED=$SCRIPT_STATUS_SED" >> "$ERROR_LOG"

# Check rsync
if [ "$(whereis rsync | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_RSYNC="OK"
else
  SCRIPT_STATUS_RSYNC="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'rsync'\\n"; exit 1
fi
echo "SCRIPT_STATUS_RSYNC=$SCRIPT_STATUS_RSYNC" >> "$ERROR_LOG"

# Check tr
if [ "$(whereis tr | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_TR="OK"
else
  SCRIPT_STATUS_TR="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'tr'\\n"; exit 1
fi
echo "SCRIPT_STATUS_TR=$SCRIPT_STATUS_TR" >> "$ERROR_LOG"

# Check xargs
if [ "$(whereis xargs | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_XARGS="OK"
else
  SCRIPT_STATUS_XARGS="Not found"
  SCRIPT_STATUS="CRITICAL"
  printf "\\n\\tError: could not find 'xargs'\\n"; exit 1
fi
echo "SCRIPT_STATUS_XARGS=$SCRIPT_STATUS_XARGS" >> "$ERROR_LOG"

# Check bc
if [ "$(whereis bc | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_BC="OK"
else
  SCRIPT_STATUS_BC="Not found"
fi
echo "SCRIPT_STATUS_BC=$SCRIPT_STATUS_BC" >> "$ERROR_LOG"

# Check Ceni
if [ "$(whereis Ceni | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_CENI="OK"
else
  SCRIPT_STATUS_CENI="Not found"
fi
echo "SCRIPT_STATUS_CENI=$SCRIPT_STATUS_CENI" >> "$ERROR_LOG"

# Check if the time change script is there
if [ -x /usr/local/bin/datetime.sh ]; then
  SCRIPT_STATUS_DATETIME="OK"
else
  SCRIPT_STATUS_DATETIME="Not found"
fi

# Detect available bootloaders
check_bootloaders() {
BOOTLOADERS="   Do not install a Bootloader <s:none>"
# Check grub
if [ "$(dpkg -l | grep -E "ii|hi" | awk '/grub/ {print $2}')" ]; then
  BOOTLOADERS=$(printf "%s\\n   GRUB<s>" "$BOOTLOADERS")
  SCRIPT_STATUS_GRUB="OK"
else
  SCRIPT_STATUS_GRUB="Not Found"
fi
echo "SCRIPT_STATUS_GRUB=$SCRIPT_STATUS_GRUB" >> "$ERROR_LOG"

# Check extlinux
if [ -f /usr/bin/extlinux ]; then
  SCRIPT_STATUS_EXTLINUX="OK"
  BOOTLOADERS=$(printf "%s\\n   ExtLinux<s>" "$BOOTLOADERS")
else
  SCRIPT_STATUS_EXTLINUX="Not found"
fi
echo "SCRIPT_STATUS_EXTLINUX=$SCRIPT_STATUS_EXTLINUX" >> "$ERROR_LOG"

# Check syslinux
if [ -f /usr/bin/syslinux ]; then
  SCRIPT_STATUS_SYSLINUX="OK"
else
  SCRIPT_STATUS_SYSLINUX="Not found"
fi
echo "SCRIPT_STATUS_SYSLINUX=$SCRIPT_STATUS_SYSLINUX" >> "$ERROR_LOG"

# Check syslinux-efi
if [ -f /usr/bin/syslinux-efi ]; then
  SCRIPT_STATUS_SYSLINUX_EFI="OK"
else
  SCRIPT_STATUS_SYSLINUX_EFI="Not found"
fi
echo "SCRIPT_STATUS_SYSLINUX_EFI=$SCRIPT_STATUS_SYSLINUX_EFI" >> "$ERROR_LOG"
}
check_bootloaders

# Search crypto tools
CRYPTO_TOOLS=""
# Check TrueCrypt
if [ "$(whereis truecrypt | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_TRUECRYPT="OK"
  CRYPTO_TOOLS="$CRYPTO_TOOLS \\n\\t> TrueCrypt <s:TrueCrypt>"
else
  SCRIPT_STATUS_TRUECRYPT="Not found"
  CRYPTO_TOOLS="$CRYPTO_TOOLS\\n\\tTrueCrypt (Not available)"
fi
echo "SCRIPT_STATUS_TRUECRYPT=$SCRIPT_STATUS_TRUECRYPT" >> "$ERROR_LOG"

# Check VeraCryt
if [ "$(whereis veracrypt | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_VERACRYPT="OK"
  CRYPTO_TOOLS="$CRYPTO_TOOLS\\n\\t> VeraCrypt <s:VeraCrypt>"
else
  SCRIPT_STATUS_VERACRYPT="Not found"
  CRYPTO_TOOLS="$CRYPTO_TOOLS\\n\\tVeraCrypt (Not available)"
fi
echo "SCRIPT_STATUS_VERACRYPT=$SCRIPT_STATUS_VERACRYPT" >> "$ERROR_LOG"

# Check cryptsetup
if [ "$(whereis cryptsetup | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_CRYPTSETUP="OK"
  CRYPTO_TOOLS="$CRYPTO_TOOLS\\n\\t> LUKS <s:LUKS>"
else
  SCRIPT_STATUS_CRYPTSETUP="Not found"
  CRYPTO_TOOLS="(cryptsetup not found)"
fi
echo "SCRIPT_STATUS_CRYPTSETUP=$SCRIPT_STATUS_CRYPTSETUP" >> "$ERROR_LOG"

CRYPTO_TOOLS="$(echo "$CRYPTO_TOOLS")"

# Search partitioning tools
PARTITION_TOOLS=""
# Check fdisk
if [ "$(whereis fdisk | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_FDISK="OK"
  PARTITION_TOOLS="$PARTITION_TOOLS\\n> fdisk<s:run_fdisk>"
else
  SCRIPT_STATUS_FDISK="Not found"
fi
echo "SCRIPT_STATUS_FDISK=$SCRIPT_STATUS_FDISK" >> "$ERROR_LOG"

# Check cfdisk
if [ "$(whereis cfdisk | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_CFDISK="OK"
  PARTITION_TOOLS="$PARTITION_TOOLS\\n> cfdisk<s:run_cfdisk>"
else
  SCRIPT_STATUS_CFDISK="Not found"
fi
echo "SCRIPT_STATUS_CFDISK=$SCRIPT_STATUS_CFDISK" >> "$ERROR_LOG"

# Check gdisk
if [ "$(whereis gdisk | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_GDISK="OK"
  PARTITION_TOOLS="$PARTITION_TOOLS\\n> gdisk<s:run_gdisk>"
else
  SCRIPT_STATUS_GDISK="Not found"
fi
echo "SCRIPT_STATUS_GDISK=$SCRIPT_STATUS_GDISK" >> "$ERROR_LOG"

# Check gparted
if [ "$(whereis gparted | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_GPARTED="OK"
  PARTITION_TOOLS="$PARTITION_TOOLS\\n> gparted<s:run_gparted>"
else
  SCRIPT_STATUS_GPARTED="Not found"
fi
echo "SCRIPT_STATUS_GPARTED=$SCRIPT_STATUS_GPARTED" >> "$ERROR_LOG"

# Check parted
if [ "$(whereis parted | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_PARTED="OK"
  PARTITION_TOOLS="$PARTITION_TOOLS\\n> parted<s:run_parted>"
else
  SCRIPT_STATUS_PARTED="Not found"
fi
echo "SCRIPT_STATUS_PARTED=$SCRIPT_STATUS_PARTED" >> "$ERROR_LOG"

# Check shred
if [ "$(whereis shred | cut -d ':' -f 2)" ]; then
  SCRIPT_STATUS_SHRED="OK"
  PARTITION_TOOLS="$PARTITION_TOOLS\\n> shred<s:run_shred>"
else
  SCRIPT_STATUS_SHRED="Not found"
fi
echo "SCRIPT_STATUS_SHRED=$SCRIPT_STATUS_SHRED" >> "$ERROR_LOG"

PARTITION_TOOLS="$(echo "$PARTITION_TOOLS")"

echo "SCRIPT_STATUS=$SCRIPT_STATUS" >> "$ERROR_LOG"
[ "$SCRIPT_STATUS" = "CRITICAL" ] && { echo "ERROR!"; exit 1; }

#Adjust Menu entries if stuff is not available
[ "$SCRIPT_STATUS_LVM" = "OK" ] && LVM_SETUP="> Use LVM <s:USE_LVM>" || LVM_SETUP="> Use LVM (Not Available)"
[ "$SCRIPT_STATUS_CRYPTSETUP" = "OK" ] && USE_CRYPTO="> Use Encryption	<s:USE_ENCRYPTION>" || USE_CRYPTO="> Use Encryption (Not Available)"

if [ "$SCRIPT_STATUS_DPKG_RECONFIGURE" = "OK" ]; then
  TIMEZONE_HANDLER="> Change Timezone         = $TIMEZONE_DATA <s:CHANGE_TIMEZONE>"
  KB_LAYOUT_HANDLER="> Change Keyboard Layout  = $KB_LAYOUT_DATA <s:CHANGE_KB_LAYOUT>"
  LOCALE_HANDER="> Change Locale Language  = $LOCALE_DATA	<s:CHANGE_LOCALE>"
  CONSOLE_SETUP_HANDER="> Console-Setup (tty)      $CONSOLE_SETUP_DATA <s:CHANGE_CONSOLE_SETUP>"
else
  TIMEZONE_HANDLER="> Change Timezone (Not Available)"
  KB_LAYOUT_HANDLER="> Change Keyboard Layout (Not Available)"
  LOCALE_HANDER="> Change Locale Language (Not Available)"
  CONSOLE_SETUP_HANDER="> Change Console-Setup (Not Available)"
fi
}
check_dependencies

# █▀▀ █ █ █▀█ █▀▀ ▀█▀ ▀█▀ █▀█ █▀█ █▀▀
# █▀▀ █ █ █ █ █    █   █  █ █ █ █ ▀▀█
# ▀   ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀
menu_update() {
#Adjust Menu entries
BOOT_HANDLER_TEXT="> Seperate Boot Partition = $SEPERATE_BOOT <s:USE_BOOT_PART>"
HOME_HANDLER_TEXT="> Seperate Home Partition = $SEPERATE_HOME <s:USE_HOME_PART>"
SWAP_HANDLER_TEXT="> Seperate Swap Partition = $SEPERATE_SWAP <s:USE_SWAP_PART>"
UEFI_HANDLER_TEXT="  --> EFI Partition       = $EFI_PART <s:SET_EFI_PART>
   --> GPT Test            = $GPT_TEST
"
case "$SEPERATE_BOOT" in
  yes)	BOOT_HANDLER="$(printf "%s\\n   --> Set Boot Partition  = %s <s:SET_BOOT_PART>\\n   --> Set Boot Filesystem = %s <s:SET_BOOT_FS_TYPE>" "$BOOT_HANDLER_TEXT" "$BOOT_PART" "$BOOT_FS_TYPE")";;
  no)	BOOT_HANDLER="$BOOT_HANDLER_TEXT";;
esac
case "$SEPERATE_HOME" in
  yes)	HOME_HANDLER="$(printf "%s\\n   --> Set Home Partition  = %s <s:SET_HOME_PART>\\n   --> Set Home Filesystem = %s <s:SET_HOME_FS_TYPE>\\n   --> Encrypt Home ?      = %s %s <s:SET_HOME_ENCRYPT>" "$HOME_HANDLER_TEXT" "$HOME_PART" "$HOME_FS_TYPE" "$HOME_ENCRYPT" "$HOME_ENCRYPTION_TOOL")";;
  no)	HOME_HANDLER="$HOME_HANDLER_TEXT";;
esac
case "$SEPERATE_SWAP" in
  yes)	SWAP_HANDLER="$(printf "%s\\n   --> Swap Partition      = %s <s:SET_SWAP_PART>" "$SWAP_HANDLER_TEXT" "$SWAP_PART")";;
  no)	SWAP_HANDLER="$(printf "%s" "$SWAP_HANDLER_TEXT")";;
  swapfile)	SWAP_HANDLER=$(printf "%s\\n   --> Swapfile location   = %s <s:USE_SWAP_FILE>\\n   --> Swapfile size       = %s <s:USE_SWAP_FILE_SIZE>" "$SWAP_HANDLER_TEXT" "$SWAPFILE" "$SWAPFILE_SIZE");;
esac
case "$BOOT_METHOD" in
  UEFI) UEFI_HANDLER="$UEFI_HANDLER_TEXT ";;
  *) UEFI_HANDLER="";;
esac
#Main Menu
MAIN_MENU="
 > System Settings <s:SYSTEM_SETTINGS>
 > User Account Settings <s:USER_SETTINGS>
 > Prepare Partitions <s:PARTITION_SETUP>
 > Boot Method             = $BOOT_METHOD <s:SELECT_BOOT_METHOD>
 > Select Bootloader       = $NEW_BOOTLOADER <s:SET_BOOTLOADER>
 $UEFI_HANDLER
 > Set Root Partition      = $ROOT_PART  $ROOT_SIZE <s:SET_ROOT_PART>
   --> Set Root Filesystem = $ROOT_FS_TYPE <s:SET_ROOT_FS_TYPE>
   --> Encrypt Root ?      = $ROOT_ENCRYPT ${ROOT_ENCRYPTION_TOOL} <s:SET_ROOT_ENCRYPT>

 $BOOT_HANDLER

 $HOME_HANDLER

 $SWAP_HANDLER

> Start Installation	<s:START_INSTALL>

< Exit <s:EXIT>"
}

check_input_status() {
NEW_INPUT_STATUS=""
#make sure there are only alphanumeric characters and '-' or '_' except at the beginning or end
if [ -z "$(printf "%s" "$NEW_INPUT" | tr -d _- | tr -d '[:alnum:]')" ] && \
   ! printf "%s" "$NEW_INPUT" | cut -b 1,$(printf "%s" "$NEW_INPUT" | wc -c) | grep -qe '[-_]'; then
  NEW_INPUT_STATUS="OK"
else
  NEW_INPUT_STATUS="Not OK"
fi
}


check_size() {
if [ "$(printf "%s" "$NEW_SIZE" | cut -b "$(printf "%s" "$NEW_SIZE" | wc -c )")" = "%" ]; then
  NEW_SIZE=$(echo "$NEW_SIZE" | sed 's/^/-l /;s/$/FREE/')
#  NEW_SIZE_STATUS="OK"
else
  NEW_SIZE=$(echo "$NEW_SIZE" | sed 's/^/-L /')
#  NEW_SIZE_STATUS="Not OK"
fi
}

update_datetime() {
# update the time and check if the datetime script is found
if [ "$SCRIPT_STATUS_DATETIME" = "OK" ]; then
  DATETIME_HANDLER="> Change Time             = $(date +%H:%M:%S) <s:CHANGE_TIME>
> Change Date             = $(date +%d.%m.%Y) <s:CHANGE_DATE> "
else
  DATETIME_HANDLER="> Change Time             = $(date +%H:%M:%S) (Not Available)
> Change Date             = $(date +%d.%m.%Y) (Not Available) "
fi
}

update_user_metadata() {
GROUPS_FILE="$tempdir/new_groups.txt"
[ ! -f "$GROUPS_FILE" ] && printf "%s" "$(grep -w "$OLD_USER_NAME" /etc/group | cut -d ':' -f 1 | xargs | tr ' ' ',')" > "$GROUPS_FILE"

OTHER_GROUPS_FILE="$tempdir/other_groups.txt"
[ ! -f "$OTHER_GROUPS_FILE" ] && printf "%s" "$(grep -v -w "$OLD_USER_NAME" /etc/group | cut -d ':' -f 1 | xargs | tr ' ' ',')" > "$OTHER_GROUPS_FILE"

USER_SETTINGS_MENU="
 > Username		$NEW_USER_NAME	<s:CHANGE_USER_NAME>
 > Password		(password can be set later)
 > Groups		$(cat "$GROUPS_FILE")	<s:CHANGE_USER_GROUPS>
 > Full Name		$USER_REAL_NAME <s:CHANGE_USER_REAL_NAME>
 > Room Number		$USER_ROOM_NR <s:CHANGE_USER_ROOM_NR>
 > Phone (work)	$USER_PHONE_WORK <s:CHANGE_USER_PHONE_WORK>
 > Phone (home)	$USER_PHONE_HOME <s:CHANGE_USER_PHONE_HOME>
 > Home Directory	$NEW_USER_HOME_DIR <s:CHANGE_USER_HOME_DIR>
 > Login Shell		$NEW_USER_LOGIN_SHELL <s:CHANGE_USER_LOGIN_SHELL>"

USER_INFO=$(grep ':1000:' /etc/passwd | cut -d ':' -f 5)
[ "$NEW_USER_REAL_NAME_EVENT" ] && USER_REAL_NAME="$NEW_USER_REAL_NAME" || USER_REAL_NAME=$(echo "$USER_INFO" | cut -d ',' -f 1)
[ "$NEW_USER_ROOM_NR_EVENT" ] && USER_ROOM_NR="$NEW_USER_ROOM_NR" || USER_ROOM_NR=$(echo "$USER_INFO" | cut -d ',' -f 2)
[ "$NEW_USER_PHONE_WORK_EVENT" ] && USER_PHONE_WORK="$NEW_USER_PHONE_WORK" || USER_PHONE_WORK=$(echo "$USER_INFO" | cut -d ',' -f 3)
[ "$NEW_USER_PHONE_HOME_EVENT" ] && USER_PHONE_HOME="$NEW_USER_PHONE_HOME" || USER_PHONE_HOME=$(echo "$USER_INFO" | cut -d ',' -f 4)
[ -z "$NEW_USER_HOME_DIR" ] && NEW_USER_HOME_DIR=$(grep ':1000:' /etc/passwd | cut -d ':' -f 6)
[ -z "$NEW_USER_LOGIN_SHELL" ] && NEW_USER_LOGIN_SHELL=$(grep ':1000:' /etc/passwd | cut -d ':' -f 7)
}

select_drive() {
SELECTED_DRIVE=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" "Select a device" \
			   "$(lsblk -dno NAME,SIZE,TYPE,MODEL | grep -v 'loop' | sed 's/$/ <s>/g')" -p 3 | cut -d ' ' -f 1 )
}

select_partition() {
PARTITION_SELECTOR=""
PARTITION_HANDLER="$(lsblk -pin -lo NAME,SIZE,TYPE | grep -w 'part' | sed 's/part/<s>/g')"

if echo "$PARTITION_HANDLER" | grep -qw 'lvm'; then
  PARTITION_HANDLER=$(echo "$PARTITION_HANDLER" | grep -vw 'lvm')
  PARTITION_HANDLER_LVM=$(lvscan | grep -wi 'ACTIVE' | tr -d \' | tr -d '[]' | awk '{print $2,$3,$4}' | sed 's/$/ Logical Volume/g;s/ MiB/M/g;s/$/<s>/g')
  PARTITION_HANDLER=$(printf "%s\\n%s" "$PARTITION_HANDLER" "$PARTITION_HANDLER_LVM")
fi
[ "$ROOT_PART" ] && PARTITION_HANDLER=$(echo "$PARTITION_HANDLER" | grep -vw "$ROOT_PART")
[ "$BOOT_PART" ] && PARTITION_HANDLER=$(echo "$PARTITION_HANDLER" | grep -vw "$BOOT_PART")
[ "$HOME_PART" ] && PARTITION_HANDLER=$(echo "$PARTITION_HANDLER" | grep -vw "$HOME_PART")
[ "$SWAP_PART" ] && PARTITION_HANDLER=$(echo "$PARTITION_HANDLER" | grep -vw "$SWAP_PART")
[ "$esp_dev" ] && PARTITION_HANDLER=$(echo "$PARTITION_HANDLER" | grep -vw "$esp_dev")

PARTITION_SELECTOR=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" " " "$SEPERATOR"\
				   "  Select a partition for $PARTITION_TYPE" "$SEPERATOR" " " "$PARTITION_HANDLER"\
				   "none<s:█>" "" " < Back<s:BACK>" -p 6 | cut -d ' ' -f 1 )
[ "$PARTITION_SELECTOR" = "BACK" ] && PARTITION_SELECTOR=""
}

select_filesystem() {
  [ -z "$SELECT_FS_LINE" ] && SELECT_FS_LINE="4"
  PARTNAME=$(echo "$SELECTION" | cut -d '_' -f 2)
  SELECT_FS=$(iselect -S -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT"\
  			"" "$SEPERATOR"\
  			"  How do you want to format $PARTNAME ?"\
  			"$SEPERATOR" " "\
  			"  ext2<s>" "  ext3<s>" "  ext4<s>" "  vfat<s>" "  ntfs<s>" "  Do not format<s:NO>"\
  			-p "$SELECT_FS_LINE")
}

create_filesystem() {
case "$NEW_FS_TYPE" in
	ext2) mkfs.ext2 -F "$NEW_FS_PART";;
	ext3) mkfs.ext3 -F "$NEW_FS_PART";;
	ext4) mkfs.ext4 -F "$NEW_FS_PART";;
	vfat) mkfs.vfat -F "$NEW_FS_PART";;
	ntfs) mkfs.ntfs -F "$NEW_FS_PART";;
esac
NEW_FS_TYPE=""; NEW_FS_PART=""
}

get_swap_file_size()  {
[ -z "$SWAP_SIZE_MENU_LINE" ] && SWAP_SIZE_MENU_LINE="8"
NEW_SWAPFILE_SIZE=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" "$SEPERATOR"\
    "  How large shall the swap file be?" "$SEPERATOR" " " " > 64MB<s:64MB>" " > 128MB<s:128MB>"\
    " > 256MB<s:256MB>" " > 512MB<s:512MB>" " > 1GB<s:1GB>" " > 2GB<s:2GB>" " > 4GB<s:4GB>"\
    " > 8GB<s:8GB>" " > 16GB<s:16GB>" " > 32GB<s:32GB>" " > 64GB<s:64GB>" " < Back<s:BACK>" -p "$SWAP_SIZE_MENU_LINE")
[ "$NEW_SWAPFILE_SIZE" ] && [ "$NEW_SWAPFILE_SIZE" != "BACK" ] && [ ! "$SWAPFILE_SIZE" = "$NEW_SWAPFILE_SIZE" ] && SWAPFILE_SIZE="$NEW_SWAPFILE_SIZE"

case "$SWAPFILE_SIZE" in
	64MB)	SWAPFILE_COUNT="65536";		SWAP_SIZE_MENU_LINE="6";;
	128MB)	SWAPFILE_COUNT="131072";	SWAP_SIZE_MENU_LINE="7";;
	256MB)	SWAPFILE_COUNT="262144";	SWAP_SIZE_MENU_LINE="8";;
	512MB)	SWAPFILE_COUNT="524288";	SWAP_SIZE_MENU_LINE="9";;
	1GB)	SWAPFILE_COUNT="1048576";	SWAP_SIZE_MENU_LINE="10";;
	2GB)	SWAPFILE_COUNT="2097152";	SWAP_SIZE_MENU_LINE="11";;
	4GB)	SWAPFILE_COUNT="4194304";	SWAP_SIZE_MENU_LINE="12";;
	8GB)	SWAPFILE_COUNT="8388608";	SWAP_SIZE_MENU_LINE="13";;
	16GB)	SWAPFILE_COUNT="16777216";	SWAP_SIZE_MENU_LINE="14";;
	32GB)	SWAPFILE_COUNT="33554432";	SWAP_SIZE_MENU_LINE="15";;
	64GB)	SWAPFILE_COUNT="67108864";	SWAP_SIZE_MENU_LINE="16";;
	*)      return;;
esac
}

ask_encryption() {
  ASK_ENCTRYPTION_VALUE=""
  PARTNAME=$(printf "%s" "$SELECTION" | cut -d '_' -f 2)
  NEW_ASK_ENCTRYPTION_VALUE=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
  							" " "$SEPERATOR" "  Select Encrytion Tool for $PARTNAME" "$SEPERATOR" "$CRYPTO_TOOLS" \
  							"       > Do not encrypt $PARTNAME<s:none>" -p 6 )
  if [ "$NEW_ASK_ENCTRYPTION_VALUE" ] && [ ! "$ASK_ENCTRYPTION_VALUE" = "$NEW_ASK_ENCTRYPTION_VALUE" ]; then
    ASK_ENCTRYPTION_VALUE="$NEW_ASK_ENCTRYPTION_VALUE"
  fi
}

auto_set_bootflag() {
if [ "$SEPERATE_BOOT" = "yes" ]; then
  FLAG_DEV="$(echo "$BOOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$BOOT_PART" | tr -d '[:alpha:] [=/=]')"
else
  FLAG_DEV="$(echo "$ROOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$ROOT_PART" | tr -d '[:alpha:] [=/=]')"
fi
# MBR or GPT
if [ "$(blkid -s PTTYPE -o value "$FLAG_DEV")" = "gpt" ]; then
  sgdisk "$FLAG_DEV" --attributes="$FLAG_PART":set:2
else
  echo "set $FLAG_PART boot on" | parted "$FLAG_DEV"
fi
}

check_menu_line_efi() {
if [ "$BOOT_METHOD" = "UEFI" ]; then
   if [ "$MAIN_MENU_LINE_HANDLE" -gt "6" ]; then
     MAIN_MENU_EFI_OFFSET="2"
   else
     MAIN_MENU_EFI_OFFSET="0"
   fi
else
  MAIN_MENU_EFI_OFFSET="0"
fi
MAIN_MENU_LINE_HANDLE="$((MAIN_MENU_LINE_HANDLE + MAIN_MENU_EFI_OFFSET))"
}

check_menu_line_boot() {
check_menu_line_efi
MAIN_MENU_BOOT_OFFSET="0"
if [ "$SEPERATE_BOOT" = "yes" ]; then
  MAIN_MENU_BOOT_OFFSET="$((MAIN_MENU_BOOT_OFFSET + 2))"
else
  MAIN_MENU_BOOT_OFFSET="$((MAIN_MENU_BOOT_OFFSET - 0))"
fi
MAIN_MENU_LINE_HANDLE="$((MAIN_MENU_LINE_HANDLE + MAIN_MENU_BOOT_OFFSET))"
}

check_menu_line_home() {
check_menu_line_boot
MAIN_MENU_BOOT_OFFSET="0"
if [ "$SEPERATE_HOME" = "yes" ]; then
  MAIN_MENU_HOME_OFFSET="$((MAIN_MENU_BOOT_OFFSET + 3))"
else
  MAIN_MENU_HOME_OFFSET="$((MAIN_MENU_BOOT_OFFSET - 0))"
fi
MAIN_MENU_LINE_HANDLE="$((MAIN_MENU_LINE_HANDLE + MAIN_MENU_HOME_OFFSET))"
}

# Main Menu
menu_open() {
  [ -z "$MAIN_MENU_LINE_HANDLE" ] && MAIN_MENU_LINE_HANDLE="3"
  [ -z "$SELECTION" ] && SELECTION=$(iselect -n "$MENU_BOTTOM_TEXT" \
  								   "Refracta Installer Main Menu" "$MAIN_MENU" \
  								   -p "$MAIN_MENU_LINE_HANDLE")
  case "$SELECTION" in
# Main Menu: Exit
  EXIT)	exit 1;;

# Main Menu: Set EFI partition
  SET_EFI_PART) MAIN_MENU_LINE_HANDLE="8"
  	ESP_COUNT=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $0 }' | wc -l)
	if [ "$ESP_COUNT" -eq 0 ]; then
	  EFI_ANSWER="$(iselect "There is no EFI partition." \
					"You will need to create a small >1MB partition with esp flag and FAT32 filesystem." \
	 				"OK<s>" -p 3)"
	    case $EFI_ANSWER in
	      OK|*) SELECTION="";;
	    esac
	elif [ "$ESP_COUNT" -eq 1 ]; then
	  EFI_PART="$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $1}')"
	elif [ "$ESP_COUNT" -gt 1 ]; then
	  ESP_LIST="$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $1}' | sed 's/$/<s>/g')"
	  NEW_EFI_PART="$(iselect "Found multiple EFI partitions" \
	  				"Select the correct one:" "$ESP_LIST" -p 3)"
	  if [ -b "$NEW_EFI_PART" ]; then
	    EFI_PART="$NEW_EFI_PART"
	  fi
	fi

	if [ -b "$EFI_PART" ]; then
	  gpt_check
	  if ! blkid -c /dev/null -s TYPE "$EFI_PART" | grep -q 'vfat' ; then
		ask_format_efi
	  fi
    else
	  if [ "$X_STATUS" = "UP" ]; then
		EFI_PART="█"
	  elif [ "$X_STATUS" = "DOWN" ]; then
		EFI_PART="*NEED TO SET AN EFI PARTITION !"
	  fi
	fi

	if [ -b "$EFI_PART" ]; then
	  esp_dev="$EFI_PART"
	fi
	SELECTION="";;

# Main Menu: Select / partition
  SET_ROOT_PART)	MAIN_MENU_LINE_HANDLE="9"
    check_menu_line_efi
	PARTITION_TYPE="/"; select_partition
	[ "$PARTITION_SELECTOR" ] && ROOT_PART="$PARTITION_SELECTOR"
	SELECTION="";;

# Main Menu: Set / filesystem
  SET_ROOT_FS_TYPE)	MAIN_MENU_LINE_HANDLE="10"
    check_menu_line_efi
  	SELECT_FS_LINE="8"; select_filesystem
  	[ "$SELECT_FS" ] && [ ! "$ROOT_FS_TYPE" = "$SELECT_FS" ] && ROOT_FS_TYPE="$SELECT_FS"
  	SELECTION="";;

# Main Menu: Set / encryption
  SET_ROOT_ENCRYPT) MAIN_MENU_LINE_HANDLE="11"; ask_encryption
    check_menu_line_efi
   	case $ASK_ENCTRYPTION_VALUE in
   		none) ROOT_ENCRYPT="no";;
		*)	  ROOT_ENCRYPT="yes"; ROOT_ENCRYPTION_TOOL="$ASK_ENCTRYPTION_VALUE";;
	esac
	if [ -z "$ROOT_ENCRYPTION_TOOL" ]; then
	  ROOT_ENCRYPT="no"
	fi
	if [ "$SEPERATE_BOOT" = "no" ] && [ "$ROOT_ENCRYPT" = "yes" ]; then
	  SEPERATE_BOOT="yes"
	fi
	SELECTION="";;

# Main Menu: Seperate /boot
  USE_BOOT_PART)	MAIN_MENU_LINE_HANDLE="13"
    check_menu_line_efi
  	case $SEPERATE_BOOT in
		yes) SEPERATE_BOOT="no"; BOOT_PART="█";;
  		no)	 SEPERATE_BOOT="yes";;
	esac
	SELECTION="";;

# Main Menu: Select /boot partition
  SET_BOOT_PART)	MAIN_MENU_LINE_HANDLE="14"
    check_menu_line_efi
	PARTITION_TYPE="/boot"; select_partition
  	[ "$PARTITION_SELECTOR" ] && [ "$PARTITION_SELECTOR" != "BACK" ] && BOOT_PART="$PARTITION_SELECTOR"
  	SELECTION="";;

# Main Menu: Set /boot filesystem
  SET_BOOT_FS_TYPE) MAIN_MENU_LINE_HANDLE="15"
    check_menu_line_efi
  	SELECT_FS_LINE="6"; select_filesystem
  	[ "$SELECT_FS" ] && [ ! "$BOOT_FS_TYPE" = "$SELECT_FS" ] && BOOT_FS_TYPE="$SELECT_FS"
  	SELECTION="";;

# Main Menu: Seperate /home
  USE_HOME_PART)	MAIN_MENU_LINE_HANDLE="15"
  	check_menu_line_boot
	case $SEPERATE_HOME in
		yes)	SEPERATE_HOME="no"; HOME_PART="█";;
		 no)	SEPERATE_HOME="yes";;
	esac
	SELECTION="";;

# Main Menu: Select /home partition
  SET_HOME_PART)	MAIN_MENU_LINE_HANDLE="16"
  	PARTITION_TYPE="/home"; select_partition;
	[ "$PARTITION_SELECTOR" ] && [ "$PARTITION_SELECTOR" != "BACK" ] && HOME_PART="$PARTITION_SELECTOR"; SELECTION=""
  	check_menu_line_boot
	SELECTION="";;

# Main Menu: Set /home filesystem
  SET_HOME_FS_TYPE)	MAIN_MENU_LINE_HANDLE="17"
	check_menu_line_boot
	SELECT_FS_LINE="8"; select_filesystem; HOME_FS_TYPE="$SELECT_FS"; SELECTION="";;

# Main Menu: Set /home encryption
  SET_HOME_ENCRYPT) MAIN_MENU_LINE_HANDLE="18"
  	check_menu_line_boot; ask_encryption
  	   	case $ASK_ENCTRYPTION_VALUE in
   		none) HOME_ENCRYPT="no";;
		*)	  HOME_ENCRYPT="yes"; HOME_ENCRYPTION_TOOL="$ASK_ENCTRYPTION_VALUE";;
	esac
	if [ -z "$HOME_ENCRYPTION_TOOL" ]; then
	  HOME_ENCRYPT="no"
	fi
    SELECTION="";;

# Main Menu: Use swap?
  USE_SWAP_PART)	MAIN_MENU_LINE_HANDLE="17"
  	check_menu_line_home
	case $SEPERATE_SWAP in
	  yes)		SEPERATE_SWAP="no";;
	  no)		SEPERATE_SWAP="swapfile";;
	  swapfile) SEPERATE_SWAP="yes";;
  	esac
  	SELECTION="";;

# Main Menu: Set swap partition
  SET_SWAP_PART)	MAIN_MENU_LINE_HANDLE="18"
  	check_menu_line_home
  	PARTITION_TYPE="swap"; select_partition; SWAP_PART="$PARTITION_SELECTOR"
  	SELECTION="";;

# Main Menu: Set swap file
  USE_SWAP_FILE)	MAIN_MENU_LINE_HANDLE="18"
  	check_menu_line_home
  	NEW_PATH_MSG="Where shall the swap file be located: /"
  	clear; printf "\\n\\n%s" "$NEW_PATH_MSG"; NEW_PATH_STATUS="";
	while [ "$NEW_PATH_STATUS" != "OK" ]; do read -r "NEW_PATH"; check_filepath; done;
	[ "$NEW_PATH" ] && SWAPFILE="/$NEW_PATH"; SELECTION="";;

# Main Menu: swap file size
  USE_SWAP_FILE_SIZE) MAIN_MENU_LINE_HANDLE="19"; check_menu_line_home; get_swap_file_size; SELECTION="";;

# Main Menu: Set boot method
  SELECT_BOOT_METHOD) MAIN_MENU_LINE_HANDLE="6"
	if [ "$UEFI_POSSIBLE" = "yes" ]; then
	  case $BOOT_METHOD in
		Legacy) efi_test; BOOT_METHOD="UEFI"; NEW_BOOTLOADER="GRUB"; gpt_check; SELECTION="";;
		UEFI)	BOOT_METHOD="Legacy"; SELECTION="";;
	  esac
    else
      BOOT_METHOD="Legacy"; SELECTION=""
    fi;;

# Main Menu: Set bootloader
  SET_BOOTLOADER)	MAIN_MENU_LINE_HANDLE="7"
  	case $NEW_BOOTLOADER in
  		GRUB)	  NEW_BOOTLOADER_LINE="7";;
  		ExtLinux) NEW_BOOTLOADER_LINE="8";;
	  	none)	  NEW_BOOTLOADER_LINE="6";;
	esac
	check_bootloaders
	if [ "$BOOT_METHOD" = "UEFI" ]; then
	  BOOTLOADERS="$(printf "%s" "$BOOTLOADERS" | grep -v 'ExtLinux')"
	fi 
	NEW_BOOTLOADER=$(iselect -S -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" " " "$SEPERATOR"\
	                "  What bootloader do you prefer?" "$SEPERATOR" " " "$BOOTLOADERS" -p "$NEW_BOOTLOADER_LINE" )
	SELECTION="";;

# Main Menu: Partition setup menu
  PARTITION_SETUP)	MAIN_MENU_LINE_HANDLE="5"
  	CRYPTO_MENU_LINE_OFFSET=""
	PREPARE_PARTITION=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "Prepare partitions" \
					  "$PARTITION_TOOLS" "> Use UUID in fstab?	= $USE_UUID<s:SET_UUID_OPTION>" \
					  "$USE_CRYPTO" \
					  "$LVM_SETUP" \
					  "< Back<s:BACK>" "" "" "" "$(lsblk -i | tr '`|-' ' ' )"  -p 3)
	case $PREPARE_PARTITION in

# Partition setup menu: start fdisk
	  run_fdisk)	select_drive; fdisk "/dev/$SELECTED_DRIVE";;

# Partition setup menu: start cfdisk
	  run_cfdisk)	select_drive; cfdisk "/dev/$SELECTED_DRIVE";;

# Partition setup menu: start gdisk
	  run_gdisk)	select_drive; gdisk "/dev/$SELECTED_DRIVE";;

# Partition setup menu: start gparted
	  run_gparted)	select_drive; gparted "/dev/$SELECTED_DRIVE";;

# Partition setup menu: start parted
	  run_parted)	select_drive; parted "/dev/$SELECTED_DRIVE";;

# Partition setup menu: start shred
	  run_shred)	select_drive
	  		[ "$SELECTED_DRIVE" ] && SHRED_WARNING=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" "" "" \
	  		"Do you really want to override /dev/$SELECTED_DRIVE with tons of zeroes?" \
	  		"> Yes, give me the zeroes pls<s:OK>" \
	  		"< Back<s:BACK>" -p 6)
			case $SHRED_WARNING in
				OK) echo "this can take a while..."; shred -vn 0 -z "/dev/$SELECTED_DRIVE";;
				*)	SELECTION="";;
			esac;;

# Partition setup menu: Use UUID in fstab? (and test for encrypted OS or home)
  SET_UUID_OPTION)	UUID_OPTION=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" "" "" \
  								" Would you like fstab to use the UUID to identify filesystems?" \
  								" This is useful if your drive order changes between reboots." \
  								"yes<s>" "no<s>" "< Back <s:BACK>" -p 6)
	case $UUID_OPTION in
	  yes)	USE_UUID="yes"
			if [ "$ROOT_ENCRYPT" = "yes" ] || [ "$HOME_ENCRYPT" = "yes" ]; then
			  uuid_message="-->	UUIDs will be used in crypttab
							/dev/mapper/<name> will be used in fstab."
			fi;;
	  no)		USE_UUID="no";;
	  *)		;;
	esac;;

# Partition setup menu: Encryption Menu
 USE_ENCRYPTION)	ENCRYPTION_MENU_EVENT=""
	while [ -z $ENCRYPTION_MENU_EVENT ]; do
	  if [ "$TRUECRYPT_MENU_OPEN" = "true" ]; then
  		TRUECRYPT_HANDLE=$(printf "> TrueCrypt<s:TRUECRYPT_MENU>\\n--> run truecrypt bin\\n--> Create TrueCrypt Container\\n--> Open TrueCrypt Container\\n--> Close TrueCrypt Container\\n--KEYFILE=%s\\n--> Generate Keyfile (GUI)\\n--> Generate Keyfile" "$KEYFILE")	
 	  else
  		TRUECRYPT_HANDLE="> TrueCrypt<s:TRUECRYPT_MENU>"
  	  fi

	  if [ "$VERACRYPT_MENU_OPEN" = "true" ]; then
		VERACRYPT_HANDLE=$(printf "> VeraCrypt<s:VERACRYPT_MENU>\\n--> run veracrypt bin\\n--> Create VeraCrypt Container\\n--> Open VeraCrypt Container\\n--> Close VeraCrypt Container\\n--KEYFILE=%s\\n--> Generate Keyfile (GUI)\\n--> Generate Keyfile" "$KEYFILE")
	  else
		VERACRYPT_HANDLE="> VeraCrypt<s:VERACRYPT_MENU>"
	  fi

	  if [ "$CRYPTSETUP_MENU_OPEN" = "true" ]; then
		CRYPTSETUP_HANDLE="> LUKS<s:CRYPTSETUP_MENU>\\nsub"
	  else
		CRYPTSETUP_HANDLE="> LUKS<s:CRYPTSETUP_MENU>"
	  fi

	  CRYPTO_MENU=$(printf "%s\\n%s\\n%s" "$TRUECRYPT_HANDLE" "$VERACRYPT_HANDLE" "$CRYPTSETUP_HANDLE")
	#truecrypt -t --create-keyfile /home/michael/keyfile.txt
	#truecrypt --create-keyfile /home/michael/keyfile.txt
	  ENCRYPTION_MENU_EVENT=""
	  ENCRYPTION_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
	  						"Encryption Menu" "" "$CRYPTO_MENU" \
	  						"< Back<s:BACK>" "" "" \
	  						" > Open a Truecrypt Disk<s:TC_OPEN>" \
	  						" > Open a LUKS Disk<s:LUKS_OPEN>" "" \
	  						" > Close a Truecrypt Disk<s:TC_CLOSE>" \
	  						" > Close a LUKS Disk<s:LUKS_CLOSE>" "" "" \
	  						"Active Crypto Devices:" "$(lsblk -n -o NAME,TYPE -J | grep crypt -b2 | paste -s | cut -d '"' -f 4,10,12,13,14,15,16,17,18 | sed 's/ //g;s/"//g;s/children/ - children - /g')" \
	  						-p 3)
	  DEVICE_MENU=$(lsblk -o NAME,TYPE,SIZE -J | grep name | cut -d '"' -f 4,8,12 | sed s'/"/ /g'| grep -v lvm | grep -v crypt | awk '{print $2,$1,$3}' | sed 's/disk/DISK/g;s/part/->Partition/g;s/$/<s>/g')
########## remove later
	#	if [ "$ENCRYPTION_MENU_EVENT" = "BACK" ] || [ -z "$ENCRYPTION_MENU_EVENT" ]; then
	#	  SELECTION=""
	#	fi
	  case $ENCRYPTION_MENU_EVENT in

# Encryption Menu: TrueCrypt submenu
		TRUECRYPT_MENU)	CRYPTO_MENU_LINE_NR="3"
			case $TRUECRYPT_MENU_OPEN in
				true)	TRUECRYPT_MENU_OPEN="false";;
				   *)	TRUECRYPT_MENU_OPEN="true";;
			esac
			ENCRYPTION_MENU_EVENT="";;

# Encryption Menu: VeraCrypt submenu
		VERACRYPT_MENU)	CRYPTO_MENU_LINE_NR="4"
			case $VERACRYPT_MENU_OPEN in
				true)	VERACRYPT_MENU_OPEN="false";;
			 	   *)	VERACRYPT_MENU_OPEN="true";;
			esac
			ENCRYPTION_MENU_EVENT="";;

# Encryption Menu: LUKS submenu
		CRYPTSETUP_MENU)	CRYPTO_MENU_LINE_NR="4"
			case $CRYPTSETUP_MENU_OPEN in
				true)	CRYPTSETUP_MENU_OPEN="false";;
				   *)	CRYPTSETUP_MENU_OPEN="true";;
			esac
			ENCRYPTION_MENU_EVENT="";;

# Encryption Menu: Truecrypt encrypt
		TC_ENCRYPT)	CRYPTO_MENU_LINE_NR="4"
			TC_ENCRYPT_DEVICE=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" "" \
							  "$DEVICE_MENU" "" "< Back<s:BACK>" -p 1)
			if [ "$TC_ENCRYPT_DEVICE" = "BACK" ]; then
			  SELECTION="USE_ENCRYPTION"
			else
			  ENCRYPTION_TARGET=$(echo "$TC_ENCRYPT_DEVICE" | awk '{print $2}' | sed 's:^:/dev/:g')
			  clear; printf "\\n\\tTARGET: %s\\n\\tNAME: " "$$ENCRYPTION_TARGET"
			  while [ "$NEW_NAME_STATUS" != "OK" ]; do
				read -r "NEW_NAME"; check_name_status
			  done
			  truecrypt -t -c "$ENCRYPTION_TARGET"
			  cryptsetup open "$ENCRYPTION_TARGET" --type tcrypt "$NEW_NAME"
			fi;;

# Encryption Menu: LUKS encrypt
		LUKS_ENCRYPT)	echo "TEST"; sleep 1;;

# Encryption Menu: Truecrypt open
		TC_OPEN)	echo "TEST"; sleep 1;;

# Encryption Menu: Truecrypt close
		TC_CLOSE)	echo "TEST"; sleep 1;;

# Encryption Menu: LUKS close
		LUKS_CLOSE)	echo "TEST"; sleep 1;;

# Encryption Menu: Back
		*) ENCRYPTION_MENU_EVENT="1"; SELECTION="PARTITION_SETUP";;
	 esac
	done;;

# Partition setup menu: LVM Menu
  USE_LVM)
	PV="";	PV_LIST="$(printf "PVSCAN:%s\\n%s" "$(pvscan | tail -n 1)" "$(pvscan | grep 'PV' | sed 's/^/  > /g;s/$/<s>/g' | sort)")"
	VG="";	VG_LIST="VGSCAN:"
	for VG in $(vgscan | awk '/Found/ {print $4}' | tr -d '"'); do
	  VG_MEMBERS=""
	  VG_MEMBER_PV="$(printf "%s" "$(pvscan | grep "$VG" | awk '{print $2}')")"
	  VG_MEMBER_LV="$(printf "%s" "$(lvscan | grep "$VG" | cut -d\' -f 2 | cut -d '/' -f 4 )")"
	  VG_LIST="$(printf "%s\\n  >   VG %s   PV-MEMBERS: %s   LV-MEMBERS: %s<s>" "$VG_LIST" "$VG" "$VG_MEMBER_PV" "$VG_MEMBER_LV")"
	done
	LV=""; LV_LIST="$(printf "LVSCAN:\\n%s" "$(lvscan | awk '{print $2,$3,$4,$1}' | sed 's/ACTIVE/   ACTIVE   /g;s/inactive/   INACTIVE /g;s/^/  >   LV /g;s/$/<s>/g' | tr -d "'" | sed s'/ / /g')")"
	if [ -z "$(pvs -o name --noheading | cut -d ' ' -f 3)" ]; then
	  LVM_MENU_ACTIVATE_VGCREATE="  > Create new Volume Group (Needs PV)"
	else
	  LVM_MENU_ACTIVATE_VGCREATE="  > Create new Volume Group <s:VGCREATE>"
	fi
	if [ -z "$(vgs -o name --noheadings | cut -d ' ' -f 3)" ]; then
	  LVM_MENU_ACTIVATE_LVCREATE="  > Create new Logical Volume (Needs VG)"
	else
	  LVM_MENU_ACTIVATE_LVCREATE="  > Create new Logical Volume <s:LVCREATE>"
	fi
	LVM_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" " " \
				   "  > Update <s:UPDATE>" "  < Back <s:BACK>" "" \
				   " > Use the lvm commandline interface<s:LVM>" "" "$PV_LIST" \
				   "  > Create new Physical Volume <s:PVCREATE>" "" "$VG_LIST" \
				   "$LVM_MENU_ACTIVATE_VGCREATE" ""  "$LV_LIST" \
				   "$LVM_MENU_ACTIVATE_LVCREATE" -p 4)

	case $LVM_MENU_EVENT in
# LVM Menu: Back
		  BACK)	SELECTION="";;

# LVM Menu: Update
		UPDATE)	SELECTION="USE_LVM";;

# LVM Menu: LVM CLI
		   LVM)	clear; echo "try help for help or exit to exit"; lvm 2>&1; SELECTION="USE_LVM";;

# LVM Menu: PV create
	  PVCREATE)	SELECT_PART=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
	  						"SELECT A DEVICE FOR THE PHYSICAL VOLUME" \
	  						"WARNING! THIS WILL PERMANENTLY OVERWRITE THE MBR AND CHANGE THE PARTITION ID" "" \
	  						"$(lsblk -no TYPE,KNAME,SIZE,MOUNTPOINT | grep part | grep -v / | awk '{print $2,$3}' | sort | sed 's:^:/dev/:g;s/$/<s>/g')" \
	  						"$(for x in $(lsblk -nilo NAME,TYPE | awk '/crypt/ {print $1}'); do ls /dev/mapper/"$x" | sed 's/$/<s>/g'; done)" -p 4)
		NEW_PV_PART=$(printf "%s" "$SELECT_PART" | awk '{print $1}')
		if echo "$NEW_PV_PART" | grep -q 'mmcblk'; then
		  MMC_DEV_ID=$(echo "$NEW_PV_PART" | tr 'p' ',' | tr -d '[:alpha:]' | tr -d / | cut -d ',' -f 1)
		  MMC_DEV=$(echo "$NEW_PV_PART" | awk -F'[0-9]' '{print $1}')$MMC_DEV_ID
		  MMC_PART_ID=$(echo "$NEW_PV_PART" | tr 'p' ',' | tr -d '[:alpha:]' | tr -d / | cut -d ',' -f 2)
		  PART_ID="$MMC_PART_ID"
		  TARGET_DEV="$MMC_DEV"
		else
		  PART_ID=$(echo "$NEW_PV_PART" | tr -d '[:alpha:]' | tr -d '/')
		  BLK_DEV=$(echo "$NEW_PV_PART" | awk -F'[0-9]' '{print $1}')
		  TARGET_DEV="$BLK_DEV"
		fi
	#this uses fdisk to change the partition type to lvm if needed
		if ! lsblk "$NEW_PV_PART" -no PARTTYPE | grep -w '0x8e'; then
		  TOTAL_PARTS="$(lsblk "$TARGET_DEV" -o KNAME,TYPE | grep -c part)"
		  if  [ "$TOTAL_PARTS" -ge "2" ]; then
			printf "t\\n%s\\n8e\\np\\nw\\n" "$PART_ID" | fdisk "$TARGET_DEV"
		  elif [ "$TOTAL_PARTS" = "1" ]; then
			printf "t\\n8e\\np\\nw\\n" | fdisk "$TARGET_DEV"
		  else
			echo "Error, no partitions on $TARGET_DEV"
		  fi
		fi
		pvcreate -y "$NEW_PV_PART";;

# LVM Menu: VG create
	  VGCREATE)	NEW_VG_NAME=""; NEW_NAME_STATUS=""
		while [ -z "$NEW_VG_NAME" ]; do
		  clear; printf "%s\\n\\n\\tEnter name for new volume group: " "$NEW_NAME_STATUS"
		  read -r "NEW_NAME"; check_name_status; check_name_case
		  if [ "$NEW_NAME_STATUS" = "OK" ] && [ "$NEW_NAME" != "new_name" ]; then
		    NEW_VG_NAME="$NEW_NAME"
		  else
		    NEW_VG_NAME=""
		  fi
		done
		SELECT_PV=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
				  "PVSCAN:$(pvscan | tail -n 1)" "$(pvscan | grep PV)" "" \
				  "SELECT PHYSICAL VOLUMES TO ADD TO $NEW_VG_NAME" \
				  "YOU CAN SELECT MULTIPLE LINES WITH SPACE" "" "" \
				  "$(pvscan | awk '/PV/ {print $2}' | sed 's/$/<s>/g')" -m -p 9)
		[ "$(printf "%s" "$SELECT_PV" | wc -l )" -gt 1 ] && SELECT_PV="$(echo "$SELECT_PV" | paste -s)"
		vgcreate "$NEW_VG_NAME" "$SELECT_PV";;

# LVM Menu: LV create
	  LVCREATE)	NEW_LV_NAME=""; NEW_NAME_STATUS="";
		while [ -z "$NEW_LV_NAME" ]; do
		  clear; echo "$NEW_NAME_STATUS"
		  printf "\\n\\tEnter name for new logical volume: "
		  read -r "NEW_NAME"; check_name_status; check_name_case
		  if [ "$NEW_NAME_STATUS" = "OK" ] && [ "$NEW_NAME" != "new_name" ]; then
		    NEW_LV_NAME="$NEW_NAME"
		  else
		    NEW_LV_NAME=""
		  fi
		done
		NEW_LV_SIZE=""; NEW_SIZE_STATUS=""
		while [ -z "$NEW_LV_SIZE" ]; do
		  clear; printf "%s\\n\\n\\tEnter size for new logical volume: (eg. '4G' or '100%%')" "$NEW_NAME_STATUS"
		  printf "\\n\\tName: %s \\n\\tSize: " "$NEW_LV_NAME"
		  read -r "NEW_SIZE"; check_size; NEW_LV_SIZE="$NEW_SIZE"
		done
		if [ "$(vgscan | grep Found | cut -d '"' -f 2 | wc -l)" -gt 1 ]; then
		  NEW_LV_VG=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
		  			"Select a Volumegroup for $NEW_LV_NAME" \
		  			"$(vgscan | grep 'Found' | cut -d '"' -f 2 | sed 's/$/<s>/g')" -m -p 9)
		else
		  NEW_LV_VG=$(vgscan | grep 'Found' | cut -d '"' -f 2)
		fi
		lvcreate -d -n "$NEW_LV_NAME" "$NEW_LV_SIZE" "$NEW_LV_VG"
		LV_FS_TYPE="";  LV_FS_TYPE="$(select_filesystem)"
		[ "$LV_FS_TYPE" ] && NEW_FS_TYPE="$LV_FS_TYPE" && NEW_FS_PART="/dev/mapper/$NEW_LV_VG-$NEW_LV_NAME" \
		&& create_filesystem
		if [ "$NEW_FS_TYPE" = "ext2" ] || [ "$NEW_FS_TYPE" = "ext3" ] || [ "$NEW_FS_TYPE" = "ext4" ]; then
		  tune2fs -r0 "$NEW_FS_PART"
		fi;;
	esac

# LVM Memu: pv-submenu
	if echo "$LVM_MENU_EVENT" | grep -q '> PV'; then
	  PV=$(echo "$LVM_MENU_EVENT" | awk '{print $3}')
	  PV_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
	  				"$(pvdisplay "$PV" | head -n "$(( $(pvdisplay "$PV" | wc -l) -1  ))" )" \
	  				"  Tags                $(pvs "$PV" -o pv_tags --noheadings )" "" \
	  				" > Add Tag <s:PVADDTAG>" " > Remove Tag <s:PVDELTAG>" " > Remove <s:PVREMOVE>" \
	  				" < Back <s:BACK>" -p 14)

	  case $PV_MENU_EVENT in

# LVM Memu: pv-submenu: Back
	  	BACK)	SELECTION="USE_LVM";;

# LVM Memu: pv-submenu: PV remove
	    PVREMOVE)	pvremove "$PV";;

# LVM Memu: pv-submenu: PV addtag
	  	PVADDTAG)	NEW_PV_TAG=""; NEW_NAME_STATUS=""
		while [ -z "$NEW_PV_TAG" ]; do
		  clear; printf "%s\\n\\n\\tEnter new tag :" "$NEW_NAME_STATUS"
		  read -r "NEW_NAME"; check_name_status; check_name_case
		  if [ "$NEW_NAME_STATUS" = "OK" ] && [ "$NEW_NAME" != "new_name" ]; then
		    NEW_PV_TAG="$NEW_NAME"
		  else
		    NEW_PV_TAG=""
		  fi
		done
		pvchange "$PV" --addtag "$NEW_PV_TAG";;

# LVM Memu: pv-submenu: PV deltag
	  	PVDELTAG)	PV_TAG_LIST=""
		for x in $(pvs "$PV" -o pv_tags --noheadings | sed 's/,/ /g');do
		  PV_TAG_LIST=$(printf "%s\\n%s<s>" "$PV_TAG_LIST" "$x")
		done
		PV_TAG=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$PV_TAG_LIST" " < Back <s:BACK>" -p 2)
		[ "$PV_TAG" != "BACK" ] &&  pvchange "$PV" --deltag "$PV_TAG";;
	  esac

# LVM Menu: vg-submenu
	elif echo "$LVM_MENU_EVENT" | grep -q '> VG'; then
	  VG="$(echo "$LVM_MENU_EVENT" | awk '{print $3}')"
	  VG_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
	  				"$(vgdisplay "$VG" | head -n "$(( $(vgdisplay "$VG" | wc -l) -1  ))" )" \
	  				"$(echo "$LVM_MENU_EVENT" | sed 's/LV-MEMBERS//g' | cut -d ':' -f 2 | sed 's/^/  PV-MEMBERS           /')" \
	  				"$(echo "$LVM_MENU_EVENT" | sed 's/PV-MEMBERS//g' | cut -d ':' -f 3 | sed 's/^/  LV-MEMBERS           /')" "" \
	  				" > Remove<s:VGREMOVE>" " > Extend<s:VGEXTEND>" " > Reduce<s:VGREDUCE>" \
	  				" > Rename<s:VGRENAME>" " < Back <s:BACK>" -p 28)
	  case $VG_MENU_EVENT in

# LVM Menu: vg-submenu: Back
	  	BACK)	SELECTION="USE_LVM";;

# LVM Menu: vg-submenu: VG remove
	  	VGREMOVE)	vgremove "$(echo "$LVM_MENU_EVENT" | awk '{print $3}')";;

# LVM Menu: vg-submenu: VG extend
	  	VGEXTEND)	PV_LIST=$(pvscan -n | awk '/PV/ {print $1,$2,$3,$4,$5}' | sed 's/$/<s>/g' | sort)
			VG_EXTEND_PV=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$PV_LIST" "" "< Back <s:BACK>" -p 14)
			if [ "$VG_EXTEND_PV" != "BACK" ]; then
		 	 PV=$(echo "$VG_EXTEND_PV" | awk '{print $2}')
		 	 vgextend "$VG" "$PV"
			fi;;

# LVM Menu: vg-submenu: VG reduce
	  VGREDUCE) PV_LIST=$(vgdisplay -v "$VG" | awk '/PV Name/ {print $1,$3}' | sed 's/$/<s>/g' | sort)
			VG_REDUCE_PV=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$PV_LIST" "" "< Back <s:BACK>" -p 14)
			[ "$VG_REDUCE_PV" != "BACK" ] && PV=$(echo "$VG_REDUCE_PV" | awk '{print $2}') && vgreduce "$VG" "$PV";;

# LVM Menu: vg-submenu: VG rename
	  VGRENAME)	NEW_VG_NAME=""; NEW_NAME_STATUS=""
		while [ -z "$NEW_VG_NAME" ]; do
		  clear; printf "%s\\n\\tEnter name for new volume group: " "$NEW_NAME_STATUS"
		  read -r "NEW_NAME"; check_name_status; check_name_case
		  if [ "$NEW_NAME_STATUS" = "OK" ] && [ "$NEW_NAME" != "new_name" ]; then
		    NEW_VG_NAME="$NEW_NAME"
		  else
		    NEW_VG_NAME=""
		  fi
		done
	    vgrename "$VG" "$NEW_VG_NAME";;
	esac


# LVM Menu: lv-submenu
	elif echo "$LVM_MENU_EVENT" | grep -q '> LV'; then
	  LV=$(echo "$LVM_MENU_EVENT" | awk '{print $3}')
	  LV_STATE=$(lvs -o active --noheadings "$LV" | awk '{print $1}')
	  LV_TYPE=$(lvs -o segtype --noheadings "$LV")
	  if [ "$LV_STATE" = "active" ]; then
		LV_STATE_SWITCH=" > Deactivate <s:LVDEACTIVATE>"
	  elif [ -z "$LV_STATE" ]; then
		LV_STATE_SWITCH=" > Activate <s:LVACTIVATE>"
	  fi
	  LV_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
	  				"$(lvdisplay "$LV" | head -n $(( $(lvdisplay "$LV" | wc -l) - 1 )) )" \
	  				"  LV Seg Type          $LV_TYPE" "" "$LV_STATE_SWITCH" " > Remove <s:LVREMOVE>" \
	  				" < Back <s:BACK>" -p 18)
	  case $LV_MENU_EVENT in

# LVM Menu: lv-submenu: Back
	  	BACK)	SELECTION="USE_LVM";;

# LVM Menu: lv-submenu: LV activate
		LVACTIVATE)	lvchange -a y "$LV";;

# LVM Menu: lv-submenu: LV deactivate
	 	LVDEACTIVATE)	lvchange -a n "$LV";;

# LVM Menu: lv-submenu: LV remove
		LVREMOVE)	lvremove -y "$LV";;

# LVM Menu: lv-submenu: change-segtype
		#	  elif [ "$LV_MENU_EVENT" = "LVCHANGETYPE" ]; then
		#		NEW_LV_TYPE=$(iselect -n "$MENU_BOTTOM_TEXT" -t "$RIGHT_STRING" "linear<s>" "striped<s>" "snapshot<s>" "mirror<s>" "raid<s>" "thin<s>" "cache<s>" "thin-pool<s>" "cache-pool<s>" "" "< Back <s:BACK>" -p 14)
		#		lvconvert $LV --type $NEW_LV_TYPE
	  esac
	fi;;
	*) SELECTION=""
  esac;;

# Main Menu: User Settings Menu
   USER_SETTINGS)	MAIN_MENU_LINE_HANDLE="4"
		update_user_metadata
		if [ -z "$USER_MENU_LINE_NR" ]; then USER_MENU_LINE_NR="3"; fi
		USER_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
						"User Account Settings" "$USER_SETTINGS_MENU" " < Back <s:BACK>" -p "$USER_MENU_LINE_NR")
		case $USER_MENU_EVENT in

# User Settings Menu: Change user name
		CHANGE_USER_NAME)	USER_MENU_LINE_NR="3"
			TMP_USER_NAME="$NEW_USER_NAME"; NEW_USER_NAME=""
			while [ -z "$NEW_USER_NAME" ]; do
				clear; printf "%s\\n\\n" "$NEW_NAME_STATUS"
				printf "The current username is %s.\\n" "$TMP_USER_NAME"
				printf "To change that, enter the new username here.\\n\\n"
				printf "You can use alphanumeric characters anywhere in the username,\\n"
				printf "and you can use the minus sign (-) as long as it's not at the beginning or end.\\n"
				printf "\\nNew username: "
				read -r "NEW_NAME"; check_name_status; check_name_case; check_name_forbidden
				[ "$NEW_NAME_STATUS" = "OK" ] && { NEW_USER_NAME="$NEW_NAME"; clear; } || NEW_USER_NAME=""
			done
			sed "s/$TMP_USER_NAME/$NEW_USER_NAME/" -i "$GROUPS_FILE"
			NEW_USER_HOME_DIR="/home/$NEW_USER_NAME";;

# User Settings Menu: Change password
#					CHANGE_USER_PASSWD)	USER_MENU_LINE_NR="4";	clear
#										echo -n "\n\nEnter a new password for $NEW_USER_NAME or leave blank to enter later: "
#										read "NEW_USER_PASSWD"; clear;;

# User Settings Menu: Change user groups
		CHANGE_USER_GROUPS)	USER_MENU_LINE_NR="5"
			update_user_metadata; clear
			GROUP_MENU_LIST=$(cat "$GROUPS_FILE" | xargs -n 1 -d ',' | sed 's/$/<s>/')
			OTHER_GROUP_MENU_LIST=$(cat "$OTHER_GROUPS_FILE" | xargs -n 1 -d ',' | sed 's/$/<s>/')
			##SUBMENU
			USER_GROUP_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" \
							" < Back<s:BACK>" "" \
							"## Remove $NEW_USER_NAME from following groups:" \
							"$GROUP_MENU_LIST" "" \
							"## Add $NEW_USER_NAME to other groups:" \
							"$OTHER_GROUP_MENU_LIST" -p 3)

			if [ "$USER_GROUP_EVENT" != "$BACK" ] && [ "$USER_GROUP_EVENT" != "$OLD_USER_NAME" ] && [ "$USER_GROUP_EVENT" != "$NEW_USER_NAME" ]; then
				if grep -vqw "$USER_GROUP_EVENT" "$GROUPS_FILE"; then
				  sed "s/^/$USER_GROUP_EVENT,/" -i "$GROUPS_FILE"
				  sed "s/$USER_GROUP_EVENT//"   -i "$OTHER_GROUPS_FILE"
				elif grep -qw "$USER_GROUP_EVENT" "$GROUPS_FILE"; then
				  sed "s/$USER_GROUP_EVENT//"   -i "$GROUPS_FILE"
			  	  sed "s/^/$USER_GROUP_EVENT,/" -i "$OTHER_GROUPS_FILE"
				fi
		[ "$(cut -b 1 "$GROUPS_FILE")" = "," ] && printf "%s" "$(cut -d ',' -f 2- "$GROUPS_FILE")" > "$GROUPS_FILE"
		grep -q ',,' "$GROUPS_FILE" && sed 's/,,/,/g' -i "$GROUPS_FILE"
		[ "$(cut -b 1 "$OTHER_GROUPS_FILE")" = "," ] && printf "%s" "$(cut -d ',' -f 2- "$OTHER_GROUPS_FILE")" > "$OTHER_GROUPS_FILE"
		grep -q ',,' "$OTHER_GROUPS_FILE" && sed 's/,,/,/g' -i "$OTHER_GROUPS_FILE"
 		update_user_metadata
			fi;;

# User Settings Menu: Change user real name
		CHANGE_USER_REAL_NAME)	USER_MENU_LINE_NR="6"
			NEW_USER_REAL_NAME_EVENT="1"; NEW_USER_REAL_NAME=""; NEW_INPUT_STATUS=""
			while [ "$NEW_INPUT_STATUS" != "OK" ]; do
			  clear; printf "%s" "$NEW_INPUT_STATUS"
			  printf "\\n\\n\\tEnter %s's real name: " "$NEW_USER_NAME"
			  read -r "NEW_INPUT"; check_input_status
			  [ "$NEW_INPUT_STATUS" = "OK" ] && NEW_USER_REAL_NAME="$NEW_INPUT" || NEW_USER_REAL_NAME=""
			done
			clear; update_user_metadata;;

# User Settings Menu: Change user room
		CHANGE_USER_ROOM_NR)	USER_MENU_LINE_NR="7"
			NEW_USER_ROOM_NR_EVENT="1"; NEW_USER_ROOM_NR=""; NEW_INPUT_STATUS=""
			while [ "$NEW_INPUT_STATUS" != "OK" ]; do
			  clear; printf "%s\\n\\n\\tEnter %s's room number: " "$NEW_INPUT_STATUS" "$NEW_USER_NAME"
			  read -r "NEW_INPUT";  check_input_status
			  [ "$NEW_INPUT_STATUS" = "OK" ] && NEW_USER_ROOM_NR="$NEW_INPUT" || NEW_USER_ROOM_NR=""
			done
			clear; update_user_metadata;;

# User Settings Menu: Change user phone (work)
		CHANGE_USER_PHONE_WORK)	USER_MENU_LINE_NR="8"
			NEW_USER_PHONE_WORK_EVENT="1"; NEW_USER_PHONE_WORK=""; NEW_INPUT_STATUS=""
			while [ "$NEW_INPUT_STATUS" != "OK" ]; do
			  clear; printf "%s\\n\\n\\tEnter %s's phone number (work): " "$NEW_INPUT_STATUS" "$NEW_USER_NAME"
			  read -r "NEW_INPUT"; check_input_status
			  [ "$NEW_INPUT_STATUS" = "OK" ] && NEW_USER_PHONE_WORK="$NEW_INPUT" || NEW_USER_PHONE_WORK=""
			done
			clear; update_user_metadata;;

# User Settings Menu: Change user phone (home)
		CHANGE_USER_PHONE_HOME)	USER_MENU_LINE_NR="9"
			NEW_USER_PHONE_HOME_EVENT="1"; NEW_USER_PHONE_HOME=""; NEW_INPUT_STATUS=""
			while [ "$NEW_INPUT_STATUS" != "OK" ]; do
			  clear; printf "%s\\n\\n\\tEnter %s's phone number (home): " "$NEW_INPUT_STATUS" "$NEW_USER_NAME"
			  read -r "NEW_INPUT"; check_input_status
			  [ "$NEW_INPUT_STATUS" = "OK" ] && NEW_USER_PHONE_HOME="$NEW_INPUT" || NEW_USER_PHONE_HOME=""
			done
			clear; update_user_metadata;;

# User Settings Menu: Change user home directory
		CHANGE_USER_HOME_DIR)	USER_MENU_LINE_NR="10"
			NEW_USER_HOME_DIR_EVENT="1"; NEW_PATH_STATUS=""
			while [ "$NEW_PATH_STATUS" != "OK" ]; do
			  clear; printf "%s\\n\\n\\tEnter a path for %s's home directory: " "$NEW_PATH_STATUS" "$NEW_USER_NAME"
			  read -r "NEW_PATH"; check_filepath
			  NEW_USER_HOME_DIR="$NEW_PATH"
			done
			clear; update_user_metadata;;

# User Settings Menu: Change user shell
		CHANGE_USER_LOGIN_SHELL) USER_MENU_LINE_NR="11"
			NEW_USER_LOGIN_SHELL=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
								 "$(grep -v '#\|tmux\|screen' /etc/shells | sed 's/$/<s>/g')" -p 1); update_user_metadata;;

# User Settings Menu: Back
		*) SELECTION="";;
		esac;;

# Main Menu: System Settings Menu
  SYSTEM_SETTINGS)	MAIN_MENU_LINE_HANDLE="3"
  					update_system_settings_menu_data; update_datetime; SYSTEM_SETTINGS_MENU_EVENT="";	SYSTEM_SETTINGS_MENU="
> Change Hostname         = $NEW_HOSTNAME	<s:SET_HOSTNAME>
> sudo Setup		   = $SUDO_MENU_HANDLE <s:SUDO_SETUP>
> Disable auto desktop	   = $DISABLE_AUTODESK <s:SET_AUTODESK>
$DATETIME_HANDLER
$TIMEZONE_HANDLER
$KB_LAYOUT_HANDLER
$LOCALE_HANDER
$CONSOLE_SETUP_HANDER
> Edit Mirrorlist <s:EDIT_MIRRORLIST>
> Edit iptables.conf <s:EDIT_IPTABLES>
> Edit rc.local <s:EDIT_RCLOCAL>
> Edit sysctl.conf <s:EDIT_SYSCTL>
> Reconfigure Packages <s:RECONFIGURE_PKG>
> Disable IPv6		  = $DISABLE_IPV6 <s:SET_IPV6>
< Back <s:BACK>"

	if [ -z "$SYSTEM_SETTINGS_LINE_NR" ]; then SYSTEM_SETTINGS_LINE_NR="3"; fi
	while [ -z "$SYSTEM_SETTINGS_MENU_EVENT" ]; do
		SYSTEM_SETTINGS_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
							   "System Settings" "$SYSTEM_SETTINGS_MENU" -p "$SYSTEM_SETTINGS_LINE_NR")
		case $SYSTEM_SETTINGS_MENU_EVENT in

# System Settings Menu: Set Hostname
			SET_HOSTNAME)			SYSTEM_SETTINGS_LINE_NR="3"
				OLD_HOSTNAME="$NEW_HOSTNAME"; NEW_HOSTNAME=""
				while [ -z "$NEW_HOSTNAME" ]; do
				  clear; printf "%s\\n" "$NEW_NAME_STATUS"
				  printf "\\nThe current hostname is %s.\\n" "$OLD_HOSTNAME"
				  printf "To change that, enter the new hostname here.\\n"
				  printf "To leave it unchanged, just press ENTER.\\n\\n"
				  printf "You can use alphanumeric characters anywhere in the hostname,\\n"
				  printf "and you can use the minus sign (-) as long as it's not at the beginning or end.\\n\\n"
				  printf "\\nNew hostname: "; read -r "NEW_NAME"; check_name_status; check_name_case
				  [ "$NEW_NAME_STATUS" = "OK" ] && NEW_HOSTNAME="$NEW_NAME" || NEW_HOSTNAME=""
				done
				update_system_settings_menu_data;;

# System Settings Menu: sudo setup
			  SUDO_SETUP)	SYSTEM_SETTINGS_LINE_NR="4"
			  	SUDO_SELECTOR=""
				while [ -z "$SUDO_SELECTOR" ]; do
				  SUDO_SELECTOR=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" "" \
				  				"Most live images use 'sudo' for access." \
				  				"No password is required." "" \
				  				"It is recommended to disable sudo in an installation and use 'su'" \
				  				"with a root password. Optionally you may permit sudo for the new" \
				  				"user or you may use sudo as default for the new user, with no root account." "" \
				  				"  > Disable sudo (recommended) <s:DISABLE_SUDO>" \
				  				"  > Permit sudo for new user (and keep root account.) <s:PERMIT_SUDO>" \
				  				"  > Use sudo as default for new user (and disable root account.) <s:DEFAULT_SUDO>" \
				  				"  > Use sudo only for shutdown (and keep root account.) <s:SHUTDOWN_SUDO>" -p 10)
				  case $SUDO_SELECTOR in
					  DISABLE_SUDO)	 SUDO_MENU_HANDLE="Disable sudo (recommended)"; break;;
					  PERMIT_SUDO)	 SUDO_MENU_HANDLE="Permit sudo for new user (and keep root account.)"; sudoconfig="TRUE"; break;;
					  DEFAULT_SUDO)	 SUDO_MENU_HANDLE="Use sudo as default for new user (and disable root account.)"; sudo_is_default="TRUE"; break;;
					  SHUTDOWN_SUDO) SUDO_MENU_HANDLE="Use sudo only for shutdown (and keep root account.)"; sudo_shutdown="TRUE"; break;;
					  *) break;;
				  esac
				done
				update_system_settings_menu_data;;

# System Settings Menu: Automatic desktop setup
			  SET_AUTODESK)	SYSTEM_SETTINGS_LINE_NR="5"
					case $DISABLE_AUTODESK in
						yes)	DISABLE_AUTODESK="no";;
						no)		DISABLE_AUTODESK="yes";;
					esac
					update_system_settings_menu_data;;

# System Settings Menu: Change time
			  CHANGE_TIME) SYSTEM_SETTINGS_LINE_NR="6"; /usr/local/bin/datetime.sh --time
			  update_system_settings_menu_data;;

# System Settings Menu: Change date
			  CHANGE_DATE) SYSTEM_SETTINGS_LINE_NR="7"; /usr/local/bin/datetime.sh --date
			  update_system_settings_menu_data;;

# System Settings Menu: Change timezone
			  CHANGE_TIMEZONE)	SYSTEM_SETTINGS_LINE_NR="8"; dpkg-reconfigure tzdata
			  update_system_settings_menu_data;;

# System Settings Menu: Set keyboard layout
			  CHANGE_KB_LAYOUT)	SYSTEM_SETTINGS_LINE_NR="9"; dpkg-reconfigure keyboard-configuration
			  update_system_settings_menu_data;;

# System Settings Menu: Set locale
			  CHANGE_LOCALE)	SYSTEM_SETTINGS_LINE_NR="10"; dpkg-reconfigure locales
			  update_system_settings_menu_data;;

# System Settings Menu: Console setup
			  CHANGE_CONSOLE_SETUP) SYSTEM_SETTINGS_LINE_NR="11"; dpkg-reconfigure console-setup
			  update_system_settings_menu_data;;

# System Settings Menu: Edit mirrorlist
			  EDIT_MIRRORLIST) SYSTEM_SETTINGS_LINE_NR="12"
					$TEXT_EDITOR /etc/apt/sources.list
					update_system_settings_menu_data;;

# System Settings Menu: Edit iptables.conf
			  EDIT_IPTABLES) SYSTEM_SETTINGS_LINE_NR="13"
					$TEXT_EDITOR /etc/iptables.conf
					update_system_settings_menu_data;;

# System Settings Menu: Edit rc.local
			  EDIT_RCLOCAL) SYSTEM_SETTINGS_LINE_NR="14"
					$TEXT_EDITOR /etc/rc.local
					update_system_settings_menu_data;;

# System Settings Menu: Edit sysctl.conf
			  EDIT_SYSCTL)	SYSTEM_SETTINGS_LINE_NR="15"
					$TEXT_EDITOR /etc/sysctl.conf
					update_system_settings_menu_data;;

# System Settings Menu: Reconfigure package
			  RECONFIGURE_PKG)	SYSTEM_SETTINGS_LINE_NR="16"
			  	SELECT_PKG=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "" \
			  			  "$(ls -1 /var/lib/dpkg/info/*.config | cut -d '/' -f 6 | cut -d '.' -f 1 | sed 's/^/> /g;s/$/<s>/g')" \
			  			  "< Back <s:BACK>" -p 3 | cut -b 3-)
				[ "$SELECT_PKG" = "BACK" ] || dpkg-reconfigure "$SELECT_PKG"
				update_system_settings_menu_data;;

# System Settings Menu: Disabl IPv6
			  SET_IPV6)	SYSTEM_SETTINGS_LINE_NR="17"
					case $DISABLE_IPV6 in
						yes)	DISABLE_IPV6="no";;
						no)		DISABLE_IPV6="yes";;
					esac
					update_system_settings_menu_data;;

# System Settings Menu: fallback
			  *) SYSTEM_SETTINGS_MENU_EVENT="1"; SELECTION="";;
			  esac
			done
			update_system_settings_menu_data;;

# Main Menu: Start installation
  START_INSTALL)
  					gpt_check
					if [ "$BOOT_METHOD" = "Legacy" ]; then
  					  bootflag_check
				      if [ "$BOOTFLAG_MSG" ]; then
				        ASK_BOOTFLAG="$(iselect "$BOOTFLAG_MSG" "" \
				          " > Let this Script set the bootflag now automatically<s:AUTO>" \
				          " < Let me go back and check that<s:BACK>" \
				          " > I don't care, continue with installation<s:CONT>" -p 3)"
					    case $ASK_BOOTFLAG in
					      AUTO) auto_set_bootflag;;
					      CONT) BOOTFLAG_MSG="";;
					      BACK|*) MAIN_MENU_LINE_HANDLE="5"; SELECTION="";;
					    esac
				      fi
# 					  if [ "$SEPERATE_BOOT" = "yes" ]; then
#                        if [ -n "$gpt_list" ] && [ -z "$bios_grub_dev" ]; then
#                        ASK_GPT="$(iselect " To boot a gpt disk in legacy bios you must create a" \
#                                   " small (>1M) unformatted partition with bios_grub flag in parted/gparted" \
#                                   " or EF02 in gdisk. Or boot from a disk that has dos partition table." \
#                                   " More info: http://www.rodsbooks.com/gdisk/bios.html" "" \
#                                   " > OK<s:BACK>" -p 6)"
#                        case $ASK_GPT in
#					      BACK|*) MAIN_MENU_LINE_HANDLE="5"; SELECTION="";;
#					    esac
#                        fi
# 					  elif [ "$SEPERATE_BOOT" = "no" ]; then
#                        if [ -n "$gpt_list" ]; then
#                        ASK_GPT="$(iselect " To boot a gpt disk in legacy bios you must create a" \
#                                   " small (>1M) unformatted partition with bios_grub flag in parted/gparted" \
#                                   " or EF02 in gdisk. Or boot from a disk that has dos partition table." \
#                                   " More info: http://www.rodsbooks.com/gdisk/bios.html" "" \
#                                   " > OK<s:BACK>" -p 6)"
#                        case $ASK_GPT in
#					      BACK|*) MAIN_MENU_LINE_HANDLE="5"; SELECTION="";;
#					    esac
#                        fi
#                      fi
					  early_efi_test
#					  if [ "$NEW_BOOTLOADER" = "GRUB" ] && [ -b "$BOOT_PART" ] && [ "$(env LC_ALL=C fdisk -l "$(echo "$BOOT_PART" | tr -d '[:digit:]')" | awk '/Disklabel type/ { print $3 }' | grep 'gpt')" ] && [ ! -b "$bios_grub_dev" ]; then
#					    ASK_GRUBDEV="$(iselect " To boot a gpt disk in legacy bios you must create" \
#					      " a small (>1M) unformatted partition with bios_grub flag in parted/gparted" \
#						  " or EF02 in gdisk. Or boot from a disk that has dos partition table."
#						  " More info: http://www.rodsbooks.com/gdisk/bios.html" "" \
#				          " < Let me go back and check that<s:BACK>" -p 6)"
#					    case $ASK_GRUBDEV in
#					      BACK|*) MAIN_MENU_LINE_HANDLE="5"; SELECTION="";;
#					    esac
#					  fi
					elif [ "$BOOT_METHOD" = "UEFI" ]; then
					  if [ -z "$gpt_list" ]; then
					    ASK_GPT="$(iselect " Could not find a gpt partition table!" \
					      " UEFI boot requires that your device has a gpt partition table" \
						  " Use parted/gparted or gdisk to create it"
				          "" " < OK <s:BACK>" -p 6)"
					    case $ASK_GPT in
					      BACK|*) MAIN_MENU_LINE_HANDLE="5"; SELECTION="";;
					    esac
					  fi
					  if [ -n "$esp_dev" ]; then
						if ! blkid -c /dev/null -s TYPE "$esp_dev" | grep -q 'vfat'; then
						  ask_format_efi
						fi
					  fi
					fi
  					if [ ! -b "$ROOT_PART" ]; then
                      clear; printf "\\n\\n\\tError, Root partition not found!"; sleep 2; SELECTION=""
                    elif [ "$BOOT_METHOD" = "UEFI" ] && [ ! -b "$EFI_PART" ]; then
                      clear; printf "\\n\\n\\tError, EFI partition not found!"; sleep 2; SELECTION=""
                    elif [ "$BOOT_METHOD" = "UEFI" ] && [ ! "$GPT_TEST" = "OK" ]; then
                      clear; printf "\\n\\n\\tError, no GPT partition table detected!"; sleep 2; SELECTION=""
                    elif [ "$SEPERATE_BOOT" = "yes" ] && [ ! -b "$BOOT_PART" ]; then
                      clear; printf "\\n\\n\\tError, Boot partition not found!"; sleep 2; SELECTION=""
                    elif [ "$SEPERATE_HOME" = "yes" ] && [ ! -b "$HOME_PART" ]; then
                      clear; printf "\\n\\n\\tError, Home partition not found!"; sleep 2; SELECTION=""
                    else
                      INSTALL_READY="true"
                    fi;;
esac
}

# Fix root's path (for Buster/Beowulf and later)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin


# █▀█ █▀█ █▀▀ █▀█   █▄█ █▀▀ █▀█ █ █
# █ █ █▀▀ █▀▀ █ █   █ █ █▀▀ █ █ █ █
# ▀▀▀ ▀   ▀▀▀ ▀ ▀   ▀ ▀ ▀▀▀ ▀ ▀ ▀▀▀
while [ "$INSTALL_READY" = "false" ]; do
  menu_update
  menu_open
done

################################################################################################################
install_dev="$ROOT_PART"

#pre-install

# comment all locales in locale.gen
#sed -i '/^[a-z][a-z]_/s/^/# /' /etc/locale.gen

##### find the current active locale and uncomment it
#CURRENT_LOCALE=$(grep -v ^\# /etc/default/locale | cut -d= -f2)
#if [ -n "$CURRENT_LOCALE" ]; then
#	sed -i "s/# $CURRENT_LOCALE/$CURRENT_LOCALE/" /etc/locale.gen
#fi

# Partition a disk
#list_disks () {
#	clear
#	echo
#	env LC_ALL=C fdisk -l | egrep "^Disk|^/dev"
#	sleep 5
#}


#fi

if [ "$NEW_BOOTLOADER" = "GRUB" ]; then
# Select location for bootloader.
# If location is entered but does not exist, then exit with error.
  select_grub_dev () {
  clear
  printf "\\nWhere would you like the GRUB bootloader to be installed?\\n"
  printf "\\n\\t(probably a drive, like /dev/sda)\\n\\n"
  printf "If you don't want to install the bootloader, leave this blank.\\n"
  read -r "grub_dev"
  if [ -n "$grub_dev" ]; then
	if ! [ -b "$grub_dev" ]; then
	  printf "%s is not a block device." "$grub_dev"; exit 1
	fi
  fi

# If you enter a partition instead of a drive for grub_dev...  ##### NOT FOR NVME DISKS (or >9 partitions)
  if [ ${grub_dev: -1} = [1-9] ]; then							#### (This way should work for nvme)
	grub_partition="$grub_dev"
  else
	partition_table=$(env LC_ALL=C fdisk -l "$grub_dev" | awk '/Disklabel type/ { print $3 }')
  fi

  if [ "$partition_table" = "gpt" ] && [ -z "$bios_grub_dev" ]; then
	bios_boot_warning="bootloader will fail without BIOS boot partition."
	grub_dev=""
	printf "\\nWARNING: There is no BIOS boot partition."
	printf "\\nPress ENTER to proceed without bootloader or ctrl-c to quit."
	read -r "confirm"
  fi
  }

  if [ "$SEPERATE_BOOT" = "yes" ] && [ -b "$BOOT_PART" ]; then
    grub_dev=$(echo "$BOOT_PART" | tr -d '[:digit:]')
  elif [ "$SEPERATE_BOOT" = "no" ] && [ -b "$ROOT_PART" ]; then
    grub_dev=$(echo "$ROOT_PART" | tr -d '[:digit:]')
  fi

  if [ "$uefi_boot" = "yes" ]; then
	grub_dev="efi"
	if [ -z "$esp_dev" ]; then
	  grub_dev=""
    fi
  elif [ -z "$grub_package" ]; then  # grub_package is null if correct grub is installed.
	if [ -z "$grub_dev" ]; then
	  select_grub_dev
	fi
  fi
fi

# Enter device for /boot partition or skip. If one is entered, test it.
boot_dev="$BOOT_PART"
	if [ "$boot_dev" = "$esp_dev" ]; then
		printf " EFI partition and /boot partition cannot be the same.
 You may continue and install without a separate boot partition,
 or you can hit ctrl-c to exit\\n"
		boot_dev=""
		printf "Press ENTER when you're ready to continue\\n"
		read -r "confirm"
	fi
###############################################################################################################
# Choose filesystem type for /boot if it exists.
if [ "$SEPERATE_BOOT" = "yes" ] && [ -n "$boot_dev" ]; then
	if [ "$no_format" = "yes" ]; then
		fs_type_boot=$(blkid -s TYPE "$boot_dev" | awk -F"\"" '{ print $2 }')
	fi
fi

# Choose filesystem type for OS.
#choose_fs_os () {
[ "$SEPERATE_BOOT" = "no" ] && [ "$ROOT_ENCRYPT" = "yes" ] && BOOT_ENCRYPT="yes"
if [ "$no_format" = "yes" ]; then
	ROOT_FS_TYPE=$(blkid -s TYPE "$install_dev" | awk -F"\"" '{ print $2 }')
#else
#	choose_fs_os
fi

# Enter device for /home partition or skip. If one is entered, test it.
home_dev="$HOME_PART"
	if [ "$home_dev" = "$esp_dev" ]; then
		printf " EFI partition and /home partition cannot be the same.
 You may continue and install without a separate home partition,
 or you can hit ctrl-c to exit\\n"
		home_dev=
		printf "Press ENTER when you're ready to continue\\n"
		read -r "confirm"
	fi

# Show available swap partitions and choose one.
swap_info=$(/sbin/blkid | awk '/TYPE="swap"/ {print "\n" $0 }')
swap_device_list=$(/sbin/blkid -s TYPE | awk -F: '/swap/ {print "\n" $1 }')

if [ -n "$swap_device_list" ]; then
	use_existing_swap="yes"
	case $SEPERATE_SWAP in
		swapfile)swap_dev="$SWAPFILE";;
		no)		swap_dev="";;
		yes)	[ -b "$swap_dev" ] || use_existing_swap="no"
				swap_dev="$SWAP_PART";;
	esac
fi

# check if root partition is set
if [ -z "$ROOT_PART" ] || [ "$ROOT_PART" = "█" ]; then printf "Warning, root device not set\\n"; exit; fi

# check if boot partition is set
if [ "$SEPERATE_BOOT" = "yes" ]; then
  if [ -z "$BOOT_PART" ] || [ "$BOOT_PART" = "█" ]; then printf "Warning, boot device not set\\n"; exit; fi
fi

# check if home partition is set
if [ "$SEPERATE_HOME" = "yes" ]; then
  if [ -z "$HOME_PART" ] || [ "$HOME_PART" = "█" ]; then printf "Warning, home device not set\\n"; exit; fi
fi

# check if swap is set
if [ "$SEPERATE_SWAP" = "yes" ]; then
  if [ -z "$SWAP_PART" ] || [ "$SWAP_PART" = "█" ]; then printf "Warning, swap device not set\\n"; exit; fi
elif [ "$SEPERATE_SWAP" = "swapfile" ]; then
  if [ -z "$SWAPFILE" ]; then printf "Warning, swap file not set\\n"; exit; fi
fi

# █▀▀ █ █ █▄█ █▄█ █▀█ █▀▄ █ █
# ▀▀█ █ █ █ █ █ █ █▀█ █▀▄  █
# ▀▀▀ ▀▀▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀  ▀
# Show a summary of what will be done
if [ "$NEW_BOOTLOADER" = "GRUB" ]; then
  if [ "$grub_dev" = "efi" ]; then
	grub_dev_message="--> EFI bootloader can be installed."
  elif [ -z "$grub_dev" ]; then
	grub_dev_message="--> Bootloader will not be installed."
  else
	grub_dev_message="--> Bootloader will be installed in $grub_dev"
  fi
elif [ "$NEW_BOOTLOADER" = "ExtLinux" ]; then
	grub_dev_message="--> ExtLinux Bootloader will be installed"
elif [ "$NEW_BOOTLOADER" = "none" ]; then
	grub_dev_message="--> Bootloader will not be installed."
fi

if [ "$ROOT_ENCRYPT" = "yes" ]; then
	os_enc_message=", and will be encrypted."
fi

if [ "$SEPERATE_HOME" = "no" ]; then
	home_dev_message="--> /home will not be on a separate partition."
else
	home_dev_message="--> /home will be installed on $home_dev and formatted as $HOME_FS_TYPE"
fi

if [ "$SEPERATE_HOME" = "no" ] && [ "$HOME_ENCRYPT" = "yes" ]; then
	home_enc_message=", and will be encrypted."
fi

if [ "$SEPERATE_BOOT" = "yes" ] && [ -n "$boot_dev" ] && [ "$BOOT_PART" ]; then
  if [ "$BOOT_FS_TYPE" = "NO" ]; then
    boot_dev_message="--> /boot will be installed on $boot_dev and not formatted"
  else
	boot_dev_message="--> /boot will be installed on $boot_dev and formatted as $BOOT_FS_TYPE."
  fi
fi

if [ "$SEPERATE_SWAP" = "swapfile" ] && [ "$SWAPFILE" ]; then
	swap_message="--> swap will be $SWAPFILE with $SWAPFILE_SIZE"
elif [ "$SEPERATE_SWAP" = "yes" ]; then
	swap_message="--> swap partition will be $SWAP_PART"
elif [ "$SEPERATE_SWAP" = "no" ]; then
	swap_message="--> no swap partition or file"
fi

while true; do
  clear; echo "\\n\\n
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
    ╻┏┓╻┏━┓╺┳╸┏━┓╻  ╻  ┏━┓╺┳╸╻┏━┓┏┓╻     ┏━┓╻ ╻┏┳┓┏┳┓┏━┓┏━┓╻ ╻
    ┃┃┗┫┗━┓ ┃ ┣━┫┃  ┃  ┣━┫ ┃ ┃┃ ┃┃┗┫     ┗━┓┃ ┃┃┃┃┃┃┃┣━┫┣┳┛┗┳┛
    ╹╹ ╹┗━┛ ╹ ╹ ╹┗━╸┗━╸╹ ╹ ╹ ╹┗━┛╹ ╹     ┗━┛┗━┛╹ ╹╹ ╹╹ ╹╹┗╸ ╹
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
 WARNING: This is your last chance to exit before any changes are made.

 $grub_dev_message
 --> Operating system will be installed on $install_dev
     and formatted as $ROOT_FS_TYPE $os_enc_message
 $boot_dev_message
 $home_dev_message $home_enc_message
 ${uuid_message}
 ${swap_message}

 --> Hostname: $NEW_HOSTNAME
 --> Username: $NEW_USER_NAME

 Proceed with the installation?
   1) Yes
   2) No, abort the installation.

"
	read -r "ans"
	case $ans in
		[1Yy]*) break;;
		[2Nn]*) exit 0;;
	esac
done

# █▀▄ █▀▀ █▀▀ ▀█▀ █▀█   ▀█▀ █▀█ █▀▀ ▀█▀ █▀█ █   █   █▀█ ▀█▀ ▀█▀ █▀█ █▀█
# █▀▄ █▀▀ █ █  █  █ █    █  █ █ ▀▀█  █  █▀█ █   █   █▀█  █   █  █ █ █ █
# ▀▀  ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀

# █ █ █▀█ █▄█ █▀█ █ █ █▀█ ▀█▀
# █ █ █ █ █ █ █ █ █ █ █ █  █
# ▀▀▀ ▀ ▀ ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀
# Unmount or close anything that might need unmounting or closing
cleanup () {
printf "\\nCleaning up...\\n" >> "$ERROR_LOG" 2>&1
[ "$ACTIVE_SWAP" ] && swapoff "$ACTIVE_SWAP" && ACTIVE_SWAP=""
grep -qw '/target/proc' /proc/mounts && umount /target/proc
grep -qw '/target/dev' /proc/mounts && umount /target/dev
grep -qw '/target/sys' /proc/mounts && umount /target/sys
grep -qw '/target_boot/efi' /proc/mounts && umount /target_boot/efi
grep -qw '/target_boot' /proc/mounts && umount -l /target_boot
grep -qw '/target_home' /proc/mounts && umount -l /target_home
grep -qw '/target' /proc/mounts && umount -l /target
grep -qw "$install_dev" /proc/mounts && umount -l "$install_dev"
grep -qw '/dev/mapper/root_fs' /proc/mounts && umount /dev/mapper/root_fs
if [ -h /dev/mapper/root_fs ]; then
  case $ROOT_ENCRYPTION_TOOL in
	LUKS)					cryptsetup luksClose /dev/mapper/root_fs;;
	TrueCrypt|VeraCrypt)	cryptsetup tcryptClose /dev/mapper/root_fs;;
  esac
fi
if [ "$SEPERATE_HOME" = "yes" ] && [ -b "$home_dev" ]; then
  grep -qw "$home_dev" /proc/mounts && umount "$home_dev"
  grep -qw '/dev/mapper/home_fs' /proc/mounts && umount /dev/mapper/home_fs
  if [ -h /dev/mapper/home_fs ]; then
    case $HOME_ENCRYPTION_TOOL in
	  LUKS)					cryptsetup luksClose /dev/mapper/home_fs;;
	  TrueCrypt|VeraCrypt)	cryptsetup tcryptClose /dev/mapper/home_fs;;
    esac
  fi
fi
if [ "$SEPERATE_BOOT" = "yes" ] && [ -b "$boot_dev" ]; then
  grep -qw "$boot_dev" /proc/mounts && umount "$boot_dev"
fi
if [ -d /target ]; then
  if [ "$(find /target -maxdepth 0 -type d -empty)" ]; then
    rmdir /target
  else
    printf "could not remove /target, because it is not empty\\n" >> "$ERROR_LOG" 2>&1
  fi
fi

if [ -d /target_home ]; then
  if [ "$(find /target_home -maxdepth 0 -type d -empty)" ]; then
    rmdir /target_home
  else
    printf "could not remove /target_home, because it is not empty\\n" >> "$ERROR_LOG" 2>&1
  fi
fi

if [ -d /target_boot ]; then
  if [ "$(find /target_boot -maxdepth 0 -type d -empty)" ]; then
    rmdir /target_boot
  else
    printf "could not remove /target_boot, because it is not empty\\n" >> "$ERROR_LOG" 2>&1
  fi
fi
}
cleanup

# /

# make mount point, format, adjust reserve and mount
# install_dev must maintain the device name for cryptsetup
# install_part will be either device name or /dev/mapper name as needed.
printf  "Preparing $s...\\n" "$install_dev"
if [ ! -d /target ]; then
  mkdir /target || check_exit
fi
if [ "$ROOT_ENCRYPT" = "yes" ]; then
  case "$ROOT_ENCRYPTION_TOOL" in
	LUKS)
      printf "You will need to create a passphrase.\\n"
      cryptsetup luksFormat "$install_dev" || check_exit
      printf "Encrypted partition created. Opening it...\\n"
      cryptsetup luksOpen "$install_dev" root_fs || check_exit
      install_part="/dev/mapper/root_fs"
      crypttype_root="luks";;
	TrueCrypt)
	  truecrypt -t -c "$ROOT_PART"
	  cryptsetup open "$ROOT_PART" --type tcrypt root_fs
	  install_part="/dev/mapper/root_fs"
	  crypttype_root="tcrypt";;
	VeraCrypt)
	  veracrypt -t -c "$ROOT_PART"
	  cryptsetup open "$ROOT_PART" --type tcrypt root_fs
	  install_part="/dev/mapper/root_fs"
	  crypttype_root="tcrypt";;
  esac
else
    install_part="$install_dev"
fi

[ "$no_format" != "yes" ] && NEW_FS_TYPE="$ROOT_FS_TYPE" && NEW_FS_PART="$install_part" && create_filesystem
if [ "$NEW_FS_TYPE" = "ext2" ] || [ "$NEW_FS_TYPE" = "ext3" ] || [ "$NEW_FS_TYPE" = "ext4" ]; then
  tune2fs -r10000 "$NEW_FS_PART"
fi
#	tune2fs -r 10000 "$install_part" || check_exit
#	mke2fs -t $ROOT_FS_TYPE "$install_part" || check_exit

if [ "$(lsblk -no MOUNTPOINT "$install_part")" != "/target" ]; then
  mount "$install_part" /target || check_exit
fi
# /home

# make mount point for separate home if needed
# and add /home/* to the excludes list if it's not already there
if [ "$SEPERATE_HOME" = "yes" ]; then
	printf "\\n\\tPreparing %s..." "$home_dev"
	if [ ! -d /target_home ]; then
	  mkdir /target_home || check_exit
	fi
	if [ "$HOME_ENCRYPT" = "yes" ]; then
	  case "$HOME_ENCRYPTION_TOOL" in
		LUKS)
		  printf "You will need to create a passphrase.\\n"
		  cryptsetup luksFormat "$home_dev" || check_exit
		  printf "Encrypted partition created. Opening it...\\n"
		  cryptsetup luksOpen "$home_dev" home_fs || check_exit
		  home_dev_real="$home_dev"
		  home_dev="/dev/mapper/home_fs"
		  crypttype_home="luks";;
		TrueCrypt)
		  truecrypt -t -c "$HOME_PART"
		  cryptsetup open "$HOME_PART" --type tcrypt home_fs
		  home_dev_real="$home_dev"
		  home_dev="/dev/mapper/home_fs"
		  crypttype_home="tcrypt";;
		VeraCrypt)
		  veracrypt -t -c "$HOME_PART"
		  cryptsetup open "$HOME_PART" --type tcrypt home_fs
		  home_dev_real="$home_dev"
		  home_dev="/dev/mapper/home_fs"
		  crypttype_home="tcrypt";;
	  esac
		else
		HOME_PART="$home_dev"
	fi
	if [ "$no_format" != "yes" ]; then
	  NEW_FS_TYPE="$HOME_FS_TYPE"
	  NEW_FS_PART="$HOME_PART"
	  create_filesystem
	fi
    if [ "$NEW_FS_TYPE" = "ext2" ] || [ "$NEW_FS_TYPE" = "ext3" ] || [ "$NEW_FS_TYPE" = "ext4" ]; then
      tune2fs -r0 "$NEW_FS_PART"
	fi
	if [ "$HOME_ENCRYPT" = "yes" ]; then
	  if [ "$(lsblk -no MOUNTPOINT "$home_dev")" != "/target_home" ]; then
		mount "$home_dev" /target_home || check_exit
	  fi
	else
	  if [ "$(lsblk -no MOUNTPOINT "$HOME_PART")" != "/target_home" ]; then
		mount "$HOME_PART" /target_home || check_exit
	  fi
	fi
	sep_home_opt="--exclude=/home/*"
fi

# /boot

# make mount point for separate /boot if needed and add /boot/* to the excludes list
# if it's not already there allow default for reserved blocks (don't need tune2fs here)
if [ "$SEPERATE_BOOT" = "yes" ] && [ -n "$boot_dev" ]; then
	if [ ! -d /target_boot ]; then
	  mkdir /target_boot || check_exit
	fi
	[ "$no_format" != "yes" ] && NEW_FS_TYPE="$BOOT_FS_TYPE" && NEW_FS_PART="$boot_dev" && create_filesystem
    if [ "$NEW_FS_TYPE" = "ext2" ] || [ "$NEW_FS_TYPE" = "ext3" ] || [ "$NEW_FS_TYPE" = "ext4" ]; then
      tune2fs -r0 "$NEW_FS_PART"
    fi
	mount "$boot_dev" /target_boot
	sep_boot_opt="--exclude=/boot/*"
fi

cat > "$rsync_excludes" <<EOF
- /dev/*
- /cdrom/*
- /media/*
- /target
- /swapfile
- /.swapfile
- $SWAPFILE
- /mnt/*
- /sys/*
- /proc/*
- /tmp/*
- /live
- /lost+found
- /boot/lost+found
- /boot/grub/grub.cfg
- /boot/grub/menu.lst
- /boot/grub/device.map
- /etc/udev/rules.d/70-persistent-cd.rules
- /etc/udev/rules.d/70-persistent-net.rules
- /etc/fstab
- /etc/mtab
- /home/snapshot
- /home/*/.gvfs
- /var/cache/apt/*
- /var/lib/dbus/machine-id
- /etc/popularity-contest.conf
# Added for newer version of live-config/live-boot in sid (to become Jessie)
- /lib/live/overlay
- /lib/live/image
- /lib/live/rootfs
- /lib/live/mount
- /run/*
# Added for symlink /lib
- /usr/lib/live/overlay
- /usr/lib/live/image
- /usr/lib/live/rootfs
- /usr/lib/live/mount
EOF

# █▄█ █▀█ ▀█▀ █▀█    █▀▀ █▀█ █▀█ █ █
# █ █ █▀█  █  █ █    █   █ █ █▀▀  █
# ▀ ▀ ▀ ▀ ▀▀▀ ▀ ▀    ▀▀▀ ▀▀▀ ▀    ▀
# copy everything over except the things listed in the exclude list
printf "\\nStarting main copy...\\n" >> "$ERROR_LOG" 2>&1
rsync -av / /target/ --filter='P lost+found' --filter='H lost+found' \
--exclude-from="$rsync_excludes" ${sep_home_opt} ${sep_boot_opt} --delete-before --delete-excluded

# copy separate /home if needed
if [ "$SEPERATE_HOME" = "yes" ]; then
  printf "Copying home folders to new partition...\\n" >> "$ERROR_LOG" 2>&1
  rsync -av /home/ /target_home/  --filter='P lost+found' --filter='H lost+found' --exclude-from="$home_boot_excludes"
fi

# copy separate /boot if needed
if [ "$SEPERATE_BOOT" = "yes" ] && [ -n "$boot_dev" ]; then
  printf "Copying files to boot partition...\\n" >> "$ERROR_LOG" 2>&1
  rsync -av /boot/ /target_boot/  --filter='P lost+found' --filter='H lost+found' --exclude-from="$home_boot_excludes"
fi

# swap
if [ "$SEPERATE_SWAP" = "swapfile" ] && [ "$SWAPFILE" ]; then
  printf "\\nCreating swap file: %s with a size of %s" "$SWAPFILE" "$SWAPFILE_SIZE" >> "$ERROR_LOG" 2>&1
  dd if=/dev/zero of="/target$SWAPFILE" bs=1K count="$SWAPFILE_COUNT" || check_exit
  chmod 600 "/target$SWAPFILE"
  mkswap --label swap "/target$SWAPFILE" || check_exit
  swapon "/target$SWAPFILE" && ACTIVE_SWAP="/target$SWAPFILE"
elif [ "$SEPERATE_SWAP" = "yes" ] && [ -b "$SWAP_PART" ]; then
  mkswap --label swap "$SWAP_PART" || check_exit
  swapon "$SWAP_PART" && ACTIVE_SWAP="$SWAP_PART"
fi

# Disallow mounting of all fixed drives with pmount
if [ -f /target/etc/pmount.allow ] && [ $pmount_fixed = "no" ]; then
  sed -i 's:/dev/sd\[a-z\]:#/dev/sd\[a-z\]:' /target/etc/pmount.allow
fi

# Re-enable updatedb if it was disabled by an older version of refractasnapshot
if [ -e /target/usr/bin/updatedb.mlocate ] && [ ! -x /target/usr/bin/updatedb.mlocate ]; then
  chmod +x /target/usr/bin/updatedb.mlocate
fi

# █▀▀ █▀▀ ▀█▀ █▀█ █▀▄
# █▀▀ ▀▀█  █  █▀█ █▀▄
# ▀   ▀▀▀  ▀  ▀ ▀ ▀▀

# /
if [ "$ROOT_ENCRYPT" != "yes" ] && [ "$USE_UUID" = "yes" ]; then
  install_part="$(blkid -s UUID "$install_dev" | awk '{ print $2 }' | sed 's/\"//g')"
fi
printf "\\nGenerating /etc/fstab...\\n" >> "$ERROR_LOG" 2>&1
printf "%s\\t/\\t%s\\tdefaults,noatime\\t0\\t1\\n" "$install_part" "$ROOT_FS_TYPE" >> /target/etc/fstab

# /home
if [ "$SEPERATE_HOME" = "yes" ] && [ -n "$HOME_PART" ]; then
  if [ "$HOME_ENCRYPT" != "yes" ]; then
	printf "Adding /home entry to fstab...\\n" >> "$ERROR_LOG" 2>&1
	if [ "$USE_UUID" = "yes" ]; then
	  printf "%s\\t/home\\t%s\\tdefaults,noatime\\t0\\t2\\n" "$(blkid -s UUID "$home_dev" | awk '{ print $2 }' | sed 's/\"//g')" "$HOME_FS_TYPE" >> /target/etc/fstab
    fi
  fi
  printf "%s\\t/home\\t%s\\tdefaults,noatime\\t0\\t2\\n" "$home_dev" "$HOME_FS_TYPE" >> /target/etc/fstab || check_exit
fi

# /boot
if [ "$SEPERATE_BOOT" = "yes" ] && [ -n "$boot_dev" ]; then
  printf "Adding /boot entry to fstab...\\n" >> "$ERROR_LOG" 2>&1
  if [ "$USE_UUID" = "yes" ]; then
	printf "%s\\t/boot\\t%s\\tdefaults,noatime,\\t0\\t1\\n" "$(blkid -s UUID "$boot_dev" | awk '{ print $2 }' | sed 's/\"//g')" "$BOOT_FS_TYPE" >> /target/etc/fstab
  else
	printf "%s\\t/boot\\t%s\\tdefaults,noatime,\\t0\\t1\\n" "$boot_dev" "$BOOT_FS_TYPE" >> /target/etc/fstab || check_exit
  fi
fi

# swap
if [ "$SEPERATE_SWAP" != "no" ]; then
  printf "Adding swap entry to fstab...\\n" >> "$ERROR_LOG" 2>&1
  if [ "$use_existing_swap" = "yes" ]; then
    [ "$USE_UUID" = "yes" ] && swap_part="$(/sbin/blkid -s UUID "$swap_dev" | awk '{ print $2 }' | sed 's/\"//g')" || swap_part="$swap_dev"
    printf "%s\\tswap\\tswap\\tdefaults\\t0\\t0\\n" "$swap_part" >> /target/etc/fstab
  elif [ "$SEPERATE_SWAP" = "yes" ] && [ -b "$SWAP_PART" ]; then
    printf "%s\\tswap\\tswap\\tdefaults\\t0\\t0\\n" "$SWAP_PART" >> /target/etc/fstab
  elif [ "$SEPERATE_SWAP" = "swapfile" ] && [ -e "/target$SWAPFILE" ]; then
    printf "%s\\tswap\\tswap\\tdefaults\\t0\\t0\\n" "$SWAPFILE" >> /target/etc/fstab
  fi
fi

# █▀▀ █▀▄ █ █ █▀█ ▀█▀ ▀█▀ █▀█ █▀▄
# █   █▀▄  █  █▀▀  █   █  █▀█ █▀▄
# ▀▀▀ ▀ ▀  ▀  ▀    ▀   ▀  ▀ ▀ ▀▀

# /
if [ "$ROOT_ENCRYPT" = "yes" ]; then
  printf "Adding %s entry to crypttab...\\n" "$install_dev" >> "$ERROR_LOG" 2>&1
  if [ "$USE_UUID" = "yes" ]; then
    if [ "$(blkid "$install_dev" | xargs -n1 | grep '^UUID=')" ]; then
      install_crypt="$(blkid "$install_dev" | xargs -n1 | grep '^UUID=' | sed 's/\"//g')"
      printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_crypt" "$crypttype_root" >> /target/etc/crypttab
    elif [ "$(blkid "$install_dev" | xargs -n1 | grep '^PARTUUID=')" ]; then
      install_crypt="$(blkid "$install_dev" | xargs -n1 | grep '^PARTUUID=' | sed 's/\"//g')"
      printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_crypt" "$crypttype_root" >> /target/etc/crypttab
    else
      printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_dev" "$crypttype_root" >> /target/etc/crypttab
    fi
  else
    printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_dev" "$crypttype_root" >> /target/etc/crypttab
  fi
fi

# /home
if [ "$HOME_ENCRYPT" = "yes" ]; then
  printf "Adding %s entry to crypttab...\\n" "$home_dev_real" >> "$ERROR_LOG" 2>&1
  if [ "$USE_UUID" = "yes" ]; then
    if [ "$(blkid "$home_dev_real" | xargs -n1 | grep '^UUID=')" ]; then
      home_crypt="$(blkid "$home_dev_real" | xargs -n1 | grep '^UUID=' | sed 's/\"//g')"
      printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_crypt" "$crypttype_home" >> /target/etc/crypttab
    elif [ "$(blkid "$home_dev_real" | xargs -n1 | grep '^PARTUUID=')" ]; then
      home_crypt="$(blkid "$home_dev_real" | xargs -n1 | grep '^PARTUUID=' | sed 's/\"//g')"
      printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_crypt" "$crypttype_home" >> /target/etc/crypttab
    else
      printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_dev_real" "$crypttype_home" >> /target/etc/crypttab
    fi
  else
    printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_dev_real" "$crypttype_home" >> /target/etc/crypttab
  fi
fi

#####  May need to check for /etc/default/grub and warn if absent ##########
# Tell grub to use encrypted /boot directory.
if [ "$BOOT_ENCRYPT" = "yes" ]; then
  if ! [ "$(grep ^GRUB_ENABLE_CRYPTODISK /target/etc/default/grub)" ]; then
	printf "\\nGRUB_ENABLE_CRYPTODISK=y\\n" >> /target/etc/default/grub
  fi
fi

# █▀▀ █▀▀ █ █
# ▀▀█ ▀▀█ █▀█
# ▀▀▀ ▀▀▀ ▀ ▀
# Allow users to login to ssh with passwords if desired.
# Allow root login only with auth keys.
# or do nothing.
if [ -f /target/etc/ssh/sshd_config ]; then
  if [ "$ssh_pass" = "yes" ]; then
	sed -i~ 's/PasswordAuthentication no/PasswordAuthentication yes/' /target/etc/ssh/sshd_config
	sed -i 's/PermitRootLogin yes/PermitRootLogin prohibit-password/' /target/etc/ssh/sshd_config
  elif [ "$ssh_pass" = "no" ]; then
	sed -i~ 's/.*PasswordAuthentication yes/PasswordAuthentication no/' /target/etc/ssh/sshd_config
	sed -i 's/PermitRootLogin yes/PermitRootLogin prohibit-password/' /target/etc/ssh/sshd_config
  elif [ -n "$ssh_pass" ]; then
	echo "WARNING: ssh_pass value not recognized. No changes were made to /etc/ssh/sshd_config"
  fi
fi

# █▀▄ █▀█ █▀█ ▀█▀ █   █▀█ █▀█ █▀▄ █▀▀ █▀▄
# █▀▄ █ █ █ █  █  █   █ █ █▀█ █ █ █▀▀ █▀▄
# ▀▀  ▀▀▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀ ▀▀  ▀▀▀ ▀ ▀
# mount stuff so grub will behave (so chroot will work)
printf "Mounting tmpfs and proc...\\n"
# /dev
if [ ! -d /target/dev ]; then
  mkdir /target/dev || check_exit
fi
if [ -d /target/dev ]; then
  mount --bind /dev/ /target/dev/ || check_exit
fi
# /proc
if [ ! -d /target/proc ]; then
  mkdir /target/proc || check_exit
fi
if [ -d /target/proc ]; then
  mount --bind /proc/ /target/proc/ || check_exit
fi
# /sys
if [ ! -d /target/sys ]; then
  mkdir /target/sys || check_exit
fi
if [ -d /target/sys ]; then
  mount --bind /sys/ /target/sys/ || check_exit
fi

# If /boot is separate partition, need to mount it in chroot for grub and for efi
if [ "$SEPERATE_BOOT" = "yes" ] && [ -n "$boot_dev" ]; then
 chroot /target mount "$boot_dev" /boot || check_exit
fi

# This test is not complete and should probably be done earlier. grub_dev="efi" above
[ -n "$esp_dev" ] && uefi_ready="yes"

# add entry for esp_dev to fstab if needed
if [ "$uefi_ready" = "yes" ] && [ $uefi_boot = "yes" ]; then
	[ "$USE_UUID" = "yes" ] && esp_part="$(/sbin/blkid -s UUID $esp_dev | awk '{ print $2 }' | sed 's/\"//g')" || esp_part="$esp_dev"
    printf "Adding esp entry to fstab...\\n"
    printf "%s\\t/boot/efi\\tvfat\\tdefaults\\t0\\t1\\n" "$esp_part" >> /target/etc/fstab
	mkdir /target/boot/efi
	mount "$esp_dev" /target/boot/efi/
fi

# █▀▀ █ █ ▀█▀ █   ▀█▀ █▀█ █ █ █ █
# █▀▀ ▄▀▄  █  █    █  █ █ █ █ ▄▀▄
# ▀▀▀ ▀ ▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀ ▀ ▀
install_extlinux () {
if [ "$SEPERATE_BOOT" = "yes" ]; then
  BOOT_DEVICE=$(echo "$boot_dev" | tr -d '[:digit:]')
elif [ "$SEPERATE_BOOT" = "no" ]; then
  BOOT_DEVICE=$(echo "$ROOT_PART" | tr -d '[:digit:]')
else
  printf "Error while installing extlinux: SEPERATE_BOOT is neither 'yes' nor 'no'\\n" >> "$ERROR_LOG" 2>&1
  exit 1
fi
kernversion=$(uname -r)
# Check boot files
	# vmlinuz
	if [ -f "/boot/vmlinuz-$kernversion" ]; then
		vmlinuz_file="/boot/vmlinuz-$kernversion"
	elif [ -f /boot/vmlinuz ]; then
		vmlinuz_file="/boot/vmlinuz"; kernversion=""
	fi
	#initrd
	if [ -f "/boot/initrd.img-$kernversion" ]; then
		initrd_file="/boot/initrd.img-$kernversion"
	elif [ -f /boot/initrd.img ]; then
		initrd_file="/boot/initrd.img"; kernversion=""
	fi
# Copy boot files
	cp "$vmlinuz_file" /target/boot/
	cp "$initrd_file" /target/boot/
# Install Bootloader
	[ -d "/target/boot/syslinux" ] || mkdir "/target/boot/syslinux"
	cp -r /usr/lib/syslinux/modules/bios/*.c32 /target/boot/syslinux/
	[ -f /boot/syslinux/splash.png ] && cp /boot/syslinux/splash.png /target/boot/syslinux/splash.png
	extlinux -i /target/boot/syslinux
# MBR or GPT
	if [ "$(blkid -s PTTYPE -o value "$BOOT_DEVICE")" = "gpt" ]; then
	  dd bs=440 count=1 if=/usr/lib/syslinux/mbr/gptmbr.bin of="$BOOT_DEVICE"
	else
	  dd bs=440 count=1 if=/usr/lib/syslinux/mbr/mbr.bin of="$BOOT_DEVICE"
	fi	
# Generate syslinux.cfg
if [ "$ROOT_ENCRYPT" = "yes" ]; then
  if [ "$USE_UUID_IN_EXTLINUX" = "yes" ]; then
    if [ "$(blkid "$install_dev" | xargs -n1 | grep '^UUID=')" ]; then
      UUID="$(blkid "$install_dev" | xargs -n1 | grep '^UUID=')"
      crypt_dev="$(echo "$UUID" | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
      root_dev="$install_part"
    elif [ "$(blkid "$install_dev" | xargs -n1 | grep '^PARTUUID=')" ]; then
      UUID="$(blkid "$install_dev" | xargs -n1 | grep '^PARTUUID=')"
      crypt_dev="$(echo "$UUID" | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
      root_dev="$install_part"
    else
      crypt_dev="$(echo "$install_dev" | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
      root_dev="$install_part"
    fi
  else
	crypt_dev="$(echo $install_dev | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
	root_dev="$install_part"
  fi
else
  if [ "$USE_UUID_IN_EXTLINUX" = "yes" ]; then
    if [ "$(blkid "$install_dev" | xargs -n1 | grep '^UUID=')" ]; then
      UUID="$(blkid "$install_dev" | xargs -n1 | grep '^UUID=' | sed 's/\"//g')"
      crypt_dev=""; root_dev="$UUID"
    elif [ "$(blkid "$install_dev" | xargs -n1 | grep '^PARTUUID=')" ]; then
      UUID="$(blkid "$install_dev" | xargs -n1 | grep '^PARTUUID=' | sed 's/\"//g')"
      crypt_dev=""; root_dev="$UUID"
    else
      crypt_dev=""; root_dev="$install_dev"
    fi
  else
    crypt_dev=""; root_dev="$install_dev"
  fi
fi

if [ "$BLACKLIST_PCSPKR_IN_EXTLINUX" = "yes" ]; then
  blacklist_pcspkr='modprobe.blacklist=pcspkr'
else
  blacklist_pcspkr=''
fi
  
cat > /target/boot/syslinux/syslinux.cfg << EOF
UI vesamenu.c32
PROMPT	1
MENU TITLE Boot Menu
MENU BACKGROUND splash.png
MENU HSHIFT 18
MENU VSHIFT 3
MENU ROWS 7
MENU WIDTH 40
MENU TABMSGROW 29
TIMEOUT	2
DEFAULT	devuan
LABEL devuan
	MENU LABEL Devuan
	LINUX	../vmlinuz-$kernversion
	APPEND	lang=de $crypt_dev root=$root_dev $blacklist_pcspkr
	INITRD	../initrd.img-$kernversion
LABEL memtest
	MENU LABEL Memory test
	KERNEL ../memtest
LABEL reboot
	MENU LABEL Reboot
	COM32 reboot.c32
LABEL poweroff
	MENU LABEL Power Off
	COM32 poweroff.c32
EOF
}

# █▀▀ █▀▄ █ █ █▀▄
# █ █ █▀▄ █ █ █▀▄
# ▀▀▀ ▀ ▀ ▀▀▀ ▀▀
install_grub () {
printf "\\n Setting up grub bootloader... Please wait...\\n" >> "$ERROR_LOG" 2>&1
#grubversion=$(dpkg -l | egrep "ii|hi" | grep -v bin | grep -v doc | awk '$2 ~ "grub-[eglp]" { print $2}')
# If grub is installed to a partition, we need to know if it's grub-pc or grub-legacy/grub-gfx to handle it properly.
if [ -n "$grub_partition" ]; then
  if [ "$grubversion" != "grub-pc" ]; then
	# isolate the device (sdx) letter then use tr like this to translate to the right number for grub
	GRUBDEVICENUM=$(echo "$grub_partition" | sed 's:/dev/sd::;s:[0-9]::'g | tr '[a-j]' '[0-9]')
	# isolate the partition number
	INSTALLPARTNUM=$(echo "$grub_partition" | sed 's:/dev/sd::;s:[a-z]::')
	# and reduce it by 1 for grub
	GRUBPARTNUM="$(( INSTALLPARTNUM - 1))"
	# finally get the finished grub root syntax
	GRUBROOT="(hd$GRUBDEVICENUM,$GRUBPARTNUM)"
	chroot /target grub-install "$grub_partition"
	grub --batch <<EOF
	root $GRUBROOT
	setup $GRUBROOT
	quit
EOF
  else
	chroot /target grub-install --recheck --no-floppy --force "$grub_partition" >> "$ERROR_LOG" || check_exit
  fi

elif [ "$grub_dev" = "efi" ]; then
	chroot /target grub-install "${efi_name_opt}" >> "$ERROR_LOG" || check_exit
elif [ -n "$grub_dev" ]; then
    printf "\\nInstalling the grub boot loader...\\n"
    chroot /target grub-install "$grub_dev" >> "$ERROR_LOG" || check_exit
fi

chroot /target update-grub || check_exit
error_message=""
}

copy_grub_packages () {
find "$grub_package_dir" -maxdepth 1 -name "$grub_package" -exec cp {} /target \;
#	chroot /target find . -name $grub_package -maxdepth 1 -exec dpkg -i {} \;
# this works, but grub-pc/grub-pc-bin installed out of order.

# This works. They installed in right order.
[ -n "$grub_package" ] && chroot /target /bin/bash -c "dpkg -i $grub_package"

echo "$grub_package" | grep -qe 'grub-pc' && grubversion="grub-pc"
echo "$grub_package" | grep -qe 'grub-efi' && grubversion="grub-efi" && grub_dev="efi"

}

################################################################################################
if [ "$NEW_BOOTLOADER" = "ExtLinux" ]; then
  install_extlinux
elif [ "$NEW_BOOTLOADER" = "GRUB" ]; then
######  INSERT PAUSE TO ALLOW MANUAL WORK BEFORE GRUB (e.g. uefi)
  ASK_GRUB=""
  while [ -z "$ASK_GRUB" ]; do
    ASK_GRUB="$(iselect " The system is now ready to install grub" \
                    	" If you want to, you now have some time to chroot into the system" "" \
                    	" > Install Grub and finish the installation <s:YES>" \
                    	" > Continue wihtout a bootloader <s:NO>" \
                    	" < Abort the installation and exit <s:EXIT>" -p 4)"
  done
# $grub_package is null if installed grub matches boot type (uefi or bios)
  case $ASK_GRUB in
    YES) if [ -n "$grub_package" ]; then
		   copy_grub_packages
		 fi
		 if [ -z "$bios_boot_warning" ]; then
		   install_grub
		 fi;;
    NO) printf "Grub bootloader will not be installed\\n";;
    EXIT) cleanup; exit 0;;
  esac
fi

# Run update-initramfs to include dm-mod if using encryption
if [ "$ROOT_ENCRYPT" = "yes" ] || [ "$HOME_ENCRYPT" = "yes" ]; then
  if [ -f /usr/sbin/update-initramfs.orig.initramfs-tools ]; then
	chroot /target /usr/sbin/update-initramfs.orig.initramfs-tools -u -k all >> "$ERROR_LOG"
  else
	chroot /target /usr/sbin/update-initramfs -u -k all >> "$ERROR_LOG"
  fi
fi

##### This should not run if grub_dev=efi and choose 3 above (no bootloader)
#if [[ -n $grub_dev ]] || [[ -n $grub_partition ]] ; then
#    chroot /target update-grub || check_exit
#fi

if [ -f /target/boot/grub/setup_left_core_image_in_filesystem ]; then
	rm -f /target/boot/grub/setup_left_core_image_in_filesystem
fi

# INSTALLATION FINISHED - BEGIN CONFIGURE USERNAME, HOSTNAME, PASSWORDS, SUDO
############################################################################################
# Need to mount the target home partition under the target root partition
# so the commands can find it (for changing user configs gksu)
if [ "$HOME_ENCRYPT" = "yes" ]; then
[ "$SEPERATE_HOME" = "yes" ] && [ -b "$home_dev" ] && mount "$home_dev" /target/home
else
  [ "$SEPERATE_HOME" = "yes" ] && [ -b "$HOME_PART" ] && mount "$HOME_PART" /target/home
fi

# it might not be on in some live builds
chroot /target /bin/sh -c "shadowconfig on"

# █ █ █▀▀ █▀▀ █▀▄   █▀█ █▀▀ █▀▀ █▀█ █ █ █▀█ ▀█▀
# █ █ ▀▀█ █▀▀ █▀▄   █▀█ █   █   █ █ █ █ █ █  █
# ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀ ▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀
if [ "$OLD_USER_NAME" != "$NEW_USER_NAME" ]; then
  chroot /target usermod -l "$NEW_USER_NAME" "$OLD_USER_NAME" || check_exit
  chroot /target usermod -d "/home/$NEW_USER_NAME" -m "$NEW_USER_NAME" || check_exit
  chroot /target groupmod -n "$NEW_USER_NAME" "$OLD_USER_NAME" || check_exit
  for i in $(grep -r "/home/$OLD_USER_NAME" "/target/home/$NEW_USER_NAME/.config" | awk -F ":" '{ print $1 }'); do
 	sed -i "s:/home/$OLD_USER_NAME:/home/$NEW_USER_NAME:g" "$i"
  done
  for i in $(grep -r "/home/$OLD_USER_NAME" "/target/home/$NEW_USER_NAME/.local" | awk -F ":" '{ print $1 }'); do
	sed -i "s:/home/$OLD_USER_NAME:/home/$NEW_USER_NAME:g" "$i"
  done
fi

[ -z "$NEW_USER_NAME" ] && NEW_USER_NAME="$OLD_USER_NAME"
while true; do
  printf "\\n\\tChange %s's password?\\n\\tPress ENTER for YES.\\n\\tPress 2 for no." "$NEW_USER_NAME"
  read -r "ans"
  case "$ans" in
	[2Nn]*) break;;
	*)  # Redirect stderr from the error log to the screen, so we can see the prompts from passwd
		exec 2>&1
		echo "Change user's password"
		chroot /target passwd "$NEW_USER_NAME"
		exec 2>> "$ERROR_LOG"
		break;;
  esac
done

[ "$NEW_USER_GROUPS" ] && chroot /target usermod -G "$(cat "$GROUPS_FILE")" "$NEW_USER_NAME"
[ "$NEW_USER_REAL_NAME_EVENT" ] && chroot /target chfn -f "$NEW_USER_REAL_NAME" "$NEW_USER_NAME"
[ "$NEW_USER_ROOM_NR_EVENT" ] && chroot /target chfn -r "$NEW_USER_ROOM_NR" "$NEW_USER_NAME"
[ "$NEW_USER_PHONE_WORK_EVENT" ] && chroot /target chfn -w "$NEW_USER_PHONE_WORK" "$NEW_USER_NAME"
[ "$NEW_USER_PHONE_HOME_EVENT" ] && chroot /target chfn -h "$NEW_USER_PHONE_HOME" "$NEW_USER_NAME"
[ ! -d "$NEW_USER_HOME_DIR" ] && mkdir -vp "$NEW_USER_HOME_DIR"
[ "$NEW_USER_HOME_DIR" != "$(sudo -u "$NEW_USER_NAME" echo "$HOME")" ] && chroot /target usermod -md "$NEW_USER_HOME_DIR" "$NEW_USER_NAME"
[ "$NEW_USER_LOGIN_SHELL" != "$(grep ':1000:' /etc/passwd | cut -d ':' -f 7)" ] && chroot /target usermod -s "$NEW_USER_LOGIN_SHELL" "$NEW_USER_NAME"

# █▀▀ █ █ █▀▄ █▀█
# ▀▀█ █ █ █ █ █ █
# ▀▀▀ ▀▀▀ ▀▀  ▀▀▀
# =>wheezy live-config now uses /etc/sudoers.d
[ -e "/target/etc/sudoers.d/live" ] && rm -f "/target/etc/sudoers.d/live"

# squeeze (or other distro) might have used /etc/sudoers
if grep -qs "$OLD_USER_NAME" /target/etc/sudoers ; then sed -i "/$OLD_USER_NAME/d" /target/etc/sudoers ; fi

if [ "$sudoconfig" = "TRUE" ] || [ "$sudo_is_default" = "TRUE" ]; then
	# $NEW_USER_NAME is permitted to use sudo so add him to sudo group
	chroot /target usermod -a -G sudo "$NEW_USER_NAME"
	# it shoud be already there in =>wheezy.. in case it's not:
	if ! grep -qs "^%sudo" /target/etc/sudoers ; then echo "%sudo ALL=(ALL:ALL) ALL" >> /etc/sudoers ; fi
fi

if [ "$sudo_is_default" = "TRUE" ]; then
	# disable root account
	printf "disabling root account...\\n"
	rootpass_hash=$(awk -F ":" '/^root/ {print $3 ":" $4 ":" $5 ":" $6}' /target/etc/shadow)
	sed -i "s|^root:.*|root:\*:${rootpass_hash}:::|" /target/etc/shadow
else
	# files that may have been written by live-config to force live sudo mode # should they just be deleted?
	# rm -f /target/home/*/.gconf/apps/gksu/%gconf.xml
	# rm -f /target/home/*/.*/share/config/*desurc

	# fix gksu in user's home ($NEW_USER_NAME will not use sudo by default)
	if [ -f /target/home/"$NEW_USER_NAME"/.gconf/apps/gksu/%gconf.xml ]; then
		sed -i '/sudo-mode/s/true/false/' /target/home/"$NEW_USER_NAME"/.gconf/apps/gksu/%gconf.xml
	fi

	sed -i 's/SU_TO_ROOT_SU=sudo/SU_TO_ROOT_SU=su/' /target/home/"$NEW_USER_NAME"/.su-to-rootrc
	# detects .kde/ .kde4/ .trinity/ (kdesurc or tdesurc)
	for file in /target/home/"$NEW_USER_NAME"/.*/share/config/*desurc ; do
		sed -i 's/super-user-command=sudo/super-user-command=su/' "$file"
	done

if [ "$sudo_shutdown" = "TRUE" ]; then
	### Maybe move this up so it's available to option "a" (disable sudo) ########
	sudo_include_file="/target/etc/sudoers.d/user_shutdown"
	[ -f "$sudo_include_file" ] && mv "$sudo_include_file" "${sudo_include_file}.old"
	echo "$NEW_USER_NAME ALL= NOPASSWD: /usr/sbin/pm-suspend, /usr/sbin/pm-hibernate, /sbin/halt, /sbin/reboot" > "$sudo_include_file"
fi
	while true; do
		printf "\\n\\tChange root password?\\n\\tPress ENTER for YES.\\n\\tPress 2 for no.\\n\\t"
		read -r "ans"
		case "$ans" in
		[2Nn]*) break;;
		*)	exec 2>&1 ; printf "Change root password\\n"; chroot /target passwd
			exec 2>> "$ERROR_LOG";	break;;
		esac
	done
fi

# █▀█ █ █ ▀█▀ █▀█ █   █▀█ █▀▀ ▀█▀ █▀█
# █▀█ █ █  █  █ █ █   █ █ █ █  █  █ █
# ▀ ▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀
if [ "$DISABLE_AUTODESK" = "yes" ]; then
# Disable autologin
  [ -f /target/etc/gdm/gdm.conf ] && sed -i 's/^AutomaticLogin/#AutomaticLogin/' /target/etc/gdm/gdm.conf
  [ -f /target/etc/gdm3/daemon.conf ] && sed -i 's/^AutomaticLogin/#AutomaticLogin/' /target/etc/gdm3/daemon.conf
  [ -f /target/etc/lightdm/lightdm.conf ] && sed -i 's/^autologin/#autologin/g' /target/etc/lightdm/lightdm.conf
  [ -f /target/etc/default/kdm.d/live-autologin ] && rm -f /target/etc/default/kdm.d/live-autologin
  [ -f /target/etc/kde3/kdm/kdmrc ] && sed -i -e 's/^AutoLogin/#AutoLogin/g' /target/etc/kde3/kdm/kdmrc -e 's/^AutoReLogin/#AutoReLogin/g' /target/etc/kde3/kdm/kdmrc
  [ -f /target/etc/kde4/kdm/kdmrc ] && sed -i -e 's/^AutoLogin/#AutoLogin/g' /target/etc/kde4/kdm/kdmrc -e 's/^AutoReLogin/#AutoReLogin/g' /target/etc/kde4/kdm/kdmrc
  [ -f /target/etc/default/kdm-trinity.d/live-autologin ] && rm -f  /target/etc/default/kdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/kdm/kdmrc ] && sed -i -e 's/^AutoLogin/#AutoLogin/g' /target/etc/trinity/kdm/kdmrc -e 's/^AutoReLogin/#AutoReLogin/g' /target/etc/trinity/kdm/kdmrc
  [ -f /target/etc/default/tdm-trinity.d/live-autologin ] && rm -f  /target/etc/default/tdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/tdm/tdmrc ] && sed -i -e 's/^AutoLogin/#AutoLogin/g' /target/etc/trinity/tdm/tdmrc -e 's/^AutoReLogin/#AutoReLogin/g' /target/etc/trinity/tdm/tdmrc
  [ -f /target/etc/slim.conf ] && sed -i -e 's/^[ ]*default_user/#default_user/' -e 's/^[ ]*auto_login.*$/#auto_login no/' /target/etc/slim.conf
  [ -f /target/etc/lxdm/lxdm.conf ] && sed -i -e 's/^autologin=/#autologin=/' /target/etc/lxdm/lxdm.conf
# No display manager
  [ -f /target/etc/profile.d/zz-live-config_xinit.sh ] && rm -f /target/etc/profile.d/zz-live-config_xinit.sh
  disable_auto_console="yes"
else
# Keep autologin and update username in the display manager config.
  [ -f /target/etc/gdm/gdm.conf ] && sed -i "/AutomaticLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/gdm/gdm.conf
  [ -f /target/etc/gdm3/daemon.conf ] && sed -i "/AutomaticLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/gdm3/daemon.conf
  [ -f /target/etc/lightdm/lightdm.conf ] && sed -i "/autologin/s/=$OLD_USER_NAME/=$NEW_USER_NAME/" /target/etc/lightdm/lightdm.conf
  [ -f /target/etc/default/kdm.d/live-autologin ] && sed -i "s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/default/kdm.d/live-autologin
  [ -f /target/etc/kde3/kdm/kdmrc ] && sed -i -e "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde3/kdm/kdmrc -e "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde3/kdm/kdmrc
  [ -f /target/etc/kde4/kdm/kdmrc ] && sed -i -e "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde4/kdm/kdmrc -e "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde4/kdm/kdmrc
  [ -f /target/etc/default/kdm-trinity.d/live-autologin ] && sed -i "s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/default/kdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/kdm/kdmrc ] && sed -i -e "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/kdm/kdmrc -e "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/kdm/kdmrc
  [ -f /target/etc/default/tdm-trinity.d/live-autologin ] && sed -i "s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/default/tdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/tdm/tdmrc ] && sed -i -e "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/tdm/tdmrc -e "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/tdm/tdmrc
  [ -f /target/etc/slim.conf ] && sed -i  -e "/default_user/s/\s\+$OLD_USER_NAME/ $NEW_USER_NAME/" /target/etc/slim.conf
  [ -f /target/etc/lxdm/lxdm.conf ] && sed -i -e "/^autologin=/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/lxdm/lxdm.conf
fi

# Disable console autologin
if [ "$disable_auto_console" = "yes" ]; then
  if grep -q "respawn:/bin/login -f" /target/etc/inittab ; then
	mv /target/etc/inittab /target/etc/inittab."$(date +%Y%m%d_%H%M)"
	cp /usr/lib/refractainstaller/inittab.debian /target/etc/inittab
  fi
else
  sed -i "/respawn:/s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/inittab
fi

if [ "$additional_partitions" = "yes" ]; then
  if ! [ -h /usr/lib/refractainstaller/post-install/move-dir-mount-gui.sh ]; then
	ln -s /usr/lib/refractainstaller/move-dir-mount-gui.sh /usr/lib/refractainstaller/post-install/move-dir-mount-gui.sh
  fi
else
  [ -h /usr/lib/refractainstaller/post-install/move-dir-mount.sh ] \
  && rm /usr/lib/refractainstaller/post-install/move-dir-mount.sh
fi

# █ █ █▀█ █▀▀ ▀█▀ █▀█ █▀█ █▄█ █▀▀
# █▀█ █ █ ▀▀█  █  █ █ █▀█ █ █ █▀▀
# ▀ ▀ ▀▀▀ ▀▀▀  ▀  ▀ ▀ ▀ ▀ ▀ ▀ ▀▀▀
if [ "$NEW_HOSTNAME" != "$(hostname)" ]; then
  sed -i "s/$(hostname)/$NEW_HOSTNAME/" /target/etc/hostname
  sed -i "s/ $(hostname) / $NEW_HOSTNAME /g" /target/etc/hosts
fi
# if [ -f "/target/etc/init.d/hostname.sh" ]; then /target/etc/init.d/hostname.sh start; fi

# █▀█ █▀▄ ▀▀█ █ █ █▀▀ ▀█▀   █▀▀ █▀▀ █▀▀ █▀▀
# █▀█ █ █   █ █ █ ▀▀█  █    █   █▀▀ █ █ ▀▀█
# ▀ ▀ ▀▀  ▀▀  ▀▀▀ ▀▀▀  ▀    ▀▀▀ ▀   ▀▀▀ ▀▀▀

# NEW_USER_NAME
if [ ! "$OLD_USER_NAME" = "$NEW_USER_NAME" ]; then

# symlinks in /root
  for LNK in $(find "/target/root" -type l); do
	if [ "$(readlink "$LNK" | grep "^/home/$OLD_USER_NAME/")" ]; then
	  NEWLNK="$(readlink "$LNK" | sed "s:/home/$OLD_USER_NAME/::;s:^:/home/$NEW_USER_NAME/:")"
	  ln -sfn "$NEWLNK" "$LNK"
	fi
  done

# symlinks in ~
  for LNK in $(find "/target/home/$NEW_USER_NAME" -type l); do
	if [ "$(readlink "$LNK" | grep "^/home/$OLD_USER_NAME/")" ]; then
	  NEWLNK="$(readlink "$LNK" | sed "s:/home/$OLD_USER_NAME/::;s:^:/home/$NEW_USER_NAME/:")"
	  ln -sfn "$NEWLNK" "$LNK"
	fi
  done

# usename references in ~
  for FL in $(grep -rl "/home/$OLD_USER_NAME/" "/target/home/$NEW_USER_NAME"); do
	sed -i "s:/home/$OLD_USER_NAME/:/home/$NEW_USER_NAME/:g" "$FL"
  done
fi

# rofi
screen_x="$(xdpyinfo | awk '/dimension/ {print $2}' | cut -d 'x' -f 1)"
screen_y="$(xdpyinfo | awk '/dimension/ {print $2}' | cut -d 'x' -f 2)"
old_val=$(grep 'width' "/target/home/$NEW_USER_NAME/.config/rofi/rofi_config_i3.rasi" | head -n1)
new_val=$(echo "$screen_x * 90 / 100" | bc | cut -d '.' -f 1)
[ "$new_val" -gt "2000" ] && new_val="2000"
sed -i "s,$old_val,width: $new_val;," "/target/home/$NEW_USER_NAME/.config/rofi/rofi_config_i3.rasi"
find /usr/share/applications | sort | sed 's:/usr/share/applications/::g;s:^:0 :g' > "/target/home/$NEW_USER_NAME/.cache/rofi3.druncache"
chown "$NEW_USER_NAME":"$NEW_USER_NAME" "/target/home/$NEW_USER_NAME/.cache/rofi3.druncache"
chmod 744 "/target/home/$NEW_USER_NAME/.cache/rofi3.druncache"
mkdir /target/root/.cache
chown root:root /target/root/.cache
touch /target/root/.cache/zshistory
chown root:root /target/root/.cache/zshistory


# █▀▀ █   █▀▀ █▀█ █▀█ █ █ █▀█
# █   █   █▀▀ █▀█ █ █ █ █ █▀▀
# ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀ ▀ ▀ ▀▀▀ ▀
# copy error log to installation before calling cleanup function
cp "$ERROR_LOG" /target/home/"$NEW_USER_NAME"/
chown 1000:1000 /target/home/"$NEW_USER_NAME"/"${ERROR_LOG##*/}"
cleanup
[ -f "$rsync_excludes" ] && rm "$rsync_excludes"

# ▀█▀ █▀█ █▀▀ ▀█▀ █▀█ █   █   █▀█ ▀█▀ ▀█▀ █▀█ █▀█
#  █  █ █ ▀▀█  █  █▀█ █   █   █▀█  █   █  █ █ █ █
# ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀
# █▀▀ █▀█ █▄█ █▀█ █   █▀▀ ▀█▀ █▀▀
# █   █ █ █ █ █▀▀ █   █▀▀  █  █▀▀
# ▀▀▀ ▀▀▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀  ▀  ▀▀▀

STRING='
   _          __       ____     __  _         
  / /__  ___ / /____ _/ / /__ _/ /_(_)__  ___  
 / / _ \(_-</ __/ _ `/ / / _ `/ __/ / _ \/ _ \ 
/_/_//_/___/\__/\_,_/_/_/\_,_/\__/_/\___/_//_/ 
                          __    __             
     _______  ____  ___  / /__ / /____         
    / __/ _ \/    \/ _ \/ / -_) __/ -_)        
    \__/\___/_/_/_/ .__/_/\__/\__/\__/         
                 /_/                           
'

INSTALLATION_COMPLETE_MSG="

$STRING

     username is $NEW_USER_NAME
     hostname is $NEW_HOSTNAME


	> Get the latest updates from the Devuan repos <s:UPDATE_PKGS>
	> Install wifi drivers (needs reboot to work) <s:INSTALL_WIFI>

	> Reboot<s:REBOOT>
	> Exit script<s:EXIT>
"

installation_complete_event() {
INSTALLATION_COMPLETE_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$INSTALLATION_COMPLETE_MSG" -p 22 )
case "$INSTALLATION_COMPLETE_EVENT" in
	UPDATE_PKGS) check_internet
				 if [ "$INTERNET_STATUS" = "CONNECTED" ]; then
				   chroot /target apt update
				   chroot /target apt upgrade
				 fi
				 installation_complete_event;;
	INSTALL_WIFI) WIFI_DRIVERS="$(locate firmware.beowulf | grep .deb | sed 's/$/<s>/g')"
				  SELECTED_DRIVER="$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$WIFI_DRIVERS" -p 2)"
				  [ -f "$SELECTED_DRIVER" ] && DRIVER_CONFIRM=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
				  	"You have selected $SELECTED_DRIVER" "" "Do you want to install it now?" \
				  	"   Yes) install $SELECTED_DRIVER now <s:YES>" \
				  	"    No) Back <s:NO>" -p 4)
				  case "$DRIVER_CONFIRM" in
					YES) chroot /target/ dpkg -i "$SELECTED_DRIVER";;
					NO) ;;
				  esac
				  installation_complete_event;;
	REBOOT) /sbin/reboot;;
	EXIT|*) exit 0;;
esac
}
installation_complete_event
}

#####################################################################################################################
#    _____                        __          __     _____           _       __
#   / ___/____  ____ _____  _____/ /_  ____  / /_   / ___/__________(_)___  / /_
#   \__ \/ __ \/ __ `/ __ \/ ___/ __ \/ __ \/ __/   \__ \/ ___/ ___/ / __ \/ __/
#  ___/ / / / / /_/ / /_/ (__  ) / / / /_/ / /_    ___/ / /__/ /  / / /_/ / /_
# /____/_/ /_/\__,_/ .___/____/_/ /_/\____/\__/   /____/\___/_/  /_/ .___/\__/
#                 /_/                                             /_/
#
check_directories () {
# Create snapshot_dir and work_dir if necessary.
# Don't use /media/* for $snapshot_dir or $work_dir unless it is a mounted filesystem
snapdir_is_remote=$(echo ${snapshot_dir} | awk -F / '{ print "/" $2 "/" $3 }' | grep /media/)
workdir_is_remote=$(echo ${work_dir} | awk -F / '{ print "/" $2 "/" $3 }' | grep /media/)

if [ -n "$snapdir_is_remote" ] && cat /proc/mounts | grep -q ${snapdir_is_remote}; then
   printf "%s is mounted" "$snapshot_dir"
elif [ -n "$snapdir_is_remote" ]; then
   printf "Error...\\nThe selected snapshot directory cannot be accessed.\\nDo you need to mount it?\\n"; exit 1
fi

if [ -n "$workdir_is_remote" ] && cat /proc/mounts | grep -q ${workdir_is_remote}; then
   printf "%s is mounted\\n" "$work_dir"
elif [ -n "$workdir_is_remote" ]; then
   printf "Error...\\nThe selected work directory cannot be accessed.\\nDo you need to mount it?\\n"; exit 1
fi

# Check that snapshot_dir exists
if [ ! -d "$snapshot_dir" ]; then
  printf "creating %s ..." "$snapshot_dir"
  mkdir -vp "$snapshot_dir"
  chmod -v 777 "$snapshot_dir"
fi

# Clear work_dir if needed
if [ "$save_work" = "no" ] && [ -d "$work_dir" ]; then
  printf "cleaning %s ..." "$work_dir"
  rm -vrf "$work_dir"
fi

# Make work_dir
if [ ! -d "$work_dir" ]; then
  printf "creating %s ..." "$work_dir"
  mkdir -vp "$work_dir"
  mkdir -vp "$work_dir"/iso
  mkdir -vp "$work_dir"/myfs
fi
}


# Prepare for Snapshot
prepare_snapshot() {
# Fix root's path (for Buster/Beowulf and later)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Record errors in a logfile.
ERROR_LOG="$ERROR_LOG_SNAPSHOT"; [ ! -e "$ERROR_LOG" ] && touch "$ERROR_LOG"
exec 2>"$ERROR_LOG"

cat > "$snapshot_excludes" <<EOF
- */.uuid
- /.swapfile
- /lost+found
- /boot/lost+found
- /root/.aptitude
- /root/.bash_history
- /root/.cache
- /root/.cdw
- /root/.disk-manager.conf
- /root/.fstab.log
- /root/.lesshst
- /root/.links2
- /root/.lnav
- /root/*/.log
- /root/.local/share/*
- /root/.nano_history
- /root/.synaptic
- /root/.thumbnails
- /root/.VirtualBox
- /root/.ICEauthority
- /root/.Xauthority
- /root/.ssh
- /target_boot
- /storage
- /home/*/Music
- /home/*/Media
- /home/*/Rec
- /home/*/Screenshots
- /home/*/.Trash*
- /home/*/.mozilla/*/Cache/*
- /home/*/.mozilla/*/urlclassifier3.sqlite
- /home/*/.mozilla/*/places.sqlite
- /home/*/.mozilla/*/cookies.sqlite
- /home/*/.mozilla/*/signons.sqlite
- /home/*/.mozilla/*/formhistory.sqlite
- /home/*/.mozilla/*/downloads.sqlite
- /home/*/.adobe
- /home/*/.aptitude
- /home/*/.bash_history
- /home/*/.bibletime/cache
- /home/*/.bibletime/indices
- /home/*/.bibletime/sessions
- /home/*/.cache
- /home/*/.cdw
- /home/*/.dbus
- /home/*/.din
- /home/*/.gksu*
- /home/*/.gvfs
- /home/*/.hardinfo
- /home/*/.ibam
- /home/*/.lesshst
- /home/*/.links2
- /home/*/.lnav
- /home/*/.log
- /home/*/.macromedia
- /home/*/.moc
- /home/*/.nano_history
- /home/*/.pulse*
- /home/*/.qpsrc
- /home/*/.recently-used
- /home/*/.recently-used.xbel
- /home/*/.local/XDG_RUNTIME_DIR/*
- /home/*/.local/share/Trash
- /home/*/.local/share/recently-used.xbel
- /home/*/.local/share/GottCode
- /home/*/.local/share/fonts/.uuid
- /home/*/.local/share/bibletime
- /home/*/.local/share/mc
- /home/*/.local/share/nano
- /home/*/.local/share/newsbeuter/*
- /home/*/.local/share/SpeedCrunch
- /home/*/.local/share/qalculate
- /home/*/.thumbnails
- /home/*/.vbox*
- /home/*/.VirtualBox
- /home/*/VirtualBox\ VMs
- /home/*/.w3m
- /home/*/.wine
- /home/*/.wget-hsts
- /home/*/.xsession-errors*
- /home/*/.ICEauthority
- /home/*/.Xauthority
- /home/*/.zcompdump*
- /home/*/.config/moc/cache
- /home/*/.config/moc/last_directory
- /home/*/.config/moc/pid
- /home/*/.config/moc/socket2
- /home/*/.config/pulse
- /home/*/.config/obs-studio/logs/
- /home/*/.config/obs-studio/profiler_data/
- /home/*/.config/smplayer/file_settings
- /dev/*
- /cdrom/*
- /media/*
- /swapfile
- /mnt/*
- /sys/*
- /proc/*
- /tmp/*
- /live
- /persistence.conf
- /boot/grub/grub.cfg
- /boot/grub/menu.lst
- /boot/grub/device.map
- /boot/*.bak
- /boot/*.old-dkms
- /etc/udev/rules.d/70-persistent-cd.rules
- /etc/udev/rules.d/70-persistent-net.rules
- /etc/fstab
- /etc/fstab.d/*
- /etc/mtab
- /etc/blkid.tab
- /etc/blkid.tab.old
- /etc/apt/sources.list~
- /etc/crypttab
- /etc/initramfs-tools/conf.d/resume     # see remove-cryptroot and nocrypt.sh
- /etc/initramfs-tools/conf.d/cryptroot  # see remove-cryptroot and nocrypt.sh
- /etc/popularity-contest.conf
- /home/snapshot

# Added for newer version of live-config/live-boot in wheezy
# These are only relevant here if you create a snapshot while you're running a live-CD or live-usb.
- /lib/live/overlay
- /lib/live/image
- /lib/live/rootfs
- /lib/live/mount
- /run/*

# Added for symlink /lib
- /usr/lib/live/overlay
- /usr/lib/live/image
- /usr/lib/live/rootfs
- /usr/lib/live/mount

## Entries below are optional. They are included either for privacy or to reduce the size of the snapshot.
## If you have any large files or directories, you should exclude them from being copied by adding them to this list.

# Uncomment this to exclude everything in /var/log/
- /var/log/*
# As of version 9.2.0, current log files are truncated, and archived log files are excluded.
#- /var/log/*gz

# The next three lines exclude everything in /var/log except /var/log/clamav/ (or anything else beginning with "c")
# and /var/log/gdm (or anything beginning with "g"). If clamav log files are excluded, freshclam will give errors at boot.
#- /var/log/[a-b,A-Z]*
#- /var/log/[d-f]*
#- /var/log/[h-z]*
- /var/cache/apt/archives/partial
- /var/cache/apt/archives/*.deb
- /var/cache/apt/pkgcache.bin
- /var/cache/apt/srcpkgcache.bin
- /var/cache/apt/apt-file/*
- /var/cache/debconf/*~old
- /var/cache/fontconfig
- /var/lib/apt/lists/*
- /var/lib/apt/*~
- /var/lib/apt/cdroms.list
- /var/lib/aptitude/*.old
- /var/lib/dhcp/*
- /var/lib/dpkg/*~old
- /var/spool/mail/*
- /var/mail/*
- /var/backups/*.gz
- /var/backups/*.bak
- /var/lib/dbus/machine-id
- /var/lib/live/config/*

- /usr/share/icons/*/icon-theme.cache

# You might want to comment these out if you're making a snapshot for your own personal use, not to be shared with others.
- /home/*/.gnupg
- /home/*/.ssh
- /home/*/.xchat2
- /home/*/.config/hexchat

# Exclude ssh_host_keys. New ones will be generated upon live boot.
# If you really want to clone your existing ssh host keys in your snapshot, comment out these two lines.
- /etc/ssh/ssh_host_*_key*
- /etc/ssh/ssh_host_key*

# To exclude all hidden files and directories in your home, uncomment the next line.
# You will lose custom desktop configs if you do.
#- /home/*/.[a-z,A-Z,0-9]*
EOF

generate_grub_template
}

check_grub () {
if [ "$make_efi" = "yes" ]; then
  if ! (dpkg -l | grep "^ii" | grep "grub-efi-amd64" |grep -v "bin"); then
	echo $"grub-efi-amd64 is not installed"
	grub_message=$"Warning: grub-efi-amd64 is not installed. The snapshot may not be compatible with UEFI.
To disable this warning, set make_efi=no in $configfile or set force_efi=yes for special use.
"
	[ "$force_efi" = "yes" ] && make_efi="yes" || make_efi="no"
	printf "force_efi is %s\\n make_efi is %s" "$force_efi" "$make_efi"
  fi
  if [ ! -e /var/lib/dpkg/info/dosfstools.list ]; then
	printf "dosfstools is not installed\\n"
	dosfstools_message=$"Warning: dosfstools is not installed. Your snapshot will not boot in uefi mode."
	force_efi="no";	make_efi="no"
	printf "force_efi is %s\\nmake_efi is %s" "$force_efi" "$make_efi"
  fi
fi
}

unpatch_init () {
#  Check for previous patch.
if $(grep -q nuke "$target_file") ; then
	echo $"
It looks like $target_file was previously patched by an
earlier version of refractasnapshot. This patch is no longer needed.
You can comment out the added lines as shown below (or remove the
commented lines) and then run 'update-initramfs -u'.

If you don't want to do that, dont worry;
it won't hurt anything if you leave it the way it is.

Do not change or remove the lines that begin with \"mount\"

	mount -n -o move /sys ${rootmnt}/sys
	#nuke /sys
	#ln -s ${rootmnt}/sys /sys
	mount -n -o move /proc ${rootmnt}/proc
	#nuke /proc
	#ln -s ${rootmnt}/proc /proc
"
	while true; do
	  echo $"Open $target_file in an editor? (y/N)"
	  read -r "ans"
		case "$ans" in
		[Yy]*)	"$TEXT_EDITOR" "$target_file"
				update-initramfs -u; break;;
		*)	break;;
		esac
	done
printf "\\nWait for the disk report to complete...\\n"
fi
}

make_snapshot() {
# Check for grub-efi.
check_grub
# Create a message to say whether the filesystem copy will be saved or not.
if [ "$save_work" = "yes" ]; then
	save_message="The temporary copy of the filesystem will be saved at $work_dir/myfs.\\n"
else
	save_message="The temporary copy of the filesystem will be created at $work_dir/myfs and removed when this program finishes.\\n"
fi

# find the correct filenames for kernel and initrafms
startbyte=""; x="1"
while [ "$x" != "$(echo "$kernel_image" | wc -c)" ]; do
	x="$((x+1))"; [ "$(echo "$kernel_image" | cut -b "$x")" = "/" ] && startbyte="$x"
done
KERNEL_NAME=$(echo "$kernel_image" | cut -b "$startbyte"-)

startbyte=""; x="1"
while [ "$x" != "$(echo "$initrd_image" | wc -c)" ]; do
	x="$((x+1))"; [ "$(echo "$initrd_image" | cut -b "$x")" = "/" ] && startbyte="$x"
done

INITRAMFS_NAME=$(echo "$initrd_image" | cut -b "$startbyte"- )

# Check disk space on mounted /, /home, /media, /mnt, /tmp
check_space () {
printf "checking disk space ...\\n"
disk_space=$(df -h -x tmpfs -x devtmpfs -x iso9660 | awk '{ print "  " $2 "\t" $3 "\t" $4 "\t" $5 "  \t" $6 "\t\t\t" $1 }')
}

# Check initrd for cryptroot, resume, cryptsetup.
check_initrd () {
printf "checking initrd...\\n"
if lsinitramfs "$initrd_image" | grep -q conf.d/cryptroot; then
  remove_cryptroot="yes"
  cryptroot_message="The snapshot initrd will be modified to allow booting the unencrypted snapshot."
elif lsinitramfs "$initrd_image" | grep -q cryptroot | grep -Ev 'scripts|crypttab|bin'; then
  remove_cryptroot="yes"
  cryptroot_message="The snapshot initrd will be modified to allow booting the unencrypted snapshot."
elif [ "$(lsinitramfs "$initrd_image" | grep cryptroot/crypttab)" ] && [ ! "$initrd_crypt" = "yes" ]; then
  remove_cryptroot="yes"
  cryptroot_message="The snapshot initrd will be modified to allow booting the unencrypted snapshot."
fi
if lsinitramfs "$initrd_image" | grep -Eq 'conf.d/resume|conf.d/zz-resume-auto'; then
  remove_resume="yes"
  swap_message="The snapshot initrd will be modified to allow booting without the host's swap partition."
fi
if [ "$initrd_crypt" = yes ]; then
  if lsinitramfs "$initrd_image" | grep -q cryptsetup ; then
	crypt_message="The host initrd already allows live-usb encrypted persistence. No change is needed."
	initrd_crypt="no"
  else
	crypt_message="The host initrd will be modified to allow live-usb encrypted persistence.
A backup copy will be made at ${initrd_image}_pre-snapshot. (Does not apply to any re-run tasks.)"
  fi
fi
}

extract_initrd () {
[ ! -d /tmp/extracted ] && mkdir /tmp/extracted
cd /tmp/extracted
  if [ -L "$initrd_image" ]; then
    real_initrd_iname="$(file "$initrd_image" | awk '{print $NF}' | sed 's:^:/:')"
  else
    real_initrd_iname="$initrd_image"
  fi
  COMPRESSION=$(file -L "$real_initrd_iname" | grep -Eo 'gzip compressed|XZ compressed|cpio archive')
  if [ "$COMPRESSION" = "gzip compressed" ]; then
	echo "Archive is gzip compressed..."
	zcat "$real_initrd_iname" | cpio -i || check_exit
  elif [ "$COMPRESSION" = "XZ compressed" ]; then
	echo "Archive is XZ compressed..."
	xzcat "$real_initrd_iname" | cpio -d -i -m || check_exit
  elif [ "$COMPRESSION" = "cpio archive" ]; then
	echo "Archive is cpio archive...";
	(cpio -i ; zcat | cpio -i) < "$real_initrd_iname" || check_exit
  else
	echo "Decompression error..." && check_exit
  fi
cd "$work_dir"
echo "Initrd is extracted"
}

edit_initrd () {
cd /tmp/extracted
  if [ -f conf/conf.d/cryptroot ]; then
  	echo "Removing cryptroot"; rm -f conf/conf.d/cryptroot
  elif [ -f cryptroot ]; then
  	echo "Removing cryptroot"; rm -f cryptroot
  fi
  if [ -f conf/conf.d/resume ]; then
  	echo "Removing resume"; rm -f conf/conf.d/resume
  elif [ -f conf/conf.d/zz-resume-auto ]; then
  	echo "Removing resume"; rm -f conf/conf.d/zz-resume-auto
  fi
  [ -d cryptroot ] && rm -rd cryptroot
cd "$work_dir"
}

rebuild_initrd () {
cd /tmp/extracted
  if [ "$COMPRESSION" = "gzip compressed" ] || [ "$COMPRESSION" = "cpio archive" ]; then
	find . -print0 | cpio -0 -H newc -o | gzip -c > ${work_dir}/iso/live/${initrd_image##*/}
  elif [ "$COMPRESSION" = "XZ compressed" ]; then
	find . | cpio -o -H newc | xz --check=crc32 --x86 --lzma2=dict=512KiB > ${work_dir}/iso/live/${initrd_image##*/}
  else
	echo "Compression error..."; exit 1
  fi
cd "$work_dir"
rm -rf /tmp/extracted
}

clean_initrd () {
extract_initrd
edit_initrd
rebuild_initrd
}

report_space () {
# Show current settings and disk space
[ -f "/usr/bin/less" ] && pager="/usr/bin/less" || pager="/bin/more"
echo $"
 You will need plenty of free space. It is recommended that free space
 (Avail) in the partition that holds the work directory (probably \"/\")
 should be two times the total installed system size (Used). You can
 deduct the space taken up by previous snapshots and any saved copies of
 the system from the Used amount.
 ${grub_message}
 ${dosfstools_message}
 $save_message
 * The snapshot directory is currently set to $snapshot_dir
 $tmp_warning
 Turn off NUM LOCK for some laptops.

 ${crypt_message}
 ${cryptroot_message}
 ${swap_message}

 Current disk usage:
 $disk_space

 To proceed, press q.
 To exit, press q and then press ctrl-c
" | "$pager"
}

housekeeping () {
# Test for systemd, util-linux version and patch intramfs-tools/init.
if [ "$patch_init_nosystemd" = "yes" ]; then
  utillinux_version=$(dpkg -l util-linux | awk '/util-linux/ { print $3 }' | cut -d '.' -f 2)
  target_file="/usr/share/initramfs-tools/init"
# patch_file="/usr/lib/refractasnapshot/init_for_util-lin.patch"
  [ ! -h /sbin/init ] && [ "$utillinux_version" -ge 25 ] && unpatch_init
fi

# Use the login name set in the config file. If not set, use the primary user's name.
# If the name is not "user" then add boot option. Also use the same username for cleaning geany history.
if [ -n "$username" ]; then
  username_opt="username=$username"
else
  username=$(awk -F":" '/1000:1000/ { print $1 }' /etc/passwd)
  [ "$username" != user ] && username_opt="username=$username"
fi

# Check that kernel and initrd exist
[ -e "$kernel_image" ] || kernel_message=" Warning:   Kernel image is missing. "
[ -e "$initrd_image" ] || initrd_message=" Warning:   initrd image is missing. "
if [ -n "$kernel_message" ] || [ -n "$initrd_message" ]; then
	echo $"
$kernel_message
$initrd_message

 Make sure the kernel_image and/or initrd_image
 set in the config file are correct, and check
 that the boot menu is also correct.
"
	exit 1
fi
# update the mlocate database
[ "$update_mlocate" = "yes" ] && printf "\\nRunning updatedb...\\n" && updatedb
}

prepare_initrd_crypt () {
# Prepare initrd to use encryption
# This is only going to work if the latest kernel version is running.
# (i.e. the one linked from /initrd.img)
# Add '-k all' or specify the initrd to use???
cp "$initrd_image" "${initrd_image}_pre-snapshot"
sed -i 's/.*CRYPTSETUP=.*/CRYPTSETUP=y/' /etc/cryptsetup-initramfs/conf-hook
if [ -f /usr/sbin/update-initramfs.orig.initramfs-tools ]; then
  /usr/sbin/update-initramfs.orig.initramfs-tools -u
else
  /usr/sbin/update-initramfs -u
fi
}

copy_isolinux () {
if [ -f /usr/lib/ISOLINUX/isolinux.bin ]; then
  isolinuxbin="/usr/lib/ISOLINUX/isolinux.bin"
elif [ -f /usr/lib/syslinux/isolinux.bin ]; then
  isolinuxbin="/usr/lib/syslinux/isolinux.bin"
else
  echo "You need to install the isolinux package."; exit 1
fi

# @@@@  Warning: This will replace these files in custom iso_dir  @@@@@
if [ -f /usr/lib/syslinux/modules/bios/vesamenu.c32 ]; then
  vesamenu="/usr/lib/syslinux/modules/bios/vesamenu.c32"
  rsync -av /usr/lib/syslinux/modules/bios/chain.c32 "$iso_dir"/isolinux/
  rsync -av /usr/lib/syslinux/modules/bios/ldlinux.c32 "$iso_dir"/isolinux/
  rsync -av /usr/lib/syslinux/modules/bios/libcom32.c32 "$iso_dir"/isolinux/
  rsync -av /usr/lib/syslinux/modules/bios/libutil.c32 "$iso_dir"/isolinux/
else
  vesamenu="/usr/lib/syslinux/vesamenu.c32"
fi
rsync -av "$isolinuxbin" "$iso_dir"/isolinux/
rsync -av "$vesamenu" "$iso_dir"/isolinux/

# Add Refracta-specific boot help files
if [ "$refracta_boot_help" = "yes" ]; then
  cp -a /usr/lib/refractasnapshot/boot_help/*  "$iso_dir"/isolinux/
fi

# copy isolinux files
rsync -av "$iso_dir"/ "$work_dir"/iso/
# this needs to be adapted

# generate /isolinux/live.cfg
[ "$DISABLE_IPV6" = "yes" ] && ipv6_opt="ipv6.disable=1" || ipv6_opt=""
KEYBOARD_LAYOUT="$(grep 'XKBLAYOUT' /etc/default/keyboard | cut -d\" -f 2)"
if [ -n "$KEYBOARD_LAYOUT" ]; then
  KBD_OPT="keyboard-layouts=$KEYBOARD_LAYOUT"
fi
LACALE_LANG="$(cut -d "=" -f 2 /etc/default/locale)"
if [ -n "$LOCALE_LANG" ]; then
  LOCALE_OPT="locales=$LOCALE_LANG"
fi
cat > "$work_dir"/iso/isolinux/live.cfg <<EOF
label live
	menu label ${DISTRO} (default)
	kernel /live/vmlinuz
	append initrd=/live/initrd.img boot=live ${ifnames_opt} ${netconfig_opt} ${username_opt} ${ipv6_opt} modprobe.blacklist=pcspkr

label lang
	menu label Other language (TAB to edit)
	kernel /live/vmlinuz
	append initrd=/live/initrd.img boot=live ${ifnames_opt} ${netconfig_opt} ${username_opt} ${LOCALE_OPT} ${KBD_OPT} ${ipv6_opt} modprobe.blacklist=pcspkr

label toram
	menu label ${DISTRO} (to RAM)
	kernel /live/vmlinuz
	append initrd=/live/initrd.img boot=live toram ${ifnames_opt} ${netconfig_opt} ${username_opt} ${ipv6_opt} modprobe.blacklist=pcspkr

label failsafe
	menu label ${DISTRO} (failsafe)
	kernel /live/vmlinuz noapic noapm nodma nomce nolapic nosmp forcepae nomodeset vga=normal ${ifnames_opt} ${netconfig_opt} ${username_opt}
	append initrd=/live/initrd.img boot=live

label memtest
	menu label Memory test
	kernel /live/memtest

label chain.c32 hd0,0
	menu label Boot hard disk
	chain.c32 hd0,0

label harddisk
	menu label Boot hard disk (old way)
	localboot 0x80
EOF

# generate /isolinux/menu.cfg
cat > "$work_dir"/iso/isolinux/menu.cfg <<EOF
menu hshift 6
menu width 64

menu title Live Media
include stdmenu.cfg
include live.cfg
label help
	menu label Help
	config prompt.cfg
EOF

# generate /isolinux/prompt.cfg
cat > "$work_dir"/iso/isolinux/prompt.cfg <<EOF
prompt 1
display f1.txt
timeout 0
include menu.cfg
include exithelp.cfg

f1 f1.txt
f2 f2.txt
f3 f3.txt
f4 f4.txt
f5 f5.txt
f6 f6.txt
f7 f7.txt
f8 f8.txt
f9 f9.txt
EOF

# generate /isolinux/stdmenu.cfg
cat > "$work_dir"/iso/isolinux/stdmenu.cfg <<EOF
menu background /isolinux/splash.png
menu color title	* #FFFFFFFF *
menu color border	* #00000000 #00000000 none
menu color sel		* #ffffffff #686373 *
menu color hotsel	1;7;37;40 #dad9dc #686373 *
menu color tabmsg	* #dad9dc #00000000 *
menu color cmdline 0 #dad9dc #00000000
menu color help		37;40 #ffdddd00 #00000000 none
menu vshift 8
menu rows 12
#menu helpmsgrow 15
#menu cmdlinerow 25
#menu timeoutrow 26
#menu tabmsgrow 14
menu tabmsg Press ENTER to boot or TAB to edit a menu entry
EOF

# generate /isolinux/isolinux.cfg
cat > "$work_dir"/iso/isolinux/isolinux.cfg <<EOF
include menu.cfg
default /isolinux/vesamenu.c32
prompt 0
timeout 200
EOF

# generate /isolinux/exithelp.cfg
cat > "$work_dir"/iso/isolinux/exithelp.cfg <<EOF
label menu
	kernel /isolinux/vesamenu.c32
	config isolinux.cfg
EOF

}
# Let iso/, vmlinuz and initrd.img get copied, even if work_dir was saved, in case they have changed.

copy_kernel () {
cp -v "$kernel_image" "$work_dir"/iso/live/
cp -v "$initrd_image" "$work_dir"/iso/live/
}

copy_filesystem () {
if [ -f /usr/bin/cpulimit ] && [ "$limit_cpu" = "yes" ]; then
  cpulimit -e rsync -l "$limit" &
  pid="$!"
fi
rsync -av / "$work_dir/myfs/" ${rsync_option1} ${rsync_option2} ${rsync_option3} \
--exclude="$work_dir" --exclude="$snapshot_dir" --exclude="$efi_work" --exclude-from="$tempdir/snapshot_exclude.list"
[ -n "$pid" ] && kill "$pid"
}

edit_system () {
# Truncate logs, remove archived logs.
find myfs/var/log -name "*gz" -print0 | xargs -0r rm -f
find myfs/var/log/ -type f -exec truncate -s 0 {} \;

# Allow all fixed drives to be mounted with pmount
if [ "$pmount_fixed" = "yes" ] && [ -f "$work_dir"/myfs/etc/pmount.allow ]; then
  sed -i 's:#/dev/sd\[a-z\]:/dev/sd\[a-z\]:' "$work_dir"/myfs/etc/pmount.allow
fi

# Clear list of recently used files in geany for primary user.
[ "$clear_geany" = "yes" ] && sed -i 's/recent_files=.*;/recent_files=/' "$work_dir"/myfs/home/"$username"/.config/geany/geany.conf

# Enable or disable password login through ssh for users (not root)
# Remove obsolete live-config file
[ -e "$work_dir"/myfs/lib/live/config/1161-openssh-server ] && rm -vf "$work_dir"/myfs/lib/live/config/1161-openssh-server
if [ -f "$work_dir"/myfs/etc/ssh/sshd_config ]; then
  sed -i 's/PermitRootLogin yes/PermitRootLogin prohibit-password/' "$work_dir"/myfs/etc/ssh/sshd_config

  if [ "$ssh_pass" = "yes" ]; then
	sed -i 's|.*PasswordAuthentication.*no|PasswordAuthentication yes|' "$work_dir"/myfs/etc/ssh/sshd_config
#	sed -i 's|#.*PasswordAuthentication.*yes|PasswordAuthentication yes|' "$work_dir"/myfs/etc/ssh/sshd_config
  elif [ $ssh_pass = "no" ]; then
	sed -i 's|.*PasswordAuthentication.*yes|PasswordAuthentication no|' "$work_dir"/myfs/etc/ssh/sshd_config
  fi
fi

# /etc/fstab should exist, even if it's empty,to prevent error messages at boot
touch "$work_dir"/myfs/etc/fstab

# Blank out systemd machine id. If it does not exist, systemd-journald
# will fail, but if it exists and is empty, systemd will automatically
# set up a new unique ID.
if [ -e "$work_dir"/myfs/etc/machine-id ]; then
  rm -f "$work_dir"/myfs/etc/machine-id
  : > "$work_dir"/myfs/etc/machine-id
fi

# add some basic files to /dev
mknod -m 622 "$work_dir"/myfs/dev/console c 5 1
mknod -m 666 "$work_dir"/myfs/dev/null c 1 3
mknod -m 666 "$work_dir"/myfs/dev/zero c 1 5
mknod -m 666 "$work_dir"/myfs/dev/ptmx c 5 2
mknod -m 666 "$work_dir"/myfs/dev/tty c 5 0
mknod -m 444 "$work_dir"/myfs/dev/random c 1 8
mknod -m 444 "$work_dir"/myfs/dev/urandom c 1 9
#chown -v root:tty "$work_dir"/myfs/dev/{console,ptmx,tty}
chown -v root:tty "$work_dir"/myfs/dev/console
chown -v root:tty "$work_dir"/myfs/dev/ptmx
chown -v root:tty "$work_dir"/myfs/dev/tty

ln -sv /proc/self/fd "$work_dir"/myfs/dev/fd
ln -sv /proc/self/fd/0 "$work_dir"/myfs/dev/stdin
ln -sv /proc/self/fd/1 "$work_dir"/myfs/dev/stdout
ln -sv /proc/self/fd/2 "$work_dir"/myfs/dev/stderr
ln -sv /proc/kcore "$work_dir"/myfs/dev/core
mkdir -v "$work_dir"/myfs/dev/shm
mkdir -v "$work_dir"/myfs/dev/pts
chmod 1777 "$work_dir"/myfs/dev/shm

# Clear configs from /etc/network/interfaces, wicd and NetworkManager
# and netman, so they aren't stealthily included in the snapshot.
#if [ -z "$netconfig_opt" ]; then

if [ -f "$work_dir"/myfs/etc/iptables.conf ]; then
cat > "$work_dir"/myfs/etc/network/interfaces <<EOF
# The loopback network interface
auto lo
iface lo inet loopback

#allow-hotplug eth0
#iface eth0 inet dhcp

# Iptables
up iptables-restore < /etc/iptables.conf
up ip6tables-restore < /etc/iptables.conf
EOF
else
cat > "$work_dir"/myfs/etc/network/interfaces <<EOF
# The loopback network interface
auto lo
iface lo inet loopback

#allow-hotplug eth0
#iface eth0 inet dhcp
EOF
fi

rm -vf "$work_dir"/myfs/var/lib/wicd/configurations/*
rm -vf "$work_dir"/myfs/etc/wicd/wireless-settings.conf
rm -vf "$work_dir"/myfs/etc/NetworkManager/system-connections/*
rm -vf "$work_dir"/myfs/etc/network/wifi/*
#fi
}

get_filename () {
# Need to define $filename here (moved up from genisoimage)
# and use it as directory name to identify the build on the cdrom.
# and put package list inside that directory
if [ "$stamp" = "datetime" ]; then
  # use this variable so iso and sha256 have same time stamp
  filename="$snapshot_basename"-$(date +%Y%m%d_%H%M).iso
elif [ -z "$stamp" ]; then
  n=1
  while [ -f "$snapshot_dir"/snapshot$n.iso ]; do n="$((n+1))"; done
  filename="$snapshot_basename"$n.iso
else
  filename="$snapshot_basename"-"$stamp".iso
fi
}

# create /boot and /efi for uefi.
mkefi () {
uefi_opt="-eltorito-alt-boot -e boot/grub/efiboot.img -isohybrid-gpt-basdat -no-emul-boot"
#################################

# for initial grub.cfg
mkdir -vp "$tempdir"/boot/grub

cat > "$tempdir"/boot/grub/grub.cfg <<EOF
search --file --set=root /isolinux/isolinux.cfg
set prefix=(\$root)/boot/grub
source \$prefix/x86_64-efi/grub.cfg
EOF

[ ! -d "$efi_work" ] && mkdir -vp "$efi_work"
# start with empty directories.
[ -d "boot" ] && rm -vrf "$efi_work/boot"
[ -d "efi" ] && rm -vrf "$efi_work/efi"
mkdir -vp "$efi_work/boot/grub/x86_64-efi"
mkdir -vp "$efi_work/efi/boot"
cd "$efi_work"

# copy splash
cp "$iso_dir"/isolinux/splash.png boot/grub/splash.png

# second grub.cfg file
for i in $(ls /usr/lib/grub/x86_64-efi|grep part_|grep \.mod|sed 's/.mod//'); do
  echo "insmod $i" >> boot/grub/x86_64-efi/grub.cfg
done

# Additional modules so we don't boot in blind mode. I don't know which ones are really needed.
for i in efi_gop efi_uga ieee1275_fb vbe vga video_bochs video_cirrus jpeg png gfxterm ; do
  echo "insmod $i" >> boot/grub/x86_64-efi/grub.cfg
done

echo "source /boot/grub/grub.cfg" >> boot/grub/x86_64-efi/grub.cfg

cd 	"$tempdir"
  # make a tarred "memdisk" to embed in the grub image
  tar -cvf memdisk boot
  # make the grub image
  grub-mkimage -O "x86_64-efi" -m "memdisk" -o "bootx64.efi" -p '(memdisk)/boot/grub' search iso9660 configfile normal memdisk tar cat part_msdos part_gpt fat ext2 ntfs ntfscomp hfsplus chain boot linux		
  cd "$efi_work"
	# copy the grub image to efi/boot (to go later in the device's root)
	cp "$tempdir"/bootx64.efi efi/boot

	## Do the boot image "boot/grub/efiboot.img"
	dd if=/dev/zero of=boot/grub/efiboot.img bs=1K count=1440
#	/sbin/mkdosfs -F 12 boot/grub/efiboot.img
	mkdosfs -F 12 boot/grub/efiboot.img
	mkdir img-mnt
	mount -o loop boot/grub/efiboot.img img-mnt
	mkdir -p img-mnt/efi/boot
	cp "$tempdir"/bootx64.efi img-mnt/efi/boot/

	# copy modules and font
	cp /usr/lib/grub/x86_64-efi/* boot/grub/x86_64-efi/

	# if this doesn't work try another font from the same place (grub's default, unicode.pf2, is much larger)
	# Either of these will work, and they look the same to me. Unicode seems to work with qemu. -fsr
#	cp /usr/share/grub/ascii.pf2 boot/grub/font.pf2
	cp /usr/share/grub/unicode.pf2 boot/grub/font.pf2

	# doesn't need to be root-owned
	chown -R 1000:1000 "$(pwd)" 2>/dev/null

	# Cleanup efi temps
	umount img-mnt ; rmdir img-mnt
  cd "$work_dir"

# Copy efi files to iso
rsync -avx "$efi_work"/boot "$work_dir"/iso/
rsync -avx "$efi_work"/efi  "$work_dir"/iso/

# Do the main grub.cfg (which gets loaded last):
cp "$grub_template" "$work_dir"/iso/boot/grub/grub.cfg
}

set_boot_options () {
# Create the boot menu unless iso_dir is not default.
if [ "$iso_dir" = "/usr/lib/refractasnapshot/iso" ]; then
  sed -i "s:\${DISTRO}:$DISTRO:g" "$work_dir"/iso/isolinux/"$boot_menu"
  sed -i "s:\${netconfig_opt}:$netconfig_opt:g" "$work_dir"/iso/isolinux/"$boot_menu"
  sed -i "s:\${ifnames_opt}:$ifnames_opt:g" "$work_dir"/iso/isolinux/"$boot_menu"
  sed -i "s:\${username_opt}:$username_opt:g" "$work_dir"/iso/isolinux/"$boot_menu"
fi

if [ "$make_efi" = "yes" ]; then
  sed -i "s:\${DISTRO}:$DISTRO:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:\${netconfig_opt}:$netconfig_opt:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:\${username_opt}:$username_opt:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:\${ifnames_opt}:$ifnames_opt:g" "$work_dir"/iso/boot/grub/grub.cfg
fi

if [ "$kernel_image" != "/vmlinuz" ]; then
  sed -i "s:/live/vmlinuz:/live/$KERNEL_NAME:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:/live/vmlinuz:/live/$KERNEL_NAME:g" "$work_dir"/iso/isolinux/"$boot_menu"
fi

if [ "$initrd_image" != "/initrd.img" ]; then
  sed -i "s:/live/initrd.img:/live/$INITRAMFS_NAME:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:/live/initrd.img:/live/$INITRAMFS_NAME:g" "$work_dir"/iso/isolinux/"$boot_menu"
fi
}

edit_boot_menus () {
if [ "$edit_boot_menu" = "yes" ]; then
  printf "\\nYou may now go to another virtual console to edit any files in\\n"
  printf "the work directory, or hit ENTER and edit the boot menu.\\n"
  read -r "ans"
  "$TEXT_EDITOR" "$work_dir"/iso/isolinux/"$boot_menu"
  [ "$make_efi" = "yes" ] && "$TEXT_EDITOR" "$work_dir"/iso/boot/grub/grub.cfg
fi
}

squash_filesystem () {
if [ "$WAIT_OPT" = "yes" ]; then
  printf "\\n###############################################"
  printf "\\nYou now can edit $snapshot_dir"
  printf "\\nPress Enter when you are ready"
  read -r "ans"
fi

echo "Squashing the filesystem..."
if [ "$limit_cpu" = "yes" ]; then
  cpulimit -e mksquashfs -l "$limit" &
  pid="$!"
fi
#  mksquashfs myfs/ iso/live/filesystem.squashfs ${mksq_opt} -noappend
case "$SQUASHFS_COMPRESSION" in
  xz-smaller) mksquashfs myfs/ iso/live/filesystem.squashfs -comp xz -Xbcj x86 -noappend;;
  xz)	  mksquashfs myfs/ iso/live/filesystem.squashfs -comp xz -noappend;;
  gzip) mksquashfs myfs/ iso/live/filesystem.squashfs -noappend;;
  *)	  mksquashfs myfs/ iso/live/filesystem.squashfs -noappend;;
esac
[ -n "$pid" ] && kill "$pid"
[ "$save_work" = "no" ] && [ -d "$work_dir/myfs" ] && rm -vrf "$work_dir/myfs"
}

make_iso_fs () {
echo "Creating CD/DVD image file..."
# If isohdpfx.bin gets moved again, maybe use:   isohdpfx=$(find /usr/lib/ -name isohdpfx.bin)
if [ "$make_isohybrid" = "yes" ]; then
  if [ -f /usr/lib/syslinux/mbr/isohdpfx.bin ]; then
    isohybrid_opt="-isohybrid-mbr /usr/lib/syslinux/mbr/isohdpfx.bin"
  elif [ -f /usr/lib/syslinux/isohdpfx.bin ]; then
    isohybrid_opt="-isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin"
  elif [ -f /usr/lib/ISOLINUX/isohdpfx.bin ]; then
    isohybrid_opt="-isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin"
  else
   printf "Can't create isohybrid.\\nFile: isohdpfx.bin not found.\\nThe resulting image will be a standard iso file.\\n"
  fi
fi

[ -n "$volid" ] || volid="liveiso"
xorriso -as mkisofs -r -J -joliet-long -l -iso-level 3 ${isohybrid_opt} \
-partition_offset 16 -V "$volid"  -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot \
-boot-load-size 4 -boot-info-table ${uefi_opt} -o "$snapshot_dir"/"$filename" iso/

}

add_extras () {
cd "$snapshot_dir"
  [ "$make_sha256sum" = "yes" ] && sha256sum "$filename" > "$filename".sha256
  [ "$make_pkglist" = "yes" ] && dpkg -l > "$filename".pkglist
cd "$work_dir"
# Add the Release Notes to the iso
#  if [ -f /usr/share/doc/_Release_Notes/Release_Notes ] ; then
#	rsync -a /usr/share/doc/_Release_Notes/Release_Notes "$work_dir"/iso/
# fi
}

cleanup_snapshot () {
if [ "$save_work" = "no" ]; then
  printf "Cleaning...\\n"; cd /
  [ -d "$work_dir" ] && rm -rf "$work_dir"
else
  rm "$work_dir"/iso/live/filesystem.squashfs
fi
}

final_message () {
printf "\\nComplete!\\n"
printf "%s/%s\\n" "$snapshot_dir" "$filename"
du -h "$snapshot_dir/$filename" | awk '{print $1}' | sed 's/$/B/'
awk '{print $1}' "$snapshot_dir/$filename.sha256"
FINAL_MSG=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT"  "All finished!" ""\
		  "File   = $snapshot_dir/$filename"\
		  "Size   = $(du -h $snapshot_dir/$filename | awk '{print $1}' | sed 's/$/B/')"\
		  "Sha256 = $(awk '{print $1}' $snapshot_dir/$filename.sha256)" ""\
		  "  > test ISO in Virtual Machine<s:RUN_QEMU>"\
		  "  > burn ISO to USB Device<s:MAKE_USB>"\
		  "  > add ISO to multiboot usb<s:MAKE_MULTI>"\
		  "  > Exit<s>"\
		  -p 10)

case "$FINAL_MSG" in
  RUN_QEMU)	[ -e /dev/kvm ] && KVM_FLAG="-enable-kvm -cpu host"
			qemu-system-x86_64 ${KVM_FLAG} -m 2560 --drive format=raw,file="$snapshot_dir"/"$filename";;
  MAKE_USB)	WARNING="(WARNING: ALL DATA ON THIS DEVICE WILL BE LOST)"; select_usb
	SOURCE_FILE="$snapshot_dir/$filename"
	[ ! -f "$SOURCE_FILE" ] && { echo "Error, ISO file not found."; exit 1; }
	if [ -f "$SOURCE_FILE" ] && [ -b "$TARGET_DEVICE" ]; then
	  printf "\\n SOURCE_FILE=\\033[1;33m %s %s\\033[0m \\n\\n TARGET_DEVICE=\\033[1;33m%s\\033[0m\\n" "$SOURCE_FILE" "$(du -h "$SOURCE_FILE"| awk '{print $1}')" "$TARGET_DEVICE"
	  lsblk "$TARGET_DEVICE" -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL| head -n 1
	  printf "\\033[1;33m %s\\033[0m\\n" "$(lsblk "$TARGET_DEVICE" -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL|tail -n 1)"
	  printf "\\n Hit Enter to Start or Control+C to Cancel\\033[1;33m \\n (Warning: All Data on %s will be lost!)\\033[0m \\n" "$TARGET_DEVICE"
	  read -r "confirm" && printf "\\n This can take a while...\\n"
	  dd if=/dev/zero of="$TARGET_DEVICE" bs=512 count=4096 oflag=direct,nocache
	  dd if="$SOURCE_FILE" of="$TARGET_DEVICE" bs=4M status=progress && sync
	  printf "Done!\\n"
    fi; exit 0;;
  MAKE_MULTI) 	SOURCE_FILE="$snapshot_dir/$filename"; multi_usb;;
  *) exit 0;;
esac
}

DISTRO="$(grep -w 'NAME' /etc/os-release | cut -d '"' -f 2) $(uname -r)"
if [ "$make_efi" = "yes" ]; then uefi_message="uefi enabled"; else uefi_message="uefi disabled"; fi

while [ -z "$SNAPSHOT_MENU_EVENT" ]; do
[ -f /usr/bin/cpulimit ] && CPU_LIMIT_PLACEHOLDER=" > limit_cpu = $limit_cpu <s:SET_CPU_LIMIT>
 > limit = $limit" || { CPU_LIMIT_PLACEHOLDER=" > limit_cpu = $limit_cpu (not available)
 > limit = (not available)"; limit_cpu="no"; }

SNAPSHOT_MENU_LIST="
Choose a task:
 > Create a snapshot ($uefi_message) <s:MAKE_SNAPSHOT>
 > Re-squash and make iso (no-copy) <s:RESQUASH>
 > Re-make efi files and iso (no-copy, no-squash) <s:REEFI>
 > Re-run xorriso only. (make iso, no-copy, no-squash) <s:REXORR>
 > Clear Work directory <s:CLEAR_DIR>

Options:
 > Distro name = $DISTRO <s:SET_DISTRO_NAME>
 > snapshot_basename = $snapshot_basename <s:SET_SNAPSHOT_BASENAME>
 > stamp = $stamp <s:SET_STAMP>
 > Snapshot directory = $snapshot_dir <s:SET_SNAPSHOT_DIR>
 > Working directory = $work_dir <s:SET_WORK_DIR>
 > save_work = $save_work <s:SET_SAVE_WORK>
 > edit excludes list <s:EDIT_EXCLUDES>
 > make_efi = $make_efi<s:EFI>
 > make_isohybrid = $make_isohybrid<s:HYBRID>
 > squashfs compression = $SQUASHFS_COMPRESSION <s:SET_SQUASHFS_COMPRESSION>
 > kernel_image = $kernel_image<s:SET_KERN_IMG>
 > initrd_image = $initrd_image<s:SET_INIT_IMG>
${CPU_LIMIT_PLACEHOLDER}
 > disable IPv6 = $DISABLE_IPV6<s:SET_IPV6>
 > Insert pause befor creating the iso file = $WAIT_OPT<s:SET_PAUSE>

 > volid = $volid

 < Exit <s:EXIT>"

[ -z "$STARTLINE" ] && STARTLINE=3
SNAPSHOT_MENU_EVENT=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$SNAPSHOT_MENU_LIST" -p $STARTLINE)
case "$SNAPSHOT_MENU_EVENT" in
  MAKE_SNAPSHOT)
#	echo "This may take a moment while the program checks for free space."
	check_directories; check_space; check_initrd; check_mounts
#	report_space
	housekeeping
	[ "$initrd_crypt" = "yes" ] && prepare_initrd_crypt
	cd "$work_dir"; copy_isolinux; copy_kernel
	  [ "$remove_cryptroot" = "yes" ] || [ "$remove_resume" = "yes" ] && clean_initrd
	  copy_filesystem; edit_system; get_filename
	  [ "$make_efi" = "yes" ] && mkefi
	  set_boot_options
	  [ "$edit_boot_menu" = "yes" ] && edit_boot_menus
	  squash_filesystem; make_iso_fs; add_extras; cleanup_snapshot; final_message; exit 0;;
  RESQUASH)	[ "$make_efi" = "yes" ] && uefi_opt="-eltorito-alt-boot -e boot/grub/efiboot.img -isohybrid-gpt-basdat -no-emul-boot"
			cd "$work_dir"; get_filename; squash_filesystem; make_iso_fs; add_extras; final_message; exit 0;;
  REEFI) [ "$make_efi" = "yes" ] || exit 1
		 cd "$work_dir"; get_filename; mkefi; set_boot_options; find_editor; edit_boot_menus
	 	 make_iso_fs; add_extras; final_message; exit 0;;
  REXORR) [ "$make_efi" = "yes" ] && uefi_opt="-eltorito-alt-boot -e boot/grub/efiboot.img -isohybrid-gpt-basdat -no-emul-boot"
	cd "$work_dir"; get_filename; make_iso_fs; add_extras; final_message; exit 0;;
  CLEAR_DIR) STARTLINE="7"; SNAPSHOT_MENU_EVENT=""; [ -d "$work_dir" ] && rm -vrd "$work_dir"/* ;;
  SET_DISTRO_NAME) STARTLINE="10"; SNAPSHOT_MENU_EVENT=""; clear
				   DISTRO="$(grep -w 'NAME' /etc/os-release | cut -d '"' -f 2) $(uname -r)"
				   while true; do
					 printf "\\n\\tThis is the distribution name that will appear in\\n\\tthe boot menu for the live image.\\n"
#		# Redirect stderr from the error log to the screen, so we can see the prompts from read.
	  				 exec 2>&1
	  				 printf "\\n\\tEnter distro name: "
					 read -r "ans"
#		# Resume logging errors.
					 exec 2>>"$ERROR_LOG"
				 	 break
				   done
				   [ ! -z "$ans" ] && DISTRO="$ans";;
  SET_SNAPSHOT_BASENAME) STARTLINE="11"; SNAPSHOT_MENU_EVENT=""; NEW_BASENAME=""
				while [ -z "$NEW_BASENAME" ]; do
				 clear; printf "\\n\\tThis is the base name for the iso file\\n"
				 printf "%s\\nEnter basename: " "$NEW_NAME_STATUS"; read -r "NEW_NAME"; check_name_status
				 [ "$NEW_NAME_STATUS" = "OK" ] && { NEW_BASENAME="$NEW_NAME"; snapshot_basename="$NEW_BASENAME"; } || NEW_BASENAME=""
				done;;
  SET_STAMP) STARTLINE="12"; SNAPSHOT_MENU_EVENT=""; NEW_STAMP=""
				while [ -z "$NEW_STAMP" ]; do
				 clear; printf "%s\\nEnter stamp: " "$NEW_NAME_STATUS"; read -r "NEW_NAME"; check_name_status
				 [ "$NEW_NAME_STATUS" = "OK" ] && { NEW_STAMP="$NEW_NAME"; stamp="$NEW_STAMP"; } ||	NEW_STAMP=""
				done;;
  SET_SNAPSHOT_DIR) STARTLINE="13"; SNAPSHOT_MENU_EVENT=""; clear
  	printf "\\n\\tThe current snapshot directory is %s\\n" "$snapshot_dir"
  	printf "\\tThis is where the .iso file will be created.\\n"
  	printf "\\t(make sure to have some free space)\\n"
  	printf "\\n\\tEnter a path for the snapshot directory: "; NEW_PATH_STATUS="";
	while [ "$NEW_PATH_STATUS" != "OK" ]; do
	  read -r "NEW_PATH"
	  if [ -n "$NEW_PATH" ]; then
	    check_filepath
	  else
	    break
	  fi
	done
	if [ -n "$NEW_PATH" ] && [ "$NEW_PATH_STATUS" = "OK" ]; then
	  snapshot_dir="/$NEW_PATH"
	fi
	if [ ! -d "$snapshot_dir" ]; then
	  mkdir -pv "$snapshot_dir"
	fi;;
  SET_SAVE_WORK) STARTLINE="15"; SNAPSHOT_MENU_EVENT=""
  				 [ "$save_work" = "yes" ] && save_work="no" || save_work="yes";;
  SET_WORK_DIR) STARTLINE="14"; SNAPSHOT_MENU_EVENT=""; NEW_PATH_STATUS=""
  				clear; printf "\\n\\tThe current working directory is %s\\n" "$work_dir"
			  	printf "\\t(make sure to have some free space)\\n\\tEnter a path for the working directory: "
				while [ "$NEW_PATH_STATUS" != "OK" ]; do read -r "NEW_PATH"; check_filepath; done;
				[ "$NEW_PATH" ] && work_dir="/$NEW_PATH"
				[ -d "$work_dir" ] || mkdir -p "$work_dir";;
  EDIT_EXCLUDES) STARTLINE="16"; SNAPSHOT_MENU_EVENT=""; list_editors
   				 TEXT_EDITOR=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" \
							  "Select a texteditor" "$EDITOR_LIST" -p 2)
 				 "$TEXT_EDITOR" "$snapshot_excludes";;
  EFI) STARTLINE="17"; SNAPSHOT_MENU_EVENT=""
  	   if [ "$make_efi" = "yes" ]; then
  	     make_efi="no"; uefi_message="uefi disabled"
  	   elif [ "$make_efi" = "no" ]; then
  	     make_efi="yes"; uefi_message="uefi enabled"
  	   fi;;
  HYBRID) STARTLINE="18"; SNAPSHOT_MENU_EVENT=""
    	   [ "$make_isohybrid" = "yes" ] && make_isohybrid="no"|| make_isohybrid="yes";;
  SET_SQUASHFS_COMPRESSION) STARTLINE="19"; SNAPSHOT_MENU_EVENT=""
							  case $SQUASHFS_COMPRESSION in
								gzip)		  SQUASHFS_COMPRESSION="xz";;
								xz)		  SQUASHFS_COMPRESSION="xz-smaller";;
								xz-smaller) SQUASHFS_COMPRESSION="gzip";;
							  esac;;
  SET_KERN_IMG) STARTLINE="20"; SNAPSHOT_MENU_EVENT=""
			    x=$(find / -maxdepth 1 -iname '*vmlinuz*'; find /boot -maxdepth 1 -iname '*vmlinuz*')
			    y=""; for n in $x; do y=$(echo "$y\\n$n<s>"); done
			    kernel_image=$(iselect "Select the kernel file" "$y" -p 3);;
  SET_INIT_IMG) STARTLINE="21"; SNAPSHOT_MENU_EVENT=""
			    x=$(find / -maxdepth 1 -iname '*initrd*'; find /boot -maxdepth 1 -iname '*initrd*')
			    y=""; for n in $x; do y=$(echo "$y\\n$n<s>"); done
			    initrd_image=$(iselect "Select init file" "$y" -p 3);;
  SET_CPU_LIMIT) STARTLINE="22"; SNAPSHOT_MENU_EVENT=""
  				[ "$limit_cpu" = "yes" ] && limit_cpu="no" || limit_cpu="yes";;
  SET_IPV6)	STARTLINE="24"; SNAPSHOT_MENU_EVENT=""
				case $DISABLE_IPV6 in
					yes)	DISABLE_IPV6="no";;
					no)		DISABLE_IPV6="yes";;
				esac;;
  SET_PAUSE) STARTLINE="25"; SNAPSHOT_MENU_EVENT=""
                case $WAIT_OPT in
					yes)	WAIT_OPT="no";;
					no)		WAIT_OPT="yes";;
				esac;;
  *) exit 0;;
esac
done

rm -rf "$tempdir"
exit 0
}

#####################################################################################################################
#     __  ___      ____  _ __  _______ ____
#    /  |/  /_  __/ / /_(_) / / / ___// __ )
#   / /|_/ / / / / / __/ / / / /\__ \/ __  |
#  / /  / / /_/ / / /_/ / /_/ /___/ / /_/ /
# /_/  /_/\__,_/_/\__/_/\____//____/_____/

multi_usb() {
MULTIUSB_DIR="/mnt/multiusb"
ISO_DIR="/mnt/ISO"
[ -d "$MULTIUSB_DIR" ] || mkdir -p "$MULTIUSB_DIR"
[ -d "$ISO_DIR" ] || mkdir -p "$ISO_DIR"

#  █▀▀ █▀▄ █ █ █▀▄
#  █ █ █▀▄ █ █ █▀▄
#  ▀▀▀ ▀ ▀ ▀▀▀ ▀▀
create_new_usb_grub() {
# Exit if there is an unbound variable or an error
#set -o nounset
#set -o errexit

# Defaults
scriptname=$(basename "$0")
hybrid=0
#clone=0
eficonfig=0
interactive=0
data_part=2
data_fmt="vfat"
data_size=""
efi_mnt=""
data_mnt=""
data_subdir="boot"
repo_dir=""
tmp_dir="${TMPDIR-/tmp}"

# Show usage
showUsage() {
cat << EOF
	Script to prepare multiboot USB drive
	Usage: $scriptname [options] device [fs-type] [data-size]

	 device                         Device to modify (e.g. /dev/sdb)
	 fs-type                        Filesystem type for the data partition [ext3|ext4|vfat|ntfs]
	 data-size                      Data partition size (e.g. 5G)
	  -b,  --hybrid                 Create a hybrid MBR
	  -c,  --clone                  Clone Git repository on the device
	  -e,  --efi                    Enable EFI compatibility
	  -i,  --interactive            Launch gdisk to create a hybrid MBR
	  -h,  --help                   Display this message
	  -s,  --subdirectory <NAME>    Specify a data subdirectory (default: "boot")

EOF
}

# Clean up when exiting
cleanup_multi_usb() {
	# Change ownership of files
	{ [ "$data_mnt" ] && \
	    chown -R "$normal_user" "${data_mnt}"/* 2>/dev/null; } \
	    || true
	# Unmount everything
	umount -f "$efi_mnt" 2>/dev/null || true
	umount -f "$data_mnt" 2>/dev/null || true
	# Delete mountpoints
	[ -d "$efi_mnt" ] && rmdir "$efi_mnt"
	[ -d "$data_mnt" ] && rmdir "$data_mnt"
	[ -d "$repo_dir" ] && rmdir "$repo_dir"
	# Exit
#	exit "${1-0}"
}

# Make sure USB drive is not mounted
unmountUSB() {
	umount -f "${1}"* 2>/dev/null || true
}

# Trap kill signals (SIGHUP, SIGINT, SIGTERM) to do some cleanup and exit
trap 'cleanup_multi_usb' 1 2 15

# Show help before checking for root
[ "$#" -eq 0 ] && showUsage && exit 0
case "$1" in
	-h|--help) showUsage; exit 0;;
esac

# Check for root
if [ "$(id -u)" -ne 0 ]; then
	printf "This script must be run as root. Using sudo...\\n" >&2
	exec sudo -k -- /bin/sh "$0" "$@" || cleanup_multi_usb 2
fi

# Get original user
normal_user="${SUDO_USER-$(who -m | awk '{print $1}')}"

# Check arguments
while [ "$#" -gt 0 ]; do
	case "$1" in
		-b|--hybrid) hybrid="1";;
#		-c|--clone)	 clone="1";;
		-e|--efi)	 eficonfig="1"; data_part="3";;
		-i|--interactive)	interactive="1";;
		-s|--subdirectory)	shift && data_subdir="$1";;
		/dev/*)	if [ -b "$1" ]; then
				  TARGET_DEVICE="$1"
				else
				  printf '%s: %s is not a valid device.\n' "$scriptname" "$1" >&2
				  cleanup_multi_usb 1
				fi;;
		[a-z]*)	data_fmt="$1";;
		[0-9]*)	data_size="$1";;
		*)	printf '%s: %s is not a valid argument.\n' "$scriptname" "$1" >&2
			cleanup_multi_usb 1;;
	esac
	shift
done

# Check for required arguments
if [ ! "$TARGET_DEVICE" ]; then
	printf '%s: No device was provided.\n' "$scriptname" >&2
	showUsage
	cleanup_multi_usb 1
fi

# Check for GRUB installation binary
grub_cmd=$(command -v grub2-install) \
    || grub_cmd=$(command -v grub-install) \
    || cleanup_multi_usb 3

# Unmount device
unmountUSB "$TARGET_DEVICE"

# Confirm the device
printf 'Are you sure you want to use %s? [y/N] ' "$TARGET_DEVICE"
read -r answer1
case "$answer1" in
	[yY][eE][sS]|[yY]) printf 'THIS WILL DELETE ALL DATA ON THE DEVICE. Are you sure? [y/N] '
					   read -r answer2
					   case $answer2 in
						[yY][eE][sS]|[yY]) true;;
										*) cleanup_multi_usb 3;;
					   esac;;
					*) cleanup_multi_usb 3;;
esac

# Print all steps
#set -o verbose
#set -x

# Remove partitions
sgdisk --zap-all "$TARGET_DEVICE"

# Create GUID Partition Table
sgdisk --mbrtogpt "$TARGET_DEVICE" || cleanup_multi_usb 10

# Create BIOS boot partition (1M)
sgdisk --new 1::+1M --typecode 1:ef02 \
    --change-name 1:"BIOS boot partition" "$TARGET_DEVICE" || cleanup_multi_usb 10

# Create EFI System partition (50M)
[ "$eficonfig" -eq 1 ] && \
    { sgdisk --new 2::+50M --typecode 2:ef00 \
    --change-name 2:"EFI System" "$TARGET_DEVICE" || cleanup_multi_usb 10; }

# Set data partition size
[ -z "$data_size" ] || data_size="+$data_size"

# Set data partition information
case "$data_fmt" in
	ext2|ext3|ext4) type_code="8300"; part_name="Linux filesystem";;
	msdos|fat|vfat|ntfs|exfat) type_code="0700"; part_name="Microsoft basic data";;
	*) printf '%s: %s is an invalid filesystem type.\n' "$scriptname" "$data_fmt" >&2
	   showUsage; cleanup_multi_usb 1;;
esac

# Create data partition
sgdisk --new ${data_part}::"${data_size}": --typecode ${data_part}:"$type_code" \
    --change-name ${data_part}:"$part_name" "$TARGET_DEVICE" || cleanup_multi_usb 10

# Unmount device
unmountUSB "$TARGET_DEVICE"

partprobe "$TARGET_DEVICE"
sleep 0.1
# Interactive configuration?
if [ "$interactive" -eq 1 ]; then
	# Create hybrid MBR manually # https://bit.ly/2z7HBrP
	gdisk "$TARGET_DEVICE"
elif [ "$hybrid" -eq 1 ]; then
	# Create hybrid MBR
	if [ "$eficonfig" -eq 1 ]; then
		sgdisk --hybrid 1:2:3 "$TARGET_DEVICE" || cleanup_multi_usb 10
	else
		sgdisk --hybrid 1:2 "$TARGET_DEVICE" || cleanup_multi_usb 10
	fi
fi

# Set bootable flag for data partion
sgdisk --attributes ${data_part}:set:2 "$TARGET_DEVICE" || cleanup_multi_usb 10

# Unmount device
unmountUSB "$TARGET_DEVICE"

# Wipe BIOS boot partition
wipefs -af "${TARGET_DEVICE}1" || true

# Format EFI System partition
if [ "$eficonfig" -eq 1 ]; then
	wipefs -af "${TARGET_DEVICE}2" || true
	mkfs.vfat -v -F 32 -n EFI "${TARGET_DEVICE}2" || cleanup_multi_usb 10
fi

# Wipe data partition
wipefs -af "${TARGET_DEVICE}${data_part}" || true

# Format data partition
if [ "$data_fmt" = "ntfs" ]; then
	# Use mkntfs quick format
	mkfs -t "$data_fmt" -f "${TARGET_DEVICE}${data_part}" || cleanup_multi_usb 10
else
	mkfs -t "$data_fmt" "${TARGET_DEVICE}${data_part}" || cleanup_multi_usb 10
fi

# Unmount device
unmountUSB "$TARGET_DEVICE"

# Create temporary directories
efi_mnt=$(mktemp -p "$tmp_dir" -d efi.XXXX)   || cleanup_multi_usb 10
data_mnt=$(mktemp -p "$tmp_dir" -d data.XXXX) || cleanup_multi_usb 10
repo_dir=$(mktemp -p "$tmp_dir" -d repo.XXXX) || cleanup_multi_usb 10

# Mount EFI System partition
[ "$eficonfig" -eq 1 ] && { mount "${TARGET_DEVICE}2" "$efi_mnt" || cleanup_multi_usb 10; }

# Mount data partition
mount "${TARGET_DEVICE}${data_part}" "$data_mnt" || cleanup_multi_usb 10

# Install GRUB for EFI
[ "$eficonfig" -eq 1 ] && \
    { $grub_cmd --target=x86_64-efi --efi-directory="$efi_mnt" \
    --boot-directory="${data_mnt}/${data_subdir}" --removable --recheck \
    || cleanup_multi_usb 10; }

# Install GRUB for BIOS
$grub_cmd --force --target=i386-pc \
    --boot-directory="${data_mnt}/${data_subdir}" --recheck "$TARGET_DEVICE" \
    || cleanup_multi_usb 10

# Install fallback GRUB
$grub_cmd --force --target=i386-pc \
    --boot-directory="${data_mnt}/${data_subdir}" --recheck "${TARGET_DEVICE}${data_part}" \
    || true

# Create necessary directories
[ ! -d "${data_mnt}/${data_subdir}/isos" ] && mkdir -p "${data_mnt}/${data_subdir}/isos" || cleanup_multi_usb 10

#if [ "$clone" -eq 1 ]; then
#	# Clone Git repository
#	(cd "$repo_dir" && \
#		git clone https://github.com/aguslr/multibootusb . && \
#		# Move all visible and hidden files and folders except '.' and '..'
#		for x in * .[!.]* ..?*; do if [ -e "$x" ]; then mv -- "$x" \
##			"${data_mnt}/${data_subdir}"/grub*/; fi; done) || cleanup_multi_usb 10
#			"${data_mnt}/${data_subdir}"/grub/; fi; done) || cleanup_multi_usb 10

#else
	# Copy files
#	cp -R ./mbusb.* "${data_mnt}/${data_subdir}"/grub*/ || cleanup_multi_usb 10
	# Copy example configuration for GRUB
#	cat > "${data_mnt}/${data_subdir}"/grub/grub.cfg <<EOF

#generate_grub_template
#cp "$grub_template" "${data_mnt}/${data_subdir}/grub/grub.cfg"

#[ -f "${data_mnt}/${data_subdir}/grub/grub.cfg" ] && mv "${data_mnt}"/"${data_subdir}"/grub/grub.cfg "${data_mnt}"/"${data_subdir}"/grub/grub.cfg.old
#cd "${data_mnt}/${data_subdir}/grub/"
echo "
if loadfont /boot/grub/fonts/unicode.pf2 ; then
  set gfxmode=640x480
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod gfxterm
  insmod jpeg
  insmod png
  terminal_output gfxterm
fi

background_image /boot/grub/splash.png
set menu_color_normal=white/black
set menu_color_highlight=dark-gray/white
set timeout=15

source /boot/grub/menu.cfg
" > "${data_mnt}/${data_subdir}/grub/grub.cfg"
#" > /grub.cfg

#fi

# Rename example configuration
#( cd "${data_mnt}/${data_subdir}"/grub*/ && cp grub.cfg.example grub.cfg ) || cleanup_multi_usb 10

# Download memdisk
#syslinux_url='https://www.kernel.org/pub/linux/utils/boot/syslinux/syslinux-6.03.tar.gz'
#{ wget -qO - "$syslinux_url" 2>/dev/null || curl -sL "$syslinux_url" 2>/dev/null; } \
#    | tar -xz -C "${data_mnt}/${data_subdir}"/grub*/ --no-same-owner --strip-components 3 \
#    'syslinux-6.03/bios/memdisk/memdisk' \
#    || cleanup_multi_usb 10

# Clean up and exit
cleanup_multi_usb
partprobe "$TARGET_DEVICE"
sleep 0.1
sync
scan_usb
}

#  ▀█▀ █▀▀ █▀█ █   ▀█▀ █▀█ █ █ █ █
#   █  ▀▀█ █ █ █    █  █ █ █ █ ▄▀▄
#  ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀ ▀ ▀
create_new_usb_isolinux() {
clear
printf "\\n\\n\\tWaring all files on %s will be lost" "$TARGET_DEVICE"
printf "\\tPress enter to continue or Ctrl-C to cancel.\\n"
read -r "confirm"
#for P in $(lsblk -nlpo NAME "$TARGET_DEVICE"[1-9]); do
for P in $(lsblk -nlo MOUNTPOINT "$TARGET_DEVICE"); do umount "$P"; done
dd if=/dev/zero of="$TARGET_DEVICE" bs=1M count=4
printf "o\\n n\\n p\\n 1\\n \\n \\n t\\n b\\n a\\n p\\n w\\n" | fdisk "$TARGET_DEVICE"
TARGET_PART="$TARGET_DEVICE"1
dd if=/dev/zero of="$TARGET_PART" bs=1M count=4
mkfs.fat -F32 -v -I "$TARGET_PART"
sync
partprobe "$TARGET_DEVICE"
sleep 0.1
syslinux -i "$TARGET_PART"
[ ! -d "$MULTIUSB_DIR" ] && mkdir -p "$MULTIUSB_DIR"
mount -o rw "$TARGET_PART" "$MULTIUSB_DIR"
cp -R -v /usr/lib/syslinux/modules/bios/*.c32 "$MULTIUSB_DIR"
cp -v /usr/share/backgrounds/multiusb_splash.png "$MULTIUSB_DIR/splash.png"
dd bs=440 count=1 if=/usr/lib/syslinux/mbr/mbr.bin of="$TARGET_DEVICE"
cat > "$MULTIUSB_DIR"/syslinux.cfg <<EOF
UI vesamenu.c32
PROMPT	1
MENU TITLE Boot Menu
MENU HSHIFT 8
MENU VSHIFT 8
MENU ROWS 7
MENU WIDTH 60
MENU TABMSGROW 29
MENU BACKGROUND splash.png
include menu.cfg
EOF
sync
umount "$TARGET_PART"
}

#  █▀▀ █▀▄ █▀▀ █▀█ ▀█▀ █▀▀   █▀█ █▀▀ █ █   █ █ █▀▀ █▀▄
#  █   █▀▄ █▀▀ █▀█  █  █▀▀   █ █ █▀▀ █▄█   █ █ ▀▀█ █▀▄
#  ▀▀▀ ▀ ▀ ▀▀▀ ▀ ▀  ▀  ▀▀▀   ▀ ▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀▀
create_new_usb() {
MENU_TEXT=$(echo "
Multiboot USB Menu \\n
Choose Boot method:
Isolinux_Legacy<s:Isolinux_Legacy>
Grub_EFI<s>
Grub_Legacy<s>")
MENU=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$MENU_TEXT" -p 5)
case $MENU in
	Grub_EFI) create_new_usb_grub -e "$TARGET_DEVICE";;
	Grub_Legacy) create_new_usb_grub "$TARGET_DEVICE";;
	Isolinux_Legacy) create_new_usb_isolinux;;
	*) return;;
esac
}

#  █▀█ █▀▄ █▀▄   █▀▄ ▀█▀ █▀▀ ▀█▀ █▀▄ █▀█
#  █▀█ █ █ █ █   █ █  █  ▀▀█  █  █▀▄ █ █
#  ▀ ▀ ▀▀  ▀▀    ▀▀  ▀▀▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀
add_distro() {
ISO_PATH=""
ISO_FILE=""
MENU_TEXT=$(echo "
Multiboot USB Menu\\n
 > search iso file in $snapshot_dir<s:SNAP>
 > search other path<s:OTHER>

 NOTE: This script only works with snapshots and some debian based distros.
	Other distros may be added manually.

 ")
MENU=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$MENU_TEXT" -p 4)
if [ "$MENU" = "SNAP" ]; then
  ISO_PATH="$snapshot_dir"
else
  while [ -z "$(find "$ISO_PATH" -maxdepth 1 -iname "*.iso")" ]; do
    clear; printf "\\n\\tEnter the directory where the iso file is located:\\n\\t"
    read -r "ans"
    # substitude ~ and .
    if printf "%s" "$ans" | grep -qe ^"~/"; then
	  ans="$(printf "%s/%s" "$HOME" "$(echo "$ans" | cut -b 3-)")"
    elif printf "%s" "$ans" | grep -qe "^\\."; then
	  ans="$(pwd"$(echo "$ans" | cut -b 2-)")"
    fi
    # make sure path begins with /
    if [ ! "$(printf "%s" "$ans" | cut -b 1)" = "/" ]; then
	  ans="/home/$SUDOER/$ans"
    fi
    # check if the path is valid
    [ -d "$ans" ] && ISO_PATH="$ans"
  done
fi
while [ -z "$ISO_FILE" ]; do
  LIST="$(find "$ISO_PATH" -maxdepth 1 -iname "*.iso" | sed 's/$/<s>/g')"
  ISO_FILE=$(iselect "Choose iso file" "$LIST" -p 2)
done

ISO_NAME=""
while [ -z "$ISO_NAME" ]; do
  clear
  printf "\\n\\n\\tThe image needs a name for the boot menu.\\n"
  printf "\\tThis is also the name for the directory where the iso files will be,\\n"
  printf "\\tso keep it simple and dont use spaces or special characters.\\n\\n\\t\\tNew Name: "
  read -r "NEW_NAME"; check_name_status; check_name_case
  [ "$NEW_NAME_STATUS" = "OK" ] && ISO_NAME="$NEW_NAME" || ISO_NAME=""
done

ACCOUNT_NAME=""
while [ -z "$ACCOUNT_NAME" ]; do
  clear
  printf "\\n\\n\\tEnter the user accout name.\\n\\tThe account must exist on that iso\\n\\n\\tAccount name: "
  read -r "NEW_NAME"; check_name_status; check_name_case
  [ "$NEW_NAME_STATUS" = "OK" ] && ACCOUNT_NAME="$NEW_NAME" || ACCOUNT_NAME=""
done

mount "$ISO_FILE" "$ISO_DIR"
mount -o rw "$TARGET_PART" "$MULTIUSB_DIR"
if [ "$(lsblk -nlo MOUNTPOINT "$TARGET_PART")" = "$MULTIUSB_DIR" ]; then
# if grub
  if [ "$BOOT_TYPE" = "grub" ]; then
    # find squashfs, vmlinuz and initrd file in target iso
    SFS=$(find "$ISO_DIR" -name '*.squashfs' | head -n 1)
    [ ! -f "$SFS" ] && SFS=$(find "$ISO_DIR" -name '*.sfs')
    [ ! -f "$SFS" ] && echo "Error: no squashfs found." && exit 1

    KERN=$(find "$ISO_DIR" -name '*vmlinuz*' | head -n 1)
    [ ! -f "$KERN" ] && echo "Error: vmlinuz not found." && exit 1

    INIT=$(find "$ISO_DIR" -name '*initrd*' | head -n 1)
    [ ! -f "$INIT" ] && INIT=$(find "$ISO_DIR" -name '*initramfs*' | head -n 1)
    [ ! -f "$INIT" ] && echo "Error: initrd not found." && exit 1

    # get the real file names without path
    SFS_NAME=$(ls "$SFS" | cut -d '/' -f $(( $(ls "$SFS" | tr -c '/' ' ' | sed 's/ //g' | wc -c) +1)) )
    KERN_NAME=$(ls "$KERN" | cut -d '/' -f $(( $(ls "$KERN" | tr -c '/' ' ' | sed 's/ //g' | wc -c) +1)) )
    INIT_NAME=$(ls "$INIT" | cut -d '/' -f $(( $(ls "$INIT" | tr -c '/' ' ' | sed 's/ //g' | wc -c) +1)) )

    # check the paths
    OLD_SFS_PATH=$(echo "$SFS" | sed s:"$SFS_NAME"::)
    OLD_KERN_PATH=$(echo "$KERN" | sed s:"$KERN_NAME"::)
    OLD_INIT_PATH=$(echo "$INIT" | sed s:"$INIT_NAME"::)

    # prepare new directory
    NEW_ISO_DIR="$MULTIUSB_DIR/boot/isos/$ISO_NAME"
    [ -d "$NEW_ISO_DIR" ] || mkdir -vp "$NEW_ISO_DIR"
    clear; printf "\\n\\n\\tcopying files, this can take a moment...\\n\\n"

    # copy everything to the new folder
    rsync -av "$OLD_SFS_PATH" "$NEW_ISO_DIR"
    rsync -av "$OLD_KERN_PATH" "$NEW_ISO_DIR"
    rsync -av "$OLD_INIT_PATH" "$NEW_ISO_DIR"

    # update grub menu
    printf "\\ngenerating grub files...\\n"
    [ -f "$MULTIUSB_DIR"/boot/grub/menu.cfg ] || touch "$MULTIUSB_DIR"/boot/grub/menu.cfg
    echo "source /boot/grub/menu_$ISO_NAME.cfg" >> "$MULTIUSB_DIR"/boot/grub/menu.cfg
    [ -f "$MULTIUSB_DIR"/boot/grub/menu_"$ISO_NAME".cfg ] || touch "$MULTIUSB_DIR"/boot/grub/menu_"$ISO_NAME".cfg
    cat > "$MULTIUSB_DIR"/boot/grub/menu_"$ISO_NAME".cfg <<EOF
submenu " $ISO_NAME" {
    menuentry $ISO_NAME {
	set gfxpayload=keep
	linux   /boot/isos/$ISO_NAME/$KERN_NAME boot=live username=$ACCOUNT_NAME ${ifnames_opt} ${netconfig_opt}
	initrd  /boot/isos/$ISO_NAME/$INIT_NAME
    }
    menuentry "$ISO_NAME (to RAM)" {
	set gfxpayload=keep
	linux   /boot/isos/$ISO_NAME/$KERN_NAME toram=filesystem.squashfs username=$ACCOUNT_NAME boot=live ${ifnames_opt} ${netconfig_opt}
	initrd  /boot/isos/$ISO_NAME/$INIT_NAME
    }
    menuentry "$ISO_NAME (failsafe)" {
	set gfxpayload=keep
	linux   /boot/isos/$ISO_NAME/$KERN_NAME username=$ACCOUNT_NAME boot=live nocomponents=xinit noapm noapic nolapic nodma nosmp forcepae nomodeset vga=normal ${ifnames_opt} ${netconfig_opt}
	initrd  /boot/isos/$ISO_NAME/$INIT_NAME
    }
}
EOF
# fix initrd live config
# this is needed to set the live path from the usb
    EXTRACT_DIR=$(mktemp -d /tmp/extracted_init.XXXX)
    cp "$NEW_ISO_DIR"/"$INIT_NAME" "$EXTRACT_DIR"/"$INIT_NAME"
    initrd_image="$INIT_NAME"
# extract init
    cd "$EXTRACT_DIR"
	unmkinitramfs "$initrd_image" initramfs/

# edit the live media path
	[ -d initramfs/main ] && MAIN="main/" || MAIN=""
    [ -f "$EXTRACT_DIR/initramfs/${MAIN}usr/lib/live/boot/0001-init-vars.sh" ] && \
    STRING=$(grep -i "LIVE_MEDIA_PATH" "$EXTRACT_DIR"/initramfs/${MAIN}usr/lib/live/boot/0001-init-vars.sh) && \
    sed -i s:"$STRING":LIVE_MEDIA_PATH=\"/boot/isos/"$ISO_NAME"/\": "$EXTRACT_DIR"/initramfs/${MAIN}usr/lib/live/boot/0001-init-vars.sh

# make backup of old init file
    mv "$NEW_ISO_DIR/$INIT_NAME" "$NEW_ISO_DIR/$INIT_NAME.bak"
	cd initramfs/${MAIN}

# recompress
    find . -print0 | cpio -0 -H newc -o | gzip -c > "$NEW_ISO_DIR"/"$INIT_NAME"
    if [ ! -d "$work_dir" ]; then
      mkdir -p "$work_dir"
    fi
    cd "$work_dir"
    rm -rf "$EXTRACT_DIR"

# if syslinux
  elif [ "$BOOT_TYPE" = "syslinux" ]; then
# copy isolinux directory
    [ -d "$MULTIUSB_DIR/$ISO_NAME/isolinux" ] || mkdir -p "$MULTIUSB_DIR/$ISO_NAME/isolinux"
    cp -R -v "$ISO_DIR"/isolinux/* "$MULTIUSB_DIR"/"$ISO_NAME"/isolinux/
# copy live directory
    [ -d "$MULTIUSB_DIR/$ISO_NAME/live" ] || mkdir -p "$MULTIUSB_DIR/$ISO_NAME/live"
    cp -R -v "$ISO_DIR"/live/* "$MULTIUSB_DIR"/"$ISO_NAME"/live/
# generate menu
    VMLINUZ_FILE=$(find "$MULTIUSB_DIR"/"$ISO_NAME" -name "*vmlinuz*" | tr '/'  '\n' | tail -n 1)
    INITRD_FILE=$(find "$MULTIUSB_DIR"/"$ISO_NAME" -name "*initrd*" | tr '/'  '\n' | tail -n 1)
    SQUASHFS_FILE=$(find "$MULTIUSB_DIR"/"$ISO_NAME" -name "*.squashfs" | tr '/'  '\n' | tail -n 1)
    [ ! -e "$MULTIUSB_DIR"/"$ISO_NAME"/live/"$VMLINUZ_FILE" ] && echo "vmlinuz file not found" && exit 1
    [ ! -e "$MULTIUSB_DIR"/"$ISO_NAME"/live/"$INITRD_FILE" ] && echo "initrd file not found" && exit 1
    [ ! -e "$MULTIUSB_DIR"/"$ISO_NAME"/live/"$SQUASHFS_FILE" ] && echo "squashfs file not found" && exit 1
    echo "	LABEL $ISO_NAME
	MENU LABEL $ISO_NAME
	CONFIG /$ISO_NAME/isolinux/isolinux.cfg
	APPEND /$ISO_NAME/isolinux/" > "$MULTIUSB_DIR"/menu_"$ISO_NAME".cfg
    echo "include menu_$ISO_NAME.cfg" >> "$MULTIUSB_DIR"/menu.cfg

    if [ -f "$MULTIUSB_DIR/$ISO_NAME/isolinux/menu.cfg" ]; then
	  # this fixes vesamenu path
	  # echo "include menu.cfg\ndefault vesamenu.c32\nprompt 0\ntimeout 200\n" > "$MULTIUSB_DIR"/"$ISO_NAME"/isolinux/isolinux.cfg
	  # this deletes the default files and generates new
	  rm "$MULTIUSB_DIR"/"$ISO_NAME"/isolinux/*.cfg
	  rm "$MULTIUSB_DIR"/"$ISO_NAME"/isolinux/*.txt

# make new cfg file
	  printf "\\ngenerating isolinux.cfg\\n"
	  [ "$DISABLE_IPV6" = "yes" ] && ipv6_opt='ipv6.disable=1' || ipv6_opt=""
      cat > "$MULTIUSB_DIR/$ISO_NAME/isolinux/isolinux.cfg" <<EOF
UI vesamenu.c32
MENU TITLE Boot Menu
MENU BACKGROUND /splash.png
MENU HSHIFT 9
MENU VSHIFT 3
MENU ROWS 6
MENU WIDTH 59
MENU TABMSGROW 11
LABEL live
	MENU LABEL Live Boot (amd64)
	MENU DEFAULT
	LINUX /$ISO_NAME/live/$VMLINUZ_FILE
	INITRD /$ISO_NAME/live/$INITRD_FILE
	APPEND live-media-path=/$ISO_NAME/live ignore_uuid cdrom-detect/try-usb=true boot=live username=$ACCOUNT_NAME net.ifnames=0 modprobe.blacklist=pcspkr ${ipv6_opt}
LABEL toram
	MENU LABEL Live Boot (amd64) (to RAM)
	LINUX /$ISO_NAME/live/$VMLINUZ_FILE
	INITRD /$ISO_NAME/live/$INITRD_FILE
	APPEND live-media-path=/$ISO_NAME/live ignore_uuid cdrom-detect/try-usb=true boot=live username=$ACCOUNT_NAME toram net.ifnames=0 modprobe.blacklist=pcspkr ${ipv6_opt}
LABEL nox
	MENU LABEL Live Boot (amd64) (noX)
	MENU DEFAULT
	LINUX /$ISO_NAME/live/$VMLINUZ_FILE
	INITRD /$ISO_NAME/live/$INITRD_FILE
	APPEND live-media-path=/$ISO_NAME/live ignore_uuid cdrom-detect/try-usb=true boot=live 3 username=$ACCOUNT_NAME net.ifnames=0 modprobe.blacklist=pcspkr ${ipv6_opt}
LABEL failsafe
	MENU LABEL Live Boot (amd64) (failsafe)
	KERNEL /$ISO_NAME/live/$VMLINUZ_FILE username=$ACCOUNT_NAME noapic noapm nodma nomce nolapic nosmp nomodeset vga=normal net.ifnames=0
	INITRD /$ISO_NAME/live/$INITRD_FILE
	APPEND live-media-path=/$ISO_NAME/live ignore_uuid cdrom-detect/try-usb=true boot=live
LABEL memtest
	MENU LABEL Memory test
	KERNEL /$MULTIUSB_DIR/$ISO_NAME/live/memtest
LABEL reboot
	MENU LABEL Reboot
	COM32 reboot.c32
LABEL poweroff
	MENU LABEL Power Off
	COM32 poweroff.c32
EOF
	  fi
  fi
  printf "\\nsynchronizing...\\n"; sync
  if grep -qw "$MULTIUSB_DIR" /proc/mounts ; then
    printf "\\nunmounting...\\n"
    umount "$MULTIUSB_DIR" "$ISO_DIR"
  fi
else
  printf "Error, could not mount %s\\n" "$TARGET_PART" && exit 1
fi
scan_usb
}

#  █▀▀ █▀▀ █▀█ █▀█   █ █ █▀▀ █▀▄
#  ▀▀█ █   █▀█ █ █   █ █ ▀▀█ █▀▄
#  ▀▀▀ ▀▀▀ ▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀▀
scan_usb() {
[ "$TARGET_DEVICE" ] || { printf "\\n\\tNo target device set.\\n"; exit 1; }
[ -b "$TARGET_DEVICE" ] || { printf "\\n\\tInvalid target device.\\n"; exit 1; }
[ -b "$TARGET_DEVICE" ] && partprobe "$TARGET_DEVICE"
sleep 0.1
for PART in $(lsblk -nplo NAME "$TARGET_DEVICE" | grep "$TARGET_DEVICE"'[1-9]'); do
  mount "$PART" "$MULTIUSB_DIR"
  [ -f "$MULTIUSB_DIR/EFI/BOOT/BOOTX64.EFI" ] && EFI_PART="$PART"
  if [ -f "$MULTIUSB_DIR/syslinux.cfg" ]; then
	BOOT_TYPE="syslinux"
	DISTRO_LIST=$(grep "include" "$MULTIUSB_DIR"/menu.cfg | sed 's:include menu_::g;s:.cfg::g;s:.txt::g')
	echo "found $DISTRO_LIST on $PART"
    TARGET_PART="$PART"
  elif [ -f "$MULTIUSB_DIR/boot/grub/grub.cfg" ]; then
    BOOT_TYPE="grub"
	DISTRO_LIST=$(grep "source" "$MULTIUSB_DIR"/boot/grub/menu.cfg | sed 's:include menu_::g;s:source /boot/grub/menu_::g;s:.cfg::g;s:.txt::g')
	echo "found $DISTRO_LIST on $PART"
    TARGET_PART="$PART"
  else
	echo "nothing found on $PART"
  fi
  umount "$PART"
done
if [ -b "$TARGET_PART" ]; then
  mount "$TARGET_PART" "$MULTIUSB_DIR"
else
  create_new_usb; add_distro
fi
}

#  █▀▄ █▀▀ █▄█ █▀█ █ █ █▀▀   █▀▄ ▀█▀ █▀▀ ▀█▀ █▀▄ █▀█
#  █▀▄ █▀▀ █ █ █ █ ▀▄▀ █▀▀   █ █  █  ▀▀█  █  █▀▄ █ █
#  ▀ ▀ ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀▀▀   ▀▀  ▀▀▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀
remove_distro() {
if [ ! -z "$DISTRO_LIST" ]; then
  RM_DISTRO=$(iselect "" "" "" " > remove a distro:" \
		    "$(echo "$DISTRO_LIST" | xargs -n1 | sed 's/$/<s>/g;s/^/ --> /g')" \
		    " < Back<s:BACK>" -p 5)
  RM_DISTRO="$(echo "$RM_DISTRO" | sed 's/ --> //g')"
  if [ "$BOOT_TYPE" = "syslinux" ]; then
    [ -d "$MULTIUSB_DIR"/"$RM_DISTRO" ] && rm -rd  "$MULTIUSB_DIR"/"$RM_DISTRO"
    [ -f "$MULTIUSB_DIR"/menu_"$RM_DISTRO".cfg ] && rm "$MULTIUSB_DIR"/menu_"$RM_DISTRO".cfg
    sed -i "s:include menu_$RM_DISTRO.cfg::" "$MULTIUSB_DIR"/menu.cfg
  elif [ "$BOOT_TYPE" = "grub" ]; then
    [ -d "$MULTIUSB_DIR"/boot/isos/"$RM_DISTRO" ] && rm -rd  "$MULTIUSB_DIR"/boot/isos/"$RM_DISTRO"
    [ -f "$MULTIUSB_DIR"/boot/grub/menu_"$RM_DISTRO".cfg ] && rm "$MULTIUSB_DIR"/boot/grub/menu_"$RM_DISTRO".cfg
    sed -i "s:source /boot/grub/menu_$RM_DISTRO.cfg::" "$MULTIUSB_DIR"/boot/grub/menu.cfg
  fi
fi
scan_usb
}

#  █▄█ █ █ █   ▀█▀ ▀█▀ █ █ █▀▀ █▀▄   █▄█ █▀▀ █▀█ █ █
#  █ █ █ █ █    █   █  █ █ ▀▀█ █▀▄   █ █ █▀▀ █ █ █ █
#  ▀ ▀ ▀▀▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀▀▀ ▀▀    ▀ ▀ ▀▀▀ ▀ ▀ ▀▀▀
multiusb_menu() {
if [ -b "$TARGET_DEVICE" ]; then
  MENU_DEVICE=" > Selected device = $TARGET_DEVICE<s:DEVICE>
 --> Scan usb device for images<s:SCAN>
 --> Create new multiboot usb device<s:NEW>
 --> Add new image<s:ADD>
 --> Remove image<s:DEL>"
else
  MENU_DEVICE=" > Select USB device<s:DEVICE>"
fi
MENU_TEXT="Multiboot USB Menu\\n
$MENU_DEVICE
 > Add data partition<s:ADD_PART>
 < Back<s:BACK>
 < Exit<s:EXIT>
\\n\\n\\n
BOOTLOADER: $BOOT_TYPE
IMAGES: $(echo "$DISTRO_LIST" | xargs)"
MENU=$(iselect -n "$MENU_BOTTOM_OPTIONS" -t "$MENU_BOTTOM_TEXT" "$(echo "$MENU_TEXT")" -p 3)
case $MENU in
  NEW) create_new_usb; add_distro;;
  DEVICE) WARNING=""; select_usb;;
  SCAN) scan_usb;;
  ADD) add_distro;;
  DEL) remove_distro;;
  ADD_PART) umount "$TARGET_PART"; gparted "$TARGET_DEVICE";;
  BACK) start_script;;
  EXIT|*) lsblk -dno MOUNTPOINT "$TARGET_PART" | grep "$MULTIUSB_DIR" && umount "$MULTIUSB_DIR";
          lsblk -dno MOUNTPOINT | grep "$ISO_DIR" && umount "$ISO_DIR";
          [ -d "$MULTIUSB_DIR" ] && rm -rd "$MULTIUSB_DIR";
          [ -d "$ISO_DIR" ] && rm -rd "$ISO_DIR";
          exit 0;;
esac
}
select_usb
scan_usb
while [ ! "$MENU" = "BACK" ]; do multiusb_menu; done && MENU=""
}


#####################################################################################################################
#    _____ __             __     __  ___
#   / ___// /_____ ______/ /_   /  |/  /__  ____  __  __
#   \__ \/ __/ __ `/ ___/ __/  / /|_/ / _ \/ __ \/ / / /
#  ___/ / /_/ /_/ / /  / /_   / /  / /  __/ / / / /_/ /
# /____/\__/\__,_/_/   \__/  /_/  /_/\___/_/ /_/\__,_/

start_script() {
START_MENU=$(iselect -n "Refracta Tools" -t "Use at your own risk" "$START_MENU_TEXT" -p 15)
case "$START_MENU" in
  INSTALL)	make_install;;
  SNAPSHOT)	check_directories; prepare_snapshot; make_snapshot;;
  MULTI) multi_usb;;
  SHOW_HELP) clear; printf "%s" "$START_MENU_TEXT" | head -n 14; printf "\\n%s\\n" "$HELP_TEXT"; exit 0;;
  CHECK_DEPENDS) check_dependencies; clear;;
  *) exit 0;;
esac
}

for PARAMETER in "$@"; do
  case "$PARAMETER" in
	-d|--debug) set -x; set -o verbose; DEBUG="TRUE";;
	-i|--install) OPTION="INSTALL";;
	-m|--multi) OPTION="MULTI";;
	-s|--snapshot) OPTION="SNAPSHOT";;
	-v|--version) printf "RefractaTools Script Version %s\\nUse at your own risk\\n" "$SCRIPT_VERSION"; exit 0;;
	-h|--help|*) printf "%s" "$START_MENU_TEXT\\n" | head -n 14; printf "\\n%s\\n" "$HELP_TEXT"; exit 0;;
    esac
done

case "$OPTION" in
  INSTALL) early_efi_test;
  		   if [ "$BOOT_METHOD" = "UEFI" ]; then
  		     efi_test
  		   fi
  		   make_install;;
  MULTI) multi_usb;;
  SNAPSHOT) check_directories; prepare_snapshot; make_snapshot;;
  *) start_script;;
esac

[ -z "$1" ] && clear; start_script
exit 0

#!/bin/sh
# keep track of ebay auctions
# only tested on german ebay wesite !
# by Michael

LISTFILE="$HOME/.cache/ebay.list"

summary(){
SUMMARY_LIST=""
clear
for URL in $(grep -v '#' "$LISTFILE"); do
ITEM=""
TIME_LINE=""
TIME_STRING=""
PRICE_LINE=""
PRICE_STRING=""
SELLER=""
N=$((N+1))
ITEM=$(echo "$URL" | cut -d '/' -f 5)
ID=$(echo "$URL" | cut -d '/' -f 6 | cut -d '?' -f 1)
CACHEFILE="/$HOME/.cache/mem.$ID.html"
curl -s "$URL" > "$CACHEFILE"
echo "\n\033[1;33m$ITEM\033[0m"
if [ "$(cat "$CACHEFILE"| grep -no "vi-cdown_timeLeft")" ]; then
  TIME_LINE=$(($(cat "$CACHEFILE"| grep -no "vi-cdown_timeLeft" | head -n1 | cut -d ':' -f 1)+1))
  TIME_STRING=$(awk "(NR==$TIME_LINE){print}(NR==$TIME_LINE){exit}" "$CACHEFILE" | html2text)
  echo "$TIME_STRING"
fi

if [ "$(cat "$CACHEFILE"| grep -no "prcIsum_bidPrice")" ]; then
  PRICE_LINE=$(cat "$CACHEFILE"| grep -no "prcIsum_bidPrice" | head -n1 | cut -d ':' -f 1)
  PRICE_STRING=$(awk "(NR==$PRICE_LINE){print}(NR==$PRICE_LINE){exit}" "$CACHEFILE" | awk '{print $5$6}' | cut -d '<' -f 1 | cut -d '>' -f 2 | sed 's/EUR/EURO /' | awk '{print $2,$1}' )
elif [ "$(cat "$CACHEFILE"| grep -no "prcIsum")" ]; then
  PRICE_LINE=$(cat "$CACHEFILE"| grep -no "prcIsum" | tail -n1 | cut -d ':' -f 1)
  PRICE_STRING=$(awk "(NR==$PRICE_LINE){print}(NR==$PRICE_LINE){exit}" "$CACHEFILE" | html2text)
elif [ "$(cat "$CACHEFILE"| grep -no "cvipPrice")" ]; then
  PRICE_LINE=$(cat "$CACHEFILE"| grep -no "cvipPrice" | tail -n1 | cut -d ':' -f 1)
  PRICE_STRING=$(awk "(NR==$PRICE_LINE){print}(NR==$PRICE_LINE){exit}" "$CACHEFILE" | tr '<' '>' | cut -d '>' -f 5 | sed 's/EUR/EURO/g' | awk '{print $2,$1}')
  TIME_STRING="BEENDED"
  echo "$TIME_STRING"
fi
echo "$PRICE_STRING"
SELLER="$(curl -s "$URL" | grep mbg-nw | unhtml | awk '{print $1}')"
SUMMARY_LIST="$SUMMARY_LIST\n$URL<s:ITEM-$N>\n$ITEM\nPrice: $PRICE_STRING\nTime: $TIME_STRING\nSeller: $SELLER\n\n"
done
}

menu(){
SELECTION="$(iselect \
  " > Edit Item List<s:EDIT>" \
  " > Refresh<s:REFRESH>" \
  " > Exit<s:EXIT>" \
  " $(echo "$SUMMARY_LIST")" \
  )"
}

while true; do
  N=0
  summary
  menu
  if [ "$SELECTION" = "EDIT" ]; then
    nano "$LISTFILE"
  elif [ "$SELECTION" = "REFRESH" ]; then
    SELECTION=""
  elif [ "$SELECTION" = "EXIT" ] || [ -z "$SELECTION" ]; then
    exit 0
  elif [ "$(echo "$SELECTION" | grep "ITEM")" ]; then
    NUM="$(echo "$SELECTION" | cut -d '-' -f 2)"
    URL="$(awk '(NR == '$NUM' ) {print}' "$LISTFILE")"
    $BROWSER "$URL" &
  fi
done

exit 0

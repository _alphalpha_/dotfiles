#!/bin/sh
BASE_DIR="$HOME/.local/opt/Symphonium"
APP_IMAGE="$(ls -1A "$BASE_DIR" | grep .AppImage$ | tail -n1)"
"$BASE_DIR"/"$APP_IMAGE"
exit 0


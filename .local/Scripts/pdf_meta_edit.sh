#!/bin/sh

RES_X="$(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 1)"
RES_Y="$(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 2)"
YAD_WIDTH="$(echo "$RES_X * 0.8" | bc | cut -d '.' -f 1)"
YAD_HEIGHT="$(echo "$RES_Y * 0.8" | bc | cut -d '.' -f 1)"

[ "$1" ] && PDF="$1"
while [ ! -n "$PDF" ] && [ ! "$(file -b --mime-type "$PDF")" = "application/pdf" ]; do
  PDF="$(yad --title=pdfmted-editor --width="$YAD_WIDTH" --height="$YAD_HEIGHT" --file)"
  [ -z "$PDF" ] && exit 0
done

pdfmted-editor "$PDF"

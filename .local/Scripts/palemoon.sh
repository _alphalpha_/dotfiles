#!/bin/sh
# script to launch / download / update the palemoon browser
# by Michael
VERSION="2.5.1"

PALEMOON_PATH="$HOME/.local/opt/palemoon"
PALEMOON_PROFILE_PATH="$HOME/.config/palemoon"
PALEMOON_PROFILE_NAME="default"
STARTPAGE="file:///var/www/html/startpage.html"
TMPDIR="$HOME/.local/tmp"


palemoon_clear () {
# Delete previous cache
[ -d "$HOME/.cache/moonchild productions" ] && rm -rdv "$HOME/.cache/moonchild productions"
[ -d "$HOME/.moonchild productions" ] && rm -rdv "$HOME/.moonchild productions"
[ -d "$HOME/.mozilla" ] && [ ! -f "/usr/bin/firefox" ] && rm -rdv "$HOME/.mozilla"
[ -d "$PROFILE/cache2" ] && rm -rdv "$PROFILE/cache2"
[ -d "$PROFILE/startupCache" ] && rm -rdv "$PROFILE/startupCache"
[ -d "$PROFILE/storage" ] && rm -rdv "$PROFILE/storage"
[ -d "$PROFILE/thumbnails" ] && rm -rdv "$PROFILE/thumbnails"
[ -f "$PROFILE/sessionstore.bak" ] && rm -v "$PROFILE/sessionstore.bak"
[ -f "$PROFILE/sessionstore.js" ] && rm -v "$PROFILE/sessionstore.js"
[ -f "$PROFILE/sessionCheckpoints.json" ] && rm -v "$PROFILE/sessionCheckpoints.json"
[ -f "$PROFILE/sessionCheckpoints.json.tmp" ] && rm -v "$PROFILE/sessionCheckpoints.json.tmp"
[ -f "$PROFILE/places.sqlite-shm" ] && rm -v "$PROFILE/places.sqlite-shm"
[ -f "$PROFILE/places.sqlite-wal" ] && rm -v "$PROFILE/places.sqlite-wal"
for x in $(ls -1A "$PROFILE" | grep .sqlite | grep -vw places.sqlite ); do
  [ -f "$PROFILE/$x" ] && rm -v "$PROFILE/$x"
done
}


check_firejail(){
# Check for firejail
if [ -f /usr/bin/firejail ]; then
  sandbox="firejail --machine-id --profile=/etc/firejail/palemoon-bin.profile"
  if [ ! -f /etc/firejail/palemoon-bin.profile ]; then
    printf "Could not find /etc/firejail/palemoon-bin.profile\\n"
    printf "You need to create a firejail profile or use this script with the '--no-firejail' option\\n"
    printf "You can run .local/Scripts/palemoon-firejail-conf.sh with sudo to autogenerate a profile\\n"
    exit 1
  fi
fi
}


palemoon_start () {
# Start command
${sandbox} "$PALEMOON_PATH/palemoon-bin" --profile "$PROFILE" --url "$STARTPAGE"
# Remove some stuff when palemoon is closed
[ -d "$HOME/.moonchild productions" ] && rm -rd "$HOME/.moonchild productions"
[ -d "$HOME/.mozilla" ] && [ ! -f "/usr/bin/firefox" ] && rm -rd "$HOME/.mozilla"
}


palemoon_update () {
# NOTE: Linux 32 bit binaries have been discontinued.
ARCH="$(uname -m)"
GTK_VERSION="gtk2"

# Connection test
curl --connect-timeout 3 -s "https://linux.palemoon.org/" 1>/dev/null || { printf "Error: could not connect to 'https://linux.palemoon.org/'\\n" exit 1; } 

# get version numbers
LATEST_VERSION="$(curl -s "https://linux.palemoon.org/download/mainline/" | grep -E -o "[0-9]?[0-9]?[0-9]\.[0-9]?[0-9]?[0-9]?\.?[0-9]?[0-9]?[0-9]?\.?[0-9]?[0-9]?[0-9]?\.linux" | head -n1 |  sed 's/.linux//g')"
[ -z "$LATEST_VERSION" ] && exit 1
FILENAME=$(curl -s "https://linux.palemoon.org/download/mainline/" | grep palemoon-"$LATEST_VERSION".linux-"$ARCH" | grep Download | grep "$GTK_VERSION" | cut -d\" -f 2 | cut -d\/ -f 4)
[ -z "$FILENAME" ] && printf "Error: could not find the correct download link" && exit 1
if [ -x "$PALEMOON_PATH/palemoon-bin" ]; then
  CURRENT_VERSION="$("$PALEMOON_PATH"/palemoon-bin -v | awk '{print $5}')"
else
  CURRENT_VERSION="0"
fi

# Compare versions
printf "Searching updates for Palemoon... "
if [ "$LATEST_VERSION" = "$CURRENT_VERSION" ]; then
  printf "%s is up to date.\\n" "$CURRENT_VERSION" && exit 0
else
  printf "found new version %s\\n" "$LATEST_VERSION"
fi

# Download
[ -d "$TMPDIR" ] || mkdir -p "$TMPDIR"
printf "Downloading https://linux.palemoon.org/datastore/release/%s \\n" "$FILENAME"
curl -L -o "$TMPDIR/$FILENAME" -C - "https://linux.palemoon.org/datastore/release/$FILENAME"

# Compare checksum
if [ "$GTK_VERSION" = "gtk2" ]; then
  checksum="$(curl -s https://linux.palemoon.org/download/mainline/ |  awk '/SHA-256/ {print $2}' | tail -n1 | cut -d '<' -f1)"
elif [ "$GTK_VERSION" = "gtk3" ]; then
  checksum="$(curl -s https://linux.palemoon.org/download/mainline/ |  awk '/SHA-256/ {print $2}' | head -n1 | cut -d '<' -f1)"
fi

if [ "$checksum" = "$(sha256sum "$TMPDIR/$FILENAME" | awk '{print $1}')" ]; then
  printf "Checksum OK!\\n"
else
  printf "Checksum ERROR!\\n" 
  [ -f "$FILENAME" ] && rm "$TMPDIR/$FILENAME"
  exit 1
fi

# Stop palemoon if it is currently running
if [ -n "$(pidof palemoon-bin)" ]; then
  printf "palemoon is currently running, shutting down...\\n"
  killall palemoon-bin
fi

# Delete old files?
if [ -d "$PALEMOON_PATH" ]; then
  printf "%s already exists, delete? [Y/n] \\n" "$PALEMOON_PATH"
  read -r ans
  case $ans in
    [Nn]) break;;
    [Yy]*) rm -rfd "$PALEMOON_PATH"
  esac
fi

# Extracting
printf "Extracting...\\n"
[ -d "$PALEMOON_PATH" ] || mkdir -p "$PALEMOON_PATH"
if [ "$(echo "$FILENAME" | grep '.tar.bz2$')" ]; then
  TAR_OPT="xjf"
elif [ "$(echo "$FILENAME" | grep '.tar.xz$')" ]; then
  TAR_OPT="xJf"
fi
if [ "$(tar -tf "$TMPDIR/$FILENAME" | grep '^palemoon/')" ]; then
  TAR_OPT_2="--strip-components 1"
fi
tar $TAR_OPT "$TMPDIR/$FILENAME" ${TAR_OPT_2} -C "$PALEMOON_PATH"

# Cleanup
[ -f "$TMPDIR/$FILENAME" ] && rm -f "$TMPDIR/$FILENAME"
[ ! -z "$(find "$TMPDIR" -maxdepth 0 -empty)" ] && rm -rdf "$TMPDIR"
}


# Help function
show_help(){
printf "%s\\n" "$0"
printf "a script to launch / download / update the Palemoon browser\\n"
printf "does also delete all cached file beroe Palemoon is launched\\n"
printf "\\nOptions:\\n"
printf " -c --clear     removes all cached files, does not start Palemoon\\n"
printf " -h --help      shows this help\\n"
printf " -p --profile   sets a profile (default=default) (%s/default)\\n" "$PALEMOON_PROFILE_PATH"
printf " -r --resume    starts Palemoon without clearing the cache first\\n"
printf " -u --update    searches for the latest version of Palemoon\\n"
printf " -v --version   shows the version of this script\\n"
printf " --no-firejail  starts Palemoon without firejail\\n"
printf " --url          sets the startpage (default=%s)\\n" "$STARTPAGE"
printf "\\nExamples:\\n"
printf "palemoon\\n"
printf "palemoon https://gnu.org\\n"
printf "palemoon --profile foo\\n\\n"
}


### Script begins here
while [ -n "$1" ]; do
  case "$1" in
    --update|-u|--install|-i) palemoon_update; exit 0;;
    --clear|-c) palemoon_clear; exit 0;;
    --resume|-r) CLEAN_START="0";;
    --no-firejail) USE_FIREJAIL="0";;
    --profile|-p) shift; PALEMOON_PROFILE_NAME="$1";;
    --help|-h) show_help; exit 0;;
    --version|-v) printf "Michaels Palemoon Script Version %s\\n" "$VERSION"; exit 0;;
    --url|*) [ "$(echo "$1" | grep -E "(([A-Za-z]{3,9})://)?([-;:&=\+\$,\w]+@{1})?(([-A-Za-z0-9]+\.)+[A-Za-z]{2,3})(:\d+)?((/[-\+~%/\.\w]+)?/?([&?][-\+=&;%@\.\w]+)?(#[\w]+)?)?")" ] && STARTPAGE="$1";;
  esac
  shift
done

PROFILE="$PALEMOON_PROFILE_PATH/$PALEMOON_PROFILE_NAME"
[ -d "$PROFILE" ] || mkdir -p "$PROFILE"
[ "$CLEAN_START" != "0" ] && palemoon_clear
[ "$USE_FIREJAIL" != "0" ] && check_firejail
palemoon_start
exit 0

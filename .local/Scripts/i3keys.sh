#!/bin/sh

#######	SET FOLDERS
CONF_FILE="$HOME/.config/i3/config"
CONF_BACKUP="$HOME/.cache/i3config.bak"
TMP_1="$HOME/.cache/hotkeys.tmp"
TMP_2="$HOME/.cache/hotkeys.txt"

generate_menu() {
#######	GENERATE TEMPFILE_1 (grep all keybinds)
echo "$(cat $CONF_FILE| grep -n bindsym | sed s'/bindsym//g' | sed s'/$mod/Super/g' | sed s'/$/\\n/g')" > "$TMP_1"
#| sed s'/+udiaeresis/+ü/g'| sed s'/+colon/+:/g'| sed s'/+semicolon/+;/g'| sed s'/+ssharp/+ß/g')" \

line=0
linecap="$(cat $TMP_1 | wc -l )"
while [ "$line" != "$linecap" ] ; do
 line="$(($line+1))"
 if ! [ -z "$(cat $TMP_1 | head -n $line | tail -n 1)" ] ; then
   echo $(cat "$TMP_1" | head -n $line | tail -n 1) >> "$TMP_1"
 fi
done

#######	GENERATE TEMPFILE_2 (remove empty lines & add delimiters)
cat $TMP_1 | tail -n "$(($line/2))" > $TMP_2
rm "$TMP_1"
sed s'/Super/!Super/g' -i $TMP_2
sed s'/exec /!/g' -i $TMP_2

#######	GENERATE TEMPFILE_1 (make things look nice for the menu)
line=0
linecap="$(cat $TMP_2 | wc -l )"
while [ "$line" != "$linecap" ] ; do
 line="$(($line+1))"
 KEY="$(cat $TMP_2 | head -n $line | tail -n 1 | cut -d '!' -f 2)"
 COMMAND="$(cat $TMP_2 | head -n $line | tail -n 1 | cut -d '!' -f 3)"
 ID="$(cat $TMP_2 | head -n $line | tail -n 1 | cut -d ':' -f 1)"
 if [ -z "$(cat $TMP_2 | head -n $line | tail -n 1 | cut -d ':' -f 2| cut -b 1 | grep '#')" ] \
 && ! [ -z "$(cat $TMP_2 | head -n $line | tail -n 1 | awk '{print $2}' | grep 'Super')" ] \
 && ! [ -z "$(cat $TMP_2 | head -n $line | tail -n 1 | grep \")" ] ; then
  echo "$KEY\t\t$COMMAND\t<s:$ID>" >> $TMP_1
 fi
done


####### The Menu
LISTE=$(cat $TMP_1)
TITLE="SELECT KEYBOARD SHORTCUT TO CHANGE"
LEFT_STRING="Secelt Keyboard Shortcut"
RIGHT_STRING="press q to quit"
TARGET_ID=$(iselect -n "$LEFT_STRING" -t "$RIGHT_STRING" "$TITLE" "" "$LISTE" -p 3)
if ! [ $TARGET_ID ] ; then  exit 1 ; fi
}


get_key() {
SELECTED_SHORTCUT=$(cat $TMP_1| grep "<s:$TARGET_ID>"|awk '{print $1}')
OLD_COMMAND=$(cat $TMP_1| grep "<s:$TARGET_ID>"| cut -d\" -f 2)

clear
#toilet -f future "$SELECTED_SHORTCUT"
printf "you selected $SELECTED_SHORTCUT, the current command is \033[1;33m"$OLD_COMMAND"\033[0m\n\nEnter a new command or press Ctrl+c to exit\n"

#######	Read new Command
echo "Enter new command"
read ans
}


make_new_cfg() {
#######	Generate new Config
cp $CONF_FILE $CONF_BACKUP
cat $CONF_BACKUP | head -n $(($TARGET_ID-1)) > $CONF_FILE
echo " $(cat $TMP_1| grep "<s:$TARGET_ID>"|awk '{print $1}'| sed s'/Super/bindsym $mod/g')\t\texec \""$ans"\" " >> $CONF_FILE
cat $CONF_BACKUP | tail -n $(($(cat $CONF_BACKUP | wc -l)-$TARGET_ID)) >> $CONF_FILE
#######	Cleanup
rm "$TMP_1"
rm "$TMP_2"
}

while true; do
generate_menu
get_key
make_new_cfg
i3-msg reload
done

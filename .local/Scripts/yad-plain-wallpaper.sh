#!/bin/sh
# generate a plain colored wallpaer
# by Michael
FILE="$HOME/.local/wallpaper.png"
# use yad to select color value
COLOR="$(yad --title="Set Wallpaper" --color)"
# use imagemagick to create the png file
if [ "$COLOR" ]; then
  [ -e "$FILE" ] && rm "$FILE"
  convert -size $(xdpyinfo | awk '/dimensions/ {print $2}') canvas:$COLOR "$FILE"
# use xwallpaper to set the wallpaper
  xwallpaper --zoom "$FILE"
fi
exit 0

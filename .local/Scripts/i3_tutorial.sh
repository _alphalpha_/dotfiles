#!/bin/sh


DURATION=3
yres="$(xdpyinfo | grep dimensions | awk '{print $2;}' |  cut -d\x -f 2)"

dzen_cmd1() {
dzen2 -fg \#300A24 -bg \#dc6434 -y $(( $yres - 300 )) -fn xft:Bitstream\Vera\Sans\Mono:pixelsize=50 -p $DURATION
}

dzen_cmd2() {
dzen2 -fg \#300A24 -bg \#dc6434 -y $(( $yres - 240 )) -fn xft:Bitstream\Vera\Sans\Mono:pixelsize=50 -p $DURATION
}

dzen_cmd3() {
dzen2 -fg \#300A24 -bg \#dc6434 -y $(( $yres - 180 )) -fn xft:Bitstream\Vera\Sans\Mono:pixelsize=50 -p $DURATION
}

dzen_cmd4() {
dzen2 -fg \#300A24 -bg \#dc6434 -y $yres -fn xft:Bitstream\Vera\Sans\Mono:pixelsize=50 -p $DURATION
}

DURATION="3"
echo "Welcome to the tutorial" | dzen_cmd1
sleep 1

DURATION="10"
 echo "This setup is optimized for Laptops" | dzen_cmd1 &
 echo "it aims for maximal screen usage" | dzen_cmd2 &
 echo "and minimal mouse usage" | dzen_cmd3 &
sleep 10

DURATION="7"
echo "you can start the application launcher" | dzen_cmd1 &
echo "with [superkey] + [return] " | dzen_cmd2 &
sleep 7


#DURATION="5"
echo "Lets try it out!" | dzen_cmd1 &
echo "press [super] + [return]" | dzen_cmd2 &
sleep 7
#pidof

DURATION=10

echo "type: System Profiler and Benchmark" | dzen_cmd1 &
echo "and start it" | dzen_cmd2 &
sleep 10

#pidof
#DURATION="7"
echo "most application have a hotkey" | dzen_cmd1 &
echo "press [super] + [x] to open a terminal" | dzen_cmd2 &
sleep 13


echo "to switch focus between windows" | dzen_cmd1 &
echo "press [super] + [arrow key]" | dzen_cmd2 &
sleep 10

echo "Now focus the terminal" | dzen_cmd1 &
echo "and press [super] + [shift] + [right arrow]" | dzen_cmd2 &
sleep 10

echo "Lets open more windows" | dzen_cmd1 &
echo "press [super] + [f] to open the file manager" | dzen_cmd2 &
sleep 10

#DURATION="7"
echo "now press [super] + [ctrl] + [d]" | dzen_cmd1 &
sleep 10

echo "press [super] + [ctrl] + [d] again" | dzen_cmd1 &
sleep 10

echo "resize windows with [super] + [ctrl] + [arrow keys] " | dzen_cmd1 &
sleep 10

echo "try [super] + [ctrl] + [s]" | dzen_cmd1 &
sleep 10

echo "and go back to tabbed" | dzen_cmd1 &
echo "with [super] + [ctrl] + [a]" | dzen_cmd2 &
sleep 10

echo "now focus the window on the left" | dzen_cmd1 &
echo "and bring it over" | dzen_cmd2 &
echo "with [super] + [shift] + [right arrow]" | dzen_cmd3 &
sleep 10

DURATION="7"
echo "bring a window to workspace 2" | dzen_cmd1 &
echo "press [super] + [shift] + [2]" | dzen_cmd2 &
sleep 7

DURATION="10"
echo "now the window is on workspace 2" | dzen_cmd1 &
echo "press [super] + [2] to go to that workspace" | dzen_cmd2 &
sleep 10

echo "to close the window in focus" | dzen_cmd1 &
echo "press [super] + [shift] + [q]" | dzen_cmd2 &
sleep 10

echo "Now go back to workspace 1" | dzen_cmd1 &
echo "and close all windows" | dzen_cmd2 &
sleep 10

echo "open the i3 config with [super] + [F3]" | dzen_cmd1 &
echo "to modify the hotkeys" | dzen_cmd2 &
sleep 10

echo "go to the end of the file" | dzen_cmd1 &
echo "comment or delete the last line" | dzen_cmd2 &
echo "to disable this tutorial" | dzen_cmd3 &
sleep 10

echo "Congratulations!" | dzen_cmd1 &
echo "you have completed the tutorial" | dzen_cmd2 &
sleep 10


#!/bin/sh

# test if this is already enabled
if [ "$(xmodmap -pke | awk '/keycode  66/ {print $4}')" = "Caps_Lock" ]; then
# swap capslock and the right ctrl
  xmodmap -e 'clear lock'
  xmodmap -e 'clear control'
  xmodmap -e 'keycode 105 = Caps_Lock'
  xmodmap -e 'keycode 66 = Control_L'
  xmodmap -e 'add control = Control_L'
  xmodmap -e 'add Lock = Control_R'
else
# reset keyboard layout to default
  setxkbmap
fi
exit 0

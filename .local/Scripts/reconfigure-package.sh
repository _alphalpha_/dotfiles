#!/bin/sh

#[ $(id -u) -eq 0 ] || { echo "\n\t You need to be root :)\n" ; exit 1 ; }

for item in $(ls -1 /var/lib/dpkg/info/*.config | cut -d '/' -f 6 | cut -d '.' -f 1) ; do
item_list="$item_list
$item <s:$item>"
done

TITLE="SELECT PACKAGE TO RECONFIGURE"
LEFT_STRING="Secelt Package"
RIGHT_STRING=""
SELECTION=$(iselect -n "$LEFT_STRING" -t "$RIGHT_STRING" "$TITLE" "$(ls -1 /var/lib/dpkg/info/*.config | cut -d '/' -f 6 | cut -d '.' -f 1 | sed s'/$/<s>/g')" -p 3)

sudo dpkg-reconfigure "$SELECTION"
exit 0

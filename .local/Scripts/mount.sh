#!/bin/sh
# mount/unmount script
# by Michael
# Options:   --mount | -m   ;   --unmount | -u

### Settings
MOUNTDIR_LINK="$HOME/Media"	# devices will be mounted in '/media' and symlinked to 'MOUNTDIR_LINK'
EXCLUDES="/dev/sda" 			# seperate multiple devices with space
ALLOW_PMOUNT="1"

# This is here to mount MP3Player in a special directory
Player_ID="U2dc0z7ws53a79rn" 		# Player_ID = $(lsblk -o SERIAL)
Player_DIR="MP3_PLAYER"

#####################################################################

# check pmount
[ "$(pmount -V)" ] && [ "$ALLOW_PMOUNT" = "1" ] && PMOUNT_OK="1" || PMOUNT_OK="0"

COLS="$(tput cols)"

show_help() {
printf "%s\\nOptions:\\n\\t-h --help\\tshow this help\\n\\t-m --mount\\tmount all usb devices\\n\\t-u --unmount\\tunmount all usb devices\\n\\t--menu\\t\\tcan be used with -m or -u to only mount/unmount a selected drive\\n" "$0"
}

header() {
if [ "$OPT" = "MOUNT" ]; then
HEAD='
 ,__ __
/|  |  |                         o
 |  |  |   __          _  _  _|_     _  _    __,
 |  |  |  /  \_|   |  / |/ |  |  |  / |/ |  /  |
 |  |  |_/\__/  \_/|_/  |  |_/|_/|_/  |  |_/\_/|/
                                              /|
                                              \|
  ____
 (|   \       o
  |    | ,_            _   ,
 _|    |/  |  |  |  |_|/  / \_
(/\___/    |_/|_/ \/  |__/ \/

'
elif [ "$OPT" = "UNMOUNT" ]; then
HEAD='
 _
(_|    |                                            o
  |    |   _  _    _  _  _    __          _  _  _|_     _  _    __,
  |    |  / |/ |  / |/ |/ |  /  \_|   |  / |/ |  |  |  / |/ |  /  |
   \__/\_/  |  |_/  |  |  |_/\__/  \_/|_/  |  |_/|_/|_/  |  |_/\_/|/
                                                                 /|
                                                                 \|
  ____
 (|   \       o
  |    | ,_            _   ,
 _|    |/  |  |  |  |_|/  / \_
(/\___/    |_/|_/ \/  |__/ \/

'
fi
}

footer() {
# print a seperator line
[ "$COLS" -gt 0 ] && printf "%${COLS}s\\n"|sed "s/ /-/g"

# list all devices
if [ "$COLS" -gt "0" ]; then
  x="0"
  while [ "$x" -lt "$(lsblk | wc -l)" ]; do
	x=$((x+1))
	printf "%*s $(lsblk -o NAME,MODEL,SIZE,MOUNTPOINT| head -n $x | tail -n1)" $((COLS/2-35))
	printf "%*s\\n" $((COLS/2-35))
  done
else
  lsblk
fi
}

format_text() {
n=1
while [ "$n" -lt "14" ] ; do
  TEXT=$(printf "%s" "$INPUT" | head -n "$n" | tail -n 1)
  [ "$n" -gt "8" ]  && LENGHT=35
  printf "%*s $TEXT" $((COLS/2-LENGHT/2)); printf "%*s\\n" $((COLS/2-(LENGHT/2)))
  n=$((n+1))
done
printf "\\n"
}

mount_msg() {
if [ "$COLS" -gt "0" ]; then
  printf "%*s \\033[1;32m$NAME \\033[1;31mmounted \\033[0m@ \\033[1;32m/media/$NAME \\033[1;31mand linked to \\033[1;32m$LINK_DIR/$NAME \\033[0m" $((COLS/2-35))
  printf "%*s\\n" $((COLS/2-35))
else
  printf "\\033[1;32m%s \\033[1;31mmounted \\033[0m@ \\033[1;32m/media/%s \\033[1;31mand linked to \\033[1;32m$LINK_DIR/%s \\033[0m\\n" "$NAME" "$NAME" "$NAME"
fi
}

umount_msg() {
if [ "$COLS" -gt "0" ]; then
  printf "%*s \\033[1;32m$NAME \\033[1;31munmounted \\033[0mfrom \\033[1;32m$MOUNTPOINT \\033[0m" $((COLS/2-20))
  printf "%*s\\n" $((COLS/2-60))
else
  printf "\\033[1;32m%s \\033[1;31munmounted \\033[0mfrom \\033[1;32m%s \\033[0m\\n" "$NAME" "$MOUNTPOINT"
fi
}


clear_broken_links() {
# remove broken links if from 'MOUNTDIR_LINK'
if [ -d "$MOUNTDIR_LINK" ]; then
  for LINK in $(find "$MOUNTDIR_LINK" -maxdepth 1 -xtype l); do
    if [ ! -d "$(readlink "$LINK")" ]; then
      rm "$LINK"
    fi
  done
#for X in $(ls -1A "$MOUNTDIR_LINK"); do
#  for Y in $(readlink "$MOUNTDIR_LINK/$X"); do
#    [ "$(ls -1A "$Y")" = ".created_by_pmount" ] && [ -L "$MOUNTDIR_LINK/$X" ] && rm "$MOUNTDIR_LINK/$X"
#  done
#done
fi
}
clear_broken_links

# █▄█ █▀█ █ █ █▀█ ▀█▀
# █ █ █ █ █ █ █ █  █
# ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀
do_mount() {
# print text
INPUT="$HEAD"; LENGHT="50"
[ "$COLS" -gt 0 ] && format_text || echo "$HEAD"

# create directory if needed
[ ! -d "$MOUNTDIR_LINK" ] && mkdir "$MOUNTDIR_LINK" && chown "$USER":"$USER" "$MOUNTDIR_LINK"

# iterate partitions
# this makes sure that devices without seperate partitions are handeled correctly

if [ "$MENU" = "1" ]; then
  DEVS="$(lsblk -lno PATH | grep -v '/dev/mapper')"
  if [ -n "$EXCLUDES" ]; then
    for x in $EXCLUDES; do
      DEVS="$(echo "$DEVS" | grep -v "$x")"
    done
  fi
  for x in $DEVS; do
    if [ "$(lsblk -lnpo TYPE "$x")" != "part" ]; then
      DEVS="$(echo "$DEVS" | grep -wv "$x")"
    fi
    if [ -n "$(lsblk -lnpo MOUNTPOINT "$x")" ]; then
      DEVS="$(echo "$DEVS" | grep -wv "$x")"
    fi
  done      
  DEVS="$(iselect -a "$DEVS")"
else
  DEVS="$(lsblk -lno PATH | grep -v '/dev/mapper')"
fi

for DEV in $DEVS; do
  NAME=""
  FSTYPE=""
  SUBSYSTEM=""
  MOUNTPOINT=""
  PARENT=""
  SERIAL=""
  LABEL=""
  FS_CHECK="0"

  if [ -b "$DEV" ]; then
    NAME="$(lsblk -nlo NAME "$DEV" | head -n1)"
    FSTYPE="$(lsblk -nlo FSTYPE "$DEV" | head -n1)"
    TYPE="$(lsblk -nlo TYPE "$DEV" | head -n1)"
    SUBSYSTEM="$(lsblk -nlo SUBSYSTEMS "$DEV" | head -n1)"
    MOUNTPOINT="$(lsblk -nlo MOUNTPOINT "$DEV" | head -n1)"
    if [ "$TYPE" = "part" ]; then
      if [ "$(echo "$DEV" | grep -E "[0-9]p[0-9]?{3}$")" ]; then
        PARENT="$(echo "$DEV" | sed -E 's/p[0-9]?{3}$//')"
      elif [ "$(echo "$DEV" | grep -E "[0-9]?{3}$")" ]; then
        PARENT="$(echo "$DEV" | sed -E 's/[0-9]?{3}$//')"
      fi  
      SERIAL="$(lsblk -nlo SERIAL "$PARENT" | head -n1)"
    else
      SERIAL="$(lsblk -nlo SERIAL "$DEV" | head -n1)"
    fi
  fi
  
  # check if device is excluded
  for x in $EXCLUDES; do
   if [ "$x" = "$NAME" ] || [ "$x" = "$PARENT" ]; then DEV=""; fi
  done

  if [ "$DEV" ] && [ "$FSTYPE" ] && [ -z "$MOUNTPOINT" ]; then

    # check if device is a usb device (or sd card)
    case "$SUBSYSTEM" in
	  block:scsi:usb:pci|block:mmc:mmc_host:pci) IS_USB="1";;
      *) IS_USB="0";;
    esac
    [ "$PMOUNT_OK" = "1" ] && [ "$IS_USB" = "1" ] && USE_PMOUNT="1" || USE_PMOUNT="0"

    # check if serail matches mp3 player
    [ "$SERIAL" = "$Player_ID" ] && IS_MP3PLAYER="1" || IS_MP3PLAYER="0"

    # check filesystem and set umask option if needed
    case "$FSTYPE" in
      ext2|ext3|ext4|ntfs|iso9660) FLAG=""; FS_CHECK="1";;
      msdos|fat|vfat) FLAG="-o umask=000"; FS_CHECK="1";;
      exfat) USE_PMOUNT="0"; FLAG="-o umask=000"; FS_CHECK="1";;
      *)  FLAG=""; FS_CHECK="0";;
    esac

    if [ "$FS_CHECK" = "1" ]; then

      # check if this is the mp3 player
      if [ "$IS_MP3PLAYER" = "1" ]; then
        LABEL="MP3Player"
        NAME="MP3Player"
        LINK_DIR="$MOUNTDIR_LINK/$Player_DIR"
      else
        LABEL="$NAME"
        LINK_DIR="$MOUNTDIR_LINK"
      fi

      # mount
      if [ "$USE_PMOUNT" = "0" ]; then
        # create subdirectory if needed
        if [ ! -d "/media/$NAME" ]; then
          sudo mkdir "/media/$NAME"
          sudo chown "$USER":"$USER" "/media/$NAME"
        fi
        sudo -p "Enter password:" mount ${FLAG} "$DEV" "/media/$NAME" && mount_msg \
        && if [ -e "/media/$NAME" ] && [ ! -L "/$LINK_DIR/$NAME" ]; then
             ln -s "/media/$NAME" "$LINK_DIR"
           fi 

      # pmount
      elif [ "$USE_PMOUNT" = "1" ]; then      
        [ "$FLAG" = "-o umask=000" ] && FLAG="-u 000"
        pmount ${FLAG} "$DEV" "${LABEL}" && mount_msg \
        &&  if [ -e "/media/$NAME" ] && [ ! -L "/$LINK_DIR/$NAME" ]; then
              ln -s "/media/$NAME" "$LINK_DIR"
            fi
      fi

    fi
  fi
done
}

# █ █ █▀█ █▄█ █▀█ █ █ █▀█ ▀█▀
# █ █ █ █ █ █ █ █ █ █ █ █  █
# ▀▀▀ ▀ ▀ ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀
do_unmount() {
INPUT="$HEAD"; LENGHT="69"
[ "$COLS" -gt 0 ] && format_text || echo "$HEAD"
sync

if [ "$MENU" = "1" ]; then
  USB_DEVS="$(lsblk -lnpo NAME $(lsblk -dlnpo NAME,SUBSYSTEMS | grep -E "block:scsi:usb:pci|block:mmc:mmc_host:pci" | awk '{print $1}'))"
  for x in $USB_DEVS; do
    if [ "$(lsblk -lnpo TYPE "$x")" = "part" ] && [ -z "$(lsblk -lnpo MOUNTPOINT "$x")" ]; then
      USB_DEVS="$(echo "$USB_DEVS" | grep -wv "$x")"
    fi
    if [ "$(lsblk -lnpo TYPE "$x")" = "crypt" ]; then
      USB_DEVS="$(echo "$USB_DEVS" | grep -wv "$x")"
    fi
  done
  USB_DEVS="$(iselect -a "$USB_DEVS")"
else
  USB_DEVS="$(lsblk -dlnpo NAME,SUBSYSTEMS | grep -E "block:scsi:usb:pci|block:mmc:mmc_host:pci" | awk '{print $1}')"
fi

if [ -n "$USB_DEVS" ]; then
  for DEVICE in $USB_DEVS; do
    for MOUNTPOINT in $(lsblk -nplo MOUNTPOINT "$DEVICE"); do
      NAME="$(lsblk -nplo NAME,MOUNTPOINT "$DEVICE" | grep -w "$MOUNTPOINT" | awk '{print $1}' )"
      if [ -b "$NAME" ] && [ ! "$(grep -w ^"$NAME" /etc/fstab)" ]; then
        if [ "$(echo "$MOUNTPOINT" | grep ^"/media")" ] && [ "$PMOUNT_OK" = "1" ]; then
          pumount "$MOUNTPOINT" && umount_msg
        else
          sudo -p "Enter password:" umount "$MOUNTPOINT" && umount_msg
        fi
      fi
    done
  done
fi
}


while [ "$#" -gt 0 ]; do
  case "$1" in
    --mount|-m) OPT="MOUNT";;
    --unmount|--umount|-u) OPT="UNMOUNT";;
    --menu) MENU="1";;
    --help|-h) show_help; exit 0;;
  esac
  shift
done

header
case "$OPT" in
  MOUNT) do_mount;;
  UNMOUNT) do_unmount;;
  *) show_help; exit 0;;
esac
footer
clear_broken_links
exit 0

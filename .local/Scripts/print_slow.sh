#!/bin/sh

DEMO="a script for delayed print statements"

TEXT="$1"
SLEEPTIME="$2"
[ -z "$TEXT" ] && printf "Enter Text:\n" && read TEXT && printf "Enter sleep time:\n" && read SLEEPTIME
[ "$TEXT" = "-h" ] || [ "$TEXT" = "--help" ] && TEXT="$DEMO" && SLEEPTIME="0.1"
[ -z "$SLEEPTIME" ] && SLEEPTIME="0.03"
i=0 ; while [ "$i" -lt $(echo "$TEXT" | wc -c) ] ; do
	i=$(($i+1)) ; CHAR=$(echo "$TEXT" | cut -b "$i")
	[ ! -z "$CHAR" ] && printf "$CHAR" || printf " "
	sleep "$SLEEPTIME"
done
printf "\n"
exit 0

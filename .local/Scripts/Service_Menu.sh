#!/bin/sh
OPTION="$1"
[ -z "$OPTION" ] && OPTION=$(iselect "start a service<s:-start>" "stop a service<s:-stop>" "exit<s>")
case $OPTION in
	-start) START=$(iselect -n "start a service" "" "Select a service that you want to start" "(to enable a service permanently use the rc-update command)" "" "$(rc-status -s | grep 'stopped' | awk '{print $1}' | sed s'/$/<s>/g' | sort)" "" "Exit<s>" -p 5)
			[ -z "$START" ] && exit 0
			[ "$START" = "Exit" ] && exit 0
			sudo service "$START" start && echo "$START started";;
	-stop)	STOP=$(iselect -n "stop a service" "" "Select a service that you want to stop" "(to disable a service permanently use the rc-update command)" "" "$(rc-status -s | grep 'started' | awk '{print $1}' | sed s'/$/<s>/g' | sort)" "" "Exit<s>" -p 5)
			[ -z "$STOP" ] && exit 0
			[ "$STOP" = "Exit" ] && exit 0
			sudo service "$STOP" stop && echo "$STOP stopped";;
	*) exit 0 ;;
esac
exit 0

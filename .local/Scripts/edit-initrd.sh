#!/bin/sh

# check if user has root privileges
[ $(id -u) -eq 0 ] || { printf "\n\tYou need to be root :)\n" ; exit 1 ; }


# select initrd
if [ "$(ls /boot | grep initrd | wc -l)" -gt "1" ]; then
  initrd_image="$(iselect "Select initrd image" "" "$(ls /boot | grep initrd | sed s'/^/<s>/g')" "" "Exit<s:exit>" -p3)"
  [ "$initrd_image" = "exit" ] && exit 0
elif [ "$(ls /boot | grep initrd | wc -l)" = "1" ]; then
  initrd_image="$(ls /boot | grep initrd)"
else
  echo "Error: initrd not found" && exit 1
fi


# extract initrd
mkdir /tmp/extracted && cd /tmp/extracted
cp /boot/"$initrd_image" /tmp/extracted/"$initrd_image"
COMPRESSION=$(file -L "$initrd_image" | egrep -o 'gzip compressed|XZ compressed|cpio archive')
if [ "$COMPRESSION" = "gzip compressed" ]; then
  echo "Archive is gzip compressed..."
  zcat "$initrd_image" | cpio -i
elif [ "$COMPRESSION" = "XZ compressed" ]; then
  echo "Archive is XZ compressed..."
  xzcat "$initrd_image" | cpio -d -i -m
elif [ "$COMPRESSION" = "cpio archive" ]; then
  echo "Archive is cpio archive..." ;
  (cpio -i ; zcat | cpio -i) < /"$initrd_image"
  exit_code="$?"
  if [ "$exit_code" -ne 0 ] ; then
	rm -r *
	(cpio -i ; xzcat | cpio -i) < /initrd.img
	exit_code="$?"
	if [ "$exit_code" -ne 0 ]; then
	  echo "Decompression error" && exit 1
	else
	  COMPRESSION="XZ compressed"
	fi
  else
	COMPRESSION="gzip compressed"
  fi
  echo "COMPRESSION is $COMPRESSION"
else
  echo "Decompession error..." && exit 1
fi
echo "Initrd is extracted"
rm /tmp/extracted/"$initrd_image"


# edit initrd
EDIT_INIT="$(iselect "Do you want to edit the initrd image?" "" "Yes<s>" "No<s>" -p3)"

remove_cryptroot() {
  if [ -f conf/conf.d/cryptroot ]; then
    echo "Removing cryptroot"; rm -f conf/conf.d/cryptroot
  elif [ -f cryptroot ]; then
    echo "Removing cryptroot"; rm -f cryptroot
  fi
}

remove_resume() {
if [ -f conf/conf.d/resume ] ; then
  echo "Removing resume"; rm -f conf/conf.d/resume
elif [ -f conf/conf.d/zz-resume-auto ] ; then
  echo "Removing resume"; rm -f conf/conf.d/zz-resume-auto
fi
}

if [ "$EDIT_INIT" = "Yes" ]; then
  EDIT_INIT="$(iselect "What do you want to change?" "" "Remove cryptroot<s:crypt>" \
  "Remove resume<s:resume>" "Remove both<s:both>" "Continue without changes<s:cont>" \
  "" "(You have time to edit the files before you select something)" -p3)"
  case "$EDIT_INIT" in
    crypt)  remove_cryptroot;;
    resume) remove_resume;;
    both)   remove_cryptroot; remove_resume;;
    cont)   ;;
  esac
fi


# rebuild initrd
COMP="$(iselect "What compression do you prefer?" "" "Gzip<s>" "XZ<s>" "Same as original<s:same>" -p3)"
case "$COMP" in
  Gzip) COMPRESSION="gzip compressed";;
  XZ)   COMPRESSION="XZ compressed";;
  same) ;;
esac

if [ "$COMPRESSION" = "gzip compressed" ]; then
  find . -print0 | cpio -0 -H newc -o | gzip -c > "$initrd_image" && STATUS="OK"
elif [ "$COMPRESSION" = "XZ compressed" ]; then
  find . | cpio -o -H newc | xz --check=crc32 --x86 --lzma2=dict=512KiB > "$initrd_image" && STATUS="OK"
else
  echo "Compression error..."; exit 1
fi

if [ "$STATUS" = "OK" ] && [ -e /tmp/extracted/"$initrd_image" ]; then
ANS="$(iselect "Do you want to replace the old initrd with the new one now?" \
    "(The old file will be deleted)" "" "Yes<s>" "No<s>" -p4)"
  if [ "$ANS" = "Yes" ]; then
    rm /boot/"$initrd_image"
    mv /tmp/extracted/"$initrd_image" /boot/"$initrd_image"
  fi
fi
cd "$HOME"
rm -rf /tmp/extracted
echo "Done."; exit 0

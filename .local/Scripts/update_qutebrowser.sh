#!/bin/sh
# https://qutebrowser.org/doc/install.html#tox

# make sure to have the dependencies
# sudo apt install python3-venv asciidoc

# path were the source code will be extracted
QUTEBROWSER_SRC_DIR="$HOME/.local/src/qutebrowser"

# qutebrowser main file
QUTEBROWSER_CMD="$HOME/.venv/bin/python3 -m qutebrowser"

# Connection test
curl --connect-timeout 3 -s "https://github.com/qutebrowser/qutebrowser/releases" 1>/dev/null \
  || { printf "Error: could not connect to 'https://github.com/qutebrowser/qutebrowser/releases'\\n"; exit 1; } 

# get the latest release version number from github
LATEST_VERSION="$(lynx --dump https://github.com/qutebrowser/qutebrowser/releases \
  | grep 'Latest release' -A 1 -m 1 | tail -n 1 | cut -d ']' -f 2)"

install_func() {
  printf "found new version %s\\n" "$LATEST_VERSION"
  [ ! -d "$QUTEBROWSER_SRC_DIR" ] && mkdir -pv "$QUTEBROWSER_SRC_DIR"
#  printf "Qutebrowser %s will be downloaded and installed now.\\n" "$LATEST_VERSION"
  wget -c https://github.com/qutebrowser/qutebrowser/archive/"$LATEST_VERSION".tar.gz
  tar xvzfC "$LATEST_VERSION".tar.gz "$QUTEBROWSER_SRC_DIR" --strip-components=1 
  [ -f "$LATEST_VERSION".tar.gz ] && rm "$LATEST_VERSION".tar.gz
  python3 "$QUTEBROWSER_SRC_DIR"/scripts/mkvenv.py
  python3 "$QUTEBROWSER_SRC_DIR"/scripts/asciidoc2html.py
}

uninstall_func() {
  [ -d "$HOME/.cache/qutebrowser" ] && rm -rdv "$HOME/.cache/qutebrowser"
  [ -d "$HOME/.cache/pip" ] && rm -rdv "$HOME/.cache/pip"
  [ -d "$HOME/.venv" ] && rm -rdv "$HOME/.venv"
  [ -d "$HOME/.local/src/qutebrowser" ] && rm -rdv "$HOME/.local/src/qutebrowser"
  [ -d "$HOME/.local/share/qutebrowser" ] && rm -rdv "$HOME/.local/share/qutebrowser"
}

update_func() {
# get the currently installed version number
if [ -f .venv/bin/qutebrowser ]; then
  QUTEBROWSER_VERSION="$($QUTEBROWSER_CMD -V 2>/dev/null | grep -e ^"qutebrowser v" | cut -d ' ' -f 2)"
  printf "Searching updates for qutebrowser... "
else
  QUTEBROWSER_VERSION="0.0.0"
fi

  #printf "\\nCURRENT VERSION: %s\\n" "$QUTEBROWSER_VERSION"
  #printf "LATEST VERSION: %s\\n" "$LATEST_VERSION"
# compare version numbers
if [ -n "$LATEST_VERSION" ]; then
  GIT_V_PRIMARY="$(echo "$LATEST_VERSION" | cut -d '.' -f 1 | tr -d 'v')"
  GIT_V_SECONDARY="$(echo "$LATEST_VERSION" | cut -d '.' -f 2)"
  GIT_V_TERTIARY="$(echo "$LATEST_VERSION" | cut -d '.' -f 3)"
else
  printf "Error. Could not determine the latest version of qutebrowser.\\n"
  cd "$CURRENT_DIR"
  exit 1
fi

if [ -n "$QUTEBROWSER_VERSION" ]; then
  LOCAL_V_PRIMARY="$(echo "$QUTEBROWSER_VERSION" | cut -d '.' -f 1 | tr -d 'v')"
  LOCAL_V_SECONDARY="$(echo "$QUTEBROWSER_VERSION" | cut -d '.' -f 2)"
  LOCAL_V_TERTIARY="$(echo "$QUTEBROWSER_VERSION" | cut -d '.' -f 3)"
else
  install_func
fi

if [ -n "$LATEST_VERSION" ] && [ -n "$QUTEBROWSER_VERSION" ]; then
  if [ "$GIT_V_PRIMARY" -gt "$LOCAL_V_PRIMARY" ]; then
    install_func
  elif [ "$GIT_V_PRIMARY" -ge "$LOCAL_V_PRIMARY" ] && [ "$GIT_V_SECONDARY" -gt "$LOCAL_V_SECONDARY" ]; then
    install_func
  elif [ "$GIT_V_PRIMARY" -ge "$LOCAL_V_PRIMARY" ] && [ "$GIT_V_SECONDARY" -ge "$LOCAL_V_SECONDARY" ] && [ "$GIT_V_TERTIARY" -gt "$LOCAL_V_TERTIARY" ]; then
    install_func
  else
    printf "%s is up to date.\\n" "$QUTEBROWSER_VERSION"
  fi
fi
}

CURRENT_DIR="$(pwd)"
cd "$HOME"

case "$1" in
  --uninstall)	uninstall_func;;
  --reinstall)	uninstall_func; install_func;;
  --install)	install_func;;
  --update|*)	update_func;;
esac

cd "$CURRENT_DIR"
exit 0

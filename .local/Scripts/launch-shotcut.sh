#!/bin/sh
BASE_DIR="$HOME/.local/opt/shotcut"
APP_IMAGE="$(ls -1A "$BASE_DIR" | grep -i .AppImage$ | tail -n1)"

if [ -f "$BASE_DIR"/"$APP_IMAGE" ]; then
  if [ ! -x "$BASE_DIR"/"$APP_IMAGE" ]; then
    chmod +x "$BASE_DIR"/"$APP_IMAGE"
  fi
  "$BASE_DIR"/"$APP_IMAGE" --noupgrade
else
  echo "Error, could not find shotcut appimage."
  if [ -x /usr/local/bin/snd.sh ]; then
    /usr/local/bin/snd.sh --error
  fi
  exit 1
fi
exit 0


#!/bin/sh
PARENT="$1"
BASHTOP_LOCATION=".local/opt/bashtop/bashtop"

if [ "$PARENT" = "xterm" ]; then
  exec "$BASHTOP_LOCATION"
else
  xterm -e "$BASHTOP_LOCATION" &
  sleep 0.5 && echo ''
fi

exit 0

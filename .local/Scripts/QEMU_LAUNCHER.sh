#!/bin/sh
HEADER="Michael's  __     ___  ,__ __  _
          /  \   / (_)/|  |  |(_|    |  
         | __ |  \__   |  |  |  |    |  
         |/  \|  /     |  |  |  |    |  
          \__/\_/\___/ |  |  |_/ \__/\_/
   _    ___, _        , _     ___  ,      ___  , __  
\_|_)  /   |(_|    | /|/ \   / (_)/|   | / (_)/|/  \ 
  |   |    |  |    |  |   | |      |___| \__   |___/ 
 _|   |    |  |    |  |   | |      |   |\/     | \   
(/\___/\__/\_/ \__/\_/|   |_/\___/ |   |/\___/ |  \_/
"
update_text() {
OPTIONS="   > SELECT PRESET <s:LOAD_PRESET>
   > CREATE NEW IMAGE <s:CREATE>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 > ARCH   = $ARCH <s:SET_ARCH>
 > IMAGE  = $IMAGE <s:SET_IMAGE>
 > KVM    = $KVM <s:SET_KVM>
 > CPU    = $USE_CPU <s:SET_CORES>
 > MEMORY = $MEMORY <s:SET_MEMORY>
 > CDROM  = $CDROM <s:SET_CDROM>
 > NETWORK BRIDGE = $NETWORK <s:SET_NETWORK>
 > USB PASSTROUGH = $USB <s:SET_USB>
 > SHARE FOLDER   = $SHARE <s:SET_SHARE>
 > CUSTOM OPTIONS = $CUSTOM <s:SET_CUSTOM>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   > START VM <s:RUN_VM>

   > SAVE AS NEW PRESET <s:SAVE_PRESET>
   > MANAGE PRESETS <s:MANAGE_PRESETS>"
}
update_text

check_input_status() {
NEW_INPUT_STATUS=""
#make sure there are only alphanumeric characters and '-' or '_' except at the beginning or end
if [ -z $(echo -n "$NEW_INPUT" | tr -d _- | tr -d '[:alnum:]') ] && [ -z $(echo "$NEW_INPUT" | cut -b 1,$(echo -n "$NEW_INPUT" | wc -c) | grep -e [-_] ) ]; then
  NEW_INPUT_STATUS="OK"
else
  NEW_INPUT_STATUS="Not OK"
fi
}

check_filepath() {
NEW_PATH_STATUS=""
# check if there is input
[ -z "$NEW_PATH" ] || PATH_NOT_EMPTY="OK"
# check if there are spaces
[ -z $(echo "$NEW_PATH" | grep ' ') ] && PATH_SPACES="OK" || PATH_SPACES="ERROR"
# check if path only contains alphanumerical characters and '.' or '/'
[ -z $(echo -n "$NEW_PATH" | tr -d /. | tr -d '[:alnum:]') ] && PATH_CHARS="OK" || PATH_CHARS="ERROR"
# make sure there is no '//'
[ -z $(echo "$NEW_PATH" | grep '//' ) ] && PATH_DOUBLE_SLASH="OK" || PATH_DOUBLE_SLASH="ERROR"
# make sure there is no '..'
[ -z $(echo "$NEW_PATH" | grep -F '..' ) ] && PATH_DOUBLE_DOT="OK" || PATH_DOUBLE_DOT="ERROR"
# make sure last byte is not '.'
[ -z $(echo "$NEW_PATH" | cut -b $(echo -n "$NEW_PATH" | wc -c) | grep -F '.') ] && PATH_END_NO_DOT="OK" || PATH_END_NO_DOT="ERROR"
# make sure last byte is not '/'
[ -z $(echo "$NEW_PATH" | cut -b $(echo -n "$NEW_PATH" | wc -c) | grep '/') ] && PATH_END_NO_SLASH="OK" || PATH_END_NO_SLASH="ERROR"
# check length
[ $(echo -n "$NEW_PATH" | tr -d /.  | wc -c) -ge 1 ] && [ $(echo -n "$NEW_PATH" | wc -c) -lt 63 ] && PATH_LENGHT="OK" || PATH_LENGHT="ERROR"

if  [ -z "$NEW_PATH" ] ; then NEW_PATH_STATUS="OK"
elif [ "$PATH_CHARS" = "ERROR" ] || [ "$PATH_SPACES" = "ERROR" ] || [ "$PATH_DOUBLE_SLASH" = "ERROR" ] || [ "$PATH_DOUBLE_DOT" = "ERROR" ] ||  [ "$PATH_END_NO_DOT" = "ERROR" ] ||  [ "$PATH_END_NO_SLASH" = "ERROR" ] ; then clear; echo "\n\t Error, invalid character detected. Try again."; echo -n "$NEW_PATH_MSG"
elif [ "$PATH_LENGHT" = "ERROR" ] ; then clear; echo "\n\t Lenght error. Try again."; echo -n "$NEW_PATH_MSG"
elif [ "$PATH_CHARS" = "OK" ] && [ "$PATH_SPACES" = "OK" ] && [ "$PATH_DOUBLE_SLASH" = "OK" ] && [ "$PATH_DOUBLE_DOT" = "OK" ] && [ "$PATH_END_NO_DOT" = "OK" ] && [ "$PATH_END_NO_SLASH" = "OK" ] && [ "$PATH_LENGHT" = "OK" ] ; then NEW_PATH_STATUS="OK" ; PATH_NOT_EMPTY=""; PATH_SPACES=""; PATH_CHARS=""; PATH_DOUBLE_SLASH=""; PATH_DOUBLE_DOT=""; PATH_END_NO_DOT=""; PATH_END_NO_SLASH=""; PATH_LENGHT=""
fi
}

create_vm() {
  clear; echo -n "Enter a path where the vm file will be created: "
read NEW_PATH
check_filepath
[ "$NEW_PATH_STATUS" = "OK" ] && mkdir -p "$NEW_PATH"
islect "$(ls -1A $NEW_PATH)"
NEW_NAME=""
NEW_INPUT=""
while [ -z "$NEW_NAME" ] ; do
  clear; echo -n "Enter a name for this VM: "
  read NEW_INPUT ; check_input_status
  [ -z "$NEW_INPUT" ] && break
  [ "$NEW_INPUT_STATUS" = "OK" ] && NEW_NAME="$NEW_INPUT" || NEW_NAME=""
done
echo "$NEW_INPUT_STATUS"
NEW_SIZE=$(iselect "" "How large shall the file be?"\
		"  1GB<s:1G>" "  2GB<s:2G>" "  4GB<s:4G>" "  8GB<s:8G>"\
		"  16GB<s:16G>" "  32GB<s:32G>" "  64GB<s:64G>" "  100GB<s:100G>"\
		" 128GB<s:128G>" " 200GB<s:200G>" " 256GB<s:256G>" -p 7)
qemu-img create -f qcow2 "$NEW_PATH/$NEW_NAME.qcow2" "$NEW_SIZE"
}

set_qcow() {
IMAGE=""
while [ -z "$IMAGE" ] ; do
DIRS=""
for i in $(ls -1); do
  [ -d "$i" ] && DIRS=$(echo "$DIRS\n$i<s>")
  [ "$(file $(pwd)/$i -b | cut -d ' ' -f 1)" = "QEMU" ] && DIRS=$(echo "$DIRS\n$i<s>")
  [ "$(file $(pwd)/$i -ib | cut -d\/ -f 2 | cut -d\; -f 1)" = "x-iso9660-image" ] && DIRS=$(echo "$DIRS\n$i<s>")

done
NEW_DIR=$(iselect "$DIRS" "..<s>" "" "BACK<s>" -p 2)
[ -d "$NEW_DIR" ] && cd "$NEW_DIR"
[ "$(file $(pwd)/$NEW_DIR -b | cut -d ' ' -f 1)" = "QEMU" ] && IMAGE="-hda $(pwd)/$NEW_DIR"
[ "$(file $(pwd)/$NEW_DIR -ib | cut -d\/ -f 2 | cut -d\; -f 1)" = "x-iso9660-image" ] && IMAGE="-hda $(pwd)/$NEW_DIR"
[ "$NEW_DIR" = "BACK" ] && return
done
}

set_memory() {
MEMORY=$(iselect "" "   How much RAM?"\
		"     256MB<s:-m 256M>"	"     512MB<s:-m 512M>"	"     1GB<s:-m 1G>"\
		"     2GB<s:-m 2G>"		"     4GB<s:-m 4G>"		"     8GB<s:-m 8G>"\
		"     16GB<s:-m 16G>" -p 3)
}

set_cd() {
CDROM=""
while [ -z "$CDROM" ] ; do
DIRS=""
for i in $(ls -1); do
  [ -d "$i" ] && DIRS=$(echo "$DIRS\n$i<s>")
  [ "$(file "$(pwd)/$i" -ib | cut -d\/ -f 2 | cut -d\; -f 1)" = "x-iso9660-image" ] && DIRS=$(echo "$DIRS\n$i<s>")
done
NEW_DIR=$(iselect "$DIRS" "..<s>" "" "BACK<s>" -p 2)
[ -d "$NEW_DIR" ] && cd "$NEW_DIR"
[ "$(file "$(pwd)/$NEW_DIR" -ib | cut -d\/ -f 2 | cut -d\; -f 1)" = "x-iso9660-image" ] && CDROM="-cdrom $(pwd)/$NEW_DIR"
[ "$NEW_DIR" = "BACK" ] && return
done
}

set_usb() {
USB=""
USB_TARGET=$(iselect -n "Select an USB device" "$(lsusb)" -a)
USB_BUS=$(echo "$USB_TARGET" | awk '{print $2}')
USB_DEVICE=$(echo "$USB_TARGET" | awk '{print $4}'| sed s'/://')
[ -z "$USB_TARGET" ] || USB="-usb -device usb-host,hostbus=$USB_BUS,hostaddr=$USB_DEVICE"

}

load_preset() {
if [ -e "$PRESET" ] ; then
ARCH=$(grep "ARCH=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
IMAGE=$(grep "IMAGE=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
KVM=$(grep "KVM=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
USE_CPU=$(grep "USE_CPU=" "$PRESET" | cut -b9-)
MEMORY=$(grep "MEMORY=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
CDROM=$(grep "CDROM=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
NETWORK=$(grep "NETWORK=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
USB=$(grep "USB=" "$PRESET" | cut -b5-)
SHARE=$(grep "SHARE=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
CUSTOM=$(grep "CUSTOM=" "$PRESET" | cut -d\= -f2 | tr '"' ' ')
fi
}

save_new() {
NEW_NAME=""
NEW_INPUT=""
while [ -z "$NEW_NAME" ] ; do
  clear; echo -n "Enter a name for this preset: "
  read NEW_INPUT ; check_input_status
  echo "$NEW_INPUT_STATUS"
  [ "$NEW_INPUT_STATUS" = "OK" ] && NEW_NAME="$NEW_INPUT" || NEW_NAME=""
done
echo "NEW_NAME = $NEW_NAME"
cat > "$HOME/.cache/qemu-launcher-preset-$NEW_NAME.txt" <<EOF
ARCH=$ARCH
IMAGE=$IMAGE
KVM=$KVM
USE_CPU=$USE_CPU
MEMORY=$MEMORY
CDROM=$CDROM
NETWORK=$NETWORK
USB=$USB
SHARE=$SHARE
CUSTOM=$CUSTOM
EOF
}

preset_func() {
PRESET_OPT=$(iselect "Set default preset <s:P_DEFAULT>" "load preset <s:P_LOAD>" "remove presets <s:P_REMOVE>" "edit presets <s:P_EDIT>")
case $PRESET_OPT in
	P_DEFAULT) set_as_default;;
	P_LOAD) PRESET=$(iselect "$(find .cache -iname "qemu-launcher-preset*.txt")" -a); load_preset;;
	P_REMOVE) rm $(iselect -n "Select a default preset" "$(find .cache -iname "qemu-launcher-preset*.txt")" -a);;
	P_EDIT) nano $(iselect -n "Select a default preset" "$(find .cache -iname "qemu-launcher-preset*.txt")" -a);;
esac
}

set_as_default() {
NEW_DEFAULT=$(iselect -n "Select a default preset" "$(find .cache -iname "qemu-launcher-preset*.txt")" -a)
for x in $(find .cache -iname "qemu-launcher-preset*.txt" | grep DEFAULT | cut -d\: -f 1) ; do
 sed -i s'/DEFAULT//' "$x"
done
echo "DEFAULT" >> "$NEW_DEFAULT"
}

set_cpu() {
SOCKETS=$(lscpu | grep "Socket(s):" | cut -d\: -f 2 | awk '{print $1}')
THREADS_CORE=$(lscpu | grep "Thread(s) per core:" | cut -d\: -f 2 | awk '{print $1}')
CORES_SOCKET=$(lscpu | grep "Core(s) per socket:" | cut -d\: -f 2 | awk '{print $1}')
CORES=$(($SOCKETS * $CORES_SOCKET))
CORELIST=""; x=0; while [ "$x" -le "$CORES" ]; do CORELIST="$CORELIST $x"; x=$(($x+1)); done
LIST_CORES=$(echo "$CORELIST" | xargs -n1 | grep -v 0 | sed s'/$/<s>/g')
THREADLIST=""; x=0; while [ "$x" -le "$THREADS_CORE" ]; do THREADLIST="$THREADLIST $x"; x=$(($x+1)); done
LIST_THREADS=$(echo "$THREADLIST" | xargs -n1 | grep -v 0 | sed s'/$/<s>/g')
USE_CORES=$(iselect "How many Cores gets this VM?" "$LIST_CORES" -p 2)
USE_THREADS=$(iselect "How many Threads per Core?" "$LIST_THREADS" -p 2)
[ "$USE_CORES" ] && [ "$USE_THREADS" ] && USE_CPU="-smp cores=$USE_CORES,threads=$USE_THREADS"
}

check_sudo() {
[ -z "$USB" ] || SUDO=sudo
}

if [ "$(find .cache -iname "qemu-launcher-preset*.txt" | wc -l)" = "1" ] ; then
  PRESET=$(find .cache -iname "qemu-launcher-preset*.txt")
elif [ "$(find .cache -iname "qemu-launcher-preset*.txt" | wc -l)" -gt "1" ] ; then
  PRESET=$(grep "DEFAULT" .cache/qemu-launcher-preset*.txt | cut -d\: -f 1) && load_preset
fi

update_command() {
check_sudo
COMMAND="${SUDO} ${ARCH} ${MEMORY} ${IMAGE} ${KVM} ${USE_CPU} ${CDROM} ${USB} ${CUSTOM}"
}
update_command
[ "$1" = "-d" ] && echo "$COMMAND" && exit 0

while true; do
update_text
SLECTION=""
SELECTION=$(iselect "$HEADER" "$OPTIONS" -p 12)
case $SELECTION in
  LOAD_PRESET) PRESET=$(iselect "$(find .cache -iname "qemu-launcher-preset*.txt")" -a); load_preset;;
  CREATE) create_vm;;
  SET_ARCH) ARCH=$(iselect "qemu-system-i386" "qemu-system-x86_64" "qemu-system-arm" -a -p 2);;
  SET_IMAGE) set_qcow;;
  SET_KVM) KVM=$(iselect "KVM Options" "enable<s>" "disable<s>" -p 2); [ "$KVM" = "enable" ] && KVM="-enable-kvm -cpu host" || KVM="";;
  SET_CORES) set_cpu;;
  SET_MEMORY) set_memory;;
  SET_CDROM) set_cd;;
  SET_NETWORK) clear; echo "Sorry, this option is not implemented yet." && sleep 2 ;;
  SET_USB) set_usb;;
  SET_SHARE) clear; echo "Sorry, this option is not implemented yet." && sleep 2 ;;
  SET_CUSTOM) clear; echo -n "Enter custom options: "; read CUSTOM;;
  RUN_VM) update_command; $COMMAND;;
  SAVE_PRESET) save_new;;
  MANAGE_PRESETS) preset_func;;
  SET_DEFAULT) set_as_default;;
  *) ;;
esac
done

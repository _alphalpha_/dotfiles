#!/bin/sh
# get youtube rss url from channel
if [ "$1" ]; then
  URL="$1"
else
  printf "Enter a youtube channel url: "
  read URL
fi
RSSURL="$(curl -s "$URL" | tr ',' '\n' | grep rssUrl | cut -d '"' -f4)"
if [ "$RSSURL" ]; then
  printf "Rss Url: %s \\n" "$RSSURL"
else
  printf "Error. \\n"
fi
exit 0

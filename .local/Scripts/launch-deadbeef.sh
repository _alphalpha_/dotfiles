#!/bin/sh
DEADBEEF="$HOME/.local/opt/deadbeef/deadbeef"
[ "$1" ] && "$DEADBEEF" --queue "$1" || "$DEADBEEF"
exit 0
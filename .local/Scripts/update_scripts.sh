#!/bin/sh
# Script to check for new versions of Michaels Scripts
GITURL="https://gitlab.com/_alphalpha_/dotfiles.git"
GITSUBDIR=".local/Scripts"
LOCALDIR="michaels-scripts-git"
TMPDIR="$HOME/.local/tmp"

# prep
[ -d "$TMPDIR" ] || mkdir -p "$TMPDIR"
[ -d "$TMPDIR/$LOCALDIR" ] && rm -rdf "$TMPDIR/$LOCALDIR"
CURRENTDIR="$(pwd)"


# git clone only .local/Scripts
cd "$TMPDIR"
git init "$LOCALDIR"
cd "$LOCALDIR"
git remote add origin "$GITURL"
git config core.sparsecheckout true
echo "$GITSUBDIR/*" >> .git/info/sparse-checkout
git pull --depth=1 origin master
mv "$GITSUBDIR"/* .
if [ "$(echo "$GITSUBDIR" | grep '/')" ]; then
  GITSUBDIR="$(echo "$GITSUBDIR" | cut -d '/' -f1)"
fi
rm -rdf "$GITSUBDIR" .git


# make a list of every script that has been updated
LIST=""
for f in $(ls -1A); do
  if [ -f "$HOME/.local/Scripts/$f" ]; then
    if [ "$(diff "$f" "$HOME/.local/Scripts/$f")" ]; then
      printf "found new version of %s\\n" "$f"
      LIST="$LIST $f"
    fi
  elif [ -f "$TMPDIR/$f" ]; then
    printf "found new script: %s\\n" "$f"
    LIST="$LIST $f"
  fi
done


# throw together a menu
#LIST="$(echo "$LIST" | xargs -n1 | sed 's/$/<s>/g')"
while [ "$(echo -n "$LIST" | wc -w)" -gt 0 ]; do
  [ -z "$OPT" ] && OPT="$(iselect -p4 \
  "Found new versions for Michaels scripts." \
  "Select individual script for more options:" "" \
  "$(echo "$LIST" | xargs -n1 | sed 's/$/<s>/g')" \
  "" "> Install everything<s:INSTALL_ALL>" "< Exit<s:EXIT>")"
  if [ "$OPT" = "INSTALL_ALL" ]; then
  for SCRIPT in $LIST; do
    cp -v "$TMPDIR/$LOCALDIR/$SCRIPT" "$HOME/.local/Scripts/"; 
  done
    exit 0
  elif [ "$OPT" = "EXIT" ]; then
    exit 0
  fi
  ACTION=""
  while [ "$OPT" ] ; do
    ACTION="$(iselect -p3 \
      "$OPT" "" \
      "view diff<s:DIFF>" \
      "open in editor<s:OPEN>" \
      "install<s:INSTALL>" \
      "skip<s:SKIP>")"
  case "$ACTION" in
    DIFF)	diff "$TMPDIR/$LOCALDIR/$OPT" "$HOME/.local/Scripts/$OPT" | less;;
    OPEN)	featherpad "$TMPDIR/$LOCALDIR/$OPT";;
    INSTALL) cp -v "$TMPDIR/$LOCALDIR/$OPT" "$HOME/.local/Scripts/$OPT";
             LIST="$(echo "$LIST" | sed "s:$OPT::")"; OPT="";;
    SKIP)	LIST="$(echo "$LIST" | sed "s:$OPT::")"; OPT="";;
    *) ACTION="-";OPT="";;
  esac
  done
done


# cleanup
cd "$CURRENTDIR"
rm -rdf "$TMPDIR/$LOCALDIR/"
[ ! -z "$(find "$TMPDIR" -maxdepth 0 -empty)" ] && rm -rdf "$TMPDIR"
exit 0

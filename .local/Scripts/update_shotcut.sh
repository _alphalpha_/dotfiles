#!/bin/sh
# this script checks for new appimages of shotcut

# connection test
curl --connect-timeout 3 -s "https://github.com" 1>/dev/null \
  || { printf "Error: could not connect to 'https://github.com'\\n"; exit 1;} 

# appimage directory
SHOTCUT_DIR="$HOME/.local/opt/shotcut"

# get current version number
CURRENT_VERSION="$(ls -1A "$SHOTCUT_DIR" | tail -n1)"

# check for lastest version on github
LATEST_RELEASE_URL=$(curl -s "https://github.com/mltframework/shotcut/releases/latest" \
  | grep -o -E "https://github.com/mltframework/shotcut/releases/tag/v[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}")
DL_URL="$(curl -s "$LATEST_RELEASE_URL" | grep AppImage | head -n1 | cut -d '"' -f2 | sed 's;^;https://github.com;')"
NEW_VERSION="$(echo "$DL_URL" | tr '/' ' ' | awk '{print $NF}' )"

# compare versions and download
printf "Searching updates for shotcut... "
if [ "$(echo "$NEW_VERSION\n$CURRENT_VERSION" | sort | tail -n1)" = "$CURRENT_VERSION" ]; then
  printf "%s is up to date \\n" "$CURRENT_VERSION"
elif [ "$(echo "$NEW_VERSION\n$CURRENT_VERSION" | sort | tail -n1)" = "$NEW_VERSION" ]; then
  printf "found new version %s\\n" "$NEW_VERSION"
  wget -c -P "$SHOTCUT_DIR" "$DL_URL" 
  [ -f "$SHOTCUT_DIR"/"$CURRENT_VERSION" ] && rm "$SHOTCUT_DIR"/"$CURRENT_VERSION"
fi

exit 0

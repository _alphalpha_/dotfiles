#!/bin/sh

screenshot_dir="$HOME/Screenshots"
screenshot_res=$(xdpyinfo | awk '/dimensions/ {print $2}')
screenshot_name=$(date +Screenshot-%Y-%m-%d_%H-%M-%S)
screenshot_format=".png"
[ -d "$screenshot_dir" ] || mkdir -pv "$screenshot_dir"
sleep 0.15
ffmpeg -f x11grab -r 1 -s "$screenshot_res" -i :0.0  "$screenshot_dir"/"$screenshot_name""$screenshot_format"
exit 0

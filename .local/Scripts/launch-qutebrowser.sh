#!/bin/sh
STARTPAGE="file:///var/www/html/startpage.html"

# Check for firejail
check_firejail(){
if [ -f /usr/bin/firejail ]; then
  sandbox="firejail --machine-id --profile=/etc/firejail/qutebrowser-m.profile"
  if [ ! -f /etc/firejail/qutebrowser-m.profile ]; then
    echo "Could not find /etc/firejail/qutebrowser-m.profile"
    echo "You need to create a firejail profile or use this script with the '--no-firejail' option"
    echo "You can run .local/Scripts/qutebrowser-firejail-conf.sh with sudo to autogenerate a profile"
    exit 1
  fi
fi
}

# Launch qutebrowser
launch_qutebrowser(){
${sandbox} .venv/bin/python3 -m qutebrowser "$STARTPAGE"
}

[ -d "$HOME/Downloads" ] || mkdir "$HOME/Downloads"

while [ -n "$1" ]; do
  case "$1" in
    --no-firejail) USE_FIREJAIL="0";;
    --url|*) [ "$(echo "$1" | grep -E "(([A-Za-z]{3,9})://)?([-;:&=\+\$,\w]+@{1})?(([-A-Za-z0-9]+\.)+[A-Za-z]{2,3})(:\d+)?((/[-\+~%/\.\w]+)?/?([&?][-\+=&;%@\.\w]+)?(#[\w]+)?)?")" ] && STARTPAGE="$1";;
  esac
  shift
done

if [ "$USE_FIREJAIL" = 0 ]; then
  launch_qutebrowser
else
  check_firejail; launch_qutebrowser
fi
exit 0

#!/bin/sh
if [ -f "$1" ]; then
  WP="$1"
else
  WP="$(iselect $(find /usr/share/backgrounds -type f) -a)"
fi
LINK="$HOME/.local/wallpaper.png"
if [ -f "$WP" ]; then
  [ -e "$LINK" ] && rm "$LINK"
  ln -s "$WP" "$LINK"
  xwallpaper --zoom "$LINK"
fi
exit 0

#!/bin/sh
# write iso file to usb device with iselect menu
ISO_DIR="/home/snapshot"

if [ "$1" ] ; then
  [ ! -f "$(pwd -L)/$1" ] && echo "Error, No such ISO file" && exit 1
  SOURCE_FILE="$1"
fi

if [ "$2" ] ; then
  [ -z "$(ls $2)" ] && echo "Error, No such USB device" && exit 1
  TARGET_DEVICE="$2"
fi

# use sudo if needed
[ $(id -u) -eq 0 ] && PRIV="" || PRIV="sudo"

if [ -z "$(lsblk -o NAME,TRAN,SUBSYSTEMS -d | grep usb)" ]; then
  echo "No USB Devices found!" && exit 1
fi

for iso_file in $(find . -maxdepth 1 -type f -iname "*.iso" | cut -d '/' -f 2); do
 iso_list="$iso_list
 $iso_file <s:$iso_file>"
done

for iso_file in $(find . -maxdepth 1 -type f -iname "*.img" | cut -d '/' -f 2); do
 iso_list="$iso_list
 $iso_file <s:$iso_file>"
done

if [ -d "$ISO_DIR" ]; then
  for iso_file in $(find "$ISO_DIR" -maxdepth 2 -type f -iname "*.iso" | sort ); do
   iso_list="$iso_list
   $iso_file <s:$iso_file>"
  done
fi

 TITLE="SELECT YOUR ISO IMAGE  (SHOWS ISOs IN WORKING DIRECTORY & IN ISO_DIR)  (ISO_DIR=$ISO_DIR)"
 LEFT_STRING="Secelt ISO File"
 RIGHT_STRING="dd-iso2usb-script"
 SOURCE_FILE=$(iselect -n "$LEFT_STRING" -t "$RIGHT_STRING" "$TITLE" "$iso_list" -p 3)
 echo " SOURCE_FILE = $SOURCE_FILE \n"

if ! [ -f "$SOURCE_FILE" ] ; then echo "Error, No such ISO file" && exit 0; fi

if [ -z "$TARGET_DEVICE" ] ; then
  device_list=""
  for usb_device in $(lsblk -dpo NAME,TRAN,SUBSYSTEMS | awk '/usb/ {print $1}' ); do
    device_list="$device_list
$(lsblk -dnpo NAME,MODEL,SIZE $usb_device) <s:$usb_device>"
  done

  TITLE="SELECT YOUR USB DEVICE   (WARNING: ALL DATA ON THIS DEVICE WILL BE LOST)"
  LEFT_STRING="Secelt USB Device"
  RIGHT_STRING="dd-iso2usb-script"
  TARGET_DEVICE=$(iselect -n "$LEFT_STRING" -t "$RIGHT_STRING" "$TITLE" "$device_list" -p 4)
fi

[ -z "$(ls $TARGET_DEVICE)" ] && echo "Error, No such USB device" && exit 1

if [ -f "$SOURCE_FILE" ] && [ -b "$TARGET_DEVICE" ]; then
  clear
  if [ -f "$(pwd -L)/$SOURCE_FILE" ]; then
	echo -n "\n SOURCE_FILE=\033[1;33m$(pwd -L)/$SOURCE_FILE  $(du -h $(pwd -L)/$SOURCE_FILE| awk '{print $1}')\033[0m \n\n TARGET_DEVICE=\033[1;33m$TARGET_DEVICE\033[0m\n "
	lsblk "$TARGET_DEVICE" -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL| head -n 1
	echo "\033[1;33m $(lsblk $TARGET_DEVICE -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL|tail -n 1)\033[0m"
	echo -n "\n Hit Enter to Start or Control+C to Cancel\n \033[1;33m(Warning: All Data on $TARGET_DEVICE will be lost!)\033[0m "
	read confirm && echo ""
	${PRIV} dd if="$(pwd -L)/$SOURCE_FILE" of="$TARGET_DEVICE" bs=4M status=progress && sync
  elif ! [ -z $(echo "$SOURCE_FILE)" | grep "$ISO_DIR") ]; then
	echo -n "\n SOURCE_FILE=\033[1;33m"$SOURCE_FILE"  $(du -h "$SOURCE_FILE"| awk '{print $1}')\033[0m \n\n TARGET_DEVICE=\033[1;33m$TARGET_DEVICE\033[0m\n "
	lsblk "$TARGET_DEVICE" -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL| head -n 1
	echo "\033[1;33m $(lsblk $TARGET_DEVICE -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL|tail -n 1)\033[0m\n"
	echo -n "\n Hit Enter to Start or Control+C to Cancel\n \033[1;33m(Warning: All Data on $TARGET_DEVICE will be lost!)\033[0m "
	read confirm && echo ""
	${PRIV} dd if="$SOURCE_FILE" of="$TARGET_DEVICE" bs=4M status=progress && sync
  fi
fi
exit 0

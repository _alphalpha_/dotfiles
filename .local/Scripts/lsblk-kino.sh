#!/bin/sh

main() {
for DEVICE in $(lsblk -dnpo NAME); do
if [ -z $LINE ]; then
  echo "\n\033[1;31m┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
else
  echo "\033[1;31m┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
fi
echo -n "\033[1;33m$(lsblk -dnpo NAME,MODEL,SIZE "$DEVICE")\033[0;31m" \
| awk '{print "┃" "\033[0;35m" $1 "  " $2 "   \033[1;35m" $3}'
  for PARTITION in $(lsblk -nplo NAME $DEVICE | grep '[1-99]$' | grep -v '^/dev/mapper'); do
      if [ "$(lsblk -no MOUNTPOINT $PARTITION | wc -l)" = "1" ] && [ "$(lsblk -no MOUNTPOINT $PARTITION)" ]; then
        echo -n "\033[0;35m$(lsblk -lnpo NAME,FSTYPE,MOUNTPOINT "$PARTITION") $(df -h "$PARTITION" --output=used,size,pcent | tail -n1)" \
         | awk '{print "\033[0;31m┃ > " "\033[0;33m" $1 "\t" "\033[1;34m" $2 "\t" "\033[1;36m" $4 \
          "\033[0;35m/\033[1;36m" $5 "\033[0;35m" " (" $6  ")" "\t" "\033[1;32m" $3 "\033[0m"}' | head -n1
      elif [ "$(lsblk -no MOUNTPOINT $PARTITION | wc -l)" -gt "1" ]; then
        echo -n "\033[0;35m$(lsblk -lnpo NAME,SIZE "$PARTITION")" \
        | awk '{print "\033[0;31m┃ > " "\033[0;35m" $1 "\t\t"  "\033[1;36m" $2 "\033[0m"}' | head -n1
      elif [ "$(lsblk -no MOUNTPOINT $PARTITION | wc -c)" = "1" ]; then
        echo -n "\033[0;35m$(lsblk -lnpo NAME,FSTYPE,SIZE "$PARTITION")" \
        | awk '{print "\033[0;31m┃ > " "\033[0;35m" $1 "\t" "\033[1;34m" $2 "\t" "\033[1;36m" $3 "\033[0m"}' | head -n1
      fi
    if [ "$(lsblk -nplo PKNAME "$PARTITION" | grep -w "$DEVICE[1-99]")"  ]; then
      MAJ="$(lsblk -lno MAJ:MIN "$PARTITION" | grep :0 | cut -d\: -f 1)"
      for SUB in $(lsblk -nplo NAME "$PARTITION");  do
        if [ "$SUB" != "$PARTITION" ]; then
          MIN="$(lsblk -nplo NAME,MAJ:MIN "$SUB" | grep -w "$SUB" | grep "$MAJ:[0-99]" | cut -d\: -f 2 | awk '{print $1}')"
          if [ "$MIN" = "0" ] ; then
            echo -n "\033[0;35m$(lsblk -lnpo NAME,SIZE,TYPE "$SUB")" \
            | awk '{print "\033[0;31m┃ └> " "\033[0;33m" $1 "\t" "\033[1;34m" $3 "\t" "\033[1;36m" $2 \
              "\033[0m"}' | head -n1
          else
           echo -n "\033[0;35m$(lsblk -lnpo NAME,MOUNTPOINT "$SUB") $(df -h "$SUB" --output=used,size,pcent | tail -n1)" \
           | awk '{print "\033[0;31m┃  └> " "\033[0;33m" $1 "\t" "\033[1;36m" $3 \
             "\033[0;35m/\033[1;36m" $4 "\033[0;35m" " (" $5 ")" "\t" "\033[1;32m" $2 "\033[0m"}' | head -n1
          fi
        fi
      done
    fi
  done
LINE="1"
done
echo "\033[1;31m┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n"
}

if [ "$1" = "--loop" ] || [ "$1" = "-l" ]; then
  clear; main
  LSBLK_OUT="$(lsblk -nlo NAME)"
  while true; do
    if [ "$LSBLK_OUT" != "$(lsblk -nlo NAME)" ]; then
      LSBLK_OUT="$(lsblk -nlo NAME)"; clear; LINE=""; main
    fi
    sleep 1
  done
else
  main
fi
exit 0

#!/bin/sh
[ "$(grep 'panel' ~/.config/jwm/jwmrc | grep -w '<!--')" ] \
  && sed -i s'*<!-- <Include>.config/jwm/panel</Include> -->*<Include>.config/jwm/panel</Include>*' ~/.config/jwm/jwmrc \
  || sed -i s'*<Include>.config/jwm/panel</Include>*<!-- <Include>.config/jwm/panel</Include> -->*' ~/.config/jwm/jwmrc
jwm -restart

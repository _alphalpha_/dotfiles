#!/usr/bin/zsh
# Observe items on amazon over time and draw prices into a graph
# by Michael
VERSION="0.3"
HEADER="
                                                         _
  /\     _  _   __, __  __ _  _  |   |   | __, _|_  __  | |    _   ,_  _
 /__\  |/ |/ | /  |  / /  \ |/ | |   |   |/  |  |  /    |/ \  |/  /  |/
/    \ |  |  | \_/| /_ \__/ |  |  \_/ \_/ \_/|_/|_/\___/|   |/|__/   |

	                                        VERSION $VERSION    BY MICHAEL
__________________________________________________________________________
"
CURRENCY="€"
FOLDER="$HOME/.cache/amazon_watcher"
LISTFILE="/$HOME/.cache/amazon_watcher/list.txt"
[ -d "$FOLDER" ] || mkdir "$FOLDER"

CHECK_CACHE() {
for ITEM in $(ls .cache/amazon_watcher -A | grep '.txt' | grep -v 'list.txt'); do
  if [ "$(cat .cache/amazon_watcher/list.txt | cut -d\  -f1 | sed s'/$/.txt/'g | grep "$ITEM")" ]; then
    echo "found $ITEM"
  else
    SUBITEM=$(echo "$ITEM" | sed s'/.txt/.py/')
    rm "$FOLDER/$ITEM" "$FOLDER/$SUBITEM"
  fi
done
}
CHECK_CACHE

READ_DESC() {
clear; echo "$HEADER"
echo "\n Enter a shot description for the item."
echo -n " "
read DESC
[ "$(echo "$DESC" | grep " ")" ] && DESC=$(echo "$DESC" | tr ' ' '_')
OFFSET=1
ADD_ITEM
}

READ_URL() {
clear; echo "$HEADER"
echo "\n Enter the URL for the item."
echo -n " "
read URL
OFFSET=2
ADD_ITEM
}

ADD_ITEM() {
[ "$OFFSET" ] || OFFSET=0
ADD_ITEM_TEXT="
 > DESCRIPTION = $DESC <s:ADD_DESC>
 > URL = $URL <s:ADD_URL>
 > ADD <s:ADD_TO_LIST>

 < BACK <s:BACK>"
OPTION=$(iselect "$HEADER" "$ADD_ITEM_TEXT" -p $((11+$OFFSET)))
case $OPTION in
 ADD_DESC) READ_DESC;;
 ADD_URL) READ_URL;;
 ADD_TO_LIST) echo "$DESC $URL" >> "$LISTFILE"; DESC=""; URL=""; clear;;
 BACK) MAIN_MENU;;
esac
MAIN_MENU
}

UPDATE() {
COUNT=0
MAXCOUNT=$(wc -l "$LISTFILE" | awk '{print $1}')
while [ "$COUNT" -lt "$MAXCOUNT" ]; do
  COUNT="$(($COUNT+1))"
  NAME=$(sed -n "$COUNT"p "$LISTFILE" | awk '{print $1}')
  URL=$(sed -n "$COUNT"p "$LISTFILE" | awk '{print $2}')
  TXTFILE="$FOLDER"/"$NAME".txt
  [ -e "$TXTFILE" ] || echo "#$NAME" > "$TXTFILE"
  VALUE=$(links2 -dump "$URL" | grep "$CURRENCY" | head -n1 | sed s"/$CURRENCY//g;s/,/./g" | awk '{print $1}')
  echo "$NAME $VALUE"
  echo "$VALUE" | grep -E "[0-9]*\.?[0-9]" && echo "$(date "+%d.%m.%Y_%H:%M:%S") $VALUE" >> "$TXTFILE"
done
}

PYPLOT() {
for file in $(ls -A .cache/amazon_watcher | grep -v "list.txt" | grep ".txt" | sed s'/.txt//g'); do
  PYFILE="$FOLDER/$file.py"
  if [ ! -e "$PYFILE" ]; then
    cat > "$PYFILE" << EOF
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np

def bytes2num(fmt, encoding='utf-8'):
	strconverter = mdates.strpdate2num(fmt)
	def bytesconverter(b):
		s = b.decode(encoding)
		return strconverter(s)
	return bytesconverter

fig = plt.figure()
ax = plt.subplot2grid((1, 1), (0, 0))

x, y = np.loadtxt('$FOLDER/$file.txt', delimiter=' ', unpack=True, converters={0: bytes2num('%d.%m.%Y_%H:%M:%S')})

ax.plot_date(x,y,'.')

maxYval = 1
for i in range (0,len(y),1):
	maxY = max(maxYval,y[i])

ax.set_ylim(bottom=0, top= maxY+50)

for label in ax.xaxis.get_ticklabels():
	label.set_rotation(70)

plt.xlabel('Date')
plt.ylabel('Price ($CURRENCY)')
plt.title('$file')
plt.subplots_adjust(left=0.1, bottom=0.2, right=0.98, top=0.95,)
plt.show()
EOF
  fi
python3 "$PYFILE"
done
}

MAIN_MENU() {
MAIN_MENU_TEXT="
 > Add Item to Watchlist<s:ADD>
 > Edit Watchlist<s:EDIT>
 > Update Data<s:UPDATE>
 > Show Text<s:TXT>
 > Show Graph<s:PYPLOT>

 < Exit<s:EXIT>"
OPTION=$(iselect "$HEADER" "$MAIN_MENU_TEXT" -p 11)
case $OPTION in
 ADD) ADD_ITEM;;
 EDIT) nano "$LISTFILE"; CHECK_CACHE ;;
 UPDATE) UPDATE;;
 TXT) nano "$FOLDER"/$(ls -A "$FOLDER" | grep -v "list.txt" | grep ".txt" | iselect -a);;
 PYPLOT) PYPLOT;;
 EXIT) exit 0;;
esac
MAIN_MENU
}

[ "$1" = "-c" ] && UPDATE || MAIN_MENU
exit 0




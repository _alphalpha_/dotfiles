#!/bin/sh
# Script to display or change the system time on gnu/linux systems
# written by Michael
# Public Domain

HELP_TEXT="
Script to display or change the system time on gnu/linux systems

Options:
 -h  | --help        show this help
 -d  | --date        change the date
 -t  | --time        change the time
 -st | --show-time   show the time
 -sd | --show-date   show the date
 -z  | --snooze      run a command at an appointed time
 --font=FONT         change the font

 'FONT' can be FINE, FAT or UGLY
"

FONT="FINE"

while [ "$#" -gt 0 ]; do
  if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    OPTION="--help"
  elif [ "$1" = "-d" ] || [ "$1" = "--date" ]; then
    OPTION="--date"
  elif [ "$1" = "-t" ] || [ "$1" = "--time" ]; then
    OPTION="--time"
  elif [ "$1" = "-st" ] || [ "$1" = "-ts" ] || [ "$1" = "-s" ] || [ "$1" = "--show-time" ]; then
    OPTION="--show-time"
  elif [ "$1" = "-sd" ] || [ "$1" = "-ds" ] || [ "$1" = "--show-date" ]; then
    OPTION="--show-date"
  elif [ "$1" = "-z" ] || [ "$1" = "--snooze" ]; then
    OPTION="--snooze"
  elif [ "$1" = "--font=fine" ] || [ "$1" = "--font=FINE" ]; then
    FONT="FINE"
  elif [ "$1" = "--font=fat" ] || [ "$1" = "--font=FAT" ]; then
    FONT="FAT"
  elif [ "$1" = "--font=ugly" ] || [ "$1" = "--font=UGLY" ]; then
    FONT="UGLY"
  fi
  shift
done

generate_font() {
if [ "$FONT" = "FAT" ]; then
zero_a=" █▀█"
zero_b=" █ █"
zero_c=" ▀▀▀"
one_a=" ▀█ "
one_b="  █ "
one_c=" ▀▀▀"
two_a=" ▀▀█"
two_b=" █▀▀"
two_c=" ▀▀▀"
three_a=" ▀▀█"
three_b=" ▀▀█"
three_c=" ▀▀▀"
four_a=" █ █"
four_b=" ▀▀█"
four_c="   ▀"
five_a=" █▀▀"
five_b=" ▀▀█"
five_c=" ▀▀▀"
six_a=" █▀▀"
six_b=" █▀█"
six_c=" ▀▀▀"
seven_a=" ▀▀█"
seven_b=" ▄▀ "
seven_c=" ▀  "
eight_a=" █▀█"
eight_b=" █▀█"
eight_c=" ▀▀▀"
nine_a=" █▀█"
nine_b=" ▀▀█"
nine_c=" ▀▀▀"
colon_a="    "
colon_b="  ▀ "
colon_c="  ▀ "
dot_a="    "
dot_b="    "
dot_c="  ▀ "
elif [ "$FONT" = "FINE" ]; then
zero_a=" ┏━┓"
zero_b=" ┃ ┃"
zero_c=" ┗━┛"
one_a=" ╺┓ "
one_b="  ┃ "
one_c=" ╺┻╸"
two_a=" ┏━┓"
two_b=" ┏━┛"
two_c=" ┗━╸"
three_a=" ┏━┓"
three_b=" ╺━┫"
three_c=" ┗━┛"
four_a=" ╻ ╻"
four_b=" ┗━┫"
four_c="   ╹"
five_a=" ┏━╸"
five_b=" ┗━┓"
five_c=" ┗━┛"
six_a=" ┏━┓"
six_b=" ┣━┓"
six_c=" ┗━┛"
seven_a=" ┏━┓"
seven_b="   ┃"
seven_c="   ╹"
eight_a=" ┏━┓"
eight_b=" ┣━┫"
eight_c=" ┗━┛"
nine_a=" ┏━┓"
nine_b=" ┗━┫"
nine_c=" ┗━┛"
colon_a="    "
colon_b="  ╹ "
colon_c="  ╹ "
dot_a="    "
dot_b="    "
dot_c="  ╹ "
elif [ "$FONT" = "UGLY" ]; then
zero_a='  _ '
zero_b=' | |'
zero_c=' |_|'
one_a="   "
one_b=" /|"
one_c="  |"
two_a=" _ "
two_b="  )"
two_c=" /_"
three_a=" _ "
three_b=" _)"
three_c=" _)"
four_a="    "
four_b=" |_|"
four_c="   |"
five_a="  _ "
five_b=" |_ "
five_c="  _)"
six_a="  _ "
six_b=" |_ "
six_c=" |_)"
seven_a=" __"
seven_b="  /"
seven_c=" / "
eight_a="  _ "
eight_b=" (_)"
eight_c=" (_)"
nine_a="  _ "
nine_b=" (_|"
nine_c="   |"
colon_a="   "
colon_b=" o "
colon_c=" o "
dot_a="   "
dot_b="   "
dot_c=" o "
fi
}
generate_font

display_digits() {
while [ "$#" -gt 0 ]; do
  if [ "$1" = "offset" ]; then
  	printf "     "
  fi
  shift
  if [ "$line" = "a" ]; then
	case "$1" in
	 1) printf "%s" "$one_a";;
	 2) printf "%s" "$two_a";;
	 3) printf "%s" "$three_a";;
	 4) printf "%s" "$four_a";;
	 5) printf "%s" "$five_a";;
	 6) printf "%s" "$six_a";;
	 7) printf "%s" "$seven_a";;
	 8) printf "%s" "$eight_a";;
	 9) printf "%s" "$nine_a";;
	 0) printf "%s" "$zero_a";;
	 ":") printf "%s" "$colon_a";;
	 ".") printf "%s" "$dot_a";;
	esac
  elif [ "$line" = "b" ]; then
	case "$1" in
	 1) printf "%s" "$one_b";;
	 2) printf "%s" "$two_b";;
	 3) printf "%s" "$three_b";;
	 4) printf "%s" "$four_b";;
	 5) printf "%s" "$five_b";;
	 6) printf "%s" "$six_b";;
	 7) printf "%s" "$seven_b";;
	 8) printf "%s" "$eight_b";;
	 9) printf "%s" "$nine_b";;
	 0) printf "%s" "$zero_b";;
	 ":") printf "%s" "$colon_b";;
	 ".") printf "%s" "$dot_b";;
	esac
  elif [ "$line" = "c" ]; then
	case "$1" in
	 1) printf "%s" "$one_c";;
	 2) printf "%s" "$two_c";;
	 3) printf "%s" "$three_c";;
	 4) printf "%s" "$four_c";;
	 5) printf "%s" "$five_c";;
	 6) printf "%s" "$six_c";;
	 7) printf "%s" "$seven_c";;
	 8) printf "%s" "$eight_c";;
	 9) printf "%s" "$nine_c";;
	 0) printf "%s" "$zero_c";;
	 ":") printf "%s" "$colon_c";;
	 ".") printf "%s" "$dot_c";;
	esac
  fi

done
}

show_time() {
clear
[ "$OPTION" = "--snooze" ] && printf "\\n\\n    Enter an appointed time to run a command.\\n    You can specify the command in the next step.\\n" || printf "\\n\\n    The time is:\\n\\n"
line="a" ; display_digits offset "$TIME_H_A" "$TIME_H_B" : "$TIME_M_A" "$TIME_M_B" : "$TIME_S_A" "$TIME_S_B" ; printf "\\n"
line="b" ; display_digits offset "$TIME_H_A" "$TIME_H_B" : "$TIME_M_A" "$TIME_M_B" : "$TIME_S_A" "$TIME_S_B" ; printf "\\n"
line="c" ; display_digits offset "$TIME_H_A" "$TIME_H_B" : "$TIME_M_A" "$TIME_M_B" : "$TIME_S_A" "$TIME_S_B" ; printf "\\n"
}

update_time() {
TIME_H_A=$(date +%H | cut -b 1)
TIME_H_B=$(date +%H | cut -b 2)
TIME_M_A=$(date +%M | cut -b 1)
TIME_M_B=$(date +%M | cut -b 2)
TIME_S_A=$(date +%S | cut -b 1)
TIME_S_B=$(date +%S | cut -b 2)
}

input_time() {
while [ -z "$time_hour" ]; do
  printf "      ▀▀▀▀▀▀▀\\n"
  printf "\\n\\t Enter hour: "
  read -r time_hour

  if [ "$time_hour" = "0" ] || [ "$time_hour" = "00" ]; then
    time_hour="0"
  else
    time_hour="$(echo "$time_hour" | sed 's/^0*//' | sed 's/[^0-9]//g')"
  fi

  if [ "$time_hour" ]; then
    if [ "$time_hour" -ge 0 -a "$time_hour" -le 9 ]; then
      TIME_H_A="0"
      TIME_H_B="$time_hour"
    elif [ "$time_hour" -ge 10 -a "$time_hour" -le 24 ]; then
      TIME_H_A="$(echo "$time_hour" | cut -b 1)"
      TIME_H_B="$(echo "$time_hour" | cut -b 2)"
    else
      time_hour=""
    fi
  fi
done

while [ -z "$time_minute" ]; do
  show_time
  printf "                  ▀▀▀▀▀▀▀\\n"
  printf "\\n\\t Enter minute: "
  read -r time_minute

  if [ "$time_minute" = "0" ] || [ "$time_minute" = "00" ]; then
    time_minute="0"
  else
    time_minute="$(echo "$time_minute" | sed 's/^0*//' | sed 's/[^0-9]//g')"
  fi

  if [ "$time_minute" ]; then
    if [ "$time_minute" -ge 0 -a "$time_minute" -le 9 ]; then
      TIME_M_A="0"
      TIME_M_B="$time_minute"
    elif [ "$time_minute" -ge 10 -a "$time_minute" -le 60 ]; then
      TIME_M_A="$(echo "$time_minute" | cut -b 1)"
      TIME_M_B="$(echo "$time_minute" | cut -b 2)"
    else
      time_minute=""
    fi
  fi
done

while [ -z "$time_second" ]; do
  show_time
  printf "                              ▀▀▀▀▀▀▀\\n"
  printf "\\n\\t Enter second: "
  read -r time_second

  if [ "$time_second" = "0" ] || [ "$time_second" = "00" ]; then
    time_second="0"
  else
    time_second="$(echo "$time_second" | sed 's/^0*//' | sed 's/[^0-9]//g')"
  fi

  if [ "$time_second" ]; then
    if [ "$time_second" -ge 0 -a "$time_second" -le 9 ]; then
      TIME_S_A="0"
      TIME_S_B="$time_second"
    elif [ "$time_second" -ge 10 -a "$time_second" -le 60 ]; then
      TIME_S_A="$(echo "$time_second" | cut -b 1)"
      TIME_S_B="$(echo "$time_second" | cut -b 2)"
    else
      time_second=""
    fi
  fi
done
}

read_time() {
update_time
show_time
input_time

show_time
printf "\\n\\n\\t the entered time is %s%s:%s%s:%s%s\\n" "$TIME_H_A" "$TIME_H_B" "$TIME_M_A" "$TIME_M_B" "$TIME_S_A" "$TIME_S_B"
printf "\\t Do you want to apply this to the hardware time? (y/N) "
read -r time_apply
if [ "$time_apply" = "y" ] || [ "$time_apply" = "Y" ]; then
  sudo hwclock --set --date "$(date +%m/%d/%Y) $TIME_H_A$TIME_H_B:$TIME_M_A$TIME_M_B:$TIME_S_A$TIME_S_B"
  sudo hwclock --hctosys && printf "\\t System Time updated!\\n"
fi
}

show_date() {
clear
printf "\\n\\n    The date is:     ( Day . Month . Year )\\n\\n"
line="a" ; display_digits offset "$DATE_D_A" "$DATE_D_B" . "$DATE_M_A" "$DATE_M_B" . "$DATE_Y_A" "$DATE_Y_B" "$DATE_Y_C" "$DATE_Y_D" ; printf "\\n"
line="b" ; display_digits offset "$DATE_D_A" "$DATE_D_B" . "$DATE_M_A" "$DATE_M_B" . "$DATE_Y_A" "$DATE_Y_B" "$DATE_Y_C" "$DATE_Y_D" ; printf "\\n"
line="c" ; display_digits offset "$DATE_D_A" "$DATE_D_B" . "$DATE_M_A" "$DATE_M_B" . "$DATE_Y_A" "$DATE_Y_B" "$DATE_Y_C" "$DATE_Y_D" ; printf "\\n"
}

update_date() {
DATE_D_A=$(date +%d | cut -b 1)
DATE_D_B=$(date +%d | cut -b 2)
DATE_M_A=$(date +%m | cut -b 1)
DATE_M_B=$(date +%m | cut -b 2)
DATE_Y_A=$(date +%Y | cut -b 1)
DATE_Y_B=$(date +%Y | cut -b 2)
DATE_Y_C=$(date +%Y | cut -b 3)
DATE_Y_D=$(date +%Y | cut -b 4)
}

read_date() {
while [ -z "$date_day" ]; do
  show_date
  printf "      ▀▀▀▀▀▀▀\\n"
  printf "\\n\\t Enter day: "
  read -r date_day
  date_day="$(echo "$date_day" | sed 's/^0*//' | sed 's/[^0-9]//g')"
  if [ "$date_day" ]; then
    if [ "$date_day" -ge 1 -a "$date_day" -le 9 ]; then
	  DATE_D_B="$date_day"
	  DATE_D_A="0"
    elif [ "$date_day" -ge 10 -a "$date_day" -le 31 ]; then
      DATE_D_A="$(echo "$date_day" | cut -b 1)"
      DATE_D_B="$(echo "$date_day" | cut -b 2)"
    else
      date_day=""
    fi
  fi
done

while [ -z "$date_month" ]; do
  show_date
  printf "                  ▀▀▀▀▀▀▀\\n"
  printf "\\n\\t Enter month: "
  read -r date_month
  date_month="$(echo "$date_month" | sed 's/^0*//' | sed 's/[^0-9]//g')"
  if [ "$date_month" ]; then
    if [ "$date_month" -ge 1 -a "$date_month" -le 9 ]; then
      DATE_M_B="$date_month"
      DATE_M_A="0"
    elif [ "$date_month" -ge 10 -a "$date_month" -le 12 ]; then
      DATE_M_A="$(echo "$date_month" | cut -b 1)"
      DATE_M_B="$(echo "$date_month" | cut -b 2)"
    else
      date_month=""
    fi
  fi
done

while [ -z "$date_year" ]; do
  show_date
  printf "                              ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀\\n"
  printf "\\n\\t Enter year: "
  read -r date_year
  date_year="$(echo "$date_year" | sed 's/^0*//' | sed 's/[^0-9]//g')"
  if [ "$date_year" ]; then
    if [ "$date_year" -ge 1 -a "$date_year" -le 9 ]; then
      DATE_Y_A="0"
      DATE_Y_B="0"
      DATE_Y_C="0"
      DATE_Y_D="$date_year"
    elif [ "$date_year" -ge 10 -a "$date_year" -le 99 ]; then
      DATE_Y_A="0"
      DATE_Y_B="0"
      DATE_Y_C="$(echo "$date_year" | cut -b 1)"
      DATE_Y_D="$(echo "$date_year" | cut -b 2)"
    elif [ "$date_year" -ge 100 -a "$date_year" -le 999 ]; then
      DATE_Y_A="0"
      DATE_Y_B="$(echo "$date_year" | cut -b 1)"
      DATE_Y_C="$(echo "$date_year" | cut -b 2)"
      DATE_Y_D="$(echo "$date_year" | cut -b 3)"
    elif [ "$date_year" -ge 1000 -a "$date_year" -le 9999 ]; then
      DATE_Y_A="$(echo "$date_year" | cut -b 1)"
      DATE_Y_B="$(echo "$date_year" | cut -b 2)"
      DATE_Y_C="$(echo "$date_year" | cut -b 3)"
      DATE_Y_D="$(echo "$date_year" | cut -b 4)"
    else
      date_year=""
    fi
  fi
done

show_date
printf "\\n\\n\\t The entered date is %s%s.%s%s.%s%s%s%s\\n" "$DATE_D_A" "$DATE_D_B" "$DATE_M_A" "$DATE_M_B" "$DATE_Y_A" "$DATE_Y_B" "$DATE_Y_C" "$DATE_Y_D"
printf "\\t Do you want to apply this to the hardware time? (y/N) "
read -r date_apply
if [ "$date_apply" = "y" ] || [ "$date_apply" = "Y" ]; then
  sudo hwclock --set --date "$DATE_M_A$DATE_M_B/$DATE_D_A$DATE_D_B/$DATE_Y_A$DATE_Y_B$DATE_Y_C$DATE_Y_D $(date +%H:%M:%S)"
  sudo hwclock --hctosys
  printf "\\t System Date updated!\\n"
fi
}

snooze() {
echo "Specify a time you want a certain command to run"
update_time
show_time
input_time
printf "Now specify the command: "
read -r CMD
[ "$(printf "%s" "$time_hour" | wc -c)" = "1" ] && time_hour="0$time_hour"
[ "$(printf "%s" "$time_minute" | wc -c)" = "1" ] && time_hour="0$time_minute"
[ "$(printf "%s" "$time_second" | wc -c)" = "1" ] && time_hour="0$time_second"
clear
echo "$CMD will executed at $time_hour:$time_minute:$time_second"
while [ "$time_hour $time_minute $time_second" != "$(date +"%H %M %S")" ]; do sleep 1; done
$CMD
}

if [ "$OPTION" = "--help" ]; then
  echo "$HELP_TEXT"; exit 0
elif [ "$OPTION" = "--date" ]; then
  update_date; read_date; exit 0
elif [ "$OPTION" = "--time" ]; then
  read_time; exit 0
elif [ "$OPTION" = "--show-time" ]; then
  while true; do update_time; show_time; sleep 1; done
elif [ "$OPTION" = "--show-date" ]; then
  while true; do update_date; show_date; sleep 60; done
elif [ "$OPTION" = "--snooze" ]; then
  snooze; exit 0
else
  while true; do update_time; show_time; sleep 1; done
fi
exit 0
